// testRB_2thread.c
// Wrote by ROH
// Distributed at 2015/09/17

// An example for using the RigBuffer, via a pair of threads.
//
// The main thread will prepare the RingBuffer of specified size,
// launch the "second_thread", loop the data-generation for the given period,
// wait the "second_thread" quit, and then finish.
//
// The "second_thread" will write the contents of RingBuffer into a output file, 
// or copy them to a local memory. If no more data comes for TIMEOUT period, 
// the "second_thread" is terminated automatically.
//
// Usage:
// $ ./testRB_2thread [RBsize_MB [DataRate_Mbps [secs [filename]]]]
//   RBsize_MB     : RingBuffer Size in MB (default=256)
//   DataRate_Mbps : Data rate in Mbps     (default=1024)
//   secs          : How many seconds?     (default=10)
//   filename      : output file           (default=NULL)
//
// Hints:
// - RBsize must be greater then data size of 1 sec at the given DataRate,
//   and double of that amount is adequate for stable operation.
//   For DataRate=1024Mbps=128MB/s, you may set as 128MB < RBsize <= 256MB.
// - If DataRate is bigger than the average writing speed of your storage,
//   needless to say, you will have lots of error messages.
// - DataRate upto 656Mbps was successful, for writing to single HDD on desktop.

#include "RingBuffer.h"

#define DIRECT_IO	// To use write() with O_DIRECT flag for file output. Slow.
//#undef  DIRECT_IO	// To use fwrite() instead.

#ifdef DIRECT_IO
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <error.h>
#include <ctype.h>	// toupper()
#include <sys/wait.h>	// waitpid()
#include <sys/time.h>	// gettimeofday()
#include <unistd.h>	// fork(),usleep()
#include <time.h>	// nanosleep()
#include <pthread.h>

// Only for thread argument
typedef struct threadarg_t {
  RingBuffer *RB;
  char *filename;
} ThreadArg;

// Try to sleep until "from+nanosec"
void nanosleep_since(long nanosec, struct timeval *from)
{
  struct timeval now;
  struct timespec request,remain;
  gettimeofday(&now,NULL);
  now.tv_usec += (now.tv_sec - from->tv_sec)*1000000L - from->tv_usec; // elapsed_usec
  nanosec -= now.tv_usec*1000L; // sleep time [nsec]
  if(0L < nanosec) {
    request.tv_sec  = nanosec/1000000000L;
    request.tv_nsec = nanosec%1000000000L;
    if(nanosleep(&request,&remain) < 0) error(0,errno,"nanosleep");
  }
}

int PressEnter(const char *msg, const char *choice)
{
  char ans[80];
  int i;
  printf("%s ",msg?msg:"Press 'Enter'");
  if(fgets(ans,sizeof(ans),stdin) == NULL) { return -1; }

  if(choice==NULL) { return -1; }

  // check if match with any of choices
  ans[0] = toupper(ans[0]);
  for(i=0;i<(int)strlen(choice);i++) {
    if(ans[0] == toupper(choice[i])) { return i; }
  }
  return -1;
}

//=============== second_thread : get data from RB ===============
// If RB->filename is NULL, the data will be copied a local memory **simply**,
// else the data will be written into the specified file. 
void *second_thread(void *targ)
{
  RingBuffer *RB = ((ThreadArg *)targ)->RB;
  char *filename = ((ThreadArg *)targ)->filename;
  size_t page_size = sysconf(_SC_PAGESIZE);

  // Do the acutal low-speed HDD-write
#ifdef DIRECT_IO
  int fd = 0, flags = O_WRONLY|O_CREAT|O_TRUNC|O_SYNC|O_DIRECT;
#else
  FILE *fpw=NULL;
#endif
  void *src,*dst=NULL;
  size_t exist,todo,done,total=0L;
  size_t dt_usec,timeout_usec=1000000L;
  struct timeval t_prev,t_now,t_last;

  if(filename) {
#ifdef DIRECT_IO
    fd = open(filename,flags,0644);
    if(fd<0) error(1,errno,"open(\"%s\",,)",filename);
#else
    fpw = fopen(filename,"w");
#endif
  }else{
    dst = malloc(page_size); // 1page
  }

  usleep(1000L); // slow start, 1msec

  gettimeofday(&t_prev,NULL);
  t_last.tv_sec = t_last.tv_usec = 0; // time of last write operation
#ifdef DIRECT_IO
  while(fd||dst) {
#else
  while(fpw||dst) {
#endif
    src = RBextractable(RB,&exist);
    if(src) {
      todo = (exist/page_size)*page_size;
      if(todo) {
        // We have more than 1 page --- save them, and loop again
        if(filename) {
#ifdef DIRECT_IO
          done = RBwrite(RB,fd,todo); // RB ---> fd
#else
          done = RBfwrite(RB,fpw,todo); // RB ---> fpw
#endif
        }else{
          for(done=0L;done<todo;done+=page_size) RBget(RB,dst,page_size); // RB ---> dst
        }
        total += done;
        gettimeofday(&t_last,NULL);
        continue;
      }
    }

    gettimeofday(&t_now,NULL);
    if(t_last.tv_sec) {
      dt_usec = (t_now.tv_sec - t_last.tv_sec)*1000000L + (t_now.tv_usec - t_last.tv_usec);
      if(dt_usec > timeout_usec) {
        // TIMEOUT -- save all that we have, and quit
        src = RBextractable(RB,&exist);
        if(src) {
          if(filename) {
#ifdef DIRECT_IO
            done = RBwrite(RB,fd,exist); // RB ---> fd
#else
            done = RBfwrite(RB,fpw,exist); // RB ---> fpw
#endif
          }else{
            for(done=0L;done<exist;) {
              size_t bytes = (exist-done < page_size) ? exist-done : page_size;
              RBget(RB,dst,bytes); // RB ---> dst
              done += bytes;
            }
          }
          total += done;
        }
        break;
      }
    }

    dt_usec = (t_now.tv_sec - t_prev.tv_sec)*1000000L + (t_now.tv_usec - t_prev.tv_usec);
    t_prev = t_now;
    if(dt_usec < 1000L) {
      // Too short interval -- sleep upto 1msec for proper loop-interval
      usleep(1000L - dt_usec);
    }
  }//while

  if(filename) {
#ifdef DIRECT_IO
    if(fd) {
      close(fd);
#else
    if(fpw) {
      fclose(fpw);
#endif
      fprintf(stderr,"C %zu bytes written to '%s'\n",total,filename);
    }
  }else{
    if(dst) {
      free(dst);
      fprintf(stderr,"C %zu bytes copied\n",total);
    }
  }

  // done
  fprintf(stderr,"second_thread --- Done\n");
  pthread_exit((void *)0);
}
//=============== second_thread ==================================

#define MEGA (1000000L)
#define Mbps (MEGA/8L) // [bytes/sec]

int main(int argc, char **argv) // [RBsize_MB [DataRate_Mbps [secs [filename]]]]
{
  size_t page_size = sysconf(_SC_PAGESIZE);

  RingBuffer *RB;
  size_t RBsize=256*MEGA;
  size_t DataRate=1024*Mbps, secs=10;
  char *filename = NULL; // output filename

  pthread_t thread;
  ThreadArg targ;

  // Set parameters
  if(argc>1) RBsize   = atol(argv[1]) * MEGA; // 256MEGA  ==> 256000000 [B]
  if(argc>2) DataRate = atol(argv[2]) * Mbps; // 1024Mbps ==> 128000000 [B/s]
  if(argc>3) secs     = atol(argv[3]);
  if(argc>4) filename = argv[4];

  // It is expected that the size of RingBuffer is bigger than twice of DataRate.
  RBsize = ((RBsize + (page_size-1))/page_size)*page_size;
  fprintf(stderr,"RBsize    = %zu Bytes, requested : %.2lf*DataRate\n",RBsize,RBsize/(double)DataRate);
//  if(RBsize < 2*DataRate) {
//    RBsize = ((2*DataRate + (page_size-1))/page_size)*page_size;
//    fprintf(stderr,"          = %zu Bytes, adjusted as 2*DataRate\n",RBsize);
//  }
  fprintf(stderr,"DataRate  = %zu Bytes/s (as %lu Mbps)\n",DataRate,DataRate/Mbps);
  fprintf(stderr,"# of secs = %zu sec (Total throughput = %zu Bytes\n",secs,secs*DataRate);
  fprintf(stderr,"output    = %s\n\n",filename?filename:"memory");

  // Create the RB
  RB = RBcreate(RBsize,"testRB");

  // query to start 
  fprintf(stderr,"\nLoop for %zu seconds ...\n",secs);
  PressEnter("Start ?",NULL);

  // split into two threads -- (main thread) writeRB : (second thread) readRB
  targ.RB = RB;
  targ.filename = filename;
  if(pthread_create(&thread, NULL, second_thread, (void *)&targ)) {
    error(1,errno,"Fail to pthread_create()");
  }else{
    fprintf(stderr,"second_thread created successfully.\n");
  }

  //=============== main thread : put data to RB ==============
  {
    // Simulate the burst input situation
    //
    // Loop the following works within every second (during "secs" seconds).
    // (1) Generate the data : 
    //     - set 1 page buffer with some values
    //     - copy the buffer to RingBuffer for "DataRate/sizeof(buf)" times
    //       as fast as possible
    //       (Here, without checking the RingBuffer space)
    // (2) Sleep for "1sec - elpased_time" in nono-second accuracy.
    //
    // After the loop finished,
    // (1) Wait until the second_thread quit.
    // (2) End.

    size_t isec, loop, nw;
    struct timeval ts;
    long nsec_to_nextrun = 1000000000L;
    char buf[page_size];
    size_t nloop = DataRate/page_size; // # of loops for 1 second
    int status;

    // put data to RB for nsec seconds
    gettimeofday(&ts,NULL); // reference timemark
    for(isec=0; isec<secs; isec++) {
      struct timeval t1,t2;

      fprintf(stderr,".");
      memset(buf,(int)('A'+(isec%26)),sizeof(buf));

      if(isec==0) { // I'd like to know "how many usec required for RBappend()?"
        gettimeofday(&t1,NULL);
      }

      for(loop=0; loop<nloop; loop++) {
        nw = RBput(RB,buf,page_size); // put 'page_size' bytes from buf to RB
        if(nw != page_size) {
          error(0,0,"P ===== Fail to append data at %zusec, %zupage : DROP %zupages",isec,loop,nloop-loop);
          RBstatus(RB,"error");
          break;
        }
      }//loop as fast as possible (as like as the burst input)

      if(isec==0) {
        gettimeofday(&t2,NULL);
        t2.tv_usec += (t2.tv_sec - t1.tv_sec)*1000000L - t1.tv_usec; // elapsed_usec
        fprintf(stderr,"(%ldusec)",t2.tv_usec);
      }

      nanosleep_since(nsec_to_nextrun,&ts);
      ts.tv_sec++; // advance the reference timemark for 1 sec
    }//isec
    fprintf(stderr,"\n");

    // wait until the second_thread quit
    if(pthread_join(thread,(void **)&status)) {
      error(0,errno,"Fail to pthread_join()");
    }else{
      fprintf(stderr,"Joined successfully, with status=%d\n",status);
    }

    // done
    fprintf(stderr,"main_thread --- Done\n\n");
  }
  //=============== main thread ===============================

  // Remove the RB
  RBremove(RB);

  return 0;
}

