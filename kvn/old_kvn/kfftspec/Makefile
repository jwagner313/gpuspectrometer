SHELL := /bin/bash

include ../Makefile.top

OBJS = kfftspec kfftspec_dbg
# OBJS = kfftspec kfftspec_cb

.PHONY: clean install

all: $(OBJS)

clean:
	rm -rf $(OBJS) $(HELPER_OBJ) *.o *.SPC.dat *.kfftspec perf.data perf.data.old

install: kfftspec
	mkdir -p $(INSTALL_PREFIX)/bin/ && cp -a kfftspec $(INSTALL_PREFIX)/bin/

## Library dep

../network/libvdifrx_rb.a: ../network/vdif_receiver_rb.c
	$(MAKE) -C ../network/ MAKEFLAGS=$(MAKEFLAGS)

## Build without cuFFT Callbacks

kfftspec: kfftspec.cu $(KERNEL_SRC_CU) $(TOPDIR)network/libvdifrx_rb.a $(HELPER_OBJ)
	$(NVCC) $(NVFLAGS_MIXED) $< -o $@ -lcufft $(TOPDIR)network/libvdifrx_rb.a

kfftspec_dbg: kfftspec.cu $(KERNEL_SRC_CU) $(TOPDIR)network/libvdifrx_rb.a $(HELPER_OBJ)
	$(NVCC) -DDEBUG $(NVFLAGS_MIXED) $< -o $@ -lcufft $(TOPDIR)network/libvdifrx_rb.a

## Build with cuFFT Callbacks 
## This cuFFT is feature only available on 64-bit Linux and CUDA >=6.5 ...

kfftspec_cb.o: kfftspec.cu $(KERNEL_SRC_CU) $(TOPDIR)network/libvdifrx_rb.a $(HELPER_OBJ)
	$(NVCC) -DHAVE_CUFFT_CALLBACKS=yes -dc $(NVFLAGS_MIXED) -o $@ -c $< 
kfftspec_cb: kfftspec_cb.o
	$(NVCC) $(NVFLAGS_MIXED) -o $@ $+ -lcufft_static -lculibos $(TOPDIR)network/libvdifrx_rb.a


## Test run options

KFFTSPEC_OPTS = -E expt001 -O jwagner -A KVN_Yonsei
#KFFTSPEC_OPTS += --cross
KFFTSPEC_OPTS += --devices=0,1 --nstreams=2
KFFTSPEC_OPTS += --winfunc=hamming
# KFFTSPEC_OPTS += 32768 0.256
KFFTSPEC_OPTS += 1048576 0.250

fila10g4Glong: kfftspec
	operf --separate-thread ./kfftspec -E fila10g256k -O kjcc -A KJCC --udpoffset=8 --rxcpu=4 --devices=0,1 --nstreams=2 262144 0.512 46227 VDIF_8192-4096-2-2 > >(tee stdout.log) 2> >(tee stderr.log >&2)

fila10g4G: kfftspec
	# ./kfftspec -E fila10g -O kjcc -A KJCC --udpoffset=8 --rxcpu=4 --devices=0,1 --nstreams=2 32768 0.256 46227 VDIF_8192-4096-2-2
	./kfftspec -E fila10g256k -O kjcc -A KJCC --udpoffset=8 --rxcpu=4 --devices=0,1 --nstreams=2 262144 0.256 46227 VDIF_8192-4096-2-2
	# ./kfftspec -E fila10g512k -O kjcc -A KJCC --udpoffset=8 --rxcpu=4 --devices=0,1 --nstreams=2 524288 0.256 46227 VDIF_8192-4096-2-2
	# ./kfftspec -E fila10g1M -O kjcc -A KJCC --udpoffset=8 --rxcpu=4 --devices=0,1 --nstreams=2 1048576 0.256 46227 VDIF_8192-4096-2-2
	#
	# note: to use FILA10G Yonsei
	# mk6-ys-10:$ udp_redirect 10.10.1.1 46227 203.250.156.96 46226 2   # Max ~4 Gbps, trunked 10x1G
	# ./kfftspec -E fila10g -O KVNYS -A KVNYS --udpoffset=8 --rxcpu=4 --devices=0,1 --nstreams=2 32768 0.256 46226 VDIF_8192-4096-2-2
	#
	# note: to use FILA10G Ulsan
	# mk6-ys-10:$ udp_redirect 10.10.1.1 46227 203.250.156.96 46226 2   # Max ~4 Gbps, trunked 10x1G
	#
	# or alternatively
	# iptables -t mangle -A PREROUTING -p udp --dport 46227 -i eth2 -j TEE --gateway 203.250.156.96
	# iptables -t mangle -L PREROUTING --line-numbers # lists rules with line numbers
	# iptables -t mangle -D PREROUTING 1  # deletes rule on line 1

synthtest: kfftspec
	rm -f synth.vdif synth.32bit synth.m5spec synth00*_KVN_Yonsei_*.kfftspec
	# ./kfftspec -E synth001 --devices=0 --nstreams=2 --winfunc=hamming 32768 0.256 synth_noise.c GENERATOR
	# ./kfftspec -E synth002 --devices=0 --nstreams=2 --winfunc=hamming 32768 0.256 synth_tone1.c GENERATOR
	./kfftspec -E synth003 --devices=0 --nstreams=2 --winfunc=hamming 32768 0.256 synth_tone2.c GENERATOR
	# m5spec.py synth.vdif VDIF_10000-128-1-2 5.0 32768

filetest: kfftspec
	# VDIF file with 2 x 256 MHz bands, after Mark5B-->VDIF conversion, has H2O spectral line
	rm -f expt001_KVN_Yonsei_2016324083959.kfftspec 2016324083959.expt001.0.SPC.dat
	# perf record -a ./kfftspec $(KFFTSPEC_OPTS) /scratch/jwagner/s14db02c_KVNYS_No0006.vdif VDIF_10000-2048-1-2
	./kfftspec $(KFFTSPEC_OPTS) /scratch/jwagner/s14db02c_KVNYS_No0006.vdif VDIF_10000-2048-1-2

filetest2: kfftspec
	# VDIF file with 2 x 256 MHz bands, after Mark5B-->VDIF conversion, has H2O spectral line
	rm -f expt001_KVN_Yonsei_2016324083959.kfftspec 2016324083959.expt001.0.SPC.dat
	./kfftspec $(KFFTSPEC_OPTS) /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-2-2

filetest_cb: kfftspec_cb
	# VDIF file with 2 x 256 MHz bands, after Mark5B-->VDIF conversion, has H2O spectral line
	rm -f expt001_KVN_Yonsei_2016324083959.kfftspec 2016324083959.expt001.0.SPC.dat
	./kfftspec_cb $(KFFTSPEC_OPTS) /scratch/jwagner/s14db02c_KVNYS_No0006.vdif.short VDIF_10000-2048-1-2
