#!/usr/bin/python
import numpy, math, sys
import matplotlib.pyplot as plt

def mad(arr):
    """ Median Absolute Deviation: a "Robust" version of standard deviation.
        Indices variabililty of the sample.
        https://en.wikipedia.org/wiki/Median_absolute_deviation 
    """
    arr = numpy.ma.array(arr).compressed() # should be faster to not use masked arrays.
    med = numpy.median(arr)
    return numpy.median(numpy.abs(arr - med))


Lfft = 8192
Nch = Lfft/2+1
N = 2048

do_window = False

fin = open('synth.32bit', 'rb')
done = False

# Select a window function
if do_window:
    # HFT248D (10-term cosine), 248.4 dB, differentiable, 0.0009 dB flatness, needs 80% FFT overlap
    # See G. Heinzel et al. (2002), http://pubman.mpdl.mpg.de/pubman/item/escidoc:152164:1
    a = [1.0, 1.985844164102, 1.791176438506, 1.282075284005, 0.667777530266, 0.240160796576, 0.056656381764, 0.008134974479, 0.000624544650, 0.000019808998, 0.000000132974]
    olap = 0.80
    w = 1.0 - olap

    istride = int(4*round(w*Lfft))
    N = int(round(N/w))
    print ('Overlap %d%% with stride of %d floats for %d-point FFT' % (int(100*olap),istride/4,Lfft))

    # Calculate window function
    nn = (2.0*numpy.pi/(Lfft-1)) * numpy.linspace(0, Lfft-1, Lfft)
    wf = a[0] * numpy.ones_like(nn)
    for ii in range(1,len(a)):
        wf += (pow(-1,ii) * a[ii]) * numpy.cos(float(ii)*nn)
else:
    istride = 4*Lfft
    wf =  numpy.ones((Lfft), dtype=numpy.float32)

ioff = 0
avg = numpy.zeros((Nch), dtype=numpy.float32)
Nspec = 0
while not done:

    avg = numpy.zeros((Nch), dtype=numpy.float32)
    Nspec = 0
    for n in range(N):
        fin.seek(ioff, 0)
        a = numpy.fromfile(fin, dtype=numpy.float32, count=Lfft)
        ioff = ioff + istride
        if len(a) < Lfft:
            done = True
            break
        A = numpy.fft.rfft(a*wf)
        avg += numpy.abs(A[0:Nch])
        Nspec += 1
    curr = numpy.abs(A[0:Nch])

    s = avg/(Nspec*Lfft/2)
    maxidx = s.argsort()[-16:][::-1]
    print s[maxidx], s[maxidx] / max(s[maxidx]), 'SNR(mad)=',max(s)/mad(s)

    plt.figure(1)
    plt.clf()
    plt.subplot(311)
    plt.plot(a,'kx:'); plt.ylabel('Amplitude (V)')
    plt.subplot(312)
    plt.plot(10*numpy.log10(s/max(s)),'kx:'); plt.ylabel('Amplitude (dB from peak %.3f)' % (max(s)))
    plt.title('Spectrum (not power spectrum) with %d averaged FFTs' % (Nspec))
    plt.axis('tight')
    plt.subplot(313)
    plt.plot(10*numpy.log10(curr),'kx:')
    plt.title('Spectrum (not power spectrum) of latest FFT')

    plt.draw()
    plt.show(block=False)

raw_input('Waiting for <enter>')

