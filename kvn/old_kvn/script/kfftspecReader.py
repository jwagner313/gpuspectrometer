"""
Class kfftspecReader for reading *.kfftspec files

The file structure is defined in kfftspec_outfile.h.
Each spectral entry consists of an N-byte long header,
and an M-byte long chunk of spectral data.

The header has a simple structure to allow easy parsing
in Python. The header starts with two 32-bit integers.
The first is the length of the header (including the
length field itself), the second is the length of the
data after the header.

The spectral data are always 32-bit floating point.
Complex cross-power spectra are stored in {Re,Im} pairs.
Spectral data always starts from the lowest FFT bin
and ends at the highest FFT bin, i.e., the first value
in the spectrum would be the DC/0 Hz bin if the original
signal was net upper sideband.
"""

import struct
import numpy

class kfftspecReader:

    infile = None
    curr_header = None

    def __init__(self,filename):
        self.filename = filename
        self.infile = open(filename, 'rb')

    def get_next_spectrum(self):
        """Return the next decoded header and spectrum as
           a tuple (header as dict, spectrum as numpy array)."""

        # The header starts with two 32-bit length fields,:
        Lhdr  = self.infile.read(4)  # length of entire header
        Lspec = self.infile.read(4)  # length of spectral data after header
        if not Lhdr or not Lspec:
            return (None,None)
        Lhdr  = struct.unpack('I', Lhdr)[0]
        Lspec = struct.unpack('I', Lspec)[0]

        # The rest of the header contains metadata
        raw_hdr_rest = self.infile.read(Lhdr - 2*4)

        # The header is followed by raw data
        raw_spectrum = numpy.fromfile(self.infile, dtype='f', count=Lspec/4)

        # Decode the header, rearrange the spectral and the state counts
        h = self.parse_header(raw_hdr_rest)
        s = self.parse_spectrum(raw_spectrum, h)
        (plevels_1,plevels_2) = self.parse_statecounts(h)
        h['plevels_src_1'] = plevels_1
        h['plevels_src_2'] = plevels_2

        return (h,s)

    def getraw_next_spectrum(self):
        """Return the next un-decoded header and spectrum as string."""

        Lhdr  = self.infile.read(4)  # length of entire header
        Lspec = self.infile.read(4)  # length of spectral data after header
        if not Lhdr or not Lspec:
            return (None,None)
        Lh = struct.unpack('I', Lhdr)[0]
        Ls = struct.unpack('I', Lspec)[0]

        # The rest of the header contains metadata
        raw_hdr_rest = self.infile.read(Lh - 2*4)

        # The header is followed by raw data
        raw_spectrum = self.infile.read(Ls)

        return Lhdr+Lspec+raw_hdr_rest+raw_spectrum

    def parse_header(self, raw_hdr):
        """Parse a spectral header. The header is found in front of every
           spectral data entry in the kfftspec file. In front of the header are
           two 32-bit length fields (header and spectral data lengths in bytes)
           and the parser parse_spectral_header() assumes that these have
           two fields already been removed from the data in 'raw_hdr'."""

        # Description of header structure as defined
        # in the file helpers/kfftspec_outfile.h
        fields = []
        fields.append(('datalayout','I'))
        fields.append(('header_version','I'))
        fields.append(('nchan','I'))
        fields.append(('signal_src_1','I'))
        fields.append(('signal_src_2','I'))
        fields.append(('Tint','d'))
        fields.append(('bandwidth','d'))
        fields.append(('weight','d'))
        fields.append(('winfunc','8s'))
        fields.append(('winoverlap','d'))
        fields.append(('nlevels_src_1','I'))
        fields.append(('nlevels_src_2','I'))
        for n in range(16):
            # The plevels are float[16] arrays, but struct.unpack
            # cannot return arrays, so this is a dirty method:
            fields.append(('plevels_src_1_%d'%n,'d'))
            fields.append(('plevels_src_2_%d'%n,'d'))
        fields.append(('GPU_device','B'))
        fields.append(('GPU_stream','B'))
        fields.append(('GPU_datasource','B'))
        fields.append(('_struct_padding_1','B'))
        fields.append(('timestamp_secondsofday','d'))
        fields.append(('timestamp_tm_seconds','d'))
        fields.append(('timestamp_str','20s'))
	#fields.append(('timestamp_tm','??')) # C 'struct tm'

        # Decode the C data into a Python dict
        h_keys = []; h_fmt = '';
        for pair in fields:
            h_keys.append(pair[0])
            h_fmt += pair[1]
        h_len  = struct.calcsize(h_fmt)
        h_vals = struct.unpack(h_fmt, raw_hdr[0:h_len])
        h = dict(zip(h_keys, h_vals))

        # Clean up some null-terminated strings
        h['winfunc'] = h['winfunc'].rstrip(' \t\r\n\0')
        h['timestamp_str'] = h['timestamp_str'].rstrip(' \t\r\n\0')

        return h

    def parse_statecounts(self, hdr):
        """Collect the state counts in the header fields into tidy arrays"""

        plevels_1 = []
        plevels_2 = []
        for n in range(16):
            plevels_1.append(hdr['plevels_src_1_%d'%n])
            plevels_2.append(hdr['plevels_src_1_%d'%n])
        return (plevels_1,plevels_2)

    def parse_spectrum(self, raw_spectrum, hdr):
        """Parse float-type spectral data and output it as an array"""

        dtype = hdr['datalayout']
        if dtype == 0:
            # Layout 0 : real-valued power spectrum
            return raw_spectrum
        elif dtype == 1:
            # Layout 1 : complex cross-power spectrum,
            # with data written as N x {Re,Im} pairs
            return (raw_spectrum[0::2] + 1j*raw_spectrum[1::2])
        else:
            # not yet implemented,
            # such as Layout 2 : complex N x {Re} + N x {Im}
            print ("Data layout %d not yet supported." % dtype)
        return numpy.zeros(0)
