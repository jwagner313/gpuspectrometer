#!/usr/bin/python
#
# One-time helper to calculate Unix times of the start of
# each of the 64 available VDIF Reference Epochs.
#
# Uses astronomy package 'ephem' from https://pypi.python.org/pypi/pyephem/,
# it can be installed with "sudo pip install ephem"
import ephem   

unix_refep = ephem.Date('1970/01/01 00:00:00.00')

# VDIF Header 'Ref Epoch' field, 6-bit:
# epoch 0 = 2000/01/01 00:00, epoch 1 = 2000/07/01 00:00,
# epoch 2 = 2001/01/01 00:00, epoch 3 = 2001/07/01 00:00, ...
eps = range(0, 2**6-1)
tunix = []
for ep in eps:
    year    = 2000 + int(ep/2)
    month   = 1 + (ep%2)*6
    T_str   = '%4d/%02d/01 00:00:00.00' % (year,month)
    T_vdif  = ephem.Date(T_str)
    dT_unix = float(T_vdif - unix_refep) * 24*60*60
    tunix.append(int(dT_unix))
    print ('Epoch %2d : base time %s : %f seconds from Unix epoch' % (ep,T_str,dT_unix))

print ('vdifEp2timeval = {')
for tv in tunix:
    print ('%d, ' % int(tv)),
print ('\n};')

# Result:
# vdifEp2timeval = {
#    946684800,  962409600,  978307200,  993945600, 1009843200,
#   1025481600, 1041379200, 1057017600, 1072915200, 1088640000,
#   1104537600, 1120176000, 1136073600, 1151712000, 1167609600,
#   1183248000, 1199145600, 1214870400, 1230768000, 1246406400,
#   1262304000, 1277942400, 1293840000, 1309478400, 1325376000, 
#   1341100800, 1356998400, 1372636800, 1388534400, 1404172800,
#   1420070400, 1435708800, 1451606400, 1467331200, 1483228800,
#   1498867200, 1514764800, 1530403200, 1546300800, 1561939200,
#   1577836800, 1593561600, 1609459200, 1625097600, 1640995200,
#   1656633600, 1672531200, 1688169600, 1704067200, 1719792000,
#   1735689600, 1751328000, 1767225600, 1782864000, 1798761600,
#   1814400000, 1830297600, 1846022400, 1861920000, 1877558400,
#   1893456000, 1909094400, 1924992000
# };

