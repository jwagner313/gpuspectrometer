#ifndef TIME_UTILS_H
#define TIME_UTILS_H

#include <stddef.h>
#include <time.h>
#include <sys/time.h>

/**
 * Helper that converts a 'struct timeval' into a string representation.
 * The output format is "%04d/%02d/%02d %02d:%02d:%06.3f".
 */
void timeval2YYYYMMDDhhmmss(const struct timeval *tv, char *dst, const size_t ndst);

/**
 * Helper that converts a 'struct timeval' into a string representation.
 * The output format is specified in 'fmt'.
 */
void timeval2YYYYMMDDhhmmss_fmt(const struct timeval *tv, char *dst, const size_t ndst, const char* fmt);

/**
 * Helper that converts a 'struct timeval' into a string representation, with Day-of-Year.
 * The output format is "%04d/%03d %02d:%02d:%06.3f".
 */
void timeval2YYYYDDDhhmmss(const struct timeval *tv, char *dst, const size_t ndst);

/**
 * Helper that converts a 'struct timeval' into a string representation, with Day-of-Year.
 * The output format is specified in 'fmt'.
 */
void timeval2YYYYDDDhhmmss_fmt(const struct timeval *tv, char *dst, const size_t ndst, const char* fmt);

#endif // TIME_UTILS_H
