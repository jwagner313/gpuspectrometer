/////////////////////////////////////////////////////////////////////////////////////
//
// A multi-threading capable buffer system.
//
// Multiple large buffers are continuously refilled by a receiver thread.
// Each buffer contains time-continuous UDP/IP data. The receiver thread (re)-fills
// empty or already processed old buffers with new data.
//
// rx_buffers_t* udp_receiver_init() : initializes buffer system
// udp_receiver_start()              : starts the receiver thread
//
// User source code can "pthread_cond_wait(&rx->data_available, &rx->lock);"
// to wait for and then grab one of the recently filled data buffers.
//
// When user code is complete it can mark the grabbed buffer as processed
// and release it for re-filling by the asynchronous network receiver thread.
//
// If user code is too slow to process data, the UDP/IP receiver thread
// waits (and drops packets) until the user releases an empty buffer again.
//
// This ensures that data inside a buffer is always in an increasing-in time order.
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef UDP_RECEIVER_H
#define UDP_RECEIVER_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define RX_TIMEOUT_SEC  5
#define MAX_RECEIVE_SIZE 9000

#ifdef __cplusplus
extern "C" {
#endif

typedef struct rx_buffers_tt {

        // Buffer dimensions
	size_t Nbuffers;     // number of buffers in total
	size_t Lbuffers;     // length in bytes of each buffer
	size_t Nunconsumed;  // number of buffers with unconsumed data

        // Buffer (meta)data
	unsigned char**  buffers;
	struct timeval*  buffer_starttimes;
	struct timeval*  buffer_stoptimes;
	unsigned char*   buffer_is_consumed; // free for (re)writing
	unsigned char*   buffer_is_held;     // occupied for reading
	size_t*          buffer_nbytes;

        // Network socket
	int		sock_d;

        // Multi-threading
	pthread_mutex_t lock;               // access to this structure
	pthread_t       rxthread;           // receiver thread
	pthread_cond_t  data_available;     // used by receiver thread to wake up the client(s)

	volatile int    do_terminate;       // set !=0 to terminate receiver thread

} rx_buffers_t;

rx_buffers_t* udp_receiver_init(const char* port, size_t Nbuffers, size_t Lbuffers);

void udp_receiver_start(rx_buffers_t* rx);

#ifdef __cplusplus
}
#endif

#endif
