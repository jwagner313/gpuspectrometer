/////////////////////////////////////////////////////////////////////////////////////
//
// A VDIF receiver (from UDP/IP) based on capture using the Ring Buffer library.
//
// Starts the data receiver into the background. Returns segments of raw VDIF sample
// data for a given integration period. The VDIF headers are removed. Any missing
// packets (missing VDIF frames) are replaced with zero-valued samples (TODO: random
// noise).
//
// Sometimes UDP/IP contains a 4-byte or 8-byte packet sequence number before the
// actual VDIF frame. The receive function can skip these preceding bytes.
//
// The raw data stripping routines work only on a full-frame level. The requested
// duration of segments to return must fit an integer number of complete VDIF frames.
//
/////////////////////////////////////////////////////////////////////////////////////

#include "RingBuffer.h"
#include "vdif_header_util.h"
#include "vdif_receiver_rb.h"
#include "math_ext.h" // for fmod_prec(), a reduced-precision fmod() variant

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <assert.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <errno.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/////////////////////////////////////////////////////////////////////////////////////

/** udp_vdif_receiver_thread()
 *
 * Worker thread that fills a ring buffer using received UDP/IP, with
 * optionally stripped packet sequence numbers. The VDIF time stamps
 * are not checked here.
 */
static void *udp_vdif_receiver_thread(void* targ)
{
    vdif_rx_t *rx = (vdif_rx_t*)targ;
    assert(targ != NULL);

    pthread_mutex_lock(&rx->mtx);
    pthread_cond_wait(&rx->go, &rx->mtx);
    pthread_mutex_unlock(&rx->mtx);

    while (rx->sd) {
        size_t nrd = RBsudppread(rx->rb, rx->sd, rx->frame_size, rx->frame_offset);
        if (nrd != rx->frame_size) {
            fprintf(stderr, "Failed to append %zu bytes to ring buffer (got %zu)\n", rx->frame_size, nrd);
            RBstatus(rx->rb, "rx error");
        }
    }

    pthread_exit(NULL);
}

/////////////////////////////////////////////////////////////////////////////////////

/** udp_vdif_receiver()
 *
 * Prepare a UDP/IP socket and start listening for incoming UDP, then return
 * an object that the user can receive those incoming data from.
 *
 * @param port UDP port to listen on, given as a text string.
 * @param vdif_framesize Exact length of the VDIF frames in bytes.
 * @param vdif_offset Number of bytes at beginning of UDP packet to ignore.
 * @param rate_Mbps Goodput payload data rate (Mbit/second) excluding overhead like VDIF headers.
 * @return A new buffered receiver object used to read incoming data, or NULL on error.
 */
vdif_rx_t* udp_vdif_receiver(const char* port, size_t vdif_framesize, off_t vdif_offset, size_t rate_Mbps)
{
    vdif_rx_t *rx;

    struct addrinfo  hints;
    struct addrinfo *res = 0;
    struct timeval tv_timeout;
    int val;

    assert((vdif_framesize >= VDIF_HDRLEN) && (vdif_framesize <= VDIF_MAXLEN));
    assert(vdif_offset >= 0 && vdif_offset < (vdif_framesize-VDIF_HDRLEN));
    assert(port != NULL);

    /* Receiver object */
    rx = calloc(1, sizeof(vdif_rx_t));
    rx->frame_size     = vdif_framesize;
    rx->frame_offset   = vdif_offset;

    /* Receiver socket */
    memset(&hints,0,sizeof(hints));
    hints.ai_family    = AF_UNSPEC;
    hints.ai_socktype  = SOCK_DGRAM;
    hints.ai_protocol  = 0;
    hints.ai_flags     = AI_PASSIVE | AI_ADDRCONFIG;
    getaddrinfo(NULL, port, &hints, &res);

    rx->sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (bind(rx->sd, res->ai_addr, res->ai_addrlen) == -1) {
        perror("Failed to bind socket");
        return NULL;
    }
    freeaddrinfo(res);

    tv_timeout.tv_sec  = UDP_TIMEOUT_SEC;
    tv_timeout.tv_usec = 0;
    setsockopt(rx->sd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv_timeout, sizeof(struct timeval));

    val = UDP_RXBUF_LEN;
    setsockopt(rx->sd, SOL_SOCKET, SO_RCVBUF, &val, sizeof(val));

    /* Create the ring buffer : default 1.0 seconds of data */
    rx->rate_Mbps      = rate_Mbps;
    rx->rate_fps       = ((rate_Mbps * 1000000)/8) / (rx->frame_size - VDIF_HDRLEN);
    rx->bufsize_frames = rx->rate_fps;
    rx->bufsize_bytes  = rx->bufsize_frames * rx->frame_size;
    fprintf(stderr,
            "Creating ring buffer to hold %zu frames (%zu bytes) at assumed %zu frames/sec on UDP/IP port %s.\n",
            rx->bufsize_frames, rx->bufsize_bytes, rx->rate_fps, port
    );
    rx->rb = RBcreate(rx->bufsize_bytes, "spectrometerRBudpRx");
    rx->workbuf = memalign(4096, 2*rx->frame_size);
    rx->is_file = 0;
    rx->is_network = 1;

    /* Start the background receiver thread */
    pthread_mutex_init(&rx->mtx, NULL);
    pthread_cond_init(&rx->go, NULL);
    pthread_create(&rx->tid, NULL, udp_vdif_receiver_thread, (void*)rx);

    return rx;
}

/////////////////////////////////////////////////////////////////////////////////////
