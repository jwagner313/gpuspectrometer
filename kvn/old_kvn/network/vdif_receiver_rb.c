/////////////////////////////////////////////////////////////////////////////////////
//
// A VDIF receiver (from UDP/IP) based on capture using the Ring Buffer library.
//
// Starts the data receiver into the background. Returns segments of raw VDIF sample
// data for a given integration period. The VDIF headers are removed. Any missing
// packets (missing VDIF frames) are replaced with zero-valued samples (TODO: random
// noise). Sometimes UDP/IP contains a 4-byte or 8-byte packet sequence number before
// the actual VDIF frame. The receive function can skip these preceding bytes.
//
// Three VDIF receivers are available:
//   udp_vdif_receiver        :  UDP/IP VDIF reception
//   filesource_vdif_receiver :  file VDIF readout
//   testsource_vdif_receiver :  synthetic sample generator (32-bit float)
//
// Sometimes UDP/IP contains a 4-byte or 8-byte packet sequence number before the
// actual VDIF frame. The receive function can skip these preceding bytes.
//
// The raw data stripping routines work only on a full-frame level. The requested
// duration of segments to return must fit an integer number of complete VDIF frames.
//
/////////////////////////////////////////////////////////////////////////////////////

#include "RingBuffer.h"
#include "vdif_header_util.h"
#include "vdif_receiver_rb.h"
#include "math_ext.h" // for fmod_prec(), a reduced-precision fmod() variant

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <assert.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <errno.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/////////////////////////////////////////////////////////////////////////////////////

/* Conversion from VDIF Ref. Epoch into Unix time, calculated with ../script/vdif_epochs_unixtime.py */
static const time_t C_vdif_epoch_to_timeval[] = {
   946684800,  962409600,  978307200,  993945600, 1009843200,
  1025481600, 1041379200, 1057017600, 1072915200, 1088640000,
  1104537600, 1120176000, 1136073600, 1151712000, 1167609600,
  1183248000, 1199145600, 1214870400, 1230768000, 1246406400,
  1262304000, 1277942400, 1293840000, 1309478400, 1325376000,
  1341100800, 1356998400, 1372636800, 1388534400, 1404172800,
  1420070400, 1435708800, 1451606400, 1467331200, 1483228800,
  1498867200, 1514764800, 1530403200, 1546300800, 1561939200,
  1577836800, 1593561600, 1609459200, 1625097600, 1640995200,
  1656633600, 1672531200, 1688169600, 1704067200, 1719792000,
  1735689600, 1751328000, 1767225600, 1782864000, 1798761600,
  1814400000, 1830297600, 1846022400, 1861920000, 1877558400,
  1893456000, 1909094400, 1924992000
};

/////////////////////////////////////////////////////////////////////////////////////

/** vdif_receiver_fit_segmenttime()
 *
 * Calculates the segmentation time closest to the desired segmentation time that
 * will result in an integer number of frames per segment. Note that this does not
 * necessarily divide evenly into integer seconds!
 *
 * @param rx Buffered UDP receiver object, contains user-specified frame rate and frame size.
 * @param T_seg_wish Desired length of the segments in seconds.
 * @return Best-fit segment length in seconds with an integer number of frames.
 */
double vdif_receiver_fit_segmenttime(const vdif_rx_t* const rx, const double T_seg_wish)
{
    double T_seg    = T_seg_wish;
    size_t fpseg    = T_seg_wish * rx->rate_fps;
    size_t fpseg_lo = fpseg;
    size_t fpseg_hi = fpseg;

    /* Get lower bound of best-fit frames/segment */
    while (fpseg_lo > 4) {
        size_t n = fpseg_lo % rx->rate_fps;
        if (n == 0) { break; }
        if ((rx->rate_fps % n) == 0) { break; }
        fpseg_lo--;
    }

    /* Get higher bound of best-fit frames/segment */
    while (fpseg_hi < 16*rx->rate_fps) {
        size_t n = fpseg_hi % rx->rate_fps;
        if (n == 0) { break; }
        if ((rx->rate_fps % n) == 0) { break; }
        fpseg_hi++;
    }

    /* Choose the frames/segment that differs least from the desired goal */
    if (abs(fpseg_hi-fpseg) < abs(fpseg_lo-fpseg)) {
        fpseg = fpseg_hi;
    } else {
        fpseg = fpseg_lo;
    }

    T_seg = fpseg / ((double)rx->rate_fps);
    // fprintf(stderr, "vdif_receiver_fit_segmenttime: wish=%f sec ~ %zd frames/segm: lo=%zd hi=%zd : closest=%zd frames/segm == best=%f sec\n",
    //    T_seg_wish, (size_t)(T_seg_wish * rx->rate_fps), fpseg_lo, fpseg_hi, fpseg, T_seg);
    return T_seg;
}

/////////////////////////////////////////////////////////////////////////////////////

/** vdif_receiver_get_segmentsize()
 *
 * Calculates the size of a raw VDIF data segment (in bytes; without VDIF headers)
 * for a given length in seconds, rounded up to the nearest full frame.
 *
 * @param rx Buffered UDP receiver object, contains user-specified frame rate and frame size.
 * @param T_seg Length of the segment in seconds.
 * @return Size of the segment in bytes.
 */
size_t vdif_receiver_get_segmentsize(const vdif_rx_t* const rx, const double T_seg)
{
    size_t n;
    assert(rx != NULL);
    n  = ((size_t)(T_seg * rx->rate_fps));
    n *= (rx->frame_size - VDIF_HDRLEN);
    return n;
}

/////////////////////////////////////////////////////////////////////////////////////

/** vdif_receiver_start()
 *
 * Tells the background data capture to start.
 */
void vdif_receiver_start(vdif_rx_t* rx)
{
    assert(rx != NULL);
    if (rx->bind_cpu > 0) {
        vdif_receiver_bind_cpu(rx, rx->bind_cpu);
    }
    pthread_mutex_lock(&rx->mtx);
    pthread_cond_broadcast(&rx->go);
    pthread_mutex_unlock(&rx->mtx);
}

/////////////////////////////////////////////////////////////////////////////////////

/** vdif_receiver_get_segment(T_seg)
 *
 * Return a contiguous piece of raw VDIF payload data. Frames that
 * are missing (buffer overflow, network loss) are replaced by zero data.
 *
 * A new segment is considered to start when the VDIF data-second is close
 * enough to a multiple of segmentation time T_seg, more precisely, when
 * fmod(T_vdif,T_seg) <= 25%*T_seg.
 *
 * @param rx Buffered UDP receiver object.
 * @param d User buffer for storing raw sample data.
 * @param T_seg Length of the segment in seconds. Should fit an integer number of frames.
 * @param &tv_mid Time of day at the middle of the data segment.
 * @return The amount of valid bytes received. The timestamp is stored into tv_mid.
 */
size_t vdif_receiver_get_segment(vdif_rx_t* const rx, unsigned char* d, double T_seg, struct timeval* tv_mid)
{
    assert(rx != NULL);

    int rx_reattempts = 0;

    int    started = 0, started_1 = 0;
    size_t AP_num = 0, AP_num_1 = 0;
    double AP_startsec = 0.0;

    const vdif_header_t* hdr        = rx->workbuf; // note: workbuf must be aligned at some 4-byte multiple!
    const size_t payload_size       = rx->frame_size - VDIF_HDRLEN;

    const size_t frames_per_seg     = round(T_seg * rx->rate_fps);
    const size_t frame_nr_mod_limit = frames_per_seg * SEGMENT_START_THRESH;

    const size_t nbytewanted = vdif_receiver_get_segmentsize(rx, T_seg);
    size_t nvalid = 0, ntotal = 0; // byte counts

    struct timeval tv_start, tv_curr;
    gettimeofday(&tv_start, NULL);

    assert(nbytewanted >= rx->frame_size);
    assert(T_seg < 10.0);
    assert((frame_nr_mod_limit != 0) && (T_seg > 0.0));

    /* Clear existing data (or keep? or fill with random?) */
    memset(d, 0x00, nbytewanted);

    /* Fill user buffer with new data */
    while (ntotal < nbytewanted) {

        double T_frame, T_mod;
        size_t frame_nr, frame_nr_in_segment;
        size_t navail = 0;

        if (rx->at_eof) {
            break;
        }

        RBextractable(rx->rb, &navail);

        if (navail < rx->frame_size) {
            if (rx_reattempts > 1000) {
                rx_reattempts = 0;
                gettimeofday(&tv_curr, NULL);
                if ((tv_curr.tv_sec - tv_start.tv_sec) >= 10) {
                    fprintf(stderr, "  Readout cancelled, no new data in buffer in the past 10 seconds\n");
                    return nvalid;
                }
            }
            // usleep(100); // good for CPU load, bad for spectrometer performance...
            rx_reattempts++;
            continue;
        }
        rx_reattempts = 0;

        navail = RBget(rx->rb, rx->workbuf, rx->frame_size);
        assert(navail == rx->frame_size);
        assert((8 * hdr->framelength8) == rx->frame_size);

        /* Get frame offset within a segment */
        frame_nr = (hdr->seconds % 1000)*rx->rate_fps + hdr->frame;
        frame_nr_in_segment = frame_nr % frames_per_seg;

        /* Determine segment (if data lost at end of segment, might end up in next segment!) */
        AP_num_1 = AP_num;
        AP_num   = (frame_nr-frame_nr_in_segment) / frames_per_seg;

        /* Get timestamp of 1st sample in frame */
        T_frame  = hdr->seconds + ((double)hdr->frame) / rx->rate_fps;
        T_mod    = fmod_prec(T_frame, T_seg, 5);

        /* Reached the start time of a new segment? Or just closely after? */
        started_1 = started;
        started = started | (!started && (frame_nr_in_segment < frame_nr_mod_limit));

#if 0
        if (started && !started_1) {
            fprintf(stderr, "  cap started at frame %d @ %zd : time %f : AP f=%zd f-per-AP=%zd mod=%zd lim=%zd : nwant=%zd ntot=%zd AP=%zd\n",
                    hdr->frame, rx->rate_fps, T_frame, frame_nr, frames_per_seg, frame_nr_in_segment, frame_nr_mod_limit,
                    nbytewanted, ntotal, AP_num
            );
        }
#endif

        /* If just started a segment, determine its mid-segment time stamp */
        if (started && !started_1) {
            double AP_startsec = T_frame - T_mod;
            double T_mid = AP_startsec + T_seg/2;

            if (tv_mid != NULL) {
                // Time in VDIF seconds, counted from VDIF epoch (epochs at 6-month intervals)
                tv_mid->tv_sec  = (time_t)T_mid;
                tv_mid->tv_usec = (long)(1e6 * (T_mid - tv_mid->tv_sec));
                // To get a Unix time, add the reference seconds of the VDIF epoch
                tv_mid->tv_sec += C_vdif_epoch_to_timeval[hdr->epoch];
            }
        }

        /* Store samples if we are inside a segment */
        if (started) {
            size_t ncp;
            off_t  offset;
            assert(T_frame >= AP_startsec);

            /* Detect change of segment (happens when data lost before end of segment) */
            if (started_1 && (AP_num != AP_num_1)) {
                if (DBG_PRINT) {
                    fprintf(stderr, "  the destination AP changed from AP#%zd to AP#%zd (perhaps lost UDP/IP near end of AP)\n", AP_num_1, AP_num);
                }
                break;
            }

            /* Grab the data */
            offset  = frame_nr_in_segment * payload_size;
            ncp     = ((offset + payload_size) <= nbytewanted) ? payload_size : (nbytewanted - offset);
            nvalid += ncp;
            ntotal  = offset + ncp;
            //memmove(d + offset, rx->workbuf + VDIF_HDRLEN, ncp);
            memcpy(d + offset, rx->workbuf + VDIF_HDRLEN, ncp);
            assert(nvalid <= nbytewanted);

            if (DBG_PRINT) {
                fprintf(stderr, "  with nbytewanted=%zu, added %zu new @ frame %zd/%zd, now at offset %zd, now nvalid=%zu ntotal=%zu\n",
                        nbytewanted, ncp, frame_nr_in_segment, frames_per_seg, offset, nvalid, ntotal
                );
            }
        }
    }

    return nvalid;
}

/////////////////////////////////////////////////////////////////////////////////////

/** vdif_receiver_bind_cpu()
 * Binds the background receiver thread to a particular CPU core.
 */
int vdif_receiver_bind_cpu(vdif_rx_t* const rx, int cpu)
{
    int rc = 0;
    assert(rx != NULL);
    cpu_set_t set;
    if (rx->tid > 0) {
        // Receiver thread already running. Bind to CPU immediately.
        CPU_ZERO(&set);
        CPU_SET(cpu, &set);
        rc = pthread_setaffinity_np(rx->tid, sizeof(cpu_set_t), &set);
        if (rc != 0) {
            errno = rc;
            perror("vdif_receiver_bind_cpu");
        } else {
            fprintf(stderr, "Successfully bound raw data receiver background thread to CPU#%d\n", cpu);
        }
        rx->bind_cpu = cpu;
    } else {
        // Remember not started yet. Bind to CPU later.
        rx->bind_cpu = cpu;
    }
    return rc;
}

/////////////////////////////////////////////////////////////////////////////////////
