/////////////////////////////////////////////////////////////////////////////////////
//
// Definition of structures used by the KVN Digital Spectrometer (hardware).
//
// Follows specifications in 2007KVN-SPC0013_SpectralResultsFile_Spec_ver_110_E.pdf
// roughly but extends if by additional information. See structures below.
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef FORMATDSMv2_HXX
#define FORMATDSMv2_HXX

#include <string>
#include <stdint.h>
#include <stddef.h>
#include <sys/time.h>

#define KVN_DSMv2_MAX_IP_PER_FILE 10000

class SpectrometerConfig;

class FormatDSMv2 {

    public:

#pragma pack(push)
#pragma pack(1)
    struct KVN_DSM_Header_t {
        uint16_t    header_length;       // Length of entire header in bytes, includes this field
        uint32_t    sequence_number;     // Begin at 0, increment +1 in every Integration Period (IP), max IP is 10,000-1 (then new file)
        uint8_t     start_time[7];       // Time at acquistion start (not middle of interval); YYYYDDDhhmmss0 with lowest 4 bit of last byte always 0
        char        _padding1;
        uint32_t    bandwidth;           // Bandwidth of the IF channel(s) in hertz, identical to the full bandwidth covered by 'spectral_channels'
        uint32_t    output_streams;      // Bitmask with bits for presence of 16 auto and 8 cross spectra
        uint32_t    valid_samples;       // Count of valid samples in each spectrum (identical in all spectra)
        uint32_t    invalid_samples;     // Count of lost/missing/invalid samples in each spectrum (identical in all spectra)
        char        integration_length;  // 1: 10.24ms, 2: 51.2ms, 3:102.4ms, 4:512ms, 5:1024ms, others undefined
        char        _padding2;
        float       actual_integration;  // Actual integration time <msec> as constrained by VDIF and DFT lengths
        uint32_t    spectral_channels;   // Number of spectral output channels in original DFT
        uint32_t    start_channel;       // First channel of spectral window taken from original DFT            ; default: 0
        uint32_t    stop_channel;        // Last channel (inclusive) of spectral window taken from original DFT ; default: spectral_channels-1
        uint8_t     sampler_stats[16][4];// Bit distributions in 16 IFs in percentages for 0b00, 0b01, 0b10, 0b11 2-bit sample states
    };

    union KVN_OutStreams_t {
        struct bit_tt {
            // Byte 0
            bool auto_f1 : 1;
            bool auto_f2 : 1;
            bool cross_f1f2_re : 1;
            bool cross_f1f2_im : 1;
            bool auto_f3 : 1;
            bool auto_f4 : 1;
            bool cross_f3f4_re : 1;
            bool cross_f3f4_im : 1;
            // Byte 1
            bool auto_f5 : 1;
            bool auto_f6 : 1;
            bool cross_f5f6_re : 1;
            bool cross_f5f6_im : 1;
            bool auto_f7 : 1;
            bool auto_f8 : 1;
            bool cross_f7f8_re : 1;
            bool cross_f7f8_im : 1;
            // Byte 2
            bool auto_f9 : 1;
            bool auto_f10 : 1;
            bool cross_f9f10_re : 1;
            bool cross_f9f10_im : 1;
            bool auto_f11 : 1;
            bool auto_f12 : 1;
            bool cross_f11f12_re : 1;
            bool cross_f11f12_im : 1;
            // Byte 3
            bool auto_f13 : 1;
            bool auto_f14 : 1;
            bool cross_f13f14_re : 1;
            bool cross_f13f14_im : 1;
            bool auto_f15 : 1;
            bool auto_f16 : 1;
            bool cross_f15f16_re : 1;
            bool cross_f15f16_im : 1;
        } bit;
        uint16_t word16[2];
        uint32_t word32;
    };
#pragma pack(pop) // restore previous pack() to avoid BOOST library crashes

    public:
        FormatDSMv2();

        /** Converts a 'struct timeval' into a string representation. The output format is specified in 'fmt'. */
        void timeval2YYYYMMDDhhmmss_fmt(char *dst, const size_t ndst, const struct timeval *tv, const char* fmt);

        /** Converts a 'struct timeval' into a string representation, with Day-of-Year. The output format is specified in 'fmt'. */
        void timeval2YYYYDDDhhmmss_fmt(char *dst, const size_t ndst, const struct timeval *tv, const char* fmt);

        /** Convert GPU spectrometer data into KVN DSM format. Allocates new 'out' buffer */
        int convertData(char** out, size_t& nbyte_out, const char* in, size_t nbyte_in, double w, struct timeval t, const SpectrometerConfig* cfg);

        /** Return current output file name */
        std::string getFilename() { return m_currFilename; }

        /** Return current integration period numer */
        uint32_t getIpNum() const { return m_ipNum; }

    private:
        int getIntegrationLengthID(double T_int_s);
        void timeval2bcd(struct timeval& tv, unsigned char* out);
        void appendSpectrum_if(bool enabled, const float*& in, float*& out, int nfloats);

    private:
        uint32_t m_serialNum;       // starts at 0, increments once every 10,000 IP (KVN_DSM_MAX_IP_PER_FILE)
        uint32_t m_ipNum;           // current Integration Period number, increments "infinitely"
        std::string m_currFilename; // file name which follows naming "basename.<serial number>.SPC.dat"

        static float m_standard_DSM_Tints_s[5];
};

#endif // FORMATDSM_HXX
