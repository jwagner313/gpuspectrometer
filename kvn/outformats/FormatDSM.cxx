/////////////////////////////////////////////////////////////////////////////////////
//
// Definition of structures used by the KVN Digital Spectrometer (hardware).
// Follows specifications in 2007KVN-SPC0013_SpectralResultsFile_Spec_ver_110_E.pdf
//
// File format:
// bytes     0 to    83 = 84B long HEADER as in kvn_dsm_dataheader_t below
// bytes    84 to 16467 = 16kB of power spectrum of F1
// bytes 16468 to 32851 = 16kB of power spectrum of F2
// bytes 32852 to 65619 = 16kB Imag + 16 kB Real of cross-power spectrum F1 x F2
// followed by any additional pairs (F3,F4,F4xF4, F5,F6,F5xF6, ...)
//
// Documentation inconsistency:
// output_streams field : Section 5.1.1 says the bitmask field is 2-byte 16-bit,
//                        Section 5.2   says the bitmask field is 4-byte 32-bit
//
/////////////////////////////////////////////////////////////////////////////////////

#include "outformats/FormatDSM.hxx"
#include "core/spectrometerconfig.hxx"
#include "core/spectralavg.hxx"
#include "logger.h"

#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <memory.h>
#include <malloc.h>

#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <iomanip>

/////////////////////////////////////////////////////////////////////////////////////

float FormatDSM::m_standard_DSM_Tints_s[5] = { 10.24e-3, 51.2e-3, 102.4e-3, 512e-3, 1024e-3 };

/////////////////////////////////////////////////////////////////////////////////////

static void str2bcd(char* io)
{
    char* out = io;
    while (1) {
        int v = toupper(*(io++));
        if (v == '\0') { *out = 0x00; out++; break; }
        *out = (v>='0' && v<='9') ? (v-'0')<<4 : (10+v-'A')<<4; // TODO: check for illegal inputs
        v = toupper(*(io++));
        if (v == '\0') { *out &= 0xF0; out++; break; }
        *out |= (v>='0' && v<='9') ? (v-'0')&0x0F : (10+v-'A')&0x0F; // TODO: check for illegal inputs
        out++;
    }
    while (out <= io) {
        *out = '\0';
        out++;
    }
}

#ifdef TEST_BCD
int main(int argc, char** argv)
{
   // g++ -Wall -I.. -DTEST_BCD FormatDSM.cxx -o testbcd
   if (argc > 1) {
       char* s = strdup(argv[1]);
       str2bcd(s);
       for (size_t i = 0; i < (strlen(argv[1])+1)/2; i++) {
           printf("%02x", (unsigned char)s[i]);
       }
       printf("\n");
   }
   return 0;
}
#endif

/////////////////////////////////////////////////////////////////////////////////////

/** Converts a 'struct timeval' into a string representation. The output format is specified in 'fmt'. */
void FormatDSM::timeval2YYYYMMDDhhmmss_fmt(char *dst, const size_t ndst, const struct timeval *tv, const char* fmt)
{
    struct tm* tm_val; // gmtime() internal static buffer, does not need free()!
    double     tm_sec;
    tm_val = gmtime((const time_t*)tv);
    tm_sec = tm_val->tm_sec + 1e-6*tv->tv_usec;
    snprintf(dst, ndst, fmt,
             1900 + tm_val->tm_year, tm_val->tm_mon, tm_val->tm_mday,
             tm_val->tm_hour, tm_val->tm_min, tm_sec
    );
}

/** Converts a 'struct timeval' into a string representation, with Day-of-Year. The output format is specified in 'fmt'. */
void FormatDSM::timeval2YYYYDDDhhmmss_fmt(char *dst, const size_t ndst, const struct timeval *tv, const char* fmt)
{
    struct tm* tm_val; // gmtime() internal static buffer, does not need free()!
    double     tm_sec;
    tm_val = gmtime((const time_t*)tv);
    tm_sec = tm_val->tm_sec + 1e-6*tv->tv_usec;
    snprintf(dst, ndst, fmt,
             1900 + tm_val->tm_year, tm_val->tm_yday + 1,
             tm_val->tm_hour, tm_val->tm_min, tm_sec
    );
}

/////////////////////////////////////////////////////////////////////////////////////

/** Convert GPU spectrometer data into KVN DSM format. Allocates new 'out' buffer */
int FormatDSM::convertData(char** out, size_t& nbyte_out, const char* in, size_t nbyte_in, double w, struct timeval tref, const SpectrometerConfig* cfg)
{
    assert(sizeof(FormatDSM::KVN_DSM_Header_t) == 84);

    // Mask of streams
    KVN_OutStreams_t streammask;
    streammask.word16 = 0;
    streammask.bit.auto_f1 = cfg->scfg.enableAutocorr[0];
    streammask.bit.auto_f2 = cfg->scfg.enableAutocorr[1];
    streammask.bit.cross_f1f2_re = cfg->scfg.enableCrosscorr[0];
    streammask.bit.cross_f1f2_im = cfg->scfg.enableCrosscorr[0];
    streammask.bit.auto_f3 = cfg->scfg.enableAutocorr[2];
    streammask.bit.auto_f4 = cfg->scfg.enableAutocorr[3];
    streammask.bit.cross_f3f4_re = cfg->scfg.enableCrosscorr[1];
    streammask.bit.cross_f3f4_im = cfg->scfg.enableCrosscorr[1];
    streammask.bit.auto_f5 = cfg->scfg.enableAutocorr[4];
    streammask.bit.auto_f6 = cfg->scfg.enableAutocorr[5];
    streammask.bit.cross_f5f6_re = cfg->scfg.enableCrosscorr[2];
    streammask.bit.cross_f5f6_im = cfg->scfg.enableCrosscorr[2];
    streammask.bit.auto_f7 = cfg->scfg.enableAutocorr[6];
    streammask.bit.auto_f8 = cfg->scfg.enableAutocorr[7];
    streammask.bit.cross_f7f8_re = cfg->scfg.enableCrosscorr[3];
    streammask.bit.cross_f7f8_im = cfg->scfg.enableCrosscorr[3];

    // Allocate output buffer
    nbyte_out = sizeof(FormatDSM::KVN_DSM_Header_t);
    for (int s = 0; s < 16; s++) {
        if ( ((streammask.word16 >> s) & 1) != 0 ) {
            nbyte_out += KVN_DSM_NCHANNELS*sizeof(float);
        }
    }
    *out = (char*)memalign(4096, nbyte_out);
    memset(*out, 0, sizeof(FormatDSM::KVN_DSM_Header_t));

    // Convenience pointers
    KVN_DSM_Header_t* h = (FormatDSM::KVN_DSM_Header_t*)(*out);
    float* dout = (float*)(*out + sizeof(FormatDSM::KVN_DSM_Header_t));
    const float* din = (float*)in;

    // Header seq nr
    h->sequence_number = m_ipNum % KVN_DSM_MAX_IP_PER_FILE;
    if (m_ipNum >= KVN_DSM_MAX_IP_PER_FILE) {
        m_serialNum++;
    }

    // File name (if used externally)
    std::ostringstream ss;
    ss << cfg->scfg.obsPath << m_serialNum << ".SPC.dat";
    m_currFilename = ss.str();

    // Header start_time
    char dtime[20];
    this->timeval2YYYYDDDhhmmss_fmt(dtime, sizeof(dtime), &tref, "%04d%03d%02d%02d%02.0f");
    str2bcd(dtime);
    memcpy(h->start_time, dtime, sizeof(h->start_time));
    h->start_time[6] &= 0xF0;

    // Other header fields
    size_t total_samples = cfg->pcfg.nfft * 2*cfg->scfg.nchan;
    for (size_t n=0; n<sizeof(h->valid_samples)/sizeof(h->valid_samples[0]); n++) {
        h->valid_samples[n] = std::ceil(w * total_samples);
        h->invalid_samples[n] = total_samples - h->valid_samples[n];
    }
    h->output_streams = streammask.word16; // filled out above, copy the bits from word16 (or word32?)
    h->input_mode[0] = 2;                  // 1: Narrow band, 2: Wide band
    h->output_mode = 2;                    // 1: Channel Select Mode, 2: Channel Bind Mode
    h->start_channel_index = 0;            // unsure what this is, but is zero when in Channel Bind Mode
    h->integration_length = 0;             // 1: 10.24ms, 2: 51.2ms, 3: 102.4ms, 4: 512ms, 5: 1024ms
    for (size_t n=0; n<sizeof(m_standard_DSM_Tints_s)/sizeof(m_standard_DSM_Tints_s[0]); n++) {
        if (std::abs(m_standard_DSM_Tints_s[n] - cfg->pcfg.Tint_s) < 1e-3) {
            h->integration_length = n;
        }
    }
    if (h->integration_length == 0) {
        L_(lwarning) << "non-DSM-standard integration length of " << std::fixed << std::setprecision(2) << (cfg->pcfg.Tint_s*1e3) << "ms";
    }

    // Spectral averaging
    SpectralAverager avg;
    int avg_factor = cfg->scfg.nchan / KVN_DSM_NCHANNELS; // TODO: what if nchan<KVN_DSM_NCHANNELS? interpolate?
    float norm_factor = 1.0 / (float)(cfg->pcfg.nfft * cfg->scfg.nchan);

    // Auto 1 and 2
    if (cfg->scfg.enableAutocorr[0]) {
        avg.spectralAvgAuto(din, dout, cfg->scfg.nchan, avg_factor);
        avg.spectralRescale(dout, KVN_DSM_NCHANNELS, norm_factor);
        dout += KVN_DSM_NCHANNELS;
    }
    din += (cfg->scfg.nchan+0);
    if (cfg->scfg.enableAutocorr[1]) {
        avg.spectralAvgAuto(din, dout, cfg->scfg.nchan, avg_factor);
        avg.spectralRescale(dout, KVN_DSM_NCHANNELS, norm_factor);
        dout += KVN_DSM_NCHANNELS;
    }
    din += (cfg->scfg.nchan+0);

    // Cross 1 x 2
    if (cfg->scfg.enableCrosscorr[0]) {
        avg.spectralAvgCross(din, dout, cfg->scfg.nchan, avg_factor);
        avg.spectralRescale(dout, 2*KVN_DSM_NCHANNELS, norm_factor);
        dout += 2*KVN_DSM_NCHANNELS;
    }
    if (cfg->scfg.do_cross) {
        din += 2*(cfg->scfg.nchan+0);
    }

    // Auto 3 and 4
    if (cfg->scfg.enableAutocorr[2]) {
        avg.spectralAvgAuto(din, dout, cfg->scfg.nchan, avg_factor);
        avg.spectralRescale(dout, KVN_DSM_NCHANNELS, norm_factor);
        dout += KVN_DSM_NCHANNELS;
    }
    din += (cfg->scfg.nchan+0);
    if (cfg->scfg.enableAutocorr[3]) {
        avg.spectralAvgAuto(din, dout, cfg->scfg.nchan, avg_factor);
        avg.spectralRescale(dout, KVN_DSM_NCHANNELS, norm_factor);
        dout += KVN_DSM_NCHANNELS;
    }
    din += (cfg->scfg.nchan+0);

    // Cross 3 x 4
    if (cfg->scfg.enableCrosscorr[1]) {
        avg.spectralAvgCross(din, dout, cfg->scfg.nchan, avg_factor);
        avg.spectralRescale(dout, 2*KVN_DSM_NCHANNELS, norm_factor);
        dout += 2*KVN_DSM_NCHANNELS;
    }
    if (cfg->scfg.do_cross) {
        din += 2*(cfg->scfg.nchan+0);
    }

    // Prepare for next
    m_ipNum++;

    return 0;
}
