//
// Class SpectralOutput
//
// Implements the ResultRecipient::takeResult() interface function through which
// other classes (esp. BackendCPU) publish new spectral results. Data are accepted
// in a flat array, and a copy is made together with the active settings.
//
// The copy of results is dispatched to an asynchronous data outputter task.
//
// The outputter task reformats the flat array data into the final output format
// using a formatter class (FormatDSM, FormatDSMv2).
//
// Reformatted data are sent to a file, and/or all connected TCP clients.
//
// A simple TCP server/worker in this class accepts and keeps a list of
// connected clients.
//
#ifndef SPECTRALOUTPUT_HXX
#define SPECTRALOUTPUT_HXX

#include "datarecipients/resultrecipient.hxx"
#include "outformats/FormatDSM.hxx"
#include "outformats/FormatDSMv2.hxx"

#include <boost/noncopyable.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>

#include <vector>

class SpectrometerConfig;

class SpectralOutput : public ResultRecipient, private boost::noncopyable
{
    public:
        /** C'stor, also starts listening on specified TCP port */
        SpectralOutput(int tcp_port);

        /** Hand a new spectral dataset to the outputter object */
        void takeResult(void* autoAndCross, size_t nbytes, double w, struct timeval t, const SpectrometerConfig* fs);

        /** D'stor */
        ~SpectralOutput();

        /** C'stor */
        SpectralOutput() { SpectralOutput(1234); }

        /** Accessor to file output -enabled setting */
        void enableFileOutput(bool enable) { m_fileOutputEnabled=enable; }
        bool getFileOutputEnabled() { return m_fileOutputEnabled; }

        /** Check if file output is active, i.e., enabled and no TCP clients connected */
        bool getFileOutputActive();

        /** Return TCP port for data-receiver clients */
        int getPort(void) { return m_port; }

    private:
        /** Thread that accepts incoming TCP and stores them in a client pool */
        void clients_maintainer(int port);

        /** Detached thread launched by every call of takeResult(). Handles results storage and distribution to any TCP clients */
        void results_storer(void* autoAndCross_copy, size_t nbytes, double w, struct timeval t, SpectrometerConfig* cfg);

    private:
        int m_port;
        bool m_fileOutputEnabled;

        //FormatDSM m_fmtDsm;
        FormatDSMv2 m_fmtDsm;

        boost::thread* cmthread;

        boost::mutex m_clientpoolmutex;
        std::vector<boost::asio::ip::tcp::iostream*> m_clientpool;
};

#endif // SPECTRALOUTPUT_HXX
