#include "rawdatabuffer.hxx"
#include <stdlib.h>
#include <sys/time.h>
#include <string>

const std::string RawDataBuffer::stateToStr(BufState s)
{
    switch (s) {
        case Free: return std::string("Free");
        case Busy: return std::string("Busy");
        case Full: return std::string("Full");
        case Overflowed: return std::string("Over");
        case Transferring: return std::string("Xfer");
    }
    return std::string("--illegal--");
}

