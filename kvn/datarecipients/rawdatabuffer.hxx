#ifndef RAWDATABUFFER_HXX
#define RAWDATABUFFER_HXX

#include <stdlib.h>
#include <sys/time.h>
#include <string>

class RawDataBuffer {

    public:
        RawDataBuffer(void* area, size_t nbytes, int nchan, int streamId)
        {
            data = area;
            capacity = nbytes;
            length = 0;
            state = Free;
            prevstate = Free;
            tmid.tv_sec = 0;
            tmid.tv_usec = 0;
            this->nchan = nchan;
            this->streamId = streamId;
        }

    private:
        RawDataBuffer();

    public:
        enum BufState { Free=0, Busy=1, Full=2, Overflowed=3, Transferring=4 };

        static const std::string stateToStr(BufState s);

        void changeState(BufState newstate)
        {
            prevstate = state;
            state = newstate;
        }

    public:
        double weight;
        BufState state;
        BufState prevstate;
        struct timeval tmid;

        int nchan;
        int streamId;

        void* data;
        size_t capacity;
        size_t length;
};

#endif

