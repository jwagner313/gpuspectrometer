
class SpectrometerInterface;

#include "core/common.hxx"

#include "kvnmc2api/kvnmc2api_factory.hxx"  // macro KVNMC2API_CREATE_AND_REGISTER_CMD()
#include "kvnmc2api/kvnmc_defs.hxx"         // definitions of CMD_RET_DONE, CMD_RET_CMDERROR, ...
#include "core/spectrometerinterface.hxx"   // class SpectrometerInterface;

