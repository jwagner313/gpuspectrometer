#include "commands/common.hxx"

#include <string>
#include <vector>

KVNMC2API_CREATE_AND_REGISTER_CMD(POLL);

int MCMsgCommandResponse_POLL::handle(const std::vector<std::string>& args, std::vector<std::string>& out, SpectrometerInterface& k, bool isQuery)
{
    if ((args.size() == 0) || (args.size() > 2)) {
        out.push_back(CMD_RET_PARAMERROR);
        out.push_back("POLL");
        return -1;
    }

    // Command
    // args[0] = device
    // [args[1] = command]
    // Response
    // args[end] = response from device

    // Response
    out.push_back(CMD_RET_DONE);
    out.push_back("POLL");
    out.push_back(args[0]);
    if (args.size() == 2) {
        out.push_back(args[1]);
    }
    out.push_back("UNSUPPORTED");

    return 0;
}

