#include "commands/common.hxx"

#include <string>
#include <vector>

KVNMC2API_CREATE_AND_REGISTER_CMD(PEND);

int MCMsgCommandResponse_PEND::handle(const std::vector<std::string>& args, std::vector<std::string>& out, SpectrometerInterface& k, bool isQuery)
{
    // "Terminates PCMD under processing"

    // TODO: implement real PEND here once PCMD's are added that background rather than complete immediately

    // Response
    out.push_back(CMD_RET_DONE);
    out.push_back("PEND");

    return 0;
}

