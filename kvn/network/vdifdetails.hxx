#ifndef VDIFDETAILS_HXX
#define VDIFDETAILS_HXX

#include <cstddef>

class VDIFDetails {

    public:

        int nthreads;       // Number of VDIF threads (currently only 1 supported!)
        int nbands;         // Number of recorded bands per VDIF thread
        double bw;          // Bandwidth of one recorded band in Hz
        int nbits;          // Bits per sample
        size_t framesize;   // Size of frame with header
        size_t headersize;  // Size of header
        size_t payloadsize; // Size of payload in frame
        size_t framespersec;// Frame rate
        double frametime_s; // Duration of one data frame, time granularity

    public:

        VDIFDetails() { clear(); }
   
        void clear()
        {
            // defaults
            nthreads = 1;
            nbits = 2;
            nbands = 1;
            bw = 32e6;
            framesize = 8032;
            headersize = 32;
            payloadsize = 8000;
            framespersec = 1000;
            frametime_s = 0;
            recalculate();
        }
    
        void recalculate()
        {
            if (framespersec != 0) {
                bw = (framespersec * 8*payloadsize) / (double)(nbits * nbands * 2);
            } else {
                framespersec = (2*bw * nbands * nbits) / (8*payloadsize);
            }
            frametime_s = 1 / (double)framespersec;
        }

        double getGoodputMBitPerSec() const
        {
            return (8.0 * payloadsize * framespersec) / 1e6;
        }

        double geRateMBitPerSec() const
        {
            return (8.0 * framesize * framespersec) / 1e6;
        }

};

#endif
