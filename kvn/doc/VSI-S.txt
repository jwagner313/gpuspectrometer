
Specifications are in

http://www.vlbi.org/vsi/docs/2003_02_13_vsi-s_final_rev_1.pdf

Command syntax:   <keyword>=<field>:<field>:...;
                  <keyword><PortNr>=<field>:<field>:...;

Response syntax:  !<keyword>=<return code>[:<custom return>:...];
                  !<keyword><PortNr>=<return code>[:<custom return>:...];

            return code is an ASCII integer
            0 - success
            1 - action initiated but not completed yet
            2 - not implemented
            3 - syntax error

Query syntax:     <keyword>?;
                  <keyword><PortNr>?;

Response syntax:  !<keyword>?<return code>[:<custom return>:...];



Then KVN DAS (or at least KVN DSM) do not follow VSI-S. They
use their own command format.
