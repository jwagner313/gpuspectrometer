#!/usr/bin/env python

import socket,os,sys
import select,signal,time
import SocketServer
import Queue
import threading

MAXIMUM_DATA_LEN = 4096*4*4*2 #524288 # 107520
# 215040

class TCPAsyncServer(SocketServer.TCPServer):
	def __init__(self,server_address, RequestHandlerClass):
		self.request_queue_size = 10
		self.allow_reuse_address = True
		self.socket_map = {}
		self.address_map = {}
		self.rfds = [] 
		self.wfds = []
		self.efds = []
		self.timeout = 0.5
		self.maximum_data_len = MAXIMUM_DATA_LEN
		self.socket_fd = 0
		self.client_fd = 0
		self.control_queue = Queue.Queue()
		self.cmd_terminator = ''
		SocketServer.TCPServer.__init__(self,server_address,RequestHandlerClass)

	def server_bind(self):
		sndbuf_size = self.socket.getsockopt(socket.SOL_SOCKET,socket.SO_SNDBUF)
		rcvbuf_size = self.socket.getsockopt(socket.SOL_SOCKET,socket.SO_RCVBUF)

		if sndbuf_size < self.maximum_data_len:
			self.socket.setsockopt(socket.SOL_SOCKET,socket.SO_SNDBUF,self.maximum_data_len)
		if rcvbuf_size < self.maximum_data_len:
			self.socket.setsockopt(socket.SOL_SOCKET,socket.SO_RCVBUF,self.maximum_data_len)
		if self.allow_reuse_address:
			self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.socket.bind(self.server_address)

	def server_activate(self):
		""" Overidden to tie server socket with file descriptor returned by fileno().
		"""
		print "Activating"
		self.socket.listen(self.request_queue_size)
		self.socket_fd = self.socket.fileno()
		self.socket_map[self.socket_fd] = self.socket
		self.address_map[self.socket_fd] = 0

	def serve_forever(self):
		isRun = True
		while isRun:
			try:
				cmd = self.control_queue.get_nowait()
				if cmd == 'stop' :
					self.server_close()
					isRun = False
				else :
					self.handle_request()

			except KeyboardInterrupt:
				self.server_close()
				isRun = False

			except Queue.Empty:
				self.handle_request()

	def server_stop(self):
		self.control_queue.put_nowait('stop')

	def server_close(self):
		""" Overidden to close all client socket connection
		"""
		for fd, socket_obj in self.socket_map.items():
			socket_obj.close()
		self.socket.close()
		self.socket_fd = 0

	def handle_request(self):
		""" Overidden to support multi-client by using select()
		"""
		is_rfds = self.socket_map.keys()
#		is_wfds = self.socket_map.keys()
		is_wfds = [] 
		is_efds = []
#		print is_rfds
		self.rfds,self.wfds,self.efds = select.select(is_rfds,is_wfds,is_efds,self.timeout)
		for fd in self.rfds:
			if fd == self.socket_fd:
				try:
					request, client_address = self.socket.accept()
				except socket.error:
					return
					
#				if len(self.socket_map) > 2 : 
##					request.close()
#					request.shutdown(1)
#				else:
				self.add_client_to_map(request,client_address)

			else:
				request, client_address = self.get_client_from_map(fd)
				try:
					self.process_request(request, client_address)
				except:
					self.handle_error(request,client_address)
					self.close_request(request)
	
#		time.sleep(0.001)

	def add_client_to_map(self,request, address):
		print 'Connected by', address
		client_fd = request.fileno()
		self.socket_map[client_fd] = request
		self.address_map[client_fd] = address
		print 'Adding client on fd = ', client_fd

	def remove_client_from_map(self,fd):
		print 'Removing client on fd =', fd
		del self.socket_map[fd]
		del self.address_map[fd] 

	def get_client_from_map(self,fd):
		self.client_fd = fd
		request = self.socket_map[fd]
		client_address = self.address_map[fd]
		return request, client_address
		
	def close_request(self,request):
		pass

class TCPDataServer(TCPAsyncServer):
	def __init__(self,server_address,databuffer, period = 0.1):
		TCPAsyncServer.__init__(self,server_address,None)
		self.databuffer = databuffer
		self.period = period

	def handle_request(self):
		try:
			is_rfds = self.socket_map.keys()
			rfds,wfds,efds = select.select(is_rfds,[],[],0.01)
			for fd in rfds:
				if fd == self.socket_fd:
					client_socket, client_address = self.get_request()
					print 'Connected by', client_address
					self.add_client_to_map(client_socket,client_address)

		except socket.error:
			pass
			
		if self.databuffer.hasNewData():
			buf = self.databuffer.getBuf()
			is_wfds = self.socket_map.keys()
			is_efds = []
			rfds,wfds,efds = select.select([],is_wfds,is_efds,0.05)
			for fd in wfds:
				socket_obj,addr = self.get_client_from_map(fd)
				try:
					socket_obj.send(buf)
				except:
					self.remove_client_from_map(fd)
		else:
			time.sleep(self.period)

class TCPRequestHandler(SocketServer.BaseRequestHandler):
	default_response = ":"
	maximum_data_len = MAXIMUM_DATA_LEN
	control_object = None

	def __init__(self,request,client_address,server):
		SocketServer.BaseRequestHandler.__init__(self,request,client_address,server)

	def handle(self):
		data ,addr = self.request.recvfrom(self.maximum_data_len)
		if len(data) == 0 and self.server.client_fd:
			self.remove_client_from_map()
		else:
			if self.control_object :
				response,msg = self.control_object.tcp_request_handle(data)
				if response :
					self.request.send(self.default_response)
				else :
					self.request.send(msg)
			else:
				self.default_handle(data)

	def remove_client_from_map(self):
		print 'Removing client on address = ',self.server.address_map[self.server.client_fd] 
		del self.server.socket_map[self.server.client_fd]
		del self.server.address_map[self.server.client_fd]
		self.server.client_fd = 0
		self.request.close()

	def default_handle(self,data):
		print 'default_handle() received data'
		self.request.send(self.default_response)

class ObsRequestHandler(SocketServer.BaseRequestHandler):
	default_response = ":"
	maximum_data_len = MAXIMUM_DATA_LEN
	control_object = None

	def __init__(self,request,client_address,server):
		SocketServer.BaseRequestHandler.__init__(self,request,client_address,server)

	def handle(self):
		data ,addr = self.request.recvfrom(self.maximum_data_len)
		if len(data) == 0 and self.server.client_fd:
			self.remove_client_from_map()
		else:
			if self.control_object :
				response,msg = self.control_object.tcp_request_handle(data)
				if response :
					self.request.send(self.default_response)
				else :
					self.request.send(msg)
			else:
				self.default_handle(data)

	def remove_client_from_map(self):
		print 'Removing client on address = ',self.server.address_map[self.server.client_fd] 
		del self.server.socket_map[self.server.client_fd]
		del self.server.address_map[self.server.client_fd]
		self.server.client_fd = 0
		self.request.close()

	def default_handle(self,data):
		print 'default_handle() received data'
		self.request.send(self.default_response)

class DataBufferBaseHandler(TCPRequestHandler):
	default_response = ":"
	maximum_data_len = MAXIMUM_DATA_LEN
	control_object = None
	def __init__(self,request,client_address,server):
		SocketServer.BaseRequestHandler.__init__(self,request,client_address,server)
	
def strip_carriage(buf):
	end = buf[-1:]
	if end in '\n\r':
		return buf[:-1]
	else: return  buf

def hangup_signal_handler(signo,frame):
	print "Catch hangup signal" 
	for fd, socket_obj in map.items():
		socket_obj.close()
	sys.exit(0)

def startServer(server_address, RequestHandler):
	serv = TCPAsyncServer(server_address, RequestHandler)
	serv.serve_forever()

class Client:
	maximum_data_len = MAXIMUM_DATA_LEN
	def __init__(self,timeout = 1.0):
		'''Create TCP/IP Client instance
		connect(address) method shoud be executed at once before 1st call of send() method.
		The control socket connection will remain open until close() method is called or server force the connection to close.
		'''
		self.__address_familly__ = socket.AF_INET 
		self.__socket_type__ = socket.SOCK_STREAM
		self.__control_port__ = 9998
		self.__monitor_port__ = 9999
		self.__aux_port__ = 10000
		self.__address__ = ("127.0.0.1",self.__control_port__)
		self.__is_connect__ = False
		self.__timeout__ = timeout
		self.cmd_terminator = ''
		self.verbose = False
#		try:
#			self.connect(self,self.address)
#		except:
#			print "Connection failed"

	def __del__(self):
		if hasattr(self,'__ctrl_socket__'):
			self.close()

	def connect(self,address):
		'''Connect server socket
		address is a tupple in form of  ("127.0.0.1", 9998)
		''' 
		try:
			self.__address__ = address
			self.__ctrl_socket__ = socket.socket(self.__address_familly__, self.__socket_type__)
			self.__ctrl_socket__.setsockopt(socket.SOL_SOCKET,socket.SO_SNDBUF,self.maximum_data_len)
			self.__ctrl_socket__.setsockopt(socket.SOL_SOCKET,socket.SO_RCVBUF,self.maximum_data_len)
			self.__ctrl_socket__.settimeout(self.__timeout__)
			self.__ctrl_socket__.connect(self.__address__)
#			self.ctrl_fd = self.ctrl_socket.fileno()
#			self.ctrl_f = self.ctrl_socket.makefile()
		except:
			self.__is_connect__ = False
		else:
			self.__is_connect__ = True

	def close(self):
		'''Close control socket connection
		'''
		self.__ctrl_socket__.close()
		self.__is_connect__ = False

	def shutdown(self):
		self.__ctrl_socket__.shutdown(2)

	def settimeout(self,to):
		self.__timeout__ = to
		self.__ctrl_socket__.settimeout(self.__timeout__)

	def gettimeout(self):
		return self.__ctrl_socket__.gettimeout()

	def send(self,buf,size=0):
		'''Send control command
		'''
		if not buf: return None
		if self.verbose: print buf

		if hasattr(self,'__ctrl_socket__'):
			if size == 0:
				self.__ctrl_socket__.send(buf)
			else:
				self.__ctrl_socket__.send(buf,size)

		self._sendbuf = buf


	def recv(self,size = MAXIMUM_DATA_LEN):
		if hasattr(self,'__ctrl_socket__'):
			rsp = ''
			for i in range(5):
#			while True:
				rsp += self.__ctrl_socket__.recv(size)
				if self.cmd_terminator == '':
					break
				if len(rsp) > 0 :
					if rsp[-1] == self.cmd_terminator:
						break
				else: break
			if self.verbose: print rsp
			self._recvbuf = rsp
		return rsp

	def is_connect(self):
		return self.__is_connect__

	def set_verbose(self): self.verbose = True
	def clr_verbose(self): self.verbose = False

class ClientLocked(Client):
	maximum_data_len = MAXIMUM_DATA_LEN
	def __init__(self,timeout = 1.0):
		Client.__init__(self,timeout = timeout)
		self.lock = threading.Lock()


	def send(self,buf,size=0):
		'''Send control command
		'''
		if not buf: return None
		if self.verbose: print buf

		if hasattr(self,'__ctrl_socket__'):
			self.lock.acquire()
			if size == 0:
				self.__ctrl_socket__.send(buf)
			else:
				self.__ctrl_socket__.send(buf,size)
		self._sendbuf = buf


	def recv(self,size = MAXIMUM_DATA_LEN):
		if hasattr(self,'__ctrl_socket__'):
			rsp = ''
			for i in range(5):
				rsp += self.__ctrl_socket__.recv(size)
				if self.cmd_terminator == '':
					break
				if len(rsp) > 0 :
					if rsp[-1] == self.cmd_terminator:
						break
				else: break

			self.lock.release()
			if self.verbose: print rsp
			self._recvbuf = rsp

			return rsp


if __name__== "__main__":
	import sys
	signal.signal(signal.SIGHUP,hangup_signal_handler)
	port = 9998
	if len(sys.argv) != 3:
		print >>sys.stderr, "Usage: %s IP_Address Port"%(sys.argv[0])
	else:
#		server_address = ("210.98.27.34",port)
		server_address = (sys.argv[1],int(sys.argv[2]))
		startServer(server_address,TCPRequestHandler)
