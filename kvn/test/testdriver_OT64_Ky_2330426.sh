#!/bin/bash

HOST=localhost

rm -rf /home/DAS_LOC/OBS/OT64_Ky_2330426

./sendCommand.py --host=$HOST PCMD,hello

./sendCommand.py --host=$HOST OBNM,OT64_Ky_2330426
./sendCommand.py --host=$HOST OBNM

./sendCommand.py --host=$HOST CONF,SEL_CORIPLEN=250
./sendCommand.py --host=$HOST CONF,SEL_CORLEN=1024

./sendCommand.py --host=$HOST CONF,SEL_CORWINDOW=NONE

./sendCommand.py --host=$HOST CONF,SEL_CORIPLEN?
./sendCommand.py --host=$HOST CONF,SEL_CORLEN?
./sendCommand.py --host=$HOST CONF,SEL_CORWINDOW?

./sendCommand.py --host=$HOST CONF,SEL_COROUTSTREAM=ALL:ON

./sendCommand.py --host=$HOST CONF,SEL_COROUTSTREAM=CROSS12:ON
./sendCommand.py --host=$HOST CONF,SEL_COROUTSTREAM=CROSS34:ON

./sendCommand.py --host=$HOST ACQD,NOSAVE
./sendCommand.py --host=$HOST ACQD

sleep 2

./sendCommand.py --host=$HOST STRT
./sendCommand.py --host=$HOST ACQD,SAVE

echo ./sendCommand.py --host=$HOST STOP
echo ./sendCommand.py --host=$HOST QUIT




