/////////////////////////////////////////////////////////////////////////////////////
//
// Windowing functions. Applied in-place to data.
//
// Uses cosf(x) (argument wrapping) or __cosf(x) (hardware intrinsic, no argument
// wrap, faster, lower precision). The CUDA C Programming Guide says about __cosf(x)
// that "For x in [-pi,pi], the maximum absolute error is 2^-21.19, and larger otherwise."
//
// Two ways to window the data:
// - without using cuFFT callback :
//     Step 1) windowing: load+multiply+store    if 300 GB/s RAM then ~37.5 Gfloat/sec max
//     Step 2) FFT : load, FFT, store            <37.5/2 Gfloat/sec i.e. <18.8 Gfloat/sec
// - using cuFFT callback :
//     Step 1) load + window + FFT + store       if 300 GB/s RAM and FFT speed neglected
//                                               the max <37.5 Gfloat/s i.e. twice faster!
//
// In case of WOLA (window & overlap & add) and without cuFFT callback the speed will be lower
// and memory usage higher (both proportial to the overlap factor).
//
// Using cuFFT callbacks requires 64-bit Linux + CUDA =>6.5 + compilation with flag -dc
//  + static linking via -lstatic_cufft + special license (free) from nVidia. If any
// of the requirements are not met, cuFFT callbacks may fail *silently*!
//
// (C) 2015 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef ARITHMETIC_WINFUNC_KERNELS_CU
#define ARITHMETIC_WINFUNC_KERNELS_CU

//#define TEST_CALLBACKS - // define to set window weights to 0.0f, thereby testing if Callbacks get invoked or fail silently
#ifdef TEST_CALLBACKS
    #pragma message "Enabled test whether or not cuFFT Callback are invoked. Window funcs set equal to 0.0f for test!"
#endif

#include <cufft.h>
#include <cufftXt.h>
#include "arithmetic_winfunc_kernels.h"

#ifndef M_2PI_f32
    #define M_2PI_f32 6.283185307179586f
#endif

#define m_cosf(x) __cosf(x)  // use cosf(x) or __cosf(x)

/**
 * Apply a Hann window to real-valued (not complex) input data.
 *
 * Hann window : w[n] = 0.5*(1 - cos(2pi*n/(N-1))) for n=0..N-1
 *
 * Performance is ~33 Gs/s on nVidia TITAN X.
 *
 * Speed is limited by memory bandwidth; for example 300 GB/s = 150 GB/s read
 * then 150 GB/s write = 37.5 Gsamp/s effective.
 */
__global__ void cu_window_hann(float4* inout, size_t fftlen_f4, size_t datalen_f4)
{
        size_t i_f4 = blockIdx.x * blockDim.x + threadIdx.x;
        float N = 4.0 * fftlen_f4;
        float n = 4.0 * (i_f4 % fftlen_f4);

        float omega = M_2PI_f32/(N-1);
        if (i_f4 < datalen_f4) {
                float4 v = inout[i_f4];
                float4 wv = {
                    v.x * (1.0f - m_cosf(omega*n)) / 2.0f,
                    v.y * (1.0f - m_cosf(omega*(n+1))) / 2.0f,
                    v.z * (1.0f - m_cosf(omega*(n+2))) / 2.0f,
                    v.w * (1.0f - m_cosf(omega*(n+3))) / 2.0f
                };
                inout[i_f4] = wv;
        }
}

/**
 * Apply a Hamming window to real-valued (not complex) input data.
 *
 * Hamming window : w[n] = 0.54 - 0.46*cos(2pi*n/(N-1))) for n=0..N-1
 *
 * Performance is ~33 Gs/s on nVidia TITAN X.
 */
__global__ void cu_window_hamming(float4* inout, size_t fftlen_f4, size_t datalen_f4)
{
        size_t i_f4 = blockIdx.x * blockDim.x + threadIdx.x;
        float N = 4.0 * fftlen_f4;
        float n = 4.0 * (i_f4 % fftlen_f4);

        float omega = M_2PI_f32/(N-1);
        if (i_f4 < datalen_f4) {
                float4 v = inout[i_f4];
                float4 wv = {
                    v.x * (0.54f - 0.46f*m_cosf(omega*n)),
                    v.y * (0.54f - 0.46f*m_cosf(omega*(n+1))),
                    v.z * (0.54f - 0.46f*m_cosf(omega*(n+2))),
                    v.w * (0.54f - 0.46f*m_cosf(omega*(n+3)))
                };
                inout[i_f4] = wv;
        }
}

//////////////////////////////////////////////////////////////////////////////////
//
// Use cuFFT Load callback (type: CUFFT_CB_LD_REAL)
//
// See http://docs.nvidia.com/cuda/cufft/#callback-parameters
// Also see CUDA example simpleCUFFT_callback.cu

__device__ cufftReal cu_window_hann_cufftCallbackLoadR(void *dataIn, size_t offset, void *callerInfo, void *sharedPointer)
{
        cu_window_cb_params_t* my_params = (cu_window_cb_params_t*)callerInfo;
        size_t    n     = offset % my_params->fftlen;
        cufftReal omega = M_2PI_f32/(my_params->fftlen-1);
        cufftReal w     = (1.0f - m_cosf(omega*n)) / 2.0f;
#ifdef TEST_CALLBACKS
        w = 0.0; // for testing whether Callbacks fail silently or if they actually do get invoked
#endif
        return w * ((cufftReal*)dataIn)[offset];
}

__device__ cufftReal cu_window_hamming_cufftCallbackLoadR(void *dataIn, size_t offset, void *callerInfo, void *sharedPointer)
{
        cu_window_cb_params_t* my_params = (cu_window_cb_params_t*)callerInfo;
        size_t    n     = offset % my_params->fftlen;
        cufftReal omega = M_2PI_f32/(my_params->fftlen-1);
        cufftReal w     = 0.54f - 0.46f*m_cosf(omega*n);
#ifdef TEST_CALLBACKS
        w = 0.0; // for testing whether Callbacks fail silently or if they actually do get invoked
#endif
        return w * ((cufftReal*)dataIn)[offset];
}

__device__ cufftCallbackLoadR cu_window_hann_cufftCallbackLoadR_ptr = cu_window_hann_cufftCallbackLoadR;
__device__ cufftCallbackLoadR cu_window_hamming_cufftCallbackLoadR_ptr = cu_window_hamming_cufftCallbackLoadR;

#endif // ARITHMETIC_WINFUNC_KERNELS_CU
