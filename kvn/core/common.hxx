
class SpectrometerTCPServer;
class SpectralSettings;
class ProcessingSettings;
class SpectrometerMonCtrl;
class SpectrometerInterface;

#include "settings/spectralsettings.hxx"     // class SpectralSettings
#include "settings/processingsettings.hxx"   // class ProcessingSettings
#include "core/spectrometerconfig.hxx"       // class SpectrometerConfig with SpectralSettings, ProcessingSettings
#include "network/spectrometertcpserver.hxx" // class SpectrometerTCPServer
#include "kvnmc2api/spectrometermonctrl.hxx" // class SpectrometerMonCtrl
#include "core/spectrometerinterface.hxx"    // class SpectrometerInterface

#ifndef lockscope
    #define lockscope // just a blank define use for nicer syntax of "{ boost::mutex::scoped_lock x(mtx); ... }" scopes
#endif
