#include "logger.h"
#include "core/defines.hxx"
#include "core/common.hxx"
#include "core/backend_cpu.hxx"
#include "datarecipients/datarecipient.hxx"
#include "datarecipients/resultrecipient.hxx"

#include <assert.h>
#include <getopt.h>
#include <malloc.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <fftw3.h>

BackendCPU::BackendCPU()
{
}

BackendCPU::~BackendCPU()
{
}

bool BackendCPU::optimizeConfiguration(SpectrometerConfig* cfg)
{
    return true;
}

bool BackendCPU::estimateMemoryRequirement(const SpectrometerConfig* cfg, size_t* hostbytes, size_t* devicebytes) const
{
    *hostbytes = 0;
    *devicebytes = 0;
    return true;
}

int BackendCPU::getDeviceCount()
{
    return 1;
}

bool BackendCPU::start(SpectrometerConfig* cfg)
{
    return true;
}

bool BackendCPU::reset()
{
    return true;
}

bool BackendCPU::stop()
{
    return true;
}

void BackendCPU::takeData(RawDataBuffer* data)
{
}

void BackendCPU::setOutput(ResultRecipient* r)
{
}

void BackendCPU::registerUserDataBuffers(int Nbuffers, void** buffers, size_t buflen)
{
}

void BackendCPU::unregisterUserDataBuffers(int Nbuffers, void** buffers, size_t buflen)
{
}

