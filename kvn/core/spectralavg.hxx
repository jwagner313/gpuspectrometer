#ifndef SPECTRALAVG_HXX
#define SPECTRALAVG_HXX

class SpectralAverager {
    public:
        SpectralAverager() {}
    public:
        void spectralAvgAuto(const float* in, float* out, int nchan, int step);
        void spectralAvgCross(const float* in, float* out, int nchan, int step);
        void spectralRescale(float* d, int nchan, const float factor);
};

#endif // SPECTRALAVG_HXX
