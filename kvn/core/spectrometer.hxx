#ifndef SPECTROMETER_HXX
#define SPECTROMETER_HXX

#include "core/common.hxx"
#include "core/spectrometerinterface.hxx"
#include "core/backendinterface.hxx"
#include "network/udpvdifreceiver.hxx"
#include "network/udpvdifreceiver_sim.hxx"
#include "datarecipients/spectraloutput.hxx"
#include "datarecipients/commandrecipient.hxx"

#include <boost/thread/mutex.hpp>

#include <string>

class Spectrometer : public SpectrometerInterface
{
    friend class SpectrometerTCPServer;

    private:
        Spectrometer();

    public:
        /** C'stor with TCP/UDP port numbers to listen on */
        Spectrometer(int cmdPort, int monPort, int outputPort, int vdifPort);

        /** Method that starts the spectrometer M&C listener loop (default: non-blocking, background) */
        void startMC(bool blocking=false);

        /** Stop the M&C listener loop; dummy call for now */
        void stopMC() { }

        /** Handle a command string and return a response string. */
        std::string handleCommand(std::string cmd); // impl. CommandRecipient::handleCommand() iface

    public:
        /** Set/change UDP port **/
        void setVDIFPort(int vdifPort) { m_udpPort = vdifPort; }

        /** Set/change VDIF frame offset (PSN length in bytes) **/
        void setVDIFOffset(int vdifOffset) { m_vdifOffset = vdifOffset; }

        /** Start background VDIF data receiption pump */
        bool startRX();

        /** Stop background VDIF data receiption pump */
        bool stopRX();

    public:
        /**
         * Start GPU processing.
         * The current spectral settings are copied into "active" settings
         * and spectral processing is initialized and started with that copy.
         * Note that active settings can not be changed on the fly, i.e.,
         * settings changed while GPU processing is already ongoing will
         * become activated only after stopRX() followed by startRX().
         */
        bool startGPU();

        /** Stop GPU processing */
        bool stopGPU();

        /** Underallocation of GPU memory to allow multiple instances without OutOfMemory */
        void setUnderallocationFactor(int factor) {
            if (factor >= 1 && factor <= 512) {
                m_cfg.pcfg.underallocfactor = factor;
            }
        }

        // TODO
        bool startProcessing() { return true; }
        bool stopProcessing() { return true; }

        /** Enable or disable spectral output to a file. Output to TCP clients is not affected. */
        void enableFileOutput(bool enable);

   public:
        /** Direct read/write accessor to spectral settings */
        SpectralSettings& spectralCfg();

        /** Direct read/write accessor to processing settings */
        ProcessingSettings& processingCfg();

    private:
        /** Pin or un-pin memory buffers of the UDPVDIFReceiver backend (GPU) */
        void mempinVDIFBuffers(bool pin);

    private:
        boost::mutex m_cmdmutex;
        SpectrometerMonCtrl m_mciface;
        SpectrometerTCPServer m_ctrlListener;
        SpectrometerTCPServer m_monitorListener;

        SpectralOutput m_outputHandler;

        Backend* m_backend;

        UDPVDIFReceiver m_vdifRx;
        //UDPVDIFReceiverSim m_vdifRx;
        int m_udpPort;
        int m_vdifOffset;
        bool m_rx_started;

        int m_gpuMemoryUnderallocationFactor;

    private:
        boost::mutex m_cfgmutex; // for safely copying m_cfg --> m_cfg_active
        SpectrometerConfig m_cfg;
        SpectrometerConfig m_cfg_active;

};

#endif // SPECTROMETER_HXX
