#ifndef SPECTROMETERCONFIG_HXX
#define SPECTROMETERCONFIG_HXX

#include "settings/spectralsettings.hxx"
#include "settings/processingsettings.hxx"
#include "network/vdifdetails.hxx"

#include <ostream>

class SpectrometerConfig {

    friend std::ostream& operator<<(std::ostream& os, const SpectrometerConfig& c);

    public:
        /** Calculate processing settings based on spectral settings and VDIF details */
        void recalculate();

    public:
        SpectralSettings scfg;
        ProcessingSettings pcfg;
        VDIFDetails vdifinfo;

};

std::ostream& operator<<(std::ostream& os, const SpectrometerConfig& c);

#endif // SPECTROMETERCONFIG_HXX
