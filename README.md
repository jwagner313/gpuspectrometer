
 Multi-GPU Spectometers
 for KVN, ALMA / ASTE / Nobeyama, DBBC3

## Intro ######################################################################

This repository contains a work in progress towards (multi-)GPU spectrometers
with file, network UDP/IP, or data acquisiton card input input. Currently they
support FFT modes with or without windowing.

1) KVN spectrometer 

The input data are expected to be in VDIF format, the output are *.kfftspec
files in custom format or KVN DSM -like files. The M&C interface follows
KVN DSM command specifications.

2) ALMA and telescopes with ALMA-like samplers (Nobeyama, ASTE)

The input data are acquired not over the network but over 3rd party
custom optical data receiver boards ("DRXP" maded by Elecs Ltd). The
output spectra are in a special MIME encapsulated format.
The M&C interface is XML-based and is described elsewhere.

3) DBBC3 quick-look spectrometer

The DBBC3 spectrometer accepts VDIF data. This quick-look spectrometer is
for firmware development/testing purposes, not for observations unlike the
other spectrometers. The implementation is still incomplete.

## Installing #################################################################

Get a copy of the source code

  https://bitbucket.org/jwagner313/gpuspectrometer/downloads

or preferrably check out the 'git' repository 

  $ git clone https://bitbucket.org/jwagner313/gpuspectrometer.git

Dependencies:  boost, xerces-c

  $ sudo aptitude install boost-devel xerces-c-devel

To compile and install:

$ nano Makefile  # check that CUDA_DIR points to your CUDA installation

$ make -j

$ make install

