/**
 * CUDA Hardware Self-Test Class:
 * 1) memory check (https://sourceforge.net/projects/cudagpumemtest/)
 * 2) kernel launch check,
 * 3) read out temperatures(?) and ECC error counts
 *
 * ALMA:
 * - allow up to 15 minutes total for server booting, services start-up, self-test.
 * - self-test has no particular requirements, in some cases sufficient to check hardware
 *   functions without checking software functions
 */

#include "SelfTestGPU.h"

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>
#include <iostream>

#include <cuda.h>

// Include CUDA GPU Memtest; unfortunately the authors didn't make it into a portable library!
#include <pthread.h>
#include "SelfTestGPU_memkernels.cu"

//////////////////////////////////////////////////////////////////////////////////////

SelfTestGPU::SelfTestGPU(SelfTestGPULevel level=IMMEDIATE, int nGPU_expected=0)
{
    m_nGPU = 0;
    m_level = level;
    m_result = NOT_COMPLETED;

    cudaGetDeviceCount(&m_nGPU);
    if (m_nGPU <= 0) {
        std::cerr << "SelfTestGPU: Error, no GPU found!" << std::endl;
    }
    if ((nGPU_expected > 0) && (m_nGPU != nGPU_expected)) {
        std::cerr << "SelfTestGPU: Warning, only " << m_nGPU << " of " << nGPU_expected << " expected GPU found!" << std::endl;
    }

    m_nworkers = m_nGPU;
    if (m_nworkers >= (int)(sizeof(m_workers)/sizeof(m_workers[0]))) {
        m_nworkers = (int)(sizeof(m_workers)/sizeof(m_workers[0]));
    }
}

void SelfTestGPU::worker(int deviceNr)
{
     struct cudaDeviceProp dp;
     cudaGetDeviceProperties(&dp, deviceNr);

     std::cout << "SelfTestGPU: Device nr=" << deviceNr << " name=" << dp.name << " size=" << dp.totalGlobalMem << std::endl;

     cudaSetDevice(deviceNr);
     cudaDeviceSynchronize();

     size_t mem_free, mem_total;
     cudaMemGetInfo(&mem_free, &mem_total);

     return;
}

void SelfTestGPU::start()
{
    for (int i = 0; i < m_nworkers; i++) {
        m_workers[i] = new boost::thread(&SelfTestGPU::worker, this, i);
    }
}

void SelfTestGPU::interrupt()
{
    for (int i = 0; i < m_nworkers; i++) {
        m_workers[i]->interrupt();
    }
}

void SelfTestGPU::waitStop()
{
    for (int i = 0; i < m_nworkers; i++) {
        m_workers[i]->join();
    }
}


#ifdef STANDALONE

int main(void)
{
    SelfTestGPU s(SelfTestGPU::IMMEDIATE, 4);
    s.start();
    s.waitStop();
    return 0;
}

#endif

