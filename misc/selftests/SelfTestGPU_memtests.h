#ifndef SELFTEST_GPU_MEMTESTS_H
#define SELFTEST_GPU_MEMTESTS_H

/** Wrapper class for tests in https://sourceforge.net/projects/cudagpumemtest/ file tests.cu */
class SelfTestGPUMemTests
{
    public:
        SelfTestGPUMemTests(void *d_testArea, int num_blocks);
        ~SelfTestGPUMemTests();

    public:
        // Tests, copied from https://sourceforge.net/projects/cudagpumemtest/ file tests.cu
        bool test0();
        bool test1();
        bool test2();
        bool test3();
        bool test4();
        bool test5();
        bool test6();
        bool test7();

    private:
        // Helpers, copied from https://sourceforge.net/projects/cudagpumemtest/ file tests.cu
        unsigned int move_inv_test(char* ptr, unsigned int tot_num_blocks, unsigned int p1, unsigned p2);
        unsigned int get_random_num(void);
        unsigned long get_random_num_long(void);

        void SHOW_PROGRESS(const char*, size_t, size_t);
        bool error_checking(const char*, size_t);

    private:
        char *m_d_testArea;
        char *m_d_testArea_end;
        int m_num_blocks;
        int m_num_iterations;
        unsigned int m_error_count;
};

#endif // SELFTEST_GPU_MEMTESTS_H
