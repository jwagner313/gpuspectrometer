#ifndef SELFTEST_GPU_MEMDEFS_H
#define SELFTEST_GPU_MEMDEFS_H

// Some silly defines expected by https://sourceforge.net/projects/cudagpumemtest/ code test.cu
#ifndef GPU_MEMTEST_BLOCKSIZE
#define GPU_MEMTEST_BLOCKSIZE ((unsigned long)1024*1024)
#endif

#ifndef GPU_MEMTEST_GRIDSIZE
#define GPU_MEMTEST_GRIDSIZE 128
#endif

#endif // SELFTEST_GPU_MEMDEFS_H
