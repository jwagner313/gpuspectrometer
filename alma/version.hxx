#ifndef ASM_VERSION_HXX
#define ASM_VERSION_HXX

#define ASM_VERSION_IFACE    1  // incremented when an interface to the outside world is changed, e.g., command format, data format, timing, restrictions etc
#define ASM_VERSION_FUNCTION 1  // incremented (1..inf) when the function of ACA Spectrometer Module program is changed, e.g., improving the calculation accuracy etc
#define ASM_VERSION_DEBUG    1  // incremented (1..inf) when a known bug is fixed after a new version of ACA Spectrometer Module program is released
#define ASM_VERSION_REFACTOR 1  // incremented (1..inf) when the internal structure of hardware or software is changed with no change in the interface or in the function

#define VER_STRINGIZER(a) #a
#define VER_2STR(a) VER_STRINGIZER(a)

#define ASM_VERSION_STR VER_2STR(ASM_VERSION_IFACE) "." VER_2STR(ASM_VERSION_FUNCTION) "." VER_2STR(ASM_VERSION_DEBUG) "." VER_2STR(ASM_VERSION_REFACTOR)

#endif // ASM_VERSION_HXX

