#include "CalibNonlinearity.hxx"

#include <iostream>
#include <iomanip>

#include <fstream>
#include "kernels/histogram_kernels.h" // definition of GPU kernel hist_t (currently 'unsigned int')

//#include <random> //if C++ 2011 available, otherwise sampleNormal() :
#include <math.h>
#include <stdlib.h>

float sampleNormal()
{
    float u = ((float) rand() / (RAND_MAX)) * 2 - 1;
    float v = ((float) rand() / (RAND_MAX)) * 2 - 1;
    float r = u*u + v*v;
    if ((r == 0) || (r > 1)) {
        return sampleNormal();
    }
    float c = sqrt(-2 * log(r) / r);
    return u*c;
}

float quant8lev(float x)
{
    // Quantize to -7.0, -5.0, -3.0, -1.0, 1.0, 3.0, 5.0, 7.0
    x = 2.0f * int(0.5f + (x + 7.0f) / 2.0f) - 7.0f;
    if (x < -7.0f) { x = -7.0f; }
    if (x > +7.0f) { x = +7.0f; }
    return x;
}

void hist8lev(float* x, hist_t* h, int N)
{
    for (int b=0; b<8; b++) {
        h[b] = 0;
    }
    for (int n=0; n<N; n++) {
        int b = int((x[n] + 7.0f) / 2.0f);
        if (b < 0) { b = 0; }
        if (b > 7) { b = 7; }
        h[b]++;
    }
}

void hist2stat(hist_t* h, float* mean, float* std)
{
    const float bincenters[8] = {-7.0f, -5.0f, -3.0f, -1.0f, 1.0f, 3.0f, 5.0f, 7.0f };
    float Nsamp = 0;
    float wsum = 0, wd2sum = 0;
    for (int n=0; n<8; n++) {
       Nsamp += h[n];
       wsum = h[n] * bincenters[n];
    }
    *mean = wsum / Nsamp;
    for (int n=0; n<8; n++) {
       float delta = bincenters[n] - (*mean);
       wd2sum += h[n] * delta*delta;
    }
    *std = sqrtf(wd2sum / Nsamp);
}

template<class Thist_t, class Tac_t>
void CalibratorNonLinearity<Thist_t,Tac_t>::selftest()
{
    std::cout << "CalibratorNonLinearity::selftest() : checking that spline evaluator for Q_{3L}^{2-} produces reference values of Table 7.5\n";
    for (int n=0; n<89; n++) {
        double factor_fitted = Q_3L(m_mRef[n][0]);
        double diff = fabs(m_mRef[n][1] - factor_fitted);
        std::cout << std::setw(4) << n << " std3_out^2=" << std::fixed << std::setprecision(2) << m_mRef[n][0]
                  << " corr_fact. expected " << std::setprecision(6) << m_mRef[n][1] << " vs. got " << factor_fitted
                  << ", diff=" << std::scientific << std::setprecision(2) << diff << "\n";
    }

    std::cout << "CalibratorNonLinearity::selftest() : checking that spline evaluator for Q_{3Li}^{2-} produces reference values of Table 7.7\n";
    for (int n=0; n<126; n++) {
        double factor_fitted = Q_3Li(m_minvRef[n][0]);
        double diff = fabs(m_minvRef[n][1] - factor_fitted);
        std::cout << std::setw(4) << n << " std3_in^2=" << std::fixed << std::setprecision(2) << m_minvRef[n][0]
                  << " corr_fact. expected " << std::setprecision(6) << m_minvRef[n][1] << " vs. got " << factor_fitted
                  << ", diff=" << std::scientific << std::setprecision(2) << diff << "\n";
    }
}

int main(int argc, char** argv)
{
    const int N = 512*1024;
    const int nlev = 8;
    float *xx = new float[N];   // raw data
    float *XX = new float[N];   // some spectrum, not really from the raw data
    float *XX_C = new float[N]; // calibrated version of spectrum 'XX'
    float x_mean, x_std;

    CalibratorNonLinearity<hist_t,float> nonLinCorr(nlev);

    hist_t h[nlev] = { 11, 101, 438, 912, 912, 438, 101, 11 }; // round(exp(-[-7,-5,-3,-1,1,3,5,7].^2/(2*(7/3)^2)) * 1000)
    hist_t xx_h[nlev]; // histogram of actual data

    // Run an internal table vs. result comparison test (manual by-eye inspection!)
    nonLinCorr.selftest();

    // Generate time-domain data and histogram of it
    for (int n=0; n<N; n++) {
        xx[n] = sampleNormal() * 7.0f/3.0f;
        xx[n] = quant8lev(xx[n]);
    }
    int levs[nlev] = { -7,-5,-3,-1,1,3,5,7 };
    hist8lev(xx, xx_h, N);
    hist2stat(xx_h, &x_mean, &x_std);
    for (int b=0; b<nlev; b++) {
        std::cout << "bin " << b << " : " << levs[b] << " : " << xx_h[b] << " " << h[b] << "\n";
    }
    std::cout << "histogram: mean=" << x_mean << " std=" << x_std << "\n";

    // Generate a spectrum
    //std::mt19937 generator;
    //std::normal_distribution<float> normal(0.0f, 1.0f);
    //for (int n=0; n<N; n++) {
    //    XX[n] = normal(generator);
    //}
    for (int n=0; n<N; n++) {
        XX[n] = fabs(sampleNormal()) * sqrtf(N/2.0) * x_std;
    }

    // Calibrate the spectrum
    //nonLinCorr.setHistogram(h);
    nonLinCorr.setHistogram(xx_h);
    nonLinCorr.calibrateAutocorr(XX,XX_C,N);

    std::ofstream dump("calib_nonlinearity.txt");
    dump << "## Columns: 1st = spectral channel <n>, 2nd = magnitude in channel <n>, 3rd = magnitude in channel <n> after non-linearity correction\n";
    for (int n=0; n<N; n++) {
        dump << std::setw(7) << n << " " << std::fixed << std::setprecision(6) << std::setw(15) << XX[n] << " " << std::setw(15) << XX_C[n] << "\n";
    }
    dump.close();
    std::cout << "Wrote input and output data into 'calib_nonlinearity.txt'\n";

    return 0;
}
