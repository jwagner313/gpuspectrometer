/**
 * \class MCConfiguration
 *
 * \brief Settings as per M&C configuration
 *
 * Contains settings from M&C (as defined in the ICD). The settings
 * need to be mapped elsewhere into* a GPU spectrometer configuration.
 * The class does not check for valid ranges or values of the settings;
 * it is up to the GPU spectrometer to determine what settings are valid.
 *
 * \author $Author: janw $
 *
 */
#ifndef MC_CONFIGURATION_HXX
#define MC_CONFIGURATION_HXX

#include "xsd_datatypes/xsd_datatypes.hxx"
#include "xsd_datatypes/derived_procconfig_t.hxx"

#include <time.h>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/property_tree/ptree.hpp>
#include <string>
#include <vector>
#include <iostream>

class MCObservation;

class MCConfiguration
{
    public:
        /** C'stor, defaults */
        MCConfiguration();

        /** C'stor, fills details from given Property Tree */
        MCConfiguration(const boost::property_tree::ptree& pt);


    public:
        /** Assignment op, needed as an explicit implementation since contained mutex is non-copyable */
        MCConfiguration& operator=(const MCConfiguration& rhs);

        /** Copy op, needed as an explicit implementation since contained mutex is non-copyable */
        MCConfiguration(const MCConfiguration& rhs);

    public:
        /** Fills own details from given Property Tree */
        bool getFrom(const boost::property_tree::ptree& pt);

        /** Writes details into given Property Tree */
        bool storeInto(boost::property_tree::ptree& pt) const;

        /** Set M&C command ID, used internally */
        void setId(unsigned int id);

        /** Return M&C command ID */
        unsigned int getId() const { return configId; }

        /** Return DFT length */
        unsigned int getDFTLength() const;

        /** Return a input sample stepping for a N-point FFT that leads to integer nr# of FFTs in 1 msec */
        unsigned int determineOverlappedFFTStep(unsigned int Lfft) const;

        /** Return shortest dwell time in 'switching' setup */
        float getMinDwellSeconds() const;

        /** Return longest dwell time in 'switching' setup */
        float getMaxDwellSeconds() const;

        /** Return the number of polarization products needed by the configured spectral window(s) */
        int getNStokes() const;

        /** Update some of the internal parameters derived from the XML 'singleConfiguration' */
        void updateDerivedParams();

        /** Update some of the internal parameters derived from the XML 'singleConfiguration' */
        void updateDerivedParamsFrom(const MCObservation*);

    public:
        /** Create a 1 spectral window config for testing */
        void createTestconfig1();

        /** Create a 2 spectral window config for testing */
        void createTestconfig2();

    private:
        /** Check that the config is valid and consistent */
        bool invariant() const;

    public:
        /** \return True if contained properties are valid */
        bool valid() const { return invariant(); }

    public:
        /** \return Descriptive string for given polarization product */
        std::string polEnum2str(const XSD::polnProduct_t t) const;

        /** \return Descriptive string for given window function */
        std::string winfunEnum2str(const XSD::windowFunction_t t) const;

        /** \return Value that represents given polarization product string (see ICD for valid strings) */
        XSD::polnProduct_t polStr2enum(const std::string& v) const;

        /** \return Value that represents given window function string (see ICD for valid strings ) */
        XSD::windowFunction_t winfunStr2enum(const std::string& v) const;

    public:
        /** \return A lookup vector, useful for translating from switching positions (ID; any numbers) to contiguous numbered index '0..(npos-1)' */
        int getSwitchingPositionsLookup(std::vector<unsigned int>& lut) const;

        /** \return The number of switching positions */
        int getNumSwitchingPositions() const;

    public:
        mutable boost::recursive_mutex mutex;
        mutable bool is_valid;                  /// true if config is valid
        unsigned int configId;                  /// running ID of configuration, see ICD for details

        XSD::singleConfiguration_t singleConfiguration; /// container for subArray_t, timeSpec_t, basebandConfig_t
        procConfig_t procConfig;                /// active processing-related details of this MCConfiguration derived from timeSpec, basebandConfig, others

    private:
        friend std::ostream& operator<<(std::ostream&, const MCConfiguration&);
};

extern std::ostream& operator<<(std::ostream &strm, const MCConfiguration &a);

#endif
