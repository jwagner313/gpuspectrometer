/**
 * \class MCConfiguration
 *
 * \brief Settings as per M&C configuration
 *
 * Contains settings from M&C (as defined in the ICD). The settings
 * need to be mapped elsewhere into* a GPU spectrometer configuration.
 * The class does not check for valid ranges or values of the settings;
 * it is up to the GPU spectrometer to determine what settings are valid.
 *
 * \author $Author: janw $
 *
 */

#include "mcwrapping/mc_configuration.hxx"
#include "mcwrapping/mc_observation.hxx"
#include "helper/logger.h"
#include "global_defs.hxx"

#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/mutex.hpp>
#include <cmath>
#include <string>
#include <vector>

using namespace XSD;

template<class Ptree> inline const Ptree &empty_ptree() {
    static Ptree pt;
    return pt;
}

/** C'stor, defaults */
MCConfiguration::MCConfiguration() : is_valid(false)
{
    configId = -1;
    invariant();
}

/** C'stor, fills details from given Property Tree */
MCConfiguration::MCConfiguration(const boost::property_tree::ptree& pt)
{
    getFrom(pt);
}

/** Assignment op, needed as an explicit implementation since contained mutex is non-copyable */
MCConfiguration& MCConfiguration::operator=(const MCConfiguration& rhs)
{
    boost::unique_lock<boost::recursive_mutex> lock1(rhs.mutex);
    boost::unique_lock<boost::recursive_mutex> lock2(mutex);
    is_valid = rhs.is_valid;
    configId = rhs.configId;
    singleConfiguration = rhs.singleConfiguration;
    procConfig = rhs.procConfig;
    return *this;
}

/** Copy op, needed as an explicit implementation since contained mutex is non-copyable */
MCConfiguration::MCConfiguration(const MCConfiguration& rhs)
{
    boost::unique_lock<boost::recursive_mutex> lock1(rhs.mutex);
    boost::unique_lock<boost::recursive_mutex> lock2(mutex);
    is_valid = rhs.is_valid;
    configId = rhs.configId;
    singleConfiguration = rhs.singleConfiguration;
    procConfig = rhs.procConfig;
}


/** Load configuration from XML (Boost PropertyTree). */
bool MCConfiguration::getFrom(const boost::property_tree::ptree& pt)
{
    using boost::property_tree::ptree;
    boost::unique_lock<boost::recursive_mutex> lock(mutex);

    singleConfiguration.subArray.name = pt.get("subArray.name", "");
    singleConfiguration.subArray.setCorrMode(pt.get("subArray.corrMode", ""));

    singleConfiguration.timeSpec.switching.clear();
    singleConfiguration.basebandConfig.spectralWindow.clear();

    timeSpec_t& timeSpec = singleConfiguration.timeSpec;
    timeSpec.integrationDuration = pt.get("timeSpec.integrationDuration", 0.0);
    timeSpec.channelAverageDuration = pt.get("timeSpec.channelAverageDuration", 0.0);
    BOOST_FOREACH(const ptree::value_type &v, pt.get_child("timeSpec")) {

       if (v.first != "switching") { continue; }

       const ptree& p = v.second;
       switching_t s;

       s.position = p.get("position", 0);
       s.dwell_sec = p.get("dwell", -1.0);
       s.transition_sec = p.get("transition", -1.0);
       timeSpec.switching.push_back(s);

    }

    basebandConfig_t& basebandConfig = singleConfiguration.basebandConfig;
    basebandConfig.sidebandSuppression = pt.get("basebandConfig.sidebandSuppression", "None");
    basebandConfig.spectralWindow.clear();
    BOOST_FOREACH(const ptree::value_type &v, pt.get_child("basebandConfig")) {

       if (v.first != "spectralWindow") { continue; }

       const ptree& p = v.second;
       const ptree& attr = p.get_child("<xmlattr>");

       spectralWindow_t w;
       w.polnProducts = polStr2enum(attr.get("polnProducts", ""));
       w.centerFrequency = p.get("centerFrequency", -1.0);
       w.spectralAveragingFactor = p.get("spectralAveragingFactor", 0);
       w.name = p.get("name", "");
       w.effectiveBandwidth = p.get("effectiveBandwidth", 0.0);
       w.effectiveNumberOfChannels = p.get("effectiveNumberOfChannels", 0);
       w.quantizationCorrection = p.get("quantizationCorrection", true);

       const std::string& swfName = p.get("simulatedWindowFunction", "");
       const std::string& wfName = p.get("windowFunction", "");
       if (swfName != "") {
           // 'simulatedWindowFunction': optional/choice, and has no 'overlap' attribute
           w.simulatedWindowFunction = winfunStr2enum(swfName);
           w.simulatedWindowFunction_chosen = true;
       }
       if (wfName != "") {
           // 'windowFunction': optional/choice, has an 'overlap' attribute
           w.windowOverlap = p.get_child("windowFunction.<xmlattr>", empty_ptree<ptree>()).get("overlap", 0.0);
           w.windowFunction = winfunStr2enum(wfName);
           w.windowFunction_chosen = true;
       }

       BOOST_FOREACH(const ptree::value_type &vs, p.get_child("")) {

           if (vs.first != "channelAverageRegion") { continue; }

           const ptree& ps = vs.second;
           channelAverageRegion_t cr;

           cr.startChannel = ps.get("startChannel", -1);
           cr.numberChannels = ps.get("numberChannels", -1);
           unsigned int lastChannel = cr.startChannel + cr.numberChannels - 1;
           if (!((0 <= cr.startChannel) && (cr.startChannel < w.effectiveNumberOfChannels))) {
               throw std::range_error("bad entry for channelAverageRegion : startChannel outside range");
           }
           if ((cr.numberChannels < 0) || (lastChannel >= w.effectiveNumberOfChannels)) {
               throw std::range_error("bad entry for channelAverageRegion");
           }
           w.channelAverageRegion.push_back(cr);
       }

       basebandConfig.spectralWindow.push_back(w);
    }

    updateDerivedParams();

    return invariant();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Store contents into XML (Boost PropertyTree). */
bool MCConfiguration::storeInto(boost::property_tree::ptree& pt) const
{
    using boost::property_tree::ptree;
    boost::unique_lock<boost::recursive_mutex> lock(mutex);

    // if(!is_valid) { return false; }

    pt.put("subArray.name", singleConfiguration.subArray.name);
    pt.put("subArray.corrMode", singleConfiguration.subArray.getCorrMode());

    const timeSpec_t& timeSpec = singleConfiguration.timeSpec;
    do {
        std::vector<switching_t>::const_iterator it = timeSpec.switching.begin();
        std::vector<switching_t>::const_iterator end = timeSpec.switching.end();
        for ( ; it != end; it++) {
            ptree psub;
            psub.put("position", (*it).position);
            psub.put("dwell", (*it).dwell_sec);
            psub.put("transition", (*it).transition_sec);
            psub.put("dwell.<xmlattr>.unit", "s");
            psub.put("transition.<xmlattr>.unit", "s");
            pt.add_child("timeSpec.switching", psub);
        }
        pt.put("timeSpec.integrationDuration", timeSpec.integrationDuration);
        pt.put("timeSpec.channelAverageDuration", timeSpec.channelAverageDuration);
        pt.put("timeSpec.integrationDuration.<xmlattr>.unit", "s");
        pt.put("timeSpec.channelAverageDuration.<xmlattr>.unit", "s");
    } while(0);

    const basebandConfig_t& basebandConfig = singleConfiguration.basebandConfig;
    pt.put("basebandConfig.sidebandSuppression", basebandConfig.sidebandSuppression);
    do {
        std::vector<spectralWindow_t>::const_iterator it = basebandConfig.spectralWindow.begin();
        std::vector<spectralWindow_t>::const_iterator end = basebandConfig.spectralWindow.end();
        for ( ; it != end; it++) {
            ptree psub;
            psub.put("<xmlattr>.polnProducts", polEnum2str((*it).polnProducts));
            psub.put("centerFrequency", (*it).centerFrequency);
            psub.put("spectralAveragingFactor", (*it).spectralAveragingFactor);
            psub.put("name", (*it).name);
            psub.put("effectiveBandwidth", (*it).effectiveBandwidth);
            psub.put("effectiveNumberOfChannels", (*it).effectiveNumberOfChannels);
            psub.put("quantizationCorrection", (*it).quantizationCorrection);
            std::vector<channelAverageRegion_t>::const_iterator it2 = (*it).channelAverageRegion.begin();
            std::vector<channelAverageRegion_t>::const_iterator end2 = (*it).channelAverageRegion.end();
            for ( ; it2 != end2; it2++) {
                ptree psub2;
                psub2.put("startChannel", (*it2).startChannel);
                psub2.put("numberChannels", (*it2).numberChannels);
                psub.add_child("channelAverageRegion", psub2);
            }
            if ((*it).windowFunction_chosen) {
                psub.put("windowFunction", winfunEnum2str((*it).windowFunction));
                psub.put("windowFunction.<xmlattr>.overlap", (*it).windowOverlap);
            }
            if ((*it).simulatedWindowFunction_chosen) {
                psub.put("simulatedWindowFunction", winfunEnum2str((*it).simulatedWindowFunction));
            }
            pt.add_child("basebandConfig.spectralWindow", psub);
        }
    } while(0);

    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Set M&C command ID, used internally */
void MCConfiguration::setId(unsigned int id)
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);
    configId = id;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Check that the config is valid and consistent */
bool MCConfiguration::invariant() const
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);
    is_valid = true;

    std::vector<spectralWindow_t>::const_iterator sw_it = singleConfiguration.basebandConfig.spectralWindow.begin();
    std::vector<spectralWindow_t>::const_iterator sw_end = singleConfiguration.basebandConfig.spectralWindow.end();
    for ( ; sw_it != sw_end; sw_it++) {

        double loEdge = sw_it->centerFrequency - sw_it->effectiveBandwidth/2;
        double hiEdge = sw_it->centerFrequency - sw_it->effectiveBandwidth/2;
        is_valid &= (loEdge >= procConfig.implicit_IF_lowedge_MHz);
        is_valid &= (hiEdge <= procConfig.implicit_IF_highedge_MHz);
        if (loEdge < procConfig.implicit_IF_lowedge_MHz) {
            L_(lerror) << "Configuration has spectral window with low edge at " << loEdge << " MHz, below IF low edge " << procConfig.implicit_IF_lowedge_MHz << " MHz";
        }
        if (hiEdge > procConfig.implicit_IF_highedge_MHz) {
            L_(lerror) << "Configuration has spectral window with high edge at " << hiEdge << " MHz, above IF high edge " << procConfig.implicit_IF_highedge_MHz << " MHz";
        }

        is_valid &= ((sw_it->effectiveNumberOfChannels % sw_it->spectralAveragingFactor) == 0);
        if ((sw_it->effectiveNumberOfChannels % sw_it->spectralAveragingFactor) != 0) {
            L_(lerror) << "Configuration has spectral window where requested nr of channels " << sw_it->effectiveNumberOfChannels
                       << " is not divisible by requested additional averaging factor " << sw_it->spectralAveragingFactor;
        }

        double reso_Hz_wide = (1e6*procConfig.implicit_IF_width_MHz)/(double(procConfig.nchan));
        double reso_Hz_req =  (1e6*sw_it->effectiveBandwidth)/(sw_it->effectiveNumberOfChannels/sw_it->spectralAveragingFactor);
        double R = reso_Hz_req / reso_Hz_wide;
        //is_valid &= (R == size_t(R)); // don't make fatal for now...
        if (R != size_t(R)) {
            L_(lerror) << "Configuration has spectral window with requested output channel spacing of " << reso_Hz_req << " Hz "
                       << "that does not evenly divide the internal fine channel spacing of " << reso_Hz_wide << " Hz";
        }
    }

    if (singleConfiguration.timeSpec.switching.size() > 0) {
        double Twallclock = 0.0;
        for (unsigned int p=0; p<singleConfiguration.timeSpec.switching.size(); p++) {
            Twallclock += singleConfiguration.timeSpec.switching[p].dwell_sec;
            Twallclock += singleConfiguration.timeSpec.switching[p].transition_sec;
        }
        is_valid &= (fabs(Twallclock - singleConfiguration.timeSpec.integrationDuration) < 1e-3);
        if (fabs(Twallclock - singleConfiguration.timeSpec.integrationDuration) >= 1e-3) {
            L_(lerror) << "Configuration has " << singleConfiguration.timeSpec.switching.size() << " <switching> elements with a total dwell+transition time of " << Twallclock << "s "
                       << "inconsistent with requested integrationDuration of " << singleConfiguration.timeSpec.integrationDuration << "s";
        }
    }

    //L_(ldebug1) << "MCConfiguration::invariant() returned valid=" << (is_valid ? "true" : "false");
    return is_valid;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Return DFT length */
unsigned int MCConfiguration::getDFTLength() const
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);
    if (singleConfiguration.basebandConfig.spectralWindow.size() <= 0) {
       L_(lwarning) << "MCConfiguration::getDFTLength() on empty configuration!";
       return DEFAULT_INTERNAL_DFT_LENGTH;
    }
    L_(linfo) << "Configuration will use a " << DEFAULT_INTERNAL_DFT_LENGTH << "-point Fourier transform";
    /* FIXME: correlator capability uses a quite different DFT length, not DEFAULT_INTERNAL_DFT_LENGTH! */
    return DEFAULT_INTERNAL_DFT_LENGTH;
}

/** Return a input sample stepping for a N-point FFT that leads to integer nr# of FFTs in 1 msec */
unsigned int MCConfiguration::determineOverlappedFFTStep(unsigned int Lfft) const
{
    const double Tgranularity = 1e-3, small = 1e-12;
    const double fs = 2*fabs(ANTENNA_IF_HIGHEDGE_MHZ*1e6 - ANTENNA_IF_LOWEDGE_MHZ*1e6);

    unsigned int fftinstep = Lfft;
    do {
        double r = Tgranularity / (fftinstep/fs);
        if (r - (long)r  < small) { break; }
        fftinstep--;
    } while (fftinstep > 0);

    L_(linfo) << "Configuration will use a " << fftinstep << "-sample stepping between Fourier transforms";
    return fftinstep;
}

/** Return the number of polarization products (1, 2, or 4) needed by the configured spectral window(s) */
int MCConfiguration::getNStokes() const
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);
    if (singleConfiguration.basebandConfig.spectralWindow.size() <= 0) {
       L_(lwarning) << "MCConfiguration::getNStokes() on empty configuration!";
       return 4;
    }
    int npp = 1;
    std::vector<spectralWindow_t>::const_iterator sw_it = singleConfiguration.basebandConfig.spectralWindow.begin();
    std::vector<spectralWindow_t>::const_iterator sw_end = singleConfiguration.basebandConfig.spectralWindow.end();
    for ( ; sw_it != sw_end; sw_it++) {
        if ((sw_it->polnProducts == POLN_XX_YY_XY) && (npp < 4)) {
            npp = 4;
        } else if ((sw_it->polnProducts == POLN_XX_YY) && (npp < 2)) {
            npp = 2;
        }
    }
    L_(linfo) << "Configuration requested processing with " << npp << " polarization products";

    return npp;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

std::string MCConfiguration::polEnum2str(const polnProduct_t t) const
{
    switch (t) {
        case POLN_XX_YY_XY: return "XX,YY,XY,YX";
        case POLN_XX_YY:    return "XX,YY";
        case POLN_XX:       return "XX";
        case POLN_YY:       return "YY";
        default:       return "<illegal>";
    }
    return "<illegal>";
}

polnProduct_t MCConfiguration::polStr2enum(const std::string& v) const
{
    if (v == "XX,YY,XY,YX") { return POLN_XX_YY_XY; } // by XML Schema, but be
    if (v == "XX,YY,XY")    { return POLN_XX_YY_XY; } // more permissive
    if (v == "XX,YY,YX,XY") { return POLN_XX_YY_XY; }
    if (v == "XX,YY")    { return POLN_XX_YY; }
    if (v == "XX")       { return POLN_XX; }
    if (v == "YY")       { return POLN_YY; }
    return POLN_UNDEFINED;
}

std::string MCConfiguration::winfunEnum2str(const windowFunction_t t) const
{
    switch (t) {
        case UNIFORM: return "UNIFORM";
        case HANNING: return "HANNING";
        case HAMMING: return "HAMMING";
        case BARTLETT: return "BARTLETT";
        case BLACKMAN: return "BLACKMANN"; // note: mistyped with double-N for compatibility reasons
        case BLACKMAN_HARRIS: return "BLACKMANN_HARRIS";
        case WELCH: return "WELCH";
        default: return "<illegal>";
    }
    return "<illegal>";
}

windowFunction_t MCConfiguration::winfunStr2enum(const std::string& v) const
{
    if (v == "UNIFORM") { return UNIFORM; }
    if (v == "HANNING") { return HANNING; }
    if (v == "HAMMING") { return HAMMING; }
    if (v == "BARTLETT") { return BARTLETT; }
    if (v == "BLACKMAN") { return BLACKMAN; }
    if (v == "BLACKMANN") { return BLACKMAN; }
    if (v == "BLACKMAN_HARRIS") { return BLACKMAN_HARRIS; }
    if (v == "BLACKMANN_HARRIS") { return BLACKMAN_HARRIS; }
    if (v == "WELCH") { return WELCH; }
    return UNIFORM;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** \return A lookup vector, useful for translating from switching positions (ID; any numbers) to contiguous numbered index '0..(npos-1)' */
int MCConfiguration::getSwitchingPositionsLookup(std::vector<unsigned int>& lut) const
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);
    lut.clear();
    for (std::vector<switching_t>::const_iterator it = singleConfiguration.timeSpec.switching.begin(); it != singleConfiguration.timeSpec.switching.end(); ++it) {
        lut.push_back((*it).position);
    }
    lut.erase(std::unique(lut.begin(), lut.end()), lut.end());
    return lut.size();
}

/** \return The number of switching positions */
int MCConfiguration::getNumSwitchingPositions() const
{
    std::vector<unsigned int> tmp;
    getSwitchingPositionsLookup(tmp);
    return tmp.size();
}

/** \return shortest dwell time in 'switching' setup in seconds */
float MCConfiguration::getMinDwellSeconds() const
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);
    float T = 1e9;
    for (size_t sw=0; sw<singleConfiguration.timeSpec.switching.size(); sw++) {
        if (singleConfiguration.timeSpec.switching[sw].dwell_sec < T) {
            T = singleConfiguration.timeSpec.switching[sw].dwell_sec;
        }
    }
    return T;
}

/** \return longest dwell time in 'switching' setup in seconds */
float MCConfiguration::getMaxDwellSeconds() const
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);
    float T = 0.0f;
    for (size_t sw=0; sw<singleConfiguration.timeSpec.switching.size(); sw++) {
        if (singleConfiguration.timeSpec.switching[sw].dwell_sec > T) {
            T = singleConfiguration.timeSpec.switching[sw].dwell_sec;
        }
    }
    return T;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Create a 1 spectral window config for testing */
void MCConfiguration::createTestconfig1()
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);

    singleConfiguration.timeSpec.switching.clear();
    singleConfiguration.basebandConfig.spectralWindow.clear();

    singleConfiguration.timeSpec.integrationDuration = 48e-3;
    singleConfiguration.timeSpec.channelAverageDuration = 48e-3;
    for (int p=0; p<1; p++) {
        switching_t sw(p, singleConfiguration.timeSpec.integrationDuration, 0.0);
        singleConfiguration.timeSpec.switching.push_back(sw);
    }

    spectralWindow_t win;
    win.name = "SW-1000M";
    win.centerFrequency = 3000.0;
    win.effectiveBandwidth = 1000.0;
    win.effectiveNumberOfChannels = 2048;
    win.spectralAveragingFactor = 1;
    win.quantizationCorrection = false;
    win.windowFunction = UNIFORM;
    win.windowFunction_chosen = true;
    win.polnProducts = POLN_XX_YY;
    channelAverageRegion_t avg;
    avg.startChannel = 0;
    avg.numberChannels = 2048;
    win.channelAverageRegion.push_back(avg);

    singleConfiguration.basebandConfig.spectralWindow.push_back(win);

    updateDerivedParams();
}

/** Create a 2 spectral window config for testing */
void MCConfiguration::createTestconfig2()
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);

    createTestconfig1();

    spectralWindow_t win;
    win.name = "SW-500M";
    win.centerFrequency = 3000.0;
    win.effectiveBandwidth = 500.0;
    win.effectiveNumberOfChannels = 2048;
    win.spectralAveragingFactor = 2;
    win.quantizationCorrection = true;
    win.windowFunction = UNIFORM;
    win.windowFunction_chosen = true;
    win.polnProducts = POLN_XX_YY;
    channelAverageRegion_t avg;
    avg.startChannel = 0;
    avg.numberChannels = 2048;
    win.channelAverageRegion.push_back(avg);

    singleConfiguration.basebandConfig.spectralWindow.push_back(win);

    updateDerivedParams();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Update some of the internal parameters derived from the XML 'singleConfiguration' */
void MCConfiguration::updateDerivedParams()
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);

    // Update some params
    procConfig.Tint_msec = singleConfiguration.timeSpec.integrationDuration*1000;
    procConfig.nchan = getDFTLength() / 2;
    procConfig.fftinstep = determineOverlappedFFTStep(getDFTLength());
    procConfig.nstokes = getNStokes();
    procConfig.nswpos = getNumSwitchingPositions();
    if (procConfig.nswpos < 1) {
        switching_t sw(0, singleConfiguration.timeSpec.integrationDuration, 0.0);
        singleConfiguration.timeSpec.switching.push_back(sw);
        procConfig.nswpos = 1;
    }

    // Set implicit params
    procConfig.implicit_IF_lowedge_MHz = ANTENNA_IF_LOWEDGE_MHZ;
    procConfig.implicit_IF_highedge_MHz = ANTENNA_IF_HIGHEDGE_MHZ;
    procConfig.implicit_IF_width_MHz = procConfig.implicit_IF_highedge_MHz - procConfig.implicit_IF_lowedge_MHz;
    procConfig.implicit_IF_sideband = ANTENNA_IF_SIDEBAND_USB ? SIDEBAND_USB : SIDEBAND_LSB;

    // Determine the length of one switch cycle over all positions
    procConfig.Tswitchingcycle_msec = 0;
    procConfig.Tswitchingdwell_msec = 0;
    for (unsigned int p=0; p<singleConfiguration.timeSpec.switching.size(); p++) {
        procConfig.Tswitchingcycle_msec += singleConfiguration.timeSpec.switching[p].dwell_sec*1e3;
        procConfig.Tswitchingcycle_msec += singleConfiguration.timeSpec.switching[p].transition_sec*1e3;
        procConfig.Tswitchingdwell_msec += singleConfiguration.timeSpec.switching[p].dwell_sec*1e3;
    }

    // Determine window function
    procConfig.windowFunction = UNIFORM;
    for (unsigned int w=0; w<singleConfiguration.basebandConfig.spectralWindow.size(); w++) {
        if (procConfig.windowFunction != singleConfiguration.basebandConfig.spectralWindow[w].windowFunction) {
            procConfig.windowFunction = singleConfiguration.basebandConfig.spectralWindow[w].windowFunction;
            L_(linfo) << "MCConfiguration updating window func type to " << int(procConfig.windowFunction)
                      << "('" << winfunEnum2str(procConfig.windowFunction) << "')";
        }
    }
}

/** Update some of the internal parameters derived from the XML 'singleConfiguration' */
void MCConfiguration::updateDerivedParamsFrom(const MCObservation* obs)
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);
    updateDerivedParams();
    obs->mutex.lock();
    procConfig.requestedStartTime = obs->ts_startTimeUTC;
    procConfig.requestedStopTime = obs->ts_stopTimeUTC;
    obs->mutex.unlock();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

std::ostream& operator<<(std::ostream &s, const MCConfiguration &a)
{
    boost::unique_lock<boost::recursive_mutex> lock(a.mutex);

    s << "MCConfiguration basebandConfig={";
    s << " sidebandSuppression:" << a.singleConfiguration.basebandConfig.sidebandSuppression;

    int Nsw = a.singleConfiguration.basebandConfig.spectralWindow.size();
    s << " spectralWindow[" << Nsw << "]={";
    for (int n=0; n<Nsw; n++) {
        const spectralWindow_t& sw = a.singleConfiguration.basebandConfig.spectralWindow[n];
        s << "(name:" << sw.name
          << " centerFrequency:" << sw.centerFrequency
          << " spectralAveragingFactor:" << sw.spectralAveragingFactor
          << " effectiveBandwidth:" << sw.effectiveBandwidth
          << " effectiveNumberOfChannels:" << sw.effectiveNumberOfChannels
          << " quantizationCorrection:" << sw.quantizationCorrection
          << " windowFunction:" << int(sw.windowFunction)
          << " windowFunction_chosen:" << sw.windowFunction_chosen
          << " windowOverlap:" << sw.windowOverlap
          << " simulatedWindowFunction:" << int(sw.simulatedWindowFunction)
          << " simulatedWindowFunction_chosen:" << sw.simulatedWindowFunction_chosen
          << " polnProducts:" << int(sw.polnProducts);
        int Nar = sw.channelAverageRegion.size();
        s  << " channelAverageRegion[" << Nar << "]=(";
        for (int m=0; m<Nar; m++) {
            s << sw.channelAverageRegion[m].startChannel << ".."
              << (sw.channelAverageRegion[m].startChannel + sw.channelAverageRegion[m].numberChannels-1);
            if (m != (Nar-1)) {
                s << ", ";
            }
        }
        s << ") )";
        if (n != (Nsw-1)) {
            s << ", ";
        }
    }
    s << "}";

    const timeSpec_t& ts = a.singleConfiguration.timeSpec;
    s << " timeSpec={";
    s << " integrationDuration:" << ts.integrationDuration
      << " channelAverageDuration: " << ts.channelAverageDuration;
    int Npos = ts.switching.size();
    s << " switching[" << Npos << "]={";
    for (int n=0; n<Npos; n++) {
        s << "(position:" << ts.switching[n].position
          << " dwell_sec:" << ts.switching[n].dwell_sec
          << " transition_sec:" << ts.switching[n].transition_sec
          << ")";
        if (n != (Npos-1)) {
            s << ", ";
        }
    }
    s << "}";

    s << " procConfig={";
    s << " IF_low_MHz=" << a.procConfig.implicit_IF_lowedge_MHz
      << " IF_high_MHz=" << a.procConfig.implicit_IF_highedge_MHz
      << " IF_width_MHz=" << a.procConfig.implicit_IF_width_MHz
      << " nchan=" << a.procConfig.nchan
      << " fftinstep=" << a.procConfig.fftinstep
      << " npol=" << a.procConfig.npol
      << " nstokes=" << a.procConfig.nstokes
      << " nswpos=" << a.procConfig.nswpos;
    s << "}";

    return s;
}
