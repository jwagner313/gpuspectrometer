/**
 * \class MCObservation
 *
 * \brief Groups together a spectral configuration and observation settings (time, ID).
 *
 * \author $Author: janw $
 *
 */

#include "mcwrapping/mc_observation.hxx"
#include "datarecipients/ResultRecipient_iface.hxx"
#include "time/TimeUTC.hxx"
#include "helper/logger.h"

#include <string>
#include <boost/thread/recursive_mutex.hpp>

/** C'stor */
MCObservation::MCObservation()
{
    state = ObsUninitialized;
    antennaId = "1"; // TODO: where should this come from
    baseBandName = "BB_1"; // TODO: where should this come from
    outputWriter = NULL;
    startTimeUTC = "";
    stopTimeUTC = "";
    ts_startTimeUTC.tv_sec = 0;
    ts_startTimeUTC.tv_nsec = 0;
    ts_stopTimeUTC.tv_sec = 0;
    ts_stopTimeUTC.tv_nsec = 0;
}

/** Assignment op */
MCObservation& MCObservation::operator=(const MCObservation& rhs)
{
    boost::unique_lock<boost::recursive_mutex> lock1(rhs.mutex);
    boost::unique_lock<boost::recursive_mutex> lock2(mutex);

    cfg = rhs.cfg;
    outputWriter = rhs.outputWriter; // shared obj

    observationId = rhs.observationId;
    antennaId = rhs.antennaId;
    baseBandName = rhs.baseBandName;
    state = rhs.state;

    startTimeUTC = rhs.startTimeUTC;
    stopTimeUTC = rhs.stopTimeUTC;
    ts_startTimeUTC = rhs.ts_startTimeUTC;
    ts_stopTimeUTC = rhs.ts_stopTimeUTC;

    return *this;
}

/** Copy c'stor */
MCObservation::MCObservation(const MCObservation& rhs)
{
    boost::unique_lock<boost::recursive_mutex> lock1(rhs.mutex);
    boost::unique_lock<boost::recursive_mutex> lock2(mutex);

    cfg = rhs.cfg;
    outputWriter = rhs.outputWriter; // shared obj

    observationId = rhs.observationId;
    antennaId = rhs.antennaId;
    baseBandName = rhs.baseBandName;
    state = rhs.state;

    startTimeUTC = rhs.startTimeUTC;
    stopTimeUTC = rhs.stopTimeUTC;
    ts_startTimeUTC = rhs.ts_startTimeUTC;
    ts_stopTimeUTC = rhs.ts_stopTimeUTC;
}

/** D'stor */
MCObservation::~MCObservation()
{
    if (outputWriter != NULL) {
        // Make sure the output writer is closed (if not already)
//        outputWriter->close();
// TODO: in some cases outputwriter is closed on d'stor of a copy(!?) of MCObservation; even thoug outputWriter is not copied? maybe in implicity copy cstor?
    }
}

/** Update some internal derived parameters */
void MCObservation::updateDerivedParams()
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);
    const unsigned long T_GRANULARITY_NS = 1000000;
    TimeUTC::stringToTimespec(startTimeUTC, &ts_startTimeUTC);
    TimeUTC::stringToTimespec(stopTimeUTC, &ts_stopTimeUTC);
    ts_startTimeUTC.tv_nsec = ((ts_startTimeUTC.tv_nsec + T_GRANULARITY_NS/2) / T_GRANULARITY_NS) * T_GRANULARITY_NS;
    ts_stopTimeUTC.tv_nsec = ((ts_stopTimeUTC.tv_nsec + T_GRANULARITY_NS/2) / T_GRANULARITY_NS) * T_GRANULARITY_NS;
}


/** Return string representation of ObservationState_t enum */
std::string MCObservation::ObservationState_str(const ObservationState_t& t)
{
    switch (t) {
        case Pending: return std::string("pending");
        case Ongoing: return std::string("ongoing");
        case Faulted: return std::string("faulted");
        case Stopped: return std::string("stopped");
        case Missed:  return std::string("missed");
        default: break;
    }
    L_(lwarning) << "Got unexpected ObservationState_t value " << t << " in MCObservation::ObservationState_str\n";
    //setError(IllegalObsState);
    return std::string("illegal_state");
}

bool MCObservation::operator<(const MCObservation& rhs) const
{
    boost::unique_lock<boost::recursive_mutex> lock1(rhs.mutex);
    boost::unique_lock<boost::recursive_mutex> lock2(mutex);
    try {
       double Tlhs = 0, Trhs = 0;
       (void)TimeUTC::stringToSeconds(startTimeUTC, &Tlhs);
       (void)TimeUTC::stringToSeconds(rhs.startTimeUTC, &Trhs);
       return Tlhs < Trhs;
    } catch (...) {
    }
    return true;
}

bool MCObservation::operator<=(const MCObservation& rhs) const
{
    boost::unique_lock<boost::recursive_mutex> lock1(rhs.mutex);
    boost::unique_lock<boost::recursive_mutex> lock2(mutex);
    try {
       double Tlhs = 0, Trhs = 0;
       (void)TimeUTC::stringToSeconds(startTimeUTC, &Tlhs);
       (void)TimeUTC::stringToSeconds(rhs.startTimeUTC, &Trhs);
       return Tlhs <= Trhs;
    } catch (...) {
    }
    return true;
}

bool MCObservation::operator>(const MCObservation& rhs) const
{
    boost::unique_lock<boost::recursive_mutex> lock1(rhs.mutex);
    boost::unique_lock<boost::recursive_mutex> lock2(mutex);
    return !(*this <= rhs);
}

bool MCObservation::operator>=(const MCObservation& rhs) const
{
    boost::unique_lock<boost::recursive_mutex> lock1(rhs.mutex);
    boost::unique_lock<boost::recursive_mutex> lock2(mutex);
    return !(*this < rhs);
}

bool MCObservation::operator==(const MCObservation& rhs) const
{
    boost::unique_lock<boost::recursive_mutex> lock1(rhs.mutex);
    boost::unique_lock<boost::recursive_mutex> lock2(mutex);
    return (observationId == rhs.observationId);
}

bool MCObservation::operator==(const std::string& rhs) const
{
    boost::unique_lock<boost::recursive_mutex> lock(mutex);
    return (observationId == rhs);
}

std::ostream& operator<<(std::ostream &s, const MCObservation &a)
{
    boost::unique_lock<boost::recursive_mutex> lock(a.mutex);
    s << a.startTimeUTC << "--" << a.stopTimeUTC << " : " << a.observationId << " : " << a.ObservationState_str(a.state);
    return s;
}
