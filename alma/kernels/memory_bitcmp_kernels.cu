/////////////////////////////////////////////////////////////////////////////////////
//
// Memory comparison kernel(s) for SEU bit-flip detections
//
// (C) 2017 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////

#ifndef MEMORY_BITCMP_KERNELS_CU

__global__ void cu_bitcmp(unsigned int* __restrict__ ref, unsigned int* cmp, unsigned int* results)
{
    size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    int sh_i = threadIdx.x;

    __shared__ unsigned int byte_errs[32];
    __shared__ unsigned int bit_errs[32];

    byte_errs[sh_i] = 0;
    bit_errs[sh_i] = 0;
    __syncthreads();

    unsigned int r = ref[i];
    unsigned int v = cmp[i];
    int biterrs = __popc(r ^ v); // CUDA integer intrinsic, Count the number of bits that are set to 1
    int byteerr = (r != v) & 1;
    atomicAdd(&bit_errs[sh_i], biterrs);
    atomicAdd(&byte_errs[sh_i], byteerr);
    __syncthreads();
 
    atomicAdd(&results[0], bit_errs[sh_i]);
    atomicAdd(&results[1], byte_errs[sh_i]);

}

/////////////////////////////////////////////////////////////////////////////////////

#ifdef BENCH // standalone compile mode for testing

#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif
#define CHECK_TIMING 1
#include "cuda_utils.cu"
#include <sys/time.h>

int main(int argc, char **argv)
{
    cudaDeviceProp cudaDevProp;

    cudaEvent_t tstart, tstop;
    struct timeval tvstart, tvstop;

    unsigned int *h_data, *d_data;
    unsigned int *h_ref, *d_ref;
    unsigned int *h_results, *d_results;
    const int nrawbytes = 144000000;
    size_t maxphysthreads;

    // Device
    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;
    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    // Alloc space
    CUDA_CALL( cudaMalloc( (void **)&d_data, nrawbytes ) );
    CUDA_CALL( cudaMalloc( (void **)&d_ref, nrawbytes ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_data, nrawbytes, cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_ref, nrawbytes, cudaHostAllocDefault ) );
    CUDA_CALL( cudaMalloc( (void **)&d_results, 2*sizeof(unsigned int) ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_results, 2*sizeof(unsigned int), cudaHostAllocDefault ) );

    memset(h_ref, 0, nrawbytes);
    memset(h_data, 0, nrawbytes);
    CUDA_CALL( cudaMemcpy( d_ref, h_ref, nrawbytes, cudaMemcpyHostToDevice) );
    CUDA_CALL( cudaMemcpy( d_data, h_data, nrawbytes, cudaMemcpyHostToDevice) );
    CUDA_CALL( cudaMemset( d_results, 0, 2*sizeof(unsigned int)));

    // Distribution
    size_t M = cudaDevProp.warpSize;
    size_t N = nrawbytes / sizeof(unsigned int);
    dim3 numBlocks(div2ceil(N,M));
    dim3 threadsPerBlock(M);
    printf("GPU exec as <<%d,%d>> grid\n", numBlocks.x, threadsPerBlock.x);
    printf("GPU max threads per SM: %d\n", cudaDevProp.maxThreadsPerMultiProcessor);

    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, maxphysthreads
    );

    /////////////////////////////////////////////////////////////////////////////////////

    for (int iter=0; iter < 4; iter++) {

        CUDA_CALL( cudaMemcpy( d_data, h_data, nrawbytes, cudaMemcpyHostToDevice) );
        CUDA_CALL( cudaMemset( d_results, 0, 2*sizeof(unsigned int)));

        gettimeofday(&tvstart, NULL);
        CUDA_TIMING_START(tstart, 0);
        cu_bitcmp <<< numBlocks, threadsPerBlock >>> (d_ref, d_data, d_results);
        CUDA_CHECK_ERRORS("cu_bitcmp");
        CUDA_TIMING_STOP(tstop, tstart, 0, "hist", nrawbytes/sizeof(unsigned int));
        gettimeofday(&tvstop, NULL);

        CUDA_CALL( cudaMemcpy( h_results, d_results, 2*sizeof(unsigned int), cudaMemcpyDeviceToHost) );
        printf("output : res[0]=%u res[1]=%u\n", h_results[0], h_results[1]);

        h_data[iter] += iter; // introduce errors

    }

    /////////////////////////////////////////////////////////////////////////////////////

}

#endif // BENCH

#endif // MEMORY_BITCMP_KERNELS_CU
