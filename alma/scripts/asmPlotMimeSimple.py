import email
import struct
import matplotlib.pyplot as plt

fp = open("/home/tnakamoto/ASM/test_20180829_013808/uid__0829T_013_816_1_BB_1.mime")
msg = email.message_from_file(fp)
fp.close()

for part in msg.walk():
    loc = part['Content-Location']

    if type(loc) is str and loc.endswith('autoData.bin'):
        # It is the binary data to plot.
        size = len(part.get_payload()) / 4
        data = struct.unpack('<%df' % size, part.get_payload())

        xx = data[0::2]
        yy = data[1::2]

        plt.figure(1)
        plt.subplot(211)
        plt.plot(range(len(xx)), xx)

        plt.subplot(212)
        plt.plot(range(len(yy)), yy)

        plt.show()

