#include "helper/logger.h"
#include "time/TimeTAI.hxx"

#include <cmath>
#include <iostream>
#include <iomanip>

#include <unistd.h>

int main(int argc, char** argv)
{
    double tai_s;

    //initLogger("TimeTAI.log", ldebug4);
    std::cout << std::fixed << std::setprecision(3);

    TimeTAI::loadLeapseconds("leap-seconds.list");
    TimeTAI::tableToCcode();

    std::string tai1("2016-10-17T13:35:35.000");
    std::cout << "Hardcoded TAI_str = " << tai1 << "\n";
    std::cout << "---------------------------------------\n";
    TimeTAI::taiStringToSec(tai1, &tai_s);
    std::cout << "TAI " << tai1 << " = " << tai_s << " sec from " << C_TAI_refepstr << "\n";
    std::cout << "TAI " << tai1 << " = " << (tai_s+C_TAI_refsec) << " sec from 1900-01-1\n";
    std::cout << "---------------------------------------\n\n";

    // http://leapsecond.com/java/gpsclock.htm
    // UTC 2016-10-17T13:34:59.000
    // TAI 2016-10-17T13:35:35.000
    // TAI = UTC + 36 leap secs
    std::string utc1("2016-10-17T13:34:59.000");
    std::cout << "Hardcoded UTC_str = " << utc1 << "\n";
    std::cout << "---------------------------------------\n";
    std::string tai_conv = TimeTAI::strUTC2TAI(utc1);
    std::cout << "UTC " << utc1 << "\n";
    std::cout << "TAI " << tai_conv << "\n";
    std::cout << "Leap = " << TimeTAI::m_leapSecs[TimeTAI::m_leapIndex].leapsecs << " sec\n";
    std::cout << "---------------------------------------\n\n";

    std::string tai2("2017-01-01T23:59:59.999");
    if (argc == 2) {
        tai2 = std::string(argv[1]);
        std::cout << "User-provided TAI_str = " << tai2 << "\n";
    } else {
        std::cout << "Hardcoded TAI_str = " << tai2 << "\n";
    }
    for (int n=0; n<4; n++) {
        double dT = TimeTAI::getSecondsSince(tai2);
        std::cout << "Now " << std::fabs(dT) << " seconds "
                  << ((dT > 0) ? "since " : "until ") << tai2 << "\n";
        sleep(1);
    }
    return 0;
}
