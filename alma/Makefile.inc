
##############################################################################################
## Environment
##############################################################################################

TOPDIR := $(dir $(lastword $(MAKEFILE_LIST)))

# Location where to install and generate
# sub-dirs ./alma ./alma/bin ./alma/conf ./alma/share
INSTALL_PREFIX := /opt/

# Location of CUDA installation
ifndef CUDA_DIR
	CUDA_DIR := /usr/local/cuda/
endif

# Location of DRXP driver sources (only drxpd.h is needed)
DRXP_DIR := /usr/include/drxp/

# Enable GPU processing?
# 1: enable GPU (CUDA) code, 0: revert to CPU code (simulator mode)
HAVE_GPU := 1

# Enable DRXP interface (ioctl)
# 1: enable access to DRXP driver interface, 0: revert to dummy DRXP object
HAVE_DRXP := 1

# Enable Xerces-C XML validation?
# 1: enable, 0: disable
HAVE_XERCES := 1

##############################################################################################
# Check CUDA compiler availability
##############################################################################################

NVCC = $(CUDA_DIR)/bin/nvcc

ifeq ($(HAVE_GPU),1)
    ifeq (,$(wildcard $(NVCC)))
        $(warning Warning: Cannot find CUDA compiler $(NVCC). Please check the settings in Makefile.inc.)
        HAVE_GPU := 0
    endif
endif

##############################################################################################
# Check DRXP driver availability
##############################################################################################

ifneq (,$(wildcard $(DRXP_DIR)/drxpd.h))
    # HAVE_DRXP := 1 (leave unchanged)
else
    $(warning Warning: Cannot find DRXP driver headers '$(DRXP_DIR)/drxpd.h'. Please check the settings in Makefile.inc.)
    HAVE_DRXP := 0
endif

##############################################################################################
## Configuration for C compiler and linker
##############################################################################################

CDEFS    := -D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_XOPEN_SOURCE=600 -D_GNU_SOURCE
CFLAGS   := -Wall -O3 -pthread -g -I. -I.. -I./xsd_datatypes/ $(CDEFS) -m64
CXXFLAGS := -Wall -O3 -pthread -g -I. -I.. -I./xsd_datatypes/ $(CDEFS) -m64
CXX	 := g++

XSDLIBS  := $(TOPDIR)/xsd_datatypes/xsd_datatypes.a
LDLIBS   := -lm -lpthread -ldl
CULIBS   := -lcuda -lcudart -lcufft
BOOSTLIB := -lboost_system -lboost_thread-mt -lboost_date_time  # note: boost_thread sometimes available as boost_thread-mt

ALMA_CXXFLAGS := $(CXXFLAGS)
ALMA_LIBS     := -L$(CUDA_DIR)/lib64/ $(LDLIBS) $(BOOSTLIB) $(XSDLIBS)
ifeq ($(HAVE_GPU),1)
    ALMA_LIBS += $(CULIBS)
endif

##############################################################################################
## CUDA target architectures and NVCC settings
##############################################################################################

NV_ARCH_MAXWELL := -gencode arch=compute_50,code=sm_50
NV_ARCH_MAXWELL += -gencode=arch=compute_52,code=sm_52 -gencode=arch=compute_52,code=compute_52
NV_ARCH_PASCAL  := -gencode=arch=compute_61,code=sm_61 -gencode=arch=compute_61,code=compute_61
NV_ARCH_VOLTA   := -gencode=arch=compute_70,code=sm_70 -gencode=arch=compute_70,code=compute_70

NVLDINC := -L$(CUDA_DIR)/lib64/
NVLIBS  := $(CULIBS) $(LDLIBS) $(ALMA_LIBS)

NVFLAGS  := $(CDEFS) -O3 -m64 -g -Xcompiler -Wall
NVFLAGS  += --use_fast_math
#NVFLAGS += --ptxas-options=-v
NVFLAGS  += --compiler-bindir=/usr/bin/gcc
NVFLAGS  += -I$(TOPDIR)kernels -I$(TOPDIR)
#NVFLAGS += -DGPU_DEBUG

NVFLAGS  += $(NV_ARCH_PASCAL) $(NV_ARCH_VOLTA)

##############################################################################################
## Environment conditional extra options
##############################################################################################

ifeq ($(HAVE_XERCES),1)
    ALMA_CXXFLAGS += -DHAVE_XERCES=1
    ALMA_LIBS += -lxerces-c
endif

ifeq ($(HAVE_DRXP),1)
    ALMA_CXXFLAGS += -DHAVE_DRXP=1 -I$(DRXP_DIR)
    NVFLAGS += -DHAVE_DRXP=1 -I$(DRXP_DIR)
endif

ifeq ($(HAVE_GPU),1)
    ALMA_CXXFLAGS += -DHAVE_GPU
    ALMA_LIBS += $(NVLDINC) $(NVLIBS)
else
    $(warning Warning: Compiling without GPU support (will use CPU code, no actual spectral output)!)
endif

##############################################################################################
## Implicit targets
##############################################################################################

%.o: %.cxx
	$(CXX) $(ALMA_CXXFLAGS) -c -o $@ $<

%.co: %.cu
	$(NVCC) $(NVFLAGS) -c -o $@ $<

