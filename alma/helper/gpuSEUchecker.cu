#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

#include <sys/time.h>
#include <string>
#include <sstream>
#include <iostream>

#include "../kernels/cuda_utils.cu"

#define TEST_REPEAT_TIME_SECS 0  // interval at which to repeat the GPU SEU tests, roughly, in seconds

#define lockscope
//#define DBG_INTRODUCE_ERRORS      // define for testing, to introduce some memory errors


//////////////////////////////////////////////////////////////////////////////////////////////////////

void usage(void)
{
    std::cout << "\nMonitoring program to detect over a long term any bit flips in the contents of\n"
              << "the large off-chip 'global memory' and tiny on-chip 'shared memory' on GPUs.\n"
              << "These data errors may be caused by cosmic rays or logic glitches.\n"
              << "\nUsage: gpuSEUchecker <gpuIds>\n\n"
              << "  gpuIds   : comma-separated list of GPU cards to use, e.g., '0,1,2,3'\n\n"
              << "To check ECC errors please use 'nvidia-smi -q -d ECC' as the CUDA API does not\n"
              << "allow direct access to these counters from used programs\n\n";
    exit(EXIT_FAILURE);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Accumulate mismatches in 'ref' vs 'cmp' data into bit error and byte error counts stored in 'results' */
__global__ void cu_bitcmp(unsigned int* __restrict__ ref, unsigned int* __restrict__ cmp, unsigned int* results)
{
    size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    int sh_i = threadIdx.x;

    __shared__ unsigned int int32_errs[32];
    __shared__ unsigned int bit_errs[32];

    int32_errs[sh_i] = 0;
    bit_errs[sh_i] = 0;
    __syncthreads();

    unsigned int r = ref[i];
    unsigned int v = cmp[i];
    int biterrs = __popc(r ^ v); // CUDA integer intrinsic, Count the number of bits that are set to 1
    int int32err = (r != v) & 1;
    atomicAdd(&bit_errs[sh_i], biterrs);
    atomicAdd(&int32_errs[sh_i], int32err);
    __syncthreads();

    atomicAdd(&results[0], bit_errs[sh_i]);
    atomicAdd(&results[1], int32_errs[sh_i]);
}

/** Test shared memory (~64kB per SM) by checking for errors that might appear after kernel launch and during ~1 second of looping time */
__global__ void cu_shtest(unsigned int* results)
{
    const unsigned int WARPSZ = 32;
    const unsigned int SHSZ = 350; // change until no more "ptxas error : Entry function '_Z9cu_shtestPj' uses too much shared data (<x> bytes, <y> max)"
    const unsigned int PATTERN32 = 0xAAAAAAAA;
    const int sh_i = threadIdx.x % WARPSZ;

    __shared__ unsigned int shm[SHSZ][WARPSZ];
    __shared__ unsigned int int32_errs[WARPSZ];
    __shared__ unsigned int bit_errs[WARPSZ];
    int32_errs[sh_i] = 0;
    bit_errs[sh_i] = 0;
    for (int j = 0; j < SHSZ; j++) {
        shm[j][sh_i] = PATTERN32;
    }
    if (0) {
        // test: introduce error
        shm[0][0] = PATTERN32 - 1;
    }
    __syncthreads();

    clock_t start_clock = clock64();
    while ((clock64() - start_clock) < 100000000) { // ticks at ~500MHz; 100000000 takes ~1.5 seconds on Titan XP
        int biterrs = 0, int32errs = 0;
        for (int j = 0; j < SHSZ; j++) {
            unsigned int v = shm[j][sh_i];
            biterrs +=  __popc(v ^ PATTERN32);
            int32errs += (v != PATTERN32) & 1;
        }
        bit_errs[sh_i] = umax(biterrs, bit_errs[sh_i]); // umax() in CUDA device_functions.hpp
        int32_errs[sh_i] = umax(int32errs, int32_errs[sh_i]);
    }
    __syncthreads();

    atomicAdd(&results[0], bit_errs[sh_i]);
    atomicAdd(&results[1], int32_errs[sh_i]);
}

/** Test both shared and global memory.
 * Initializes shared memory with a pattern first and lets this sit idle
 * while running a check on global memory. After this, the contents of
 * the shared memory is checked over a ~1 second looping time.
 */
__global__ void cu_gshtest(unsigned int* __restrict__ ref, unsigned int* __restrict__ cmp, unsigned int* results, size_t gmem_half_size)
{
    const unsigned int WARPSZ = 32;
    const unsigned int SHSZ = 350; // change until no more "ptxas error : Entry function '_Z9cu_shtestPj' uses too much shared data (<x> bytes, <y> max)"
    const unsigned int PATTERN32 = 0xAAAAAAAA;

    const size_t gstride = blockDim.x * gridDim.x;
    size_t n = blockIdx.x * blockDim.x + threadIdx.x;
    const int sh_i = threadIdx.x % WARPSZ;

    __shared__ unsigned int shm[SHSZ][WARPSZ];
    __shared__ unsigned int int32_errs[WARPSZ];
    __shared__ unsigned int bit_errs[WARPSZ];
    __shared__ unsigned int int32_errs_g[WARPSZ];
    __shared__ unsigned int bit_errs_g[WARPSZ];
    int32_errs[sh_i] = 0;
    bit_errs[sh_i] = 0;
    int32_errs_g[sh_i] = 0;
    bit_errs_g[sh_i] = 0;
    for (int j = 0; j < SHSZ; j++) {
        shm[j][sh_i] = PATTERN32;
    }
#ifdef DBG_INTRODUCE_ERRORS
    // test: introduce error
    shm[0][0] = PATTERN32 - 1;
#endif
    __syncthreads();

    // Test global memory for mismatches
    while (n < gmem_half_size) {
        unsigned int r = ref[n];
        unsigned int v = cmp[n];
        int biterrs = __popc(r ^ v); // CUDA integer intrinsic, Count the number of bits that are set to 1
        int int32err = (r != v) & 1;
        atomicAdd(&bit_errs_g[sh_i], biterrs);
        atomicAdd(&int32_errs_g[sh_i], int32err);
        n += gstride;
    }
    __syncthreads();
    atomicAdd(&results[0], bit_errs_g[sh_i]);
    atomicAdd(&results[1], int32_errs_g[sh_i]);

    // Test shared memory for mismatches
    clock_t start_clock = clock64();
    while ((clock64() - start_clock) < 200000000) { // ticks at ~500MHz; 100000000 takes ~1.5 seconds on Titan XP
        int biterrs = 0, int32errs = 0;
        for (int j = 0; j < SHSZ; j++) {
            unsigned int v = shm[j][sh_i];
            biterrs +=  __popc(v ^ PATTERN32);
            int32errs += (v != PATTERN32) & 1;
        }
        bit_errs[sh_i] = umax(biterrs, bit_errs[sh_i]); // umax() in CUDA device_functions.hpp
        int32_errs[sh_i] = umax(int32errs, int32_errs[sh_i]);
    }
    __syncthreads();
    atomicAdd(&results[2], bit_errs[sh_i]);
    atomicAdd(&results[3], int32_errs[sh_i]);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

static boost::mutex m_gpuMutex;

typedef struct worker_context_tt {
    boost::thread* thrd;
    int gpuId;
    bool stop;
} worker_context_t;

/** Worker thread for single-GPU SEU checking */
void* gpuWorker(worker_context_t* volatile ctx)
{
    if (ctx == NULL) { return NULL; }

    unsigned int* x;
    unsigned int* cmpresults_all;
    unsigned int* cmpresults_gmem;
    unsigned int* cmpresults_shmem;
    size_t nbytes, nints, iter = 0;
    size_t shmem_bit_err_accu = 0, shmem_byte_err_accu = 0;
    float T_kernel_total_ms = 0.0f;

    size_t numBlocks, threadsPerBlock;
    cudaDeviceProp cudaDevProp;
    cudaStream_t stream;
    cudaEvent_t gstart, kstart, kstop;

    lockscope {

        boost::mutex::scoped_lock sharedGPUlock(m_gpuMutex);

        // Switch GPU
        CUDA_CALL( cudaSetDevice(ctx->gpuId) );
        CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, ctx->gpuId) );
        CUDA_CALL( cudaStreamCreate(&stream) );
        CUDA_CALL( cudaEventCreate(&gstart) );
        CUDA_CALL( cudaEventCreate(&kstart) );
        CUDA_CALL( cudaEventCreate(&kstop) );

        // Allocate most of GPU memory
        size_t free, total;
        CUDA_CALL( cudaMemGetInfo(&free,&total) );
        nbytes = free - 16*1024*1024;
        nbytes = nbytes - (nbytes % sizeof(unsigned int));
        nints = nbytes/sizeof(unsigned int);
        CUDA_CALL( cudaMallocManaged(&x, nbytes) );
        CUDA_CALL( cudaMallocManaged(&cmpresults_all, 4*sizeof(unsigned int)) );
        CUDA_CALL( cudaMallocManaged(&cmpresults_gmem, 2*sizeof(unsigned int)) );
        CUDA_CALL( cudaMallocManaged(&cmpresults_shmem, 2*sizeof(unsigned int)) );
        memset(cmpresults_all, 0, 4*sizeof(unsigned int));
        memset(cmpresults_gmem, 0, 2*sizeof(unsigned int));
        memset(cmpresults_shmem, 0, 2*sizeof(unsigned int));
        fprintf(stderr, "GPU #%d : allocated %.3f GByte\n", ctx->gpuId, double(nbytes) / 1073741824.0);

        // Pattern fill
        CUDA_CALL( cudaMemset(x, 0xAA, nbytes) );
        CUDA_CALL( cudaEventRecord(gstart, stream) );

#ifdef DBG_INTRODUCE_ERRORS
        // test: introduce error in global mem
        x[12] = 0xAAAAAABA;
#endif
    }

    // Periodically check memory content
    while (!ctx->stop) {
        time_t now = time(NULL);
        struct tm *tm_now = localtime(&now);
        char str_now[256];
        strftime(str_now, 255, "%Y-%m-%d %H:%M:%S", tm_now);
        float dt_msec, dt_msec_kernel, erate;

        boost::mutex::scoped_lock sharedGPUlock(m_gpuMutex);

        CUDA_CALL( cudaSetDevice(ctx->gpuId) );
        CUDA_CALL( cudaDeviceSynchronize() );

        // Error check 1+2 : compare global memory lower to upper half, while also testing for changes in (non-persistent) shared memory
        memset(cmpresults_all, 0, 4*sizeof(unsigned int));
        unsigned int* x_lo = x;
        unsigned int* x_hi = x_lo + nints/2;
        threadsPerBlock = cudaDevProp.warpSize;
        numBlocks = (cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor) / threadsPerBlock;
        CUDA_CALL( cudaEventRecord(kstart, stream) );
        cu_gshtest <<< numBlocks, threadsPerBlock, 0, stream >>> (x_lo, x_hi, cmpresults_all, nints/2);
        CUDA_CALL( cudaEventRecord(kstop, stream) );
        m_gpuMutex.unlock();

        // Wait for results of check 1+2
        CUDA_CALL( cudaEventSynchronize(kstop) );
        CUDA_CALL( cudaEventElapsedTime(&dt_msec_kernel, kstart, kstop) ); // kernel execution time
        CUDA_CALL( cudaEventElapsedTime(&dt_msec, gstart, kstop) );        // total time since start
        T_kernel_total_ms += dt_msec_kernel;
        fprintf(stderr, "%s : GPU #%d iter %zu : test kernel took %.2f s, cumulative kernel runtime now %.2f s, worker runtime %.2f s\n", str_now, ctx->gpuId, iter, dt_msec_kernel*1e-3, T_kernel_total_ms*1e-3, dt_msec*1e-3);

        // Error rates
        // global memory : nr of errors is the total nr of errors since program start, rate = #errors / runtime
        erate = cmpresults_all[0]/((dt_msec*1e-3)/3600.0);
        fprintf(stderr, "%s : GPU #%d iter %zu : GMEM  : %5u bit-err, %5u int32-err (cumulative)  : cumulative  bit-err rate %.3e err/hour\n", str_now, ctx->gpuId, iter, cmpresults_all[0], cmpresults_all[1], erate);
        // shared memory : nr of errors is occurrences detectable only during runtime of the most recent kernel launch
        erate = cmpresults_all[2]/(dt_msec_kernel*1e-3/3600.0);
        fprintf(stderr, "%s : GPU #%d iter %zu : SHMEM : %5u bit-err, %5u int32-err (current run) : current run bit-err rate %.3e err/hour\n", str_now, ctx->gpuId, iter, cmpresults_all[2], cmpresults_all[3], erate);
        // shared memory: translate per-kernel launch bit errors into cumulative errors and rate
        shmem_bit_err_accu += cmpresults_all[2];
        shmem_byte_err_accu += cmpresults_all[3];
        erate = shmem_bit_err_accu/((T_kernel_total_ms*1e-3)/3600.0);
        fprintf(stderr, "%s : GPU #%d iter %zu : SHMEM : %5zu bit-err, %5zu int32-err (cumulative)  : cumulative  bit-err rate %.3e err/hour\n", str_now, ctx->gpuId, iter, shmem_bit_err_accu, shmem_byte_err_accu, erate);

        // Error check 3: get the opinion of the hardware ECC on memory issues
        if (cudaDevProp.ECCEnabled) {
            // TODO. No clear method to check ECC status. Kernels/calls return error "cudaErrorECCUncorrectable" on uncorrectable errors,
            // but there is no access to ECC error counters via CUDA API nor CUDA Runtime API.
            // �'nvidia-smi -q -d EC' reports ECC errors in detail
        }

        iter++;
        if ((TEST_REPEAT_TIME_SECS - dt_msec_kernel*1e-3) >= 1) {
            sleep(TEST_REPEAT_TIME_SECS - dt_msec_kernel*1e-3);
        }
    }

    // Clean-up
    lockscope {
        boost::mutex::scoped_lock sharedGPUlock(m_gpuMutex);
        CUDA_CALL( cudaFree(x) );
        CUDA_CALL( cudaFree(cmpresults_gmem) );
        CUDA_CALL( cudaFree(cmpresults_shmem) );
    }

    return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    worker_context_t workers[16];
    int nworkers = 0;

    if (argc != 2) {
        usage();
    }

#ifdef DBG_INTRODUCE_ERRORS
    std::cout << "Warning: compiled in debug mode with artifical error generation enabled!\n";
#endif

    char* ch = strtok(argv[1], ",");
    while (ch != NULL) {
        std::cout << "Launching worker for GPU " << ch << std::endl;
        workers[nworkers].gpuId = atoi(ch);
        workers[nworkers].stop = false;
        workers[nworkers].thrd = new boost::thread(&gpuWorker, &workers[nworkers]);
        ch = strtok(NULL, ",");
        nworkers++;
    }

    int run = 1;
    while(run); // TODO: break somehow?

    for (int n=0; n<nworkers; n++) {
        workers[n].thrd->interrupt();
        workers[n].thrd->join();
        delete workers[n].thrd;
    }

    return 0;
}


