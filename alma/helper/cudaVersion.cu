#include <cuda.h>
#include <stdio.h>

int main(void)
{
    int ver;
    cudaError_t rc = cudaRuntimeGetVersion(&ver); // 8000 = v8.0, 9000 = v9.0, ...
    if (rc == cudaSuccess) {
        printf("%d\n", ver/1000);
    } else {
        printf("0\n");
    }
    return 0;
}

