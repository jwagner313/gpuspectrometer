/**
 * \class ASM
 *
 * \brief Interface definition for container of ASM functions
 *
 * \author $Author: janw $
 *
 */
#ifndef ASM_HXX
#define ASM_HXX

#include <string>
#include <map>

#include <boost/noncopyable.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>

#include "asm/ASMInterface.hxx"

class DRXPIface;
class SpectralProcessorIFace;
class XML2ApiRouter;

class ASM : public ASMInterface
{
    private:
        ASM();

    public:

        ASM(std::string configfile);
        ~ASM() { dstor(); }

    private:
        void dstor();

    public:

        /** Load settings from a configuration file (JSON) */
        bool loadConfigurationFile(const std::string&) { return true; }

        /** Start DRXP(s) */
        bool startDRXPs();

        /** Stop DRXP(s) */
        bool stopDRXPs();

        /** Start observing queue handler */
        bool startObservingQueue();

        /** Stop observing queue handlers */
        bool stopObservingQueue();

        /** Start M&C service */
        bool startMC();

        /** Stop M&C service */
        bool stopMC();

    public:
        /// Act on an XML-containing string and return an XML response
        std::string xmlHandover(std::string xmlIn);

    public:

        /// Accessors
        MCSpectrometerState& getStateContainer(void)    { return spectrometerstate; }
        MCSpectralBackend& getBackend(void)             { return backend; }
        DRXPIface* getDRXP(int ifaceindex=0)            { return drxp; }
        MCObservationQueue& getObservationQueue(void)   { return observationqueue; }
        MCConfigurationsMap_t& getConfigsMap(void)      { return configurations; }
        boost::property_tree::ptree& getUserConfig(void) { return userconfig; }
        MCSubarrays& getSubarrays(void)                 { return subarrays; }
        DRXPPCIeLookup& getDRXPLookup(void)             { return drxplookup; }
        CorrelationSettings& getCorrelationSettings(void) { return corrSettings; }

    public:

        /** Start service log */
        void startLogging(int verbosity=0, bool logfile_enabled=true);

    private:
        boost::property_tree::ptree userconfig; /// user-provided settings in a JSON file

        MCSpectrometerState spectrometerstate;  /// storage and accessor to internal state of spectrometer
        MCObservationQueue observationqueue;    /// storage and accessor to upcoming and past observations of M&C 'startObservation'
        MCConfigurationsMap_t configurations;   /// storage of all M&C command 'loadConfiguration { SingleConfigurationType }' items
        MCSubarrays subarrays;                  /// storage of all M&C command 'setSubArray { SubArrayType, 1..4 x SpectrometerInputType}' mappings

        MCSpectralBackend backend;              /// interface to spectral processing (GPU/CPU/iface)

    private:
        boost::thread* moncontrol_thread;       /// worker with XMLServer to accept TCP clients
        void moncontrolAcceptorWorker(int port);
        bool moncontrol_thread_keepalive;

        XML2ApiRouter* xml2apirouter;           /// mapping from XML M&C commands to ASM API functions and settings
        boost::mutex xml2apirouter_mutex;       /// prevent parallel calls to xml2apirouter functions

    private:
        void prepareDRXP();

        DRXPPCIeLookup drxplookup;
        DRXPIface* drxp;
        MCDRXPStatus* drxpstatus;

    private:
        ResultRecipient* resultwriter;          /// MIME or simulation or none (ResultRecipientMIME, ResultRecipientSim, ResultRecipientPlain, ResultRecipientNoOp)

    private:
        void observationQueueWorker();          /// Monitor the MCObservationQueue and control the MCSpectralBackend accordingly
        boost::thread* observationQueueWorker_thread;
        bool observationQueueWorker_keepalive;

        MCObservation* observationGetNext();        /// Get the next observation in queue
        bool observationPrepare(MCObservation*);    /// Prepare for running the observation
        bool observationRun(MCObservation*);        /// Wait and check while observation is running
        bool observationFinish(MCObservation*, bool faulted=false); /// Clean up after end of observation

    private:
        CorrelationSettings corrSettings;

};

#endif // ASM_HXX
