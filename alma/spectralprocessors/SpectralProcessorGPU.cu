/**
 * \class SpectralProcessorGPU
 *
 * \brief Spectral processing on CUDA device(s)
 *
 * \author $Author: janw $
 *
 */

#include "global_defs.hxx"
#include "spectralprocessors/SpectralProcessorGPU.hxx"
#include "spectralprocessors/SpectralProcessorGPU_cuda_worker_t.hxx"
#include "spectralprocessors/SpectralProcessorGPU_cuda_config_pool_t.hxx"
#include "spectralprocessors/SpectralPostprocessor.hxx"
#include "datarecipients/ResultRecipient_iface.hxx"
#include "datarecipients/SpectralResultSet.hxx"
#include "mcwrapping/mc_configuration.hxx"
#include "mcwrapping/mc_observation.hxx"
#include "time/TimeUTC.hxx"
#include "drxp/DRXPInfoFrame.hxx"
#include "drxp/DRXPIface.hxx"
#include "drxp/drxp_const.hxx"
#include "helper/logger.h"

#ifdef GPU_DEBUG
    #define CHECK_TIMING 1
#else
    #define CHECK_TIMING 0
#endif
#include "cuda_utils.cu"
#include "histogram_kernels.cu"
#include "decoder_3b32f_kernels.cu"
#include "arithmetic_xmac_kernels.cu"
#include "arithmetic_autospec_kernels.cu"
#include "arithmetic_winfunc_kernels.cu"

#include <cuda.h>
#include <cufft.h>
#include <cufftXt.h>
#include <cuda_profiler_api.h>

#include <assert.h>
#include <getopt.h>
#include <malloc.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define lockscope                // just a blank define use for nicer syntax of "{ boost::mutex::scoped_lock x(mtx); ... }" scopes
#define ORDER_STREAM_FIRST       // define to distribute tasks stream-first, undefine to distribute in GPU-first order
#define REPORT_SEGMENT_RESULTS 1 // 1 to report each completed segment

#ifndef MIN
    #define MIN(a,b) (((a)<(b))?(a):(b))
    #define MAX(a,b) (((a)>(b))?(a):(b))
#endif

inline bool operator<(const struct timespec& t0, const struct timespec& t1)
{
    return (t0.tv_sec < t1.tv_sec) || (t0.tv_sec == t1.tv_sec && t0.tv_nsec < t1.tv_nsec);
}

void L_CUDA_PRINT_MEMORY_INFO(const char* msg)
{
    size_t free, total;
    int device;
    CUDA_CALL(cudaGetDevice(&device));
    CUDA_CALL(cudaMemGetInfo(&free,&total));
    L_(linfo) << "GPU : CUDA device " << device << " memory: " << msg << ": " << (free*(1/1073741824.0)) << " GB free out of " << (total*(1/1073741824.0)) << " GB total";
}

static int64_t getMillisecSince(const struct timespec* arg, const struct timespec* ref)
{
    int64_t dt_msec = 1000*((int64_t)arg->tv_sec - ref->tv_sec);
    dt_msec += ((int64_t)arg->tv_nsec - ref->tv_nsec + 1000000/2)/1000000; // rounded to closest millisec
    return dt_msec;
}

static inline void cpuAccumulatef32(const float* __restrict__ src, float * __restrict__ dst, const int nfloats)
{
    const int step = 8;
    int n = 0;
    for ( ; n <= (nfloats-step); n += step) {
        for (int k = 0; k < step; k++) {
            dst[n+k] += src[n+k];
            dst[n+k] += src[n+k];
            dst[n+k] += src[n+k];
            dst[n+k] += src[n+k];
        }
    }
    for ( ; n < nfloats; n++) {
        dst[n] += src[n];
    }
}

/////////////////////////////////////////////////////////////////////////////////////

SpectralProcessorGPU::SpectralProcessorGPU()
{
    // Member vars to zero
    m_init_done = false;
    pimpl = new cuda_config_pool_t;
    m_shutdown_monitoringThread = true;
    m_monitoringThread = NULL;
    m_shutdown_specHandlerThread = true;
    m_specHandlerThread = NULL;

    /* Default affinity, can override with ::setAffinity() before ::initialize() */
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    CUDA_CALL( cudaGetDeviceCount(&cp->nGPUsInstalled) );
    cp->nGPUs = cp->nGPUsInstalled;
    for (int g = 0; g < cp->nGPUs; g++) {
        cp->gpuIds[g] = g;
    }
}

SpectralProcessorGPU::~SpectralProcessorGPU()
{
    if (pimpl != NULL) {
        deinitialize();
        delete (cuda_config_pool_t*)pimpl;
        pimpl = NULL;
    }

    /* Flush profiling data. Does something only when ASM started under 'nvvp' NVidia Profiler. */
    cudaProfilerStop();
}

/////////////////////////////////////////////////////////////////////////////////////

bool SpectralProcessorGPU::reset()
{
    if (m_init_done) {
        deinitialize();
    }

    /* Reset all assigned GPUs () */
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    for (int g = 0; g < cp->nGPUs; g++) {
        boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
        CUDA_CALL( cudaSetDevice(cp->gpuIds[g]) );
        CUDA_CALL( cudaDeviceSynchronize() );
        CUDA_CALL( cudaDeviceReset() ); // "Destroy all allocations and reset all state on the current device in the **current process**."
    }

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Set up GPUs for a new observation, potentially with changed configuration.
 */
bool SpectralProcessorGPU::initialize(MCObservation* obs)
{
    boost::mutex::scoped_lock poollock(m_implMutex);
    assert(obs != NULL);
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;

    if (m_init_done) {
        // TODO: de-init first then re-init?
        return false;
    }

    /* General settings */
    cp->obsRef = obs;    // pointer to Observation object
    cp->scfg = obs->cfg; // copy of entire object, for internal modifications
    cp->scfg.updateDerivedParamsFrom(cp->obsRef);
    cp->scfg.mutex.lock();
    cp->scfg.procConfig.nstokes = 4;
    cp->scfg.procConfig.Tsubint_max_msec = DRXP_DT_MSEC / 2;

    /* GPU-specific settings */
    cp->scfg.procConfig.nGPUs = cp->nGPUs;
    cp->scfg.procConfig.nstreams = 2; // TODO: get from somewhere?
    for (int g = 0; g < cp->scfg.procConfig.nGPUs; g++) {
        cp->scfg.procConfig.mapGPUs[g] = cp->gpuIds[g];
    }
    cp->scfg.mutex.unlock();

    /* Clear any old stored spectral results */
    cp->resultsManager.clear();

    /* Allocate GPU and workers */
    if (!allocate()) {
        return false;
    }

    /* Launch spectral results monitor and writeout thread */
    startSpectralWriteout();

    m_init_done = true;
    L_(linfo) << "Main GPU object initialized, worker threads still initializing in background";
    return true;
}

bool SpectralProcessorGPU::deinitialize()
{
    boost::mutex::scoped_lock poollock(m_implMutex);
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;

    /* Already deinitialized? */
    if (!m_init_done || (cp == NULL)) {
        return false;
    }

    /* Stop all workers */
    deallocate();

    /* Stop status multicast */
    stopPeriodicMonitoring();

    /* Stop spectral results monitor and writeout thread; after that we handle leftover spectra below */
    stopSpectralWriteout();

    /* Write out any yet unwritten complete spectra */
    handleCompletedSpectra();

    /* Look for any leftover partially-integrated spectra */
    int nincomplete = 0;
    float wincomplete = 0.0f;
    while (1) {
        SpectralResultSet* r = cp->resultsManager.first();
        if (r == NULL) {
            break;
        }
        L_(lwarning) << "At GPU deinit had incomplete spectrum with ID#" << r->id << " with " << r->total_msec << "ms of data";
        cp->resultsManager.setCompleted(r->id);
        r->weight = float(r->total_msec) / float(cp->scfg.procConfig.Tswitchingcycle_msec);
        wincomplete += r->weight;
        nincomplete++;
        lockscope {
            boost::mutex::scoped_lock handlerlock(cp->resultshandlermutex);
            if (1 && (cp->obsRef != NULL) && (cp->resultsHandler != NULL)) {
                // L_(linfo) << "Writing out incomplete spectrum with ID#" << r->id << ", weight " << std::fixed << std::setprecision(4) << r->weight;
                cp->postProcess.process(r, &cp->scfg);
                r->setPostprocessed();
                r->deallocateRaw();
                cp->resultsHandler->takeResult(r);
            } else {
                L_(lwarning) << "Leftover spectrum to write out, but have no obs or writer! obs=" << (void*)cp->obsRef << " resultsHandler=" << (void*)cp->resultsHandler;
            }
        }
        r->lock.unlock();
        cp->resultsManager.releaseCompleted(r->id);
    }

    lockscope {
        boost::mutex::scoped_lock handlerlock(cp->resultshandlermutex);
        if (cp->resultsHandler != NULL) {
            cp->resultsHandler->sync();
        }
    }

    if (nincomplete > 0) {
        L_(linfo) << "At GPU deinit had a total of " << nincomplete << " incompletely time integrated spectra, average weight " << wincomplete/float(nincomplete);
    }

    /* Update the final state of the observation */
    if (cp->obsRef != NULL) {
        cp->obsRef->mutex.lock();
        if (cp->obsRef->state == MCObservation::Pending) {
            cp->obsRef->state = MCObservation::Missed;
        } else if (cp->obsRef->state == MCObservation::Ongoing) {
            cp->obsRef->state = MCObservation::Stopped;
        }
        cp->obsRef->mutex.unlock();
        cp->obsRef = NULL;
    }

    m_init_done = false;

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Allocate arrays (host, device) required to run FFT spectrometer.
 * Caller holds mutex m_implMutex.
 *
 * @param fs Pointer to FFT configuration in which to allocate arrays.
 * @return true on success
 */
bool SpectralProcessorGPU::allocate()
{
    assert(this->pimpl != NULL);

    /* Prepare workers */
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    cp->nworkers = cp->scfg.procConfig.nGPUs * cp->scfg.procConfig.nstreams;
    assert(cp->nworkers <= CUDA_CFG_POOL_MAXWORKERS);

    /* Map workers to (stream,GPU) or (GPU,stream) tuple */
    int w = 0;
#ifdef ORDER_STREAM_FIRST
    // workers [0..N] by stream-first order
    for (int g = 0; g < cp->scfg.procConfig.nGPUs; g++) {
        for (int s = 0; s < cp->scfg.procConfig.nstreams; s++, w++) {
#else
    // workers [0..N] by GPU-first order
    for (int s = 0; s < cp->scfg.procConfig.nstreams; s++) {
        for (int g = 0; g < cp->scfg.procConfig.nGPUs; g++, w++) {
#endif
            cp->pool[w].g = cp->scfg.procConfig.mapGPUs[g];
            cp->pool[w].s = s;
            cp->pool[w].n = w;
            cp->pool[w].state = cuda_worker_t::Idle;
            cp->pool[w].scfg = cp->scfg; // copy of a copy of obsRef->cfg
        }
    }
    cp->peak_busy_workers = 0;
    cp->min_idle_workers = cp->nworkers;

    /* Launch each worker, letting it do its own allocations */
    for (int n=0; n < cp->nworkers; n++) {
        cp->pool[n].initialized = false;
        cp->pool[n].terminate = false;
        cp->pool[n].state = cuda_worker_t::Exited;
    }
    for (int n=0; n < cp->nworkers; n++) {
        void* slotcontext = (void*)&(cp->pool[n]);
        cp->pool[n].workerthread = new boost::thread(&SpectralProcessorGPU::workerThread, this, slotcontext);
    }

    return true;
}

/**
 * Deallocate arrays (host, device) used by FFT spectrometer.
 * Caller holds mutex m_implMutex.
 *
 * @return true on success
 */
bool SpectralProcessorGPU::deallocate()
{
    assert(this->pimpl != NULL);
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    for (int n=0; n < cp->nworkers; n++) {
        cp->pool[n].terminate = true;
        cp->pool[n].workerthread->interrupt();
        cp->pool[n].newdatacond.notify_one(); // wake up if waiting for data
    }
    for (int n=0; n < cp->nworkers; n++) {
        cp->idlegpucond.notify_all(); // de-block reserveSlot() just in case
        cp->pool[n].workerthread->join();
        delete cp->pool[n].workerthread;
        cp->pool[n].workerthread = NULL;
    }
    cp->nworkers = 0;
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Reserve a processing slot and return a handle to it
 * \return Slot handle >=0 on success, -1 on error
 */
int SpectralProcessorGPU::reserveSlot()
{
    assert(this->pimpl != NULL);
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    static int n0 = 0;

    /* Quick statistical check of busy/idle counts */
    int nbusy = 0, nidle = 0;
    for (int n = 0; n < cp->nworkers; n++) {
        if (!cp->pool[n].terminate && cp->pool[n].initialized) {
            if (cp->pool[n].state == cuda_worker_t::Idle) { nidle++; } else { nbusy++; }
        }
    }
    cp->peak_busy_workers = (nbusy > cp->peak_busy_workers) ? nbusy : cp->peak_busy_workers;
    cp->min_idle_workers = (nidle < cp->min_idle_workers) ? nidle : cp->min_idle_workers;

retry:
    /* Look up an idle worker, if any. With proper mutexing. */
    int nuninitialized = 0, nalive = 0;
    lockscope {
        //boost::mutex::scoped_lock poollock(m_implMutex);
        for (int n = 0; n < cp->nworkers; n++) {
            int s = (n + n0) % cp->nworkers;
            // Try to lock worker; to avoid a long wait, don't do a blocking lock()!
            bool got_lock = cp->pool[s].workermutex.try_lock();
            if (!got_lock) {
                nalive++;
                continue;
            }
            // Worker in non-initialized state
            if (cp->pool[s].terminate) {
                cp->pool[s].workermutex.unlock();
                continue;
            }
            if (!cp->pool[s].initialized) {
                cp->pool[s].workermutex.unlock();
                nuninitialized++;
                continue;
            }
            // Worker is running, and idle?
            if (cp->pool[s].state == cuda_worker_t::Idle) {
                cp->pool[s].state = cuda_worker_t::Reserved;
                cp->pool[s].workermutex.unlock();
                n0 = s + 1; // for round-robin assignment
                return s;
            } else {
                // Worker is running, but busy
                cp->pool[s].workermutex.unlock();
                nalive++;
            }
        }
    }

    // No workers running nor starting up
    if ((nalive == 0) && (nuninitialized == 0)) {
        return -1;
    }

    // Got no GPU slot but some were still initializing, try again!
    if (nuninitialized > 0) {
        boost::this_thread::yield();
        //std::cout << "nuninitialized = " << nuninitialized << "\n";
        goto retry;
    }

    // All GPU were busy, wait till some GPU signals 'idlegpucond'
    if (nalive > 0) {
        boost::mutex::scoped_lock poollock(m_implMutex);
        cp->peak_busy_workers = nalive;
        cp->idlegpucond.wait(poollock);
        goto retry;
    }

    // All GPU busy, return an error
    return -1;
}

/////////////////////////////////////////////////////////////////////////////////////

/** Performance optimization related: function to lock pages into memory */
void SpectralProcessorGPU::pinUserBuffers(int Nbuffers, void** buffers, size_t buflen)
{
    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
    const size_t DRXP_DMA_BLOCK_SIZE = 4194304; // DMA block size that drxpd.ko driver uses internally when allocating memory
    bool failed = false;
    cudaError_t rc;

    if (buffers == NULL || Nbuffers < 0) return;

    // Register every large buffer as 4MB -sized pieces of memory
    for (int n = 0; n < Nbuffers; n++) {
        if (buffers[n] != NULL) {
            //std::cerr << "pinUserBuffers buf #" << (n+1) << " of " << Nbuffers << ", p=" << buffers[n] << ", len=" << buflen << std::endl;
            char *blkptr = (char*)buffers[n];
            for (size_t nreg = 0; nreg < buflen; nreg += DRXP_DMA_BLOCK_SIZE) {
                // Note 1: cudaSetDevice() is not needed
                // Note 2: flag cudaHostRegisterIoMemory fails if non-root, allow to fail
                rc = cudaHostRegister(blkptr, DRXP_DMA_BLOCK_SIZE, cudaHostRegisterIoMemory);
                blkptr += DRXP_DMA_BLOCK_SIZE;
                if (rc != cudaSuccess) {
                    failed = true;
                }
            }
        }
    }

    // Did all cudaHostRegister()'s succeed?
    assert(this->pimpl != NULL);
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    cp->drxpMemoryIsPinned = !failed;
}

/** Performance optimization related: function to unlock pages in memory */
void SpectralProcessorGPU::unpinUserBuffers(int Nbuffers, void** buffers, size_t buflen)
{
    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
    const size_t DRXP_DMA_BLOCK_SIZE = 4194304; // DMA block size that drxpd.ko driver uses internally when allocating memory
    assert(this->pimpl != NULL);
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;

    if (buffers == NULL || Nbuffers < 0) return;
    if (buflen <= 0) return;
    if (!cp->drxpMemoryIsPinned) return;

    for (int n = 0; n < Nbuffers; n++) {
        if (buffers[n] != NULL) {
            char *blkptr = (char*)buffers[n];
            for (size_t nreg = 0; nreg < buflen; nreg += DRXP_DMA_BLOCK_SIZE) {
                cudaHostUnregister(blkptr);
                blkptr += DRXP_DMA_BLOCK_SIZE;
            }
        }
    }
}

/** Set the recipient for spectral results */
void SpectralProcessorGPU::setSpectralRecipient(ResultRecipient* r)
{
    assert(this->pimpl != NULL);
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    lockscope {
        boost::mutex::scoped_lock handlerlock(cp->resultshandlermutex);
        for (int n = 0; n < cp->nworkers; n++) {
            cp->pool[n].workermutex.lock();
        }
        cp->resultsHandler = r;
        for (int n = 0; n < cp->nworkers; n++) {
            cp->pool[n].workermutex.unlock();
        }
    }
}

/** Assign to a subset of devices (GPU) */
void SpectralProcessorGPU::setAffinity(int ndevices, const int* deviceIds)
{
    assert(this->pimpl != NULL);
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    lockscope {
        boost::mutex::scoped_lock poollock(m_implMutex);
        cp->nGPUs = (ndevices <= CUDA_CFG_POOL_MAXWORKERS) ? ndevices : CUDA_CFG_POOL_MAXWORKERS;
        for (int g = 0; g < ndevices; g++) {
            cp->gpuIds[g] = deviceIds[g];
            if (cp->gpuIds[g] >= cp->nGPUsInstalled) {
                L_(lwarning) << "GPU setAffinity() mapping " << g << " device " << deviceIds[g] << " exceeds available IDs of 0.." << (cp->nGPUsInstalled-1);
                cp->gpuIds[g] %= cp->nGPUsInstalled;
            }
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////

/** Data PUSH method; accept new data via callback from DRXP.
 * \return True if data will be used and group free later, False if group can be freed immediately
 */
bool SpectralProcessorGPU::takeData(RawDRXPDataGroup data)
{
    cuda_config_pool_t* cp = (cuda_config_pool_t*)(this->pimpl);

    if (!m_init_done) {
        L_(lwarning) << "GPU::takeData() invoked but not initialized yet, discarding data";
        return false;
    }

    // Make sure we catch background changes to Observation stopTime, and changes in Observation state
    cp->obsRef->mutex.lock();
    cp->scfg.mutex.lock();
    cp->scfg.procConfig.requestedStopTime = cp->obsRef->ts_stopTimeUTC;
    bool obsIsRunning = (cp->obsRef->state == MCObservation::Pending) || (cp->obsRef->state == MCObservation::Ongoing);
    cp->scfg.mutex.unlock();
    cp->obsRef->mutex.unlock();

    // Make sure Observation is in data-accepting state
    if (!obsIsRunning) {
        L_(ldebug) << "GPU::takeData() : observation no longer marked as Pending or Ongoing, discarding data";
        return false;
    }

    // Make sure data is in timerange of observation
    if (cp->scfg.procConfig.requestedStartTime.tv_sec != 0) {
        int64_t msec_from_start = getMillisecSince(&data.frameinfo->swRxTime_UTC, &cp->scfg.procConfig.requestedStartTime);
        if (msec_from_start <= -DRXP_DT_MSEC) {
            L_(ldebug) << "GPU::takeData() : data timestamp not yet in range of observation (still " << (-msec_from_start) << " msec)";
            return false;
        }
    }
    if (cp->scfg.procConfig.requestedStopTime.tv_sec != 0) {
        int64_t msec_from_stop = getMillisecSince(&data.frameinfo->swRxTime_UTC, &cp->scfg.procConfig.requestedStopTime);
        // Stop the observation if stopTime is past
        if (msec_from_stop >= 0) {
            L_(ldebug) << "GPU::takeData() : data timestamp past the range of observation (since " << msec_from_stop << " msec)";
            cp->obsRef->mutex.lock();
            cp->obsRef->state = MCObservation::Stopped;
            cp->obsRef->mutex.unlock();
            return false;
        }
    } else {
        L_(ldebug) << "GPU::takeData() : no stop time is set, maybe will be set later?";
    }

    // Are the data needed?
    if (!cp->resultsManager.isFrameDataIntegratable(data.frameinfo, &cp->scfg)) {
        // Nothing to do with any part of the data, release it back to DRXP
        L_(ldebug1) << "GPU::takeData() drop frame due not isFrameDataIntegratable()";
        return false;
    }

    // Find and reserve an idle slot
    int h = reserveSlot();
    if (h < 0) {
        L_(lwarning) << " GPU::takeData() : all GPU are offline, discarding data";
        // boost::this_thread::yield();
        // h = reserveSlot(); // TODO: give up after some time?
        return false;
    }

    // Begin copying of the raw 48ms input data into our reserved slot, while also resetting old spectral accumulators
    cuda_worker_t& cs = cp->pool[h];
    assert((size_t)data.bufSize == DRXP_RX_DATA_SIZE);
    lockscope {
        boost::mutex::scoped_lock lock(cs.workermutex);
        cs.h_rawgroup = data;
        cs.spec_weight = data.weight;
    }
    lockscope {
        boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
        CUDA_CALL( cudaSetDevice(cs.g) );
        CUDA_CALL( cudaEventRecord(cs.evt_start, cs.sid_h2d) );
        for (int s = 0; s < cs.max_ints_per_48ms; s++) {
            CUDA_CALL( cudaMemsetAsync(cs.d_powspecs[s], 0x00, cs.powspecslen, cs.sid_h2d) );
        }
        CUDA_TIMING_START(cs.evt_kstart, cs.sid_h2d);
        if (!cp->drxpMemoryIsPinned) {
            CUDA_CALL( cudaMemcpyAsync(cs.d_rawbuf, data.buf, data.bufSize, cudaMemcpyHostToDevice, cs.sid_h2d) );
            L_(ldebug1) << "GPU::takeData() start copying to GPU " << (cs.g) << " in stream " << (cs.sid_h2d);
        } else {
            // DRXP DMA Ring Buffer was pinned into CUDA, must xfer in the same 4MB-sized blocks that DRXP kmalloc() used
            const size_t DRXP_DMA_BLOCK_SIZE = 4194304;
            char* d_dst = (char*)cs.d_rawbuf;
            const char* h_src_ringbuf = (const char*)data.buf;
            for (size_t ncp = 0; ncp < DRXP_RX_GROUP_SIZE; ncp += DRXP_DMA_BLOCK_SIZE) {
                CUDA_CALL( cudaMemcpyAsync(d_dst, h_src_ringbuf, DRXP_DMA_BLOCK_SIZE, cudaMemcpyHostToDevice, cs.sid_h2d) );
                d_dst += DRXP_DMA_BLOCK_SIZE;
                h_src_ringbuf += DRXP_DMA_BLOCK_SIZE;
            }
            L_(ldebug1) << "GPU::takeData() start direct DRXP RB copying to GPU " << (cs.g) << " in stream " << (cs.sid_h2d);
        }
        CUDA_CALL( cudaEventRecord(cs.evt_raw_transferred, cs.sid_h2d) );
        CUDA_TIMING_STOP_M(cs.evt_kstop, cs.evt_kstart, cs.sid_h2d, m_cuCtxMutex, "cpy h2d/pol", DRXP_DT_MSEC*DRXP_SAMP_PER_MILLISEC);
    }

    // Wake up the worker
    cs.newdatacond.notify_one();

    // Return with 'true' to signal the caller that GPU will free up the group later
    return true;
}

/////////////////////////////////////////////////////////////////////////////////////

bool SpectralProcessorGPU::allocateWorker(int slot)
{
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    cuda_worker_t* cs = &(cp->pool[slot]);
    boost::mutex::scoped_lock lock(cs->workermutex);

    /* Name in messages */
    std::stringstream ssname;
    ssname << "GPU Worker[" << (cs->g) << "," << (cs->s) << "] ";
    cs->name = ssname.str();

    /* Derived settings */
    CUDA_CALL( cudaGetDeviceProperties(&cs->devprop, cs->g) );
    cs->max_ints_per_48ms = /*earlier tailing 48ms*/ 1
        + /*complete from inside 48ms*/ int(float(DRXP_DT_MSEC) / (1e3*cs->scfg.getMinDwellSeconds()))
        + /*partial tailing from 48ms*/ 1;
    cs->d_powspecs = (float**)malloc(cs->max_ints_per_48ms * sizeof(float*));
    cs->h_powspecs = (float**)malloc(cs->max_ints_per_48ms * sizeof(float*));
    cs->powspecslen = sizeof(float)*(cs->scfg.procConfig.nchan+0)*cs->scfg.procConfig.nstokes;
    cs->npol = cs->scfg.procConfig.npol;
    cs->spec_weight = SPEC_INVALID_FLAG;

    /* Overlapped FFT settings */
    // example: 2*nchan = 2^20-point DFT, 262.144 usec/DFT ~= 183.11 FFTs in 48ms of no overlap
    //          fftinstep = 1e6 samples, 250 usec/step ~= 192 FFTs in 48ms with some percent overlap, 4 FFTs in 1ms
    size_t nfft_per_subint = (cs->scfg.procConfig.Tsubint_max_msec*DRXP_SAMP_PER_MILLISEC) / cs->scfg.procConfig.fftinstep;
    size_t fftbytes_in  = sizeof(float)*((nfft_per_subint-1)*cs->scfg.procConfig.fftinstep + 2*cs->scfg.procConfig.nchan + /*margin*/cs->scfg.procConfig.nchan)*cs->scfg.procConfig.npol;
    size_t fftbytes_out = 2*sizeof(float)*(cs->scfg.procConfig.nchan+1)*(nfft_per_subint+/*margin*/2)*cs->scfg.procConfig.npol;

    //std::cout << "allocateWorker(" << slot << ") : max_ints_per_48ms=" << cs->max_ints_per_48ms << ", fftbytes_in=" << fftbytes_in << ", fftbytes_out=" << fftbytes_out << ", nfft_per_subint=" << nfft_per_subint << " fftinstep=" << cs->scfg.procConfig.fftinstep << std::endl;

    /* GPU allocs while correct GPU is selected */
    L_(linfo) << cs->name << "initializing CUDA arrays";
    lockscope {
        boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);

        CUDA_CALL( cudaSetDevice(cs->g) );
        L_CUDA_PRINT_MEMORY_INFO("prior to allocs");

        /* Create time and task tracking events */
        CUDA_CALL( cudaEventCreateWithFlags(&cs->evt_start, cudaEventBlockingSync) );
        CUDA_CALL( cudaEventCreateWithFlags(&cs->evt_stop, cudaEventBlockingSync) );
        CUDA_CALL( cudaEventCreateWithFlags(&cs->evt_raw_transferred, cudaEventDisableTiming|cudaEventBlockingSync) );
        CUDA_CALL( cudaEventCreateWithFlags(&cs->evt_raw_overwriteable, cudaEventDisableTiming|cudaEventBlockingSync) );
        CUDA_CALL( cudaEventCreateWithFlags(&cs->evt_spectrum_avail, cudaEventDisableTiming|cudaEventBlockingSync) );
        CUDA_CALL( cudaEventCreateWithFlags(&cs->evt_sync_with_yield, cudaEventDisableTiming|cudaEventBlockingSync) );
        CUDA_CALL( cudaEventCreate(&cs->evt_kstart) );
        CUDA_CALL( cudaEventCreate(&cs->evt_kstop) );

        /* Set up stream(s) */
        CUDA_CALL( cudaStreamCreate( &(cs->sid) ) );
        CUDA_CALL( cudaStreamCreate( &(cs->sid_h2d) ) );
        CUDA_CALL( cudaStreamCreate( &(cs->sid_d2h) ) );

        /* Raw input data (GPU side) */
        //CUDA_CALL( cudaMalloc( (void **)&(cs->d_rawbuf), DRXP_RX_DATA_SIZE ) );
        CUDA_CALL( cudaMalloc( (void **)&(cs->d_rawbuf), DRXP_RX_GROUP_SIZE ) ); // allocate some extra in case DRXP RingBuffer will be pinned/registered into CUDA
        CUDA_CALL( cudaMemset( cs->d_rawbuf, 0xFF, DRXP_RX_DATA_SIZE ) );
        L_CUDA_PRINT_MEMORY_INFO("after raw data alloc");

        /* Histogram related array(s) */
        const int nlevels = 8;
        CUDA_CALL( cudaMalloc((void **)&(cs->d_hist_X), nlevels*sizeof(hist_t)) );
        CUDA_CALL( cudaMalloc((void **)&(cs->d_hist_Y), nlevels*sizeof(hist_t)) );
        CUDA_CALL( cudaMallocHost((void **)&(cs->h_hist_X), nlevels*sizeof(hist_t), cudaHostAllocDefault) );
        CUDA_CALL( cudaMallocHost((void **)&(cs->h_hist_Y), nlevels*sizeof(hist_t), cudaHostAllocDefault) );
        memset(cs->h_hist_X, 0x00, nlevels*sizeof(hist_t));
        memset(cs->h_hist_Y, 0x00, nlevels*sizeof(hist_t));

        /* FFT-related arrays */
        CUDA_CALL( cudaMalloc( (void **)&(cs->d_fft_in),  fftbytes_in  ) );
        CUDA_CALL( cudaMalloc( (void **)&(cs->d_fft_out), fftbytes_out ) );
        CUDA_CALL( cudaMemset( cs->d_fft_in,   0x00, fftbytes_in ) );
        CUDA_CALL( cudaMemset( cs->d_fft_out,  0x00, fftbytes_out ) );
        L_CUDA_PRINT_MEMORY_INFO("after FFT in&out alloc");

        /* Time integrated output data. Nyquist point N/2+1 is discarded. */
        for (int s = 0; s < cs->max_ints_per_48ms; s++) {
            CUDA_CALL( cudaMalloc( (void **)&(cs->d_powspecs[s]), cs->powspecslen ) );
            CUDA_CALL( cudaMallocHost( (void **)&(cs->h_powspecs[s]), cs->powspecslen, cudaHostAllocPortable ) );
            CUDA_CALL( cudaMemset( cs->d_powspecs[s], 0x00, cs->powspecslen) );
            memset(cs->h_powspecs[s], 0x00, cs->powspecslen);
        }
        L_CUDA_PRINT_MEMORY_INFO("after output spectrum alloc");

        /* FFT plans */
        // Note: cuFFT API offers a multi-GPU capability (cufftXtSetGPUs(), cufftXtExecDescriptorC2C()) in which
        // intermediate data will be transferred GPU-to-GPU. This makes most sense for 3D FFT, not 1D FFT.
        // Config that throws away Nyquist bin
        int dimn[1] = {2*cs->scfg.procConfig.nchan};  // r2c DFT size
        int inembed[1] = {0};         // ignored for 1D xform
        int onembed[1] = {0};         // ignored for 1D xform
        int istride = 1;              // step between successive in elements
        int ostride = 1;              // step between successive out elements
        int idist = cs->scfg.procConfig.fftinstep;    // step between batches (cufft r2c input = real)
        int odist = cs->scfg.procConfig.nchan+1;      // step between batches (cufft r2c output = 1st Nyquist only)
                                  // with +0 rather than +1 we'd let overwrite the N/2+1 point,
                                  // works in principle but in practice found that CUFFT corrupts 1..3 bins near DC,
                                  // --> let CUFFT produce Nbins/2+1 output points and let later kernels hop over Nyquist
        CUFFT_CALL( cufftPlanMany(&(cs->cufftplan), 1, dimn,
            inembed, istride, idist,
            onembed, ostride, odist,
            CUFFT_R2C,
            nfft_per_subint * cs->scfg.procConfig.npol)
        );
        L_(linfo) << cs->name << "using " << nfft_per_subint << "*" << cs->scfg.procConfig.npol << "+1 -batched "
                  << 2*cs->scfg.procConfig.nchan << "-point DFT with input stride of " << idist << " samples, and "
                  << odist << " output channels";
        #if defined(CUDA_VERSION) && (CUDA_VERSION < 8000)
        CUFFT_CALL( cufftSetCompatibilityMode(cs->cufftplan, CUFFT_COMPATIBILITY_NATIVE) );
        #endif
        CUFFT_CALL( cufftSetStream(cs->cufftplan, cs->sid) );
        //L_CUDA_PRINT_MEMORY_INFO("after CuFFT plan");
        L_CUDA_PRINT_MEMORY_INFO("after GPU stream allocations");
    }

    /* Mark as ready */
    boost::this_thread::yield();
    CUDA_CALL( cudaStreamSynchronize(cs->sid) );
    cs->initialized = true;
    cs->state = cuda_worker_t::Idle;
    L_(linfo) << cs->name << "finished initializing CUDA arrays";

    lock.unlock();
    cs->idlegpucond->notify_one(); // wake up potential waiters

    return true;
}

bool SpectralProcessorGPU::deallocateWorker(int slot)
{
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    cuda_worker_t* cs = &(cp->pool[slot]);
    boost::mutex::scoped_lock lock(cs->workermutex);

    if (!cs->initialized) {
        L_(linfo) << cs->name << "attempted to de-initialize CUDA arrays, but was already marked as not nitialized!";
        return false;
    }

    /* Sync up on the main stream of that GPU */
    CUDA_CALL( cudaStreamSynchronize(cs->sid) );
    cs->initialized = false;
    cs->state = cuda_worker_t::Exited;

    /* Select correct GPU */
    lockscope {
        boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
        L_(linfo) << cs->name << "de-initializing CUDA arrays";
        CUDA_CALL( cudaSetDevice(cs->g) );

        /* Destroy time and task tracking events */
        CUDA_CALL( cudaEventDestroy(cs->evt_start) );
        CUDA_CALL( cudaEventDestroy(cs->evt_stop) );
        CUDA_CALL( cudaEventDestroy(cs->evt_raw_transferred) );
        CUDA_CALL( cudaEventDestroy(cs->evt_raw_overwriteable) );
        CUDA_CALL( cudaEventDestroy(cs->evt_spectrum_avail) );
        CUDA_CALL( cudaEventDestroy(cs->evt_sync_with_yield) );
        CUDA_CALL( cudaEventDestroy(cs->evt_kstart) );
        CUDA_CALL( cudaEventDestroy(cs->evt_kstop) );

        /* Destroy stream(s) */
        CUDA_CALL( cudaStreamDestroy(cs->sid) );
        CUDA_CALL( cudaStreamDestroy(cs->sid_h2d) );
        CUDA_CALL( cudaStreamDestroy(cs->sid_d2h) );

        /* Free raw input data (GPU side) */
        CUDA_CALL( cudaFree(cs->d_rawbuf) );

        /* Free histogram related array(s) */
        CUDA_CALL( cudaFree(cs->d_hist_X) );
        CUDA_CALL( cudaFree(cs->d_hist_Y) );
        CUDA_CALL( cudaFreeHost(cs->h_hist_X) );
        CUDA_CALL( cudaFreeHost(cs->h_hist_Y) );

        /* Free FFT-related arrays */
        CUDA_CALL( cudaFree(cs->d_fft_in) );
        CUDA_CALL( cudaFree(cs->d_fft_out) );

        /* Free time integrated output data */
        for (int s = 0; s < cs->max_ints_per_48ms; s++) {
            CUDA_CALL( cudaFree(cs->d_powspecs[s]) );
            CUDA_CALL( cudaFreeHost(cs->h_powspecs[s]) );
        }

        /* FFT plans */
        CUFFT_CALL( cufftDestroy(cs->cufftplan) );

        L_CUDA_PRINT_MEMORY_INFO("after GPU stream deallocations");
    }

    free(cs->d_powspecs);
    free(cs->h_powspecs);

    L_(linfo) << cs->name << "finished de-initializing CUDA arrays";

    lock.unlock();
    cs->idlegpucond->notify_one(); // wake up potential waiters

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Worker thread. Note: since CUDA 4.0(?) one process and all CPU threads of it have
 * a shared device context. That is CPU thread #1 if it switches to GPU#1 can interfere
 * with another CPU thread #2 that is expecting to work on GPU#2 (but gets switched
 * to GPU#1 unknowingly due to thread #1, and all objects and events are invalid).
 * It seems like the only option is to have a "global lock", under which
 * to select the target GPU and then dispatch any asynchronous tasks, then
 * release the lock (allowing other CPU threads to queue tasks onto the same
 * or another GPU), then acquire the lock periodically to check for task completion.
 */
void SpectralProcessorGPU::workerThread(void* slotcontext)
{
    cuda_worker_t* cs = (cuda_worker_t*)slotcontext;
    if (cs == NULL) {
        return;
    }

    /* Prepare our arrays (m_cuCtxMutex is acuired inside the call until it returns) */
    allocateWorker(cs->n);

    /* Data handling loop */
    bool interrupted = false;
    while (!cs->terminate && !interrupted) {

        /* Wait for new data */
        try {
            boost::this_thread::interruption_point();
            boost::mutex::scoped_lock lock(cs->workermutex);
            while ((cs->state != cuda_worker_t::Reserved) && !cs->terminate) {
                boost::this_thread::interruption_point();
                cs->newdatacond.wait(lock);
            }
            L_(ldebug1) << cs->name << ": woke up";
        } catch (boost::thread_interrupted& e) {
            L_(ldebug) << cs->name << " was interrupted while waiting for DRXP data and is exiting";
            interrupted = true;
            break;
        }

        /* Terminate early, or, process current data (if any) and terminate afterwards */
        //if (cs->terminate) { break; }

        /* Wait for complete data to arrive */
        CUDA_CALL( cudaEventSynchronize(cs->evt_raw_transferred) );

        /* Notify CPU-side that the DRXP group can be released/re-used */
        try {
            boost::this_thread::interruption_point();
            boost::mutex::scoped_lock lock(cs->workermutex);
            cs->state = cuda_worker_t::InputCopied;
            assert( cs->h_rawgroup.group >= 0);
            cs->h_rawgroup.originator->releaseGroup(cs->h_rawgroup.group);
        } catch (boost::thread_interrupted& e) {
            L_(ldebug) << cs->name << " was interrupted while releasing DRXP group and is exiting";
            interrupted = true;
            break;
        }

        /* Process new data */
        try {
            boost::this_thread::interruption_point();
            boost::mutex::scoped_lock lock(cs->workermutex);
            cs->state = cuda_worker_t::WorkLaunched;
        } catch (boost::thread_interrupted& e) {
            L_(ldebug) << cs->name << " was interrupted and is exiting";
            interrupted = true;
            break;
        }
        processSlot(cs->n); // outside of interruption_point()!

        /* Finish up on the processing */
        lockscope {
            boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
            CUDA_CALL( cudaSetDevice(cs->g) );
            CUDA_CALL( cudaEventRecord(cs->evt_stop, cs->sid) );
        }
        CUDA_CALL( cudaEventSynchronize(cs->evt_stop) );

        float dT_msec = -1.0f;
        CUDA_CALL( cudaEventElapsedTime(&dT_msec, cs->evt_start, cs->evt_stop) );

        /* Notify reserveSlot() that "some" GPU/stream is done */
        try {
            boost::this_thread::interruption_point();
            boost::mutex::scoped_lock lock(cs->workermutex);
            if (!interrupted) {
                cs->state = cuda_worker_t::Idle;
                lock.unlock();
                cs->idlegpucond->notify_one();
            }
        } catch (boost::thread_interrupted& e) {
            L_(ldebug) << cs->name << " was interrupted and is exiting";
            interrupted = true;
            break;
        }

        /* Done */
        float Msps = 1e-6 * ((DRXP_RX_DATA_SIZE * 8.0)/(3.0)) / (dT_msec*1e-3);
        Msps /= 2.0;
        L_(ldebug1) << cs->name << ": done, took " << std::fixed << std::setprecision(2) << dT_msec
                    << " msec, rate " << std::fixed << (1e-3*Msps) << " Gs/s/pol in 2 polarizations";
    }

    /* Clean up on exit */
    L_(linfo) << cs->name << ": cleaning up and terminating";
    deallocateWorker(cs->n);
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Carry out raw spectral processing of data in a slot
 */
int SpectralProcessorGPU::processSlot(const int slothandle)
{
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    cuda_worker_t* cs = &(cp->pool[slothandle]);
    const MCConfiguration* cfg = &(cs->scfg);

    const size_t maxphysthreads = cs->devprop.multiProcessorCount * cs->devprop.maxThreadsPerMultiProcessor;
    const size_t Lfft = 2*cfg->procConfig.nchan;
    const size_t nch = cfg->procConfig.nchan + 0;
    const size_t Lfft_out = nch;
    const size_t nfft_per_subint = (cfg->procConfig.Tsubint_max_msec*DRXP_SAMP_PER_MILLISEC) / cfg->procConfig.fftinstep; // 48ms:192 overlapped FFTs, 24ms:96 overlapped FFTs
    size_t threadsPerBlock, numBlocks;

    // TODO: consider possibly more efficient approaches for intra-frame FFT processing
    //
    // The requirement for short (1ms) or poorly fitting (e.g. 40ms, 50ms) integrations
    // and the 1.00-second aligned observation start time all complicate the handling of the
    // natively (DRXP) 48ms-long data frames. The issues are:
    // - the sampling rate is k*powers-of-10 but the (compatibility-)requirement is powers-of-2 DFT length
    // - the 1048576-point FFT (1M) imposes 262.144 usec granularity; a 1000000-point DFT ("1M") would have nicer 250 usec granularity,
    // - the current DRXP Format imposes a 16-sample (4 nanosec) x 2 pol = 6-byte x 2pol = 12-byte granularity into the frame data
    //   and the proposed changed DRXP Format imposes a 4-sample (1 nanosec) x 2 pol = 3-byte granularity into the frame data
    // - processSlot() must be able to integrate starting at some point 0ms/1ms/2ms/../47ms into the 48ms frame
    // - processSlot() must be able to integrate short periods (48/47/46../1ms) starting at 1ms boundaries in the 48ms frame
    // - multiple different-length integration periods are likely to be requested from within one 48ms frame
    // - CUDA FFT is batched, a set of N long FFTs, the larger N is the less the impact of overhead is and FFT becomes "faster"
    // - CUDA FFT must be planned and the batch size N cannot be adjusted later; how to effectively support multiple different-length
    //   integration periods within a single 48ms frame?
    //   a) large-batched FFT
    //       make FFT batch of say 24ms of data (1st half 0..~24ms; later 2nd half 24..~48ms; one batch of 91 FFTs = 23.8551 ms)
    //       look up the correct FFT indices# from these e.g. 91 FFTs to use
    //       accumulate those FFTs into the output spectum
    //       pro: speed   con: integration start has coarse 262.144us instead of 4ns granularity
    //   b) multiple very small FFT batches
    //       granularity is 1 millisec due to various constraints (8ms for 1PPS alignment, 2ms for NRO 50ms integration, 1ms for ALMA 1ms integration
    //       split integration of eg 40ms into subintegrations of 1ms
    //       make fixed-size batch FFT of ~1ms of data (1ms = 3.8 FFTs -> 4 FFTs -> 1.048576ms actual)
    //       integrate these 1.048576ms of spectral data
    //       pro: integration start time has fine 4ns granularity   con: very slow
    int fft_start_ms = 0, fft_stop_ms = 0; // for bookkeeping in method (a) above at a granularity of MCConfiguration::procConfig::Tsubint_max_msec

    /* Vars to keep track of what spectra produced on GPU should afterwards be accumulated on CPU to SpectralResultSet */
    int prev_AP_id = -1, prev_AP_swpos = -1, curr_AP_id = 0;
    int produced_AP_ids[cs->max_ints_per_48ms];
    int produced_AP_swpos[cs->max_ints_per_48ms];
    unsigned int produced_AP_total_msec[cs->max_ints_per_48ms];
    unsigned int produced_AP_integrated_msec[cs->max_ints_per_48ms];
    memset(&produced_AP_ids, 0, sizeof(produced_AP_ids));
    memset(&produced_AP_swpos, 0, sizeof(produced_AP_swpos));
    memset(&produced_AP_total_msec, 0, sizeof(produced_AP_total_msec));
    memset(&produced_AP_integrated_msec, 0, sizeof(produced_AP_integrated_msec));
    int nproduced = 0;

    /* Get all integrations possible during this 48ms frame */
    int offset_ms = 0;
    while (offset_ms < DRXP_DT_MSEC) {

        int swpos, integ_use_ms, transit_use_ms;
        bool do_Histogram = false;

        // Look up (and auto-create if necessary) the SpectralResultSet responsible for next piece of data in this 48ms
        lockscope {
            SpectralResultSet* prs = NULL;
            bool ok = cp->resultsManager.lookupSpectralResultSet(cs->h_rawgroup.frameinfo, &offset_ms, cfg, &prs, &swpos, &integ_use_ms, &transit_use_ms);
            if (!ok || prs == NULL) {
                L_(linfo) << cs->name << "got data that SpectralResultSetManager could not map into observing time range";
                break;
            }
            curr_AP_id = prs->id;
            do_Histogram = (prs->hasHistogram() == false);
            prs->lock.unlock();
        }

        // Nothing to do if in a 'transition' stage of switching cycle
        if (integ_use_ms <= 0) {
            produced_AP_total_msec[nproduced] += transit_use_ms;
            offset_ms += transit_use_ms;
            continue;
        }

        // Histogram calculation; 1 millisec of data, only once in the entire integration
        if (do_Histogram) {

            boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
            CUDA_CALL( cudaSetDevice(cs->g) );

            threadsPerBlock = 32; // hardcoded cu_histogram_3bit_2ch() for warpsize 32
            numBlocks = div2ceil(maxphysthreads, threadsPerBlock);
            unsigned char* d_raw = cs->d_rawbuf + size_t(offset_ms) * DRXP_BYTE_PER_MILLISEC;

            const int nlevels = 8;
            CUDA_TIMING_START(cs->evt_kstart, cs->sid_d2h);
            CUDA_CALL( cudaMemsetAsync( cs->d_hist_X, 0, nlevels*sizeof(hist_t), cs->sid_d2h ) );
            CUDA_CALL( cudaMemsetAsync( cs->d_hist_Y, 0, nlevels*sizeof(hist_t), cs->sid_d2h ) );
#ifdef USE_DRXP_PRODUCTION_SAMPLELAYOUT
            cu_histogram_3bit_2ch <<<numBlocks, threadsPerBlock, 32*8, cs->sid_d2h>>> (d_raw, DRXP_BYTE_PER_MILLISEC, cs->d_hist_X, cs->d_hist_Y); // just ~1ms was deemed "enough"
            CUDA_CHECK_ERRORS("histogram_3bit_2ch");
#else
            cu_histogram_drxp_3bit_2ch <<<numBlocks, threadsPerBlock, 32*8, cs->sid_d2h>>> (d_raw, DRXP_BYTE_PER_MILLISEC, cs->d_hist_X, cs->d_hist_Y); // just ~1ms was deemed "enough"
            CUDA_CHECK_ERRORS("histoDRXP_3bit_2ch");
#endif
            CUDA_CALL( cudaMemcpyAsync( cs->h_hist_X, cs->d_hist_X, nlevels*sizeof(hist_t), cudaMemcpyDeviceToHost, cs->sid_d2h ) );
            CUDA_CALL( cudaMemcpyAsync( cs->h_hist_Y, cs->d_hist_Y, nlevels*sizeof(hist_t), cudaMemcpyDeviceToHost, cs->sid_d2h ) );
            CUDA_TIMING_STOP_M(cs->evt_kstop, cs->evt_kstart, cs->sid_d2h, m_cuCtxMutex, "histogram_3bit", (DRXP_BYTE_PER_MILLISEC*8)/3);
            // Note: if input data are single-pol we can just sum hist_X and hist_Y together later on CPU
        }

        // Spectral processing
        while (integ_use_ms > 0) {

            // How much of batched FFT data can be (re)used
            bool run_FFT = false;
            int batch_avail_ms = MAX(fft_stop_ms - offset_ms, 0);
            int subint_msec = MIN(integ_use_ms, batch_avail_ms);
            while (subint_msec <= 0) {
                // Need a new batch of FFTs
                run_FFT = true;
                if (fft_stop_ms <= 0) {
                    // first iteration just set fft_stop_ms and mark run_FFT=true
                    fft_stop_ms = fft_start_ms + cfg->procConfig.Tsubint_max_msec;
                } else {
                    fft_start_ms += cfg->procConfig.Tsubint_max_msec;
                    fft_stop_ms = fft_start_ms + cfg->procConfig.Tsubint_max_msec;
                }
                if (fft_stop_ms > DRXP_DT_MSEC) {
                    fft_stop_ms = DRXP_DT_MSEC;
                }
                batch_avail_ms = MAX(fft_stop_ms - offset_ms, 0);
                subint_msec = MIN(integ_use_ms, batch_avail_ms);
            }
            assert(subint_msec > 0);
            assert(offset_ms >= fft_start_ms);
            assert((offset_ms + subint_msec) <= fft_stop_ms);
            assert(fft_start_ms < DRXP_DT_MSEC);

            // Make a new set of batched FFTs
            if (run_FFT) {

                /* Unpack more data 3-bit --> float32 */
                size_t unpack_offset = size_t(fft_start_ms) * DRXP_BYTE_PER_MILLISEC;
                size_t unpack_bytes = size_t(fft_stop_ms - fft_start_ms) * DRXP_BYTE_PER_MILLISEC;
                assert((unpack_offset + unpack_bytes) <= DRXP_DT_MSEC*DRXP_BYTE_PER_MILLISEC);

                const size_t nsamples = (unpack_bytes * 8)/3; // total nr# of samples from all polarizations combined
                const size_t nwords24 = (nsamples*3)/24;      // total nr# of 24-bit words with usable samples

                // Raw data pointer for unpacking of 3-bit data
                unsigned char* d_raw = cs->d_rawbuf + unpack_offset;

                // Unpacked signal x[n] and y[n] data pointers
                float* d_x = (float*)cs->d_fft_in;
                float* d_y = (float*)cs->d_fft_in + (nfft_per_subint-1)*cs->scfg.procConfig.fftinstep + 2*cs->scfg.procConfig.nchan;
                //if ((nsamples / cs->npol) % (nfft_per_subint*Lfft) != 0) {
                //    L_(lwarning) << cs->name << "unpack " << fft_start_ms << ".." << fft_stop_ms << "ms has #samples/pol " << (nsamples/cs->npol) << ", not a FFT batch multiple (" << nfft_per_subint << "*" << Lfft << ")";
                //    L_(lwarning) << cs->name << "d_x = d_fft_in[0], d_y = d_fft_in[" << nfft_per_subint*Lfft << "]";
                //}

                //L_(ldebug1) << cs->name << "unpack for FFT batch " << fft_start_ms << ".." << fft_stop_ms << "ms for offset " << offset_ms << "..<N>ms";

#ifdef USE_DRXP_PRODUCTION_SAMPLELAYOUT
                // Assume the simplest format, 3-bit X and Y pairs (6-bit), in time increasing order
                threadsPerBlock = cs->devprop.warpSize;
                numBlocks = div2ceil(nwords24, threadsPerBlock);
                if (cs->npol == 1) {
                    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
                    CUDA_CALL( cudaSetDevice(cs->g) );
                    CUDA_TIMING_START(cs->evt_kstart, cs->sid);
                    if (cfg->procConfig.windowFunction == XSD::UNIFORM) {
                        cu_decode_3bit1ch <<<numBlocks, threadsPerBlock, 0, cs->sid>>> ( d_raw, (float4*)d_x, unpack_bytes );
                    } else if (cfg->procConfig.windowFunction == XSD::HANNING) {
                        cu_decode_3bit1ch_Hann <<<numBlocks, threadsPerBlock, 0, cs->sid>>> ( d_raw, (float4*)d_x, unpack_bytes, Lfft );
                    } else if (cfg->procConfig.windowFunction == XSD::HAMMING) {
                        cu_decode_3bit1ch_Hamming <<<numBlocks, threadsPerBlock, 0, cs->sid>>> ( d_raw, (float4*)d_x, unpack_bytes, Lfft );
                    } else {
                        cu_decode_3bit1ch <<<numBlocks, threadsPerBlock, 0, cs->sid>>> ( d_raw, (float4*)d_x, unpack_bytes );
                        L_(lerror) << "GPU 3-bit unpacker : 1-pol Window func " << cfg->winfunEnum2str(cfg->procConfig.windowFunction) << " not implemented!";
                    }
                    CUDA_CHECK_ERRORS("decode_3bit");
                    CUDA_TIMING_STOP_M(cs->evt_kstop, cs->evt_kstart, cs->sid, m_cuCtxMutex, "decode_3bit", nsamples);
                } else if (cs->npol == 2) {
                    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
                    CUDA_CALL( cudaSetDevice(cs->g) );
                    CUDA_TIMING_START(cs->evt_kstart, cs->sid);
                    if (cfg->procConfig.windowFunction == XSD::UNIFORM) {
                        cu_decode_3bit2ch_split <<<numBlocks, threadsPerBlock, 0, cs->sid>>> ( d_raw, (float4*)d_x, (float4*)d_y, unpack_bytes );
                    } else if (cfg->procConfig.windowFunction == XSD::HANNING) {
                        cu_decode_3bit2ch_Hann_split <<<numBlocks, threadsPerBlock, 0, cs->sid>>> ( d_raw, (float4*)d_x, (float4*)d_y, unpack_bytes, Lfft );
                    } else if (cfg->procConfig.windowFunction == XSD::HAMMING) {
                        cu_decode_3bit2ch_Hamming_split <<<numBlocks, threadsPerBlock, 0, cs->sid>>> ( d_raw, (float4*)d_x, (float4*)d_y, unpack_bytes, Lfft );
                    } else {
                        cu_decode_3bit2ch_split <<<numBlocks, threadsPerBlock, 0, cs->sid>>> ( d_raw, (float4*)d_x, (float4*)d_y, unpack_bytes );
                        L_(lerror) << "GPU 3-bit unpacker : 2-pol Window func " << cfg->winfunEnum2str(cfg->procConfig.windowFunction) << " not implemented!";
                    }
                    CUDA_CHECK_ERRORS("decode_3bit");
                    CUDA_TIMING_STOP_M(cs->evt_kstop, cs->evt_kstart, cs->sid, m_cuCtxMutex, "decode_3bit", nsamples);
                }
#else
                // Assume a more complicated format, DRXP Data Format, with 16 x 3-bit X then 16 x 3-bit, in a bit-shuffled order
                threadsPerBlock = cs->devprop.warpSize;
                numBlocks = div2ceil(nwords24/4, threadsPerBlock);
                if (cs->npol == 1) {
                    L_(lerror) << "GPU 3-bit DRXP unpacker : 1-pol Window func " << cfg->winfunEnum2str(cfg->procConfig.windowFunction) << " not implemented!";
                } else if (cs->npol == 2) {
                    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
                    CUDA_CALL( cudaSetDevice(cs->g) );
                    CUDA_TIMING_START(cs->evt_kstart, cs->sid);
                    if (cfg->procConfig.windowFunction == XSD::UNIFORM) {
                        cu_decode_drxp_3bit2ch_split <<<numBlocks, threadsPerBlock, 0, cs->sid>>> ( d_raw, (float4*)d_x, (float4*)d_y, unpack_bytes );
                    } else {
                        L_(lerror) << "GPU 3-bit DRXP unpacker : 2-pol Window func " << cfg->winfunEnum2str(cfg->procConfig.windowFunction) << " not implemented! int=" << int(cfg->procConfig.windowFunction);
                    }
                    CUDA_CHECK_ERRORS("decode_drxp_3bit");
                    CUDA_TIMING_STOP_M(cs->evt_kstop, cs->evt_kstart, cs->sid, m_cuCtxMutex, "decode_drxp_3bit", nsamples);
                }
#endif

                //L_(ldebug1) << cs->name << "r2c FFT of batch " << fft_start_ms << ".." << fft_stop_ms << "ms for offset " << offset_ms << "..<N>ms";

                /* Real-to-Complex DFT of the data; batchsize is fixed!! */
                lockscope {
                    boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
                    CUDA_CALL( cudaSetDevice(cs->g) );
                    CUDA_TIMING_START(cs->evt_kstart, cs->sid);
                    CUFFT_CALL( cufftExecR2C(cs->cufftplan, (cufftReal*)cs->d_fft_in, (cufftComplex*)cs->d_fft_out) );
                    CUDA_TIMING_STOP_M(cs->evt_kstop, cs->evt_kstart, cs->sid, m_cuCtxMutex, "r2c FFT", nfft_per_subint*Lfft*cs->npol);
                }

            } //if(run_FFT)

            // Frequency-domain post FFT signal X[w] and Y[w] data pointers
            // octave> DRXP_SAMP_PER_MILLISEC = 4000000; Lfft = 1048576; fftinstep = 1000000; Lfft_out = Lfft/2 + 0;
            // octave> fft_start_ms = 24; fft_stop_ms = 48; % assume our FFT batch is covering [24ms..48ms[ of the 48ms frame
            // octave> offset_ms = 28;    % time point from which we should integrate from
            // octave> Tfft_ms = fft_start_ms + (0:95)*1e3*(fftinstep/4e9); % starting timestamps of each overlapped 262.144us FFT, spaced by 250us
            // octave> fftIdx = floor( ((offset_ms - fft_start_ms) * DRXP_SAMP_PER_MILLISEC) / fftinstep )  % get an index 0..n-1
            // octave> Tfft_ms(fftIdx+1)  % +1 as Octave indexes (1..n), result == offset_ms
            // --> fftIdx=16, Tfft_ms(fftIdx+1)=28.0ms, 1e3*(offset_ms-Tfft_ms(fftIdx+1))=0usec
            const size_t fft_index = ((offset_ms - fft_start_ms) * DRXP_SAMP_PER_MILLISEC) / cfg->procConfig.fftinstep;
            const size_t fft_offset = fft_index * Lfft_out;
            // octave> subint_msec = 24;
            // octave> fft_avail = (fft_stop_ms-fft_start_ms)*DRXP_SAMP_PER_MILLISEC/fftinstep
            // octave> fft_count = min(fft_avail, (subint_msec*DRXP_SAMP_PER_MILLISEC)/fftinstep)
            // --> fftAvail=96, fft_count=96
            const size_t fft_avail = ((fft_stop_ms - fft_start_ms) * DRXP_SAMP_PER_MILLISEC) / cfg->procConfig.fftinstep;
            const size_t fft_count = MIN( (size_t)fft_avail, (size_t)((subint_msec * DRXP_SAMP_PER_MILLISEC) / cfg->procConfig.fftinstep));
            cufftComplex* d_X = (cufftComplex*)cs->d_fft_out + fft_offset;
            cufftComplex* d_Y = d_X + nfft_per_subint*Lfft_out;

            //std::cout << "nfft_per_subint=" << nfft_per_subint << ", fft_avail=" << fft_avail << ", fft_count=" << fft_count << " for offset_ms=" << offset_ms << ".." << offset_ms+subint_msec << "ms, subint_msec=" << subint_msec << " in " << fft_start_ms << ".." << fft_stop_ms << "ms" << std::endl;

            //L_(linfo) << cs->name << "xmac: fft_index=" << fft_index << " fft_offset=" <<fft_offset << " fft_avail=" << fft_avail << " fft_count=" << fft_count;
            //L_(ldebug1) << cs->name << "averaging FFT " << fft_index << ".." << (fft_index+fft_count-1) << " of [0.." << (fft_avail-1) << "]" << " in " << offset_ms << ".." << (offset_ms+subint_msec) << "ms";

            /* Time-averaged power spectra */
            float4* avg_output = (float4*)cs->d_powspecs[nproduced]; // dedicated buf in case [nproduced-1] are still being copied back to CPU
            if (cs->npol == 1) {
                // Auto XX
                // TODO: choose the proper pol! Can have YY or XX!
                boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
                CUDA_CALL( cudaSetDevice(cs->g) );
                CUDA_TIMING_START(cs->evt_kstart, cs->sid);
                threadsPerBlock = 64;
                numBlocks = div2ceil(max(maxphysthreads,nch), threadsPerBlock);
                autoPowerSpectrum_v3_skipNyquist <<< numBlocks, threadsPerBlock, 0, cs->sid >>> ( d_X, (float*)avg_output, nch, fft_count );
                CUDA_CHECK_ERRORS("autoPowerSpectrum_v3");
                CUDA_TIMING_STOP_M(cs->evt_kstop, cs->evt_kstart, cs->sid, m_cuCtxMutex, "ac(v3)", nch*fft_count);
            } else {
                // Auto XX, auto YY, and always also cross XY even if not used later
                boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
                CUDA_CALL( cudaSetDevice(cs->g) );
                CUDA_TIMING_START(cs->evt_kstart, cs->sid);
                threadsPerBlock = 32;
                numBlocks = div2ceil(max(maxphysthreads,nch), threadsPerBlock);
                //std::cout << "avg cs->d_powspecs[" << nproduced << "]=" << (void*)cs->d_powspecs[nproduced] << " grid " << numBlocks << "x" << threadsPerBlock << " nch=" << nch << " fft_count=" << fft_count << std::endl;
                cu_accumulate_2pol_skipNyquist <<< numBlocks, threadsPerBlock, 0, cs->sid>>> ((float2*)d_X, (float2*)d_Y, avg_output, nch, fft_count);
                CUDA_CHECK_ERRORS("cu_accumulate_2pol");
                CUDA_TIMING_STOP_M(cs->evt_kstop, cs->evt_kstart, cs->sid, m_cuCtxMutex, "xmac(X,Y)", 2*nch*fft_count);
            }

            /* Integration progress counters (for the outer loops) */
            produced_AP_total_msec[nproduced] += subint_msec;
            produced_AP_integrated_msec[nproduced] += subint_msec;
            integ_use_ms -= subint_msec;
            offset_ms += subint_msec;

        } // while (integ_use_ms > 0)

        if (!(nproduced <= cs->max_ints_per_48ms)) {
            L_(lerror) << "Produced " << nproduced << " spectra from 48ms data chunk, expected at most " << cs->max_ints_per_48ms;
        }
        assert(nproduced <= cs->max_ints_per_48ms);

        /* Copy current 'dwell' spectrum back to CPU */
        lockscope {
            boost::mutex::scoped_lock sharedGPUlock(m_cuCtxMutex);
            CUDA_CALL( cudaSetDevice(cs->g) );
            CUDA_CALL( cudaMemcpyAsync(cs->h_powspecs[nproduced], cs->d_powspecs[nproduced], cs->powspecslen, cudaMemcpyDeviceToHost, cs->sid) );
            CUDA_CALL( cudaEventRecord(cs->evt_spectrum_avail, cs->sid) );
        }

        /* Keep track of work done */
        if ((prev_AP_id != curr_AP_id) || (prev_AP_swpos != swpos)) {
            prev_AP_id = curr_AP_id;
            prev_AP_swpos = swpos;
            produced_AP_ids[nproduced] = curr_AP_id;
            produced_AP_swpos[nproduced] = swpos;
            nproduced++; // go to next on-GPU spectral area
        }

    }  // while (offset_ms < 48)

    /* Wait until results are in CPU/main memory */
    CUDA_CALL( cudaEventSynchronize(cs->evt_spectrum_avail) );

/*
    for (int s=0; s<nproduced; s++) {
std::cout << cs->name << " produced " << s+1 << "/" << nproduced << " : " << " ap=" << produced_AP_ids[s] << " pos=" << produced_AP_swpos[s] << " totl=" << produced_AP_total_msec[s] << " int=" << produced_AP_integrated_msec[s] << std::endl;
    }
*/

    /* Handle the averaged spectra returned from GPU processing */
    for (int s=0; s<nproduced; s++) {

        int id = produced_AP_ids[s];
        SpectralResultSet* prs = cp->resultsManager.getSpectralResultSet(id);
        if (prs == NULL) {
            //L_(linfo) << cs->name << "result set nr " << s << " with ID#" << id << " no longer found, likely completed & written by other worker";
            continue;
        }

        if (!prs->allocated()) {
            // Other thread writing to this same ID# got here first,
            // already wrote out the spectral data; nothing to do then
/*
std::cout << cs->name << " accu subint of " << produced_AP_total_msec[s] << "ms/" << produced_AP_integrated_msec[s] << "ms subint"
          << " to AP#" << prs->id << " total " << prs->total_msec << "ms/"  << prs->integrated_msec << "ms"
          << " -- skipped as res set !allocated()"
          << std::endl;
*/
            prs->lock.unlock();
            continue;
        }

        // Accumulate GPUs (sub)integration data to the main integration
        cpuAccumulatef32(/*src*/cs->h_powspecs[s], /*dst*/prs->spectra[produced_AP_swpos[s]], cs->powspecslen/sizeof(float));
/*
std::cout << cs->name << " accu subint of " << produced_AP_total_msec[s] << "ms/" << produced_AP_integrated_msec[s] << "ms subint"
          << " to AP#" << prs->id << " total " << prs->total_msec << "ms/"  << prs->integrated_msec << "ms"
          << " goal " << cfg->procConfig.Tswitchingcycle_msec << "ms\*"
          << " : post-accu " << (prs->total_msec+produced_AP_total_msec[s]) << "ms/" << (prs->integrated_msec+produced_AP_integrated_msec[s]) << "ms"
          << std::endl;
*/
        prs->total_msec += produced_AP_total_msec[s];
        prs->integrated_msec += produced_AP_integrated_msec[s];

        // Attach latest 1ms-data histogram from any (potentially different) integration during current 48ms frame
        if (!prs->hasHistogram()) {
            prs->storeHistogram(cs->h_hist_X, cs->h_hist_Y);
        }

        // Complete? Mark as ready for later post-processing
        if (prs->total_msec >= cfg->procConfig.Tswitchingcycle_msec) {
            L_(ldebug) << cs->name << "completed AP #" << id << " that reached Tint=" << prs->total_msec << "ms of Tap=" << cfg->procConfig.Tswitchingcycle_msec << "ms";
            prs->lock.unlock();
            cp->resultsManager.setCompleted(id);
        } else {
            prs->lock.unlock();
        }

    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/** Start background spectral results writeout */
bool SpectralProcessorGPU::startSpectralWriteout()
{
    if (m_specHandlerThread != NULL) {
        stopSpectralWriteout();
    }

    m_shutdown_specHandlerThread = false;
    m_specHandlerThread = new boost::thread(&SpectralProcessorGPU::handleCompletedSpectraThread, this);

    return true;
}

/** Stop background spectral results writeout */
bool SpectralProcessorGPU::stopSpectralWriteout()
{
    m_shutdown_specHandlerThread = true;
    if (m_specHandlerThread != NULL) {
        m_specHandlerThread->interrupt();
        m_specHandlerThread->join();
        delete m_specHandlerThread;
        m_specHandlerThread = NULL;
    }

    return true;
}

/** Thread that regularly checks for completion of new spectra */
void SpectralProcessorGPU::handleCompletedSpectraThread()
{
    L_(linfo) << "Spectral output queue monitoring and writeout worker thread started";
    while (!m_shutdown_specHandlerThread) {

        // Interruptible sleep
        try {
            boost::this_thread::interruption_point();
            boost::this_thread::sleep(boost::posix_time::milliseconds(OBS_QUEUE_SLEEP_MSEC));
        } catch (boost::thread_interrupted& e) {
            L_(linfo) << "Spectral output queue monitor/writeout interrupted";
            return;
        }

        // Check for ready spectra; non-interruptible to avoid corruption
        {
            boost::this_thread::disable_interruption di;
            handleCompletedSpectra();
        }
    }
    L_(linfo) << "Spectral output queue monitoring and writeout worker thread stopped";
}

/** Get all currently completed spectra from internal SpectralResultManager and write them out to the results handler */
void SpectralProcessorGPU::handleCompletedSpectra()
{
    cuda_config_pool_t* cp = (cuda_config_pool_t*)this->pimpl;
    const float T_intended = cp->scfg.procConfig.Tswitchingcycle_msec;

    // Get all currently available results one by one
    SpectralResultSet* rs = cp->resultsManager.getNextCompleted(); // returns NULL, or rs with rs->lock locked
    while (rs != NULL) {

        const int id = rs->id;

        //std::cout << "handleCompletedSpectra : getNextCompleted=" << (void*)rs << std::endl;
        if (cp->obsRef != NULL) {
            rs->weight = float(rs->total_msec) / T_intended;
            cp->postProcess.process(rs, &cp->scfg);
            rs->setPostprocessed();
            rs->deallocateRaw();
        } else {
            L_(lerror) << "Error: cuda_config_pool_t did not have obsRef set, cannot post-process spectral results!";
        }

        lockscope {
            boost::mutex::scoped_lock handlerlock(cp->resultshandlermutex); // in case of background (un)registering of resultsHandler*
            if (cp->resultsHandler != NULL) {
                cp->resultsHandler->takeResult(rs);
                // L_(linfo) << "Writing out spectrum with ID#" << id << ", weight " << std::fixed << std::setprecision(4) << rs->weight;
            } else {
                L_(lwarning) << "Received spectra in GPU::handleCompletedSpectra() but have no obs or writer! obs=" << (void*)cp->obsRef << " resultsHandler=" << (void*)cp->resultsHandler;
            }
        }

        rs->lock.unlock();
        cp->resultsManager.releaseCompleted(id);

        // Get next if any
        rs = cp->resultsManager.getNextCompleted();
    }

}

/////////////////////////////////////////////////////////////////////////////////////

void SpectralProcessorGPU::monitoringThreadFunc(const char* mgroup, int mport, float interval_secs)
{
    using namespace boost::asio;
    ip::address destaddr = boost::asio::ip::address::from_string(mgroup);
    ip::udp::endpoint source(ip::address_v4::any(), mport);
    ip::udp::endpoint destination(destaddr, mport);

    while (!m_shutdown_monitoringThread) {
        {
            // Disable thread interruption in this scope
            boost::this_thread::disable_interruption diTmp;

            // Send a summary by multicast
            std::stringstream ss;
            ss << "ASM #<X> : <not implemented>\n";

            std::string summary = ss.str();
            try {
                io_service service;
                ip::udp::socket socket(service);
                socket.open(ip::udp::v4());
                socket.set_option(ip::udp::socket::reuse_address(true));
                socket.set_option(ip::multicast::enable_loopback(true));
                socket.send_to(buffer(summary), destination);
            } catch (...) {
                std::cerr << "Multicast transmission in " << __func__ << "() failed\n";
            }
        }

        // Pause and potential thread exit point
        try {
            boost::this_thread::sleep(boost::posix_time::seconds(interval_secs));
        } catch (boost::thread_interrupted& e) {
            return;
        }
    }
}

/** Start periodic monitoring. Periodically multicasts a GPU processor status summary string */
bool SpectralProcessorGPU::startPeriodicMonitoring(const char* mgroup, int mport, float interval_secs)
{
    if (m_monitoringThread != NULL) {
        stopPeriodicMonitoring();
    }
    if ((interval_secs < 0.192f) || (interval_secs > 30.0f)) {
        interval_secs = 5.0f;
    }

    m_shutdown_monitoringThread = false;
    m_monitoringThread = new boost::thread(&SpectralProcessorGPU::monitoringThreadFunc, this, mgroup, mport, interval_secs);

    return true;
}

/** Stop periodic monitoring */
void SpectralProcessorGPU::stopPeriodicMonitoring()
{
    if (m_monitoringThread != NULL) {
        m_shutdown_monitoringThread = true;
        m_monitoringThread->interrupt();
        m_monitoringThread->join();
        delete m_monitoringThread;
        m_monitoringThread = NULL;
        m_shutdown_monitoringThread = false;
    }
}
