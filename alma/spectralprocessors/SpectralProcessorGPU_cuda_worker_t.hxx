/**
 * \class cuda_worker_t
 *
 * \brief Low-level details for a single GPU and Stream, with relevant data pointers and auxiliary data.
 *
 * \author $Author: janw $
 *
 */
#ifndef SPECTRALPROCESSOR_GPU_CUDA_WORKER_T_HXX
#define SPECTRALPROCESSOR_GPU_CUDA_WORKER_T_HXX

//#include "arithmetic_winfunc_kernels.h"                   // def. cu_window_cb_params_t
#include "histogram_kernels.h"                              // def. hist_t
#include "mcwrapping/mc_configuration.hxx"                  // class MCConfiguration
#include "datarecipients/SpectralResultSetManager.hxx"      // class SpectralResultSetManager
#include "spectralprocessors/SpectralPostprocessor.hxx"     // class SpectralPostprocessor
#include "drxp/DRXPInfoFrame.hxx"                           // class DRXPInfoFrame
#include "drxp/RawDRXPDataGroup.hxx"

class ResultRecipient;
class MCObservation;
struct cuda_config_pool_t;

#include <cuda.h>
#include <cufft.h>

#include <time.h>
#include <sys/time.h>

#include <string>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/noncopyable.hpp>

struct cuda_worker_t : private boost::noncopyable
{
    /* Mutexing */
    boost::mutex   workermutex;   /// mutex to protect access to worker pool, also used in conjuction with newdatacond to signal new raw data

    /* GPU and Stream and FFT setup */
    int            g;          /// CUDA GPU device number
    int            s;          /// CUDA stream number
    int            n;          /// Worker slot index
    cudaDeviceProp devprop;    /// Properties of the target GPU device
    cudaStream_t   sid;        /// CUDA stream handle for computation
    cudaStream_t   sid_h2d;    /// CUDA stream handle for host->device memcpy
    cudaStream_t   sid_d2h;    /// CUDA stream handle for device->host memcpy
    cufftHandle    cufftplan;  /// Handle to CUFFT plan on the target GPU device, assigned to target stream
    std::string    name;

    /* Raw input sample data (headerless, with a fill pattern for missing frames) */
    RawDRXPDataGroup h_rawgroup;  /// (to replace h_rawbuf_info, h_rawbuf_group, and allow DRXP releaseSlot() callback)
    unsigned char*   d_rawbuf;    /// Pointer to raw data on GPU
    int              npol;        /// 1 or 2 polarizations

    /* On-GPU arrays */
    hist_t*        h_hist_X;   /// storage of histogram levels for X-polarization (8 bins)
    hist_t*        h_hist_Y;   /// storage of histogram levels for Y-polarization (8 bins)
    float*         d_fft_in;   /// input to r2c FFT
    float*         d_fft_out;  /// output of r2c FFT (type is recast cufftComplex)
    //cu_window_cb_params_t  h_cufft_userparams;     /// extra user params in cuFFT Callback
    //cu_window_cb_params_t* d_cufft_userparams;     /// device copy ofextra user params in cuFFT Callback, one copy per GPU is enough

    /* Output arrays */
    hist_t*        d_hist_X;    /// storage of histogram levels for X-polarization (8 histogram bins)
    hist_t*        d_hist_Y;    /// storage of histogram levels for Y-polarization (8 histogram bins)
    float**        d_powspecs;  /// accumulated auto&cross power spectra on GPU (interleaved {XX,Re XY,Im XY, YY), d_powspecs[max_ints_per_48ms][Nchan]
    float**        h_powspecs;  /// accumulated auto&cross power spectra on host, in each binning position, h_powspecs[max_ints_per_48ms][Nchan]
    double         spec_weight; /// weights of each spectrum = valid data / total data; -1 if invalid spectrum; identical for all binning positions
    size_t         powspecslen; /// length of d_powspecs in byte; all auto&cross power spectra included
    int            max_ints_per_48ms; /// maximum expected number of independent spectra produceable from a 48ms DRP frame given a MCConfiguration::timeSpec::switching setup

    /* Performance tracking */
    cudaEvent_t evt_start;    /// Event for wallclock timestamp when processing of segment started
    cudaEvent_t evt_stop;     /// Event for wallclock timestamp when processing of segment finished
    cudaEvent_t evt_raw_transferred; /// Event happens after raw data to GPU transfer has completed
    cudaEvent_t evt_raw_overwriteable; /// Event happens after raw input data area can be freely overwritten
    cudaEvent_t evt_spectrum_avail;    /// Event happens after spectrum completely copied to host
    cudaEvent_t evt_kstart;            /// kernel timing
    cudaEvent_t evt_kstop;             /// kernel timing
    cudaEvent_t evt_sync_with_yield;   /// generic sync-up, without timing, and busy-poll replaced by yield (flag cudaEventBlockingSync)

    /* Settings applicable to the data (private copy) */
    MCConfiguration scfg;

    /* CPU thread that handles the processing */
    boost::thread *workerthread;  /// thread that runs SpectralProcessorGPU::workerThread()
    boost::condition_variable newdatacond;  /// signaled in takeData() upon new data to wake an idle waiting worker thread
    boost::condition_variable* idlegpucond; /// signaled by workerThread() when returning back to an idle state, pointer to shared condition obj
    enum WorkerState { Idle=0, Reserved=1, InputCopied=2, WorkLaunched=3, Exited=4 }; /// possible processing states of GPU worker thread
    WorkerState state;           /// current processing state of GPU worker thread
    bool initialized;            /// 'true' if object is initialized and CUDA objects were created
    bool terminate;              /// 'false' to keep running

    /* C'stor. Sets up defaults. */
    cuda_worker_t() {
        g = -1; s = -1;
        d_rawbuf = NULL;
        workerthread = NULL;
        idlegpucond = NULL;
        initialized = false;
        terminate = false;
        state = cuda_worker_t::Exited;
    }

};

#endif // SPECTRALPROCESSOR_GPU_CUDA_WORKER_T_HXX
