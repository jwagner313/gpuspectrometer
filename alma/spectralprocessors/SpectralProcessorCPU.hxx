/**
 * \class SpectralProcessorCPU
 *
 * Simple CPU-only implementation of the SpectralProcessor interface definition.
 * Intended for systems without graphics cards.
 * Currently simply accepts new raw data, but does not carry out any
 * actual spectral computations. TODO: add Fourier transform (FFTW library)
 * and some basic spectral processing if desired, with the caveat that on CPUs
 * such processing is much too slow for realtime operation at ALMA bandwidths.
 *
 */
#ifndef SPECTRALPROCESSOR_CPU_HXX
#define SPECTRALPROCESSOR_CPU_HXX

#include "spectralprocessors/SpectralProcessor_iface.hxx"

class DRXPIface;
class DRXPInfoFrame;
class ResultRecipient;
class MCConfiguration;
class MCObservation;
class SpectralResultSet;

class SpectralProcessorCPU : public SpectralProcessorIFace {

    public:

        SpectralProcessorCPU();

    public:

        /** Prepare spectral processing based on given observation details (configuration, time, metadata) */
        bool initialize(MCObservation* obs);

        /** Reset the spectral processing*/
        bool reset() { return true; }

        /** Shut down spectral processing of current observation */
        bool deinitialize();

        /** Replace delay lists in case of correlation mode */
        bool replaceDelayLists(const XSD::delayListList_t&) { return true; }

    public:

        /** Interface inherited by SpectralProcessor from DataRecipient
         * \return True if data will be used and group free later, False if group can be freed immediately
         */
        bool takeData(RawDRXPDataGroup data);

    public:

        /** Performance optimization related: function to lock pages into memory */
        void pinUserBuffers(int Nbuffers, void** buffers, size_t buflen);

        /** Performance optimization related: function to unlock pages in memory */
        void unpinUserBuffers(int Nbuffers, void** buffers, size_t buflen);

    public:

        /** Set the recipient for spectral results */
        void setSpectralRecipient(ResultRecipient* r);

        /** Assign to a subset of devices (CPU) */
        void setAffinity(int ndevices, const int* deviceIds);

    public:

        /** Start periodic monitoring. Sends out status messages onto the network. */
        bool startPeriodicMonitoring(const char*, int, float interval=5.0f) { return false; }

        /** Stop periodic monitoring. */
        void stopPeriodicMonitoring() { }

    private:

        /** Spectral computations (dummy data for now) */
        void process48ms(const void* src, int n, double w, const DRXPInfoFrame* frameinfo);

    private:
        void* pimpl;
};

#endif
