#include "singleConfiguration_t.hxx"

namespace XSD {

bool singleConfiguration_t::get(const boost::property_tree::ptree& in)
{
    return validate();
}

bool singleConfiguration_t::put(boost::property_tree::ptree& out) const
{
    return validate();
}

}
