/**
 * \class switching_t
 *
 * Container for XSD SwitchingType
 */
#ifndef SWITCHING_T_HXX
#define SWITCHING_T_HXX

#include "xsd_ptree_iface.hxx"

namespace XSD {

class switching_t : public XSDPtreeInterface
{
    public:
        unsigned int position;
        double dwell_sec;
        double transition_sec;

    public:
        switching_t() : position(0),dwell_sec(0.0),transition_sec(0.0) {
        }

        switching_t(int pos, double dwell, double transition) : position(pos),dwell_sec(dwell),transition_sec(transition) {
        }

    public:
        bool validate() const {
            bool valid = true;
            /* valid &= (position >= 0); */
            valid &= (dwell_sec >= 0);
            valid &= (transition_sec >= 0);
            valid &= ((dwell_sec + transition_sec) > 0);
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
