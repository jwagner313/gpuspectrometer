/**
 * \class drxpDfrSyncErrorCounter_t
 *
 * Container for XSD DrxpDfrSyncErrorCounterType
 */
#ifndef DRXPDFRSYNCERRORCOUNTER_T_HXX
#define DRXPDFRSYNCERRORCOUNTER_T_HXX

#include "xsd_ptree_iface.hxx"

#include <stdint.h>
//#include <cstdint> // c++x11

namespace XSD {

class drxpDfrSyncErrorCounter_t : public XSDPtreeInterface
{
    public:
        uint64_t ch1BitD;
        uint64_t ch2BitC;
        uint64_t ch3BitB;

    public:
        drxpDfrSyncErrorCounter_t() { }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif

