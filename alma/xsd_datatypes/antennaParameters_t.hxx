/**
 * \class antennaParameters_t
 *
 * Container for XSD AntennaParametersType
 */
#ifndef ANTENNAPARAMETERS_T_HXX
#define ANTENNAPARAMETERS_T_HXX

#include "xsd_ptree_iface.hxx"
#include "spectrometerInput_t.hxx"

namespace XSD
{

class antennaParameters_t : public XSDPtreeInterface
{
    public:
        spectrometerInput_t spectrometerInput;
        unsigned int loOffsetIndex;
        unsigned int walshIndex;

    public:
        antennaParameters_t() : loOffsetIndex(0), walshIndex(0) {
            spectrometerInput = 1;
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= spectrometerInput.validate();
            valid &= /* (loOffsetIndex >= 0) && */ (loOffsetIndex <= 128);
            valid &= /* (walshIndex >= 0) && */ (walshIndex <= 128);
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
