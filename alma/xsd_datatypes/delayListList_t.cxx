#include "delayListList_t.hxx"
#include "global_defs.hxx"

#include <boost/foreach.hpp>

namespace XSD {

bool delayListList_t::get(const boost::property_tree::ptree& in)
{
    using boost::property_tree::ptree;

    delayListList.clear();

    BOOST_FOREACH(const ptree::value_type &v, in.get_child("")) {
        if (v.first != "delayList") { continue; }
        delayList_t dlist;
        dlist.get(v.second);
        push_back(dlist);
    }

    return validate();
}

bool delayListList_t::put(boost::property_tree::ptree& out) const
{
    using boost::property_tree::ptree;
    std::deque<delayList_t>::const_iterator it;

    for (it = delayListList.begin(); it != delayListList.end(); it++) {
        ptree dout;
        it->put(dout);
        out.add_child("delayList", dout);
    }

    return validate();
}

void delayListList_t::push_back(delayList_t& delayList)
{
    if (delayListList.size() >= MAX_DELAYLIST_LIST_LENGTH) {
        delayListList.pop_front();
    }
    delayListList.push_back(delayList);
}

}
