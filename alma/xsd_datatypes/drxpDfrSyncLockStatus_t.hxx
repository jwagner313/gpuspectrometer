/**
 * \class drxpDfrSyncLockStatus_t
 *
 * Container for XSD DrxpDfrSyncLockStatusType
 */
#ifndef DRXPDFRSYNCLOCKSTATUS_T_HXX
#define DRXPDFRSYNCLOCKSTATUS_T_HXX

#include "drxpBitSyncLockStatus_t.hxx"
#include "xsd_ptree_iface.hxx"

namespace XSD {

class drxpDfrSyncLockStatus_t : public XSDPtreeInterface
{
    public:
        drxpBitSyncLockStatus_t ch1BitD;
        drxpBitSyncLockStatus_t ch2BitC;
        drxpBitSyncLockStatus_t ch3BitB;

    public:
        drxpDfrSyncLockStatus_t() : ch1BitD(), ch2BitC(), ch3BitB() { }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
