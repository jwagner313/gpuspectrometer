#include "drxpBitPowerAlarm_t.hxx"

namespace XSD {

bool drxpBitPowerAlarm_t::get(const boost::property_tree::ptree& in)
{
    // NOTE: there is currently no use for get() since values arrive read-only from DRXP hardware

    return true;
}

bool drxpBitPowerAlarm_t::put(boost::property_tree::ptree& out) const
{
    // Note: drxpStatus with elements like <p0.95VForFpgaCore>false</p0.95VForFpgaCore> have to use an
    // alternate delimiter call e.g. pt.put(boost::property_tree::ptree::path_type("p0.95VForFpgaCore", '_'), false);

    using boost::property_tree::ptree;

    out.put(ptree::path_type("p3.3VWarning", '|'), p3_3VWarning);
    out.put(ptree::path_type("p3.3VAlarm", '|'), p3_3VAlarm);
    out.put(ptree::path_type("i2cAccessError", '|'), i2cAccessError);

    return true;
}

}
