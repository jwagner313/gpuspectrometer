/**
 * \class drxpDfrChecksumCounter_t
 *
 * Container for XSD DrxpDfrChecksumCounterType
 */
#ifndef DRXPDFRCHECKSUMCOUNTER_T_HXX
#define DRXPDFRCHECKSUMCOUNTER_T_HXX

#include "xsd_ptree_iface.hxx"

#include <stdint.h>
//#include <cstdint> // c++x11

namespace XSD {

/** Container for DRXP Checksum Counters as in DRXP User Manual */
class drxpDfrChecksumCounter_t : public XSDPtreeInterface
{
    public:
        uint64_t ch1BitD;
        uint64_t ch2BitC;
        uint64_t ch3BitB;

    public:
        drxpDfrChecksumCounter_t() { }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
