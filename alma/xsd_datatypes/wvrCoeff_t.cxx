
#include "wvrCoeff_t.hxx"

#include <algorithm>
#include <cstdlib>
#include <sstream>
#include <string>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>

using boost::property_tree::ptree;

namespace XSD
{

template<typename T> void wvrCoeff_t::parse_coefficients(std::string raw, std::vector<T>& out, const char separator /* default: ' ' */)
{
    std::istringstream iss(raw);
    std::string coeff;
    while (std::getline(iss, coeff, separator)) {
        out.push_back((T)atof(coeff.c_str()));
    }
}

template<typename T> std::string wvrCoeff_t::generate_coefficients(const std::vector<T>& vec, const char* separator /* default: " " */ ) const
{
    std::ostringstream oss;
    std::copy(vec.begin(), vec.end()-1, typename std::ostream_iterator<T>(oss, separator));
    oss << vec.back();
    return oss.str();
}

template<typename T> void wvrCoeff_t::parse_elementSequence(const boost::property_tree::ptree& in, const char* multielementname, std::vector<T>& vec)
{
    BOOST_FOREACH(const ptree::value_type &v, in.get_child("")) {
        if (v.first != multielementname) { continue; }
        vec.push_back(v.second.get_value<T>());
    }
}

template<typename T> void wvrCoeff_t::generate_elementSequence(boost::property_tree::ptree& out, const char* multielementname, const std::vector<T>& vec) const
{
    typename std::vector<T>::const_iterator it;
    for (it = vec.begin(); it != vec.end(); it++) {
        out.add<T>(multielementname, *it);
    }
}

bool wvrCoeff_t::get(const boost::property_tree::ptree& in)
{
    spectrometerInput = in.get("spectrometerInput", 1);
    antennaName = in.get("antennaName", "");
    startValidTime = in.get("startValidTime", "");
    endValidTime = in.get("endValidTime", "");

    if (in.get("WVRMethod", "") == "ATM_MODEL") {
        WVRMethod = ATM_MODEL;
    } else {
        WVRMethod = EMPIRICAL;
    }

    // PathCoeff is nested: { N x pathCoef { index; M x inputCoeff { index; L x chanCoeff { index; K x double } } } } }
    BOOST_FOREACH(const ptree::value_type &v, in.get_child("")) {
        if (v.first != "pathCoeff") { continue; }
        XSD::pathCoeff_t pt;
        pt.index = v.second.get("index", 0);
        BOOST_FOREACH(const ptree::value_type &v2, v.second.get_child("")) {
            if (v2.first != "inputCoeff") { continue; }
            XSD::inputCoeff_t ic;
            ic.index = v2.second.get("index", 0);
            BOOST_FOREACH(const ptree::value_type &v3, v2.second.get_child("")) {
                if (v3.first != "chanCoeff") { continue; }
                XSD::chanCoeff_t cc;
                cc.index = v3.second.get("index", 0);
                std::string raw_coeff = v3.second.get("polyCoeff", "0");
                parse_coefficients<double>(raw_coeff, cc.polyCoeff);
                ic.chanCoeff.push_back(cc);
            }
            pt.inputCoeff.push_back(ic);
        }
        pathCoeff.push_back(pt);
    }

    // RefTemp is nested:  { N x refTemp { index; M x inputRefTemp { index; L x double } } } }
    BOOST_FOREACH(const ptree::value_type &v, in.get_child("")) {
        if (v.first != "refTemp") { continue; }
        XSD::refTemp_t rt;
        rt.index = v.second.get("index", 0);
        BOOST_FOREACH(const ptree::value_type &v2, v.second.get_child("")) {
            if (v2.first != "inputRefTemp") { continue; }
            XSD::inputRefTemp_t it;
            it.index = v2.second.get("index", 0);
            std::string raw_coeff = v2.second.get("chanRefTemp", "0");
            parse_coefficients<double>(raw_coeff, it.chanRefTemp);
            rt.inputRefTemp.push_back(it);
        }
        refTemp.push_back(rt);
    }

    // chanFreq and others are not nested
    parse_elementSequence<double>(in, "chanFreq", chanFreq);
    parse_elementSequence<double>(in, "chanWidth", chanWidth);
    parse_elementSequence<double>(in, "polyFreqLimits", polyFreqLimits); // should be 2 elements exactly
    numChan = in.get("numChan", 0);
    numInputAntenna = in.get("numInputAntenna", 0);
    parse_elementSequence<std::string>(in, "inputAntennaNames", inputAntennaNames);
    numPoly = in.get("numPoly", 0);

    return true;
}

bool wvrCoeff_t::put(boost::property_tree::ptree& out) const
{
    out.put("spectrometerInput", spectrometerInput.input);
    out.put("antennaName", antennaName);
    out.put("startValidTime", startValidTime);
    out.put("endValidTime", endValidTime);
    if (WVRMethod == ATM_MODEL) {
        out.put("WVRMethod", "ATM_MODEL");
    } else {
        out.put("WVRMethod", "EMPIRICAL");
    }

    // TODO: add empty elements if pathCoeff is empty, or pathCoeff::inputCoeff empty, or pathCoeff::inputCoeff::polycoeff empty
    //       because currently if pathCoeff is empty the element is not generated (skips to "refTemp" or "chanFreq") and XML is not valid

    std::vector<pathCoeff_t>::const_iterator pc;
    for (pc = pathCoeff.begin(); pc != pathCoeff.end(); pc++) {
        ptree pathcoeff;
        pathcoeff.add("index", pc->index);
        std::vector<inputCoeff_t>::const_iterator ic;
        for (ic = pc->inputCoeff.begin(); ic != pc->inputCoeff.end(); ic++) {
            ptree inputCoeff;
            inputCoeff.add("index", ic->index);
            std::vector<chanCoeff_t>::const_iterator cc;
            for (cc = ic->chanCoeff.begin(); cc != ic->chanCoeff.end(); cc++) {
                ptree chancoeff;
                std::string polycoeff = generate_coefficients<double>(cc->polyCoeff);
                chancoeff.add("index", cc->index);
                chancoeff.add("polyCoeff", polycoeff);
                inputCoeff.add_child("chanCoeff", chancoeff);
            }
            pathcoeff.add_child("inputCoeff", inputCoeff);
        }
        out.add_child("pathCoeff", pathcoeff);
    }

    std::vector<refTemp_t>::const_iterator rt;
    for (rt = refTemp.begin(); rt != refTemp.end(); rt++) {
        ptree reftemp;
        reftemp.add("index", rt->index);
        std::vector<inputRefTemp_t>::const_iterator it;
        for (it = rt->inputRefTemp.begin(); it != rt->inputRefTemp.end(); it++) {
            ptree inputReftemp;
            std::string coeffs = generate_coefficients<double>(it->chanRefTemp);
            inputReftemp.add("index", it->index);
            inputReftemp.add("chanRefTemp", coeffs);
            reftemp.add_child("inputRefTemp", inputReftemp);
        }
        out.add_child("refTemp", reftemp);
    }

    generate_elementSequence<double>(out, "chanFreq", chanFreq);
    generate_elementSequence<double>(out, "chanWidth", chanWidth);
    generate_elementSequence<double>(out, "polyFreqLimits", polyFreqLimits);
    out.put("numChan", numChan);
    out.put("numInputAntenna", numInputAntenna);
    generate_elementSequence<std::string>(out, "inputAntennaNames", inputAntennaNames);
    out.put("numPoly", numPoly);

    return true;
}

}
