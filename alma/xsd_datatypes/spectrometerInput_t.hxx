/**
 * \class spectrometerInput_t
 *
 * Container for XSD SpectrometerInputType
 */
#ifndef SPECTROMETERINPUT_T_HXX
#define SPECTROMETERINPUT_T_HXX

#include "xsd_enums.hxx"
#include "xsd_ptree_iface.hxx"

#include <iostream>
#include <string>

namespace XSD  {

class spectrometerInput_t : public XSDPtreeInterface
{
    public:
        int input;

    public:
        spectrometerInput_t() : input(1) { }

        spectrometerInput_t(int i) : input(i) { }

        int operator=(int v) {
            input = v;
            return input;
        }

        bool operator==(int& rhs) {
            return input==rhs;
        }

        bool operator==(spectrometerInput_t& rhs) {
            return input==rhs.input;
        }

        bool operator>(int rhs) {
            return input>rhs;
        }

        bool operator>(spectrometerInput_t& rhs) {
            return input>rhs.input;
        }

        bool operator>=(int rhs) {
            return input>=rhs;
        }

        bool operator>=(spectrometerInput_t& rhs) {
            return input>=rhs.input;
        }

        bool operator<(int rhs) {
            return input<rhs;
        }

        bool operator<(spectrometerInput_t& rhs) {
            return input<rhs.input;
        }

        bool operator<=(int rhs) {
            return input<=rhs;
        }

        bool operator<=(spectrometerInput_t& rhs) {
            return input<=rhs.input;
        }

    public:
        bool validate() const {
            bool valid = true;
            valid &= input >= 1;
            valid &= input <= 16;
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
