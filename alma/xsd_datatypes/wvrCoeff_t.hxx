/**
 * \class wvrCoeff_t
 *
 * Container for XSD WvrCoeffType
 */
#ifndef WVRCOEFF_T_HXX
#define WVRCOEFF_T_HXX

#include "xsd_enums.hxx"
#include "xsd_ptree_iface.hxx"

#include "xsd_datatypes/spectrometerInput_t.hxx"
#include "xsd_datatypes/pathCoeff_t.hxx"
#include "xsd_datatypes/refTemp_t.hxx"

#include <cassert>
#include <string>
#include <vector>

namespace XSD
{

class wvrCoeff_t : public XSDPtreeInterface
{
    public:
        spectrometerInput_t spectrometerInput;
        std::string antennaName;
        std::string startValidTime; // UtcDateTimeMs
        std::string endValidTime; // UtcDateTimeMs
        WVRMethod_t WVRMethod;
        std::vector<pathCoeff_t> pathCoeff;
        std::vector<refTemp_t> refTemp;
        std::vector<double> chanFreq;
        std::vector<double> chanWidth;
        //double polyFreqLimits[2];
        std::vector<double> polyFreqLimits;
        unsigned int numChan;
        unsigned int numInputAntenna;
        std::vector<std::string> inputAntennaNames;
        unsigned int numPoly;

    public:
        wvrCoeff_t() : WVRMethod(ATM_MODEL) {
        }

    public:
        bool validate() const {
            assert(numInputAntenna == inputAntennaNames.size());
            bool valid = true;
            valid &= spectrometerInput.validate();
            valid &= (pathCoeff.size() >= 1);
            valid &= (refTemp.size() >= 1);
            valid &= (chanFreq.size() >= 1);
            valid &= (chanWidth.size() >= 1);
            valid &= (polyFreqLimits.size() == 2); // polyFreqLimits[2]
            valid &= (inputAntennaNames.size() >= 1);
            valid &= (numInputAntenna == inputAntennaNames.size());
            for (std::vector<pathCoeff_t>::const_iterator it = pathCoeff.begin(); it != pathCoeff.end(); it++) {
                valid &= it->validate();
            }
            for (std::vector<refTemp_t>::const_iterator it = refTemp.begin(); it != refTemp.end(); it++) {
                valid &= it->validate();
            }
            return valid;
        }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

    private:
        template<typename T> void parse_coefficients(std::string raw, std::vector<T>& out, const char separator=' ');
        template<typename T> std::string generate_coefficients(const std::vector<T>& vec, const char* separator=" ") const;
        template<typename T> void parse_elementSequence(const boost::property_tree::ptree& in, const char* multielementname, std::vector<T>& vec);
        template<typename T> void generate_elementSequence(boost::property_tree::ptree& out, const char* multielementname, const std::vector<T>& vec) const;

};

}

#endif
