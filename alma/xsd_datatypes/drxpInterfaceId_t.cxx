#include "drxpInterfaceId_t.hxx"

namespace XSD {

bool drxpInterfaceId_t::get(const boost::property_tree::ptree& in)
{
    return validate();
}

bool drxpInterfaceId_t::put(boost::property_tree::ptree& out) const
{
    return validate();
}

}
