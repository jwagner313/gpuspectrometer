#ifndef DERIVED_PROCCONFIG_T_HXX
#define DERIVED_PROCCONFIG_T_HXX

#include "xsd_enums.hxx"    // defs of window function UNIFORM, sidebandsSIDEBAND_USB, SIDEBAND_LSB
#include "sideBand_t.hxx"   // def sideBand_t
#include "global_defs.hxx"  // defs of ANTENNA_IF_LOWEDGE_MHZ, ANTENNA_IF_HIGHEDGE_MHZ, ANTENNA_IF_SIDEBAND_USB

#include <time.h>
#include <string.h>

/** Implementation-specific settings and internal representation of some ICD settings derived from basebandConfig_t and timeSpec_t */
struct procConfig_t
{
    public:
        /* Implicit info of sampler/IF system that is not part of M&C "Configuration" */
        double implicit_IF_lowedge_MHz;  /// Low edge in MHz of the IF; needed to interpret 'centerFrequency', 'loOffset'
        double implicit_IF_highedge_MHz; /// High edge in MHz of the IF; needed to interpret 'centerFrequency', 'loOffset'
        double implicit_IF_width_MHz;
        XSD::sideBand_t implicit_IF_sideband; /// Orientation of the IF

        /* Time range and timing */
        struct timespec requestedStartTime;
        struct timespec requestedStopTime;
        int Tswitchingcycle_msec;    /// Length in milliseconds of timeSpec::switching[], sum of all dwell_sec and transition_sec
        int Tswitchingdwell_msec;    /// Length in milliseconds of timeSpec::switching[], sum of only dwell_sec (actual integration)

        /* Walshing, LO offsetting */
        unsigned int loOffsetIndices[MAX_SPECTROMETER_INPUTS];
        unsigned int walshIndices[MAX_SPECTROMETER_INPUTS];

        /* Time division multiplexing on GPU */
        int nGPUs;       /// How many GPU to use in parallel
        int nstreams;    /// How many CUDA Streams per GPU to create
        int mapGPUs[32]; /// Map the range [0:nGPUs-1] --> [gpu a, gpu b, ..., gpu x]

        /* Derived */
        int Tint_msec;   /// Requested 'integrationDuration' in milliseconds
        int Tsubint_max_msec; /// Maximum length of a subintegration (1ms..48ms) constrained by available GPU/CPU memory
        int nchan;       /// Number of output channels; currently fixed to 512k (2^20-pt DFT) for ACA compatibility and averaged down later
        int fftinstep;   /// Number of samples to advance by in input data for each FFT; overlapped FFTs occur with fftinstep<2*nchan
        int npol;        /// 1 or 2 polarizations per IF; ASTE has 1, ALMA and Nobeyama have 2
        int nstokes;     /// 1 if single pol, 2 if dual pol XX,YY only, 4 if dual pol XX,YY,Re Im XY
        int nswpos;      /// 1 or more switching positions

        XSD::windowFunction_t windowFunction;

    public:
        procConfig_t() {
            requestedStartTime.tv_sec = 0;
            requestedStartTime.tv_nsec = 0;
            requestedStopTime.tv_sec = 0;
            requestedStopTime.tv_nsec = 0;
            Tswitchingcycle_msec = 0;
            Tswitchingdwell_msec = 0;
            windowFunction = XSD::UNIFORM;
            implicit_IF_lowedge_MHz = ANTENNA_IF_LOWEDGE_MHZ;
            implicit_IF_highedge_MHz = ANTENNA_IF_HIGHEDGE_MHZ;
            implicit_IF_width_MHz = implicit_IF_highedge_MHz - implicit_IF_lowedge_MHz;
            implicit_IF_sideband = ANTENNA_IF_SIDEBAND_USB ? XSD::SIDEBAND_USB : XSD::SIDEBAND_LSB;
            npol = 2;
            nstokes = 4;
            nchan = DEFAULT_INTERNAL_DFT_LENGTH/2;
            fftinstep = DEFAULT_INTERNAL_DFT_INPUTSTEP;
            Tsubint_max_msec = 48;
            nGPUs = 1;
            nstreams = 1;
            memset(mapGPUs, 0, sizeof(mapGPUs));
        }

};


#endif
