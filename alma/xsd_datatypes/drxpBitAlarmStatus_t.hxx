/**
 * \class drxpBitAlarmStatus_t
 *
 * Container for XSD DrxpBitAlarmStatusType
 */
#ifndef DRXPBITALARMSTATUS_T_HXX
#define DRXPBITALARMSTATUS_T_HXX

#include "xsd_ptree_iface.hxx"

namespace XSD {

class drxpBitAlarmStatus_t : public XSDPtreeInterface
{
    public:
        bool temperatureWarning;
        bool outputBiasCurrentWarning;
        bool outputPowerWarning;
        bool rxInputPowerWarning;
        bool temperatureAlarm;
        bool outputBiasCurrentAlarm;
        bool outputPowerAlarm;
        bool rxInputPowerAlarm;
        bool i2cAccessError;

    public:
        drxpBitAlarmStatus_t() : temperatureWarning(false), outputBiasCurrentWarning(false),
            outputPowerWarning(false), rxInputPowerWarning(false), temperatureAlarm(false),
            outputBiasCurrentAlarm(false), outputPowerAlarm(false), rxInputPowerAlarm(false),
            i2cAccessError(false)
        {
        }

    public:
        bool validate() const { return true; }
        bool get(const boost::property_tree::ptree&);
        bool put(boost::property_tree::ptree&) const;

};

}

#endif
