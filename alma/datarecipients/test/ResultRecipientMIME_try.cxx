
#include "datarecipients/ResultRecipientMIME.hxx"
#include "datarecipients/SpectralResultSet.hxx"
#include "mcwrapping/mc_observation.hxx"

#include <cstring>

int main(int argc, char** argv)
{
    MCObservation obs;
    obs.observationId = "uid://A002/xyz98/xf7c6/5fb1";
    obs.antennaId = "1";
    obs.baseBandName = "BB_1";

    const char resultDummy[] = "[BINARY DATA HERE]        ";
    const int N = sizeof(resultDummy)/sizeof(float);

    // Fake histogram; eval ' round(exp(-[-7,-5,-3,-1,1,3,5,7].^2/(2*(7/3)^2)) * 1000) '
    hist_t histDummy[8] = { 11, 101, 438, 912, 912, 438, 101, 11 };

    ResultRecipientMIME* r = new ResultRecipientMIME(DEFAULT_MIME_OUTPUT_PATH);
    obs.outputWriter = r;
    r->configureFrom(&obs);
    r->open();

    for (int s = 0; s < 10; s++) {

        SpectralResultSet* res = new SpectralResultSet();
        res->allocate(1, sizeof(resultDummy)-1, sizeof(resultDummy)-1);
        res->storeHistogram(histDummy, histDummy);
        memcpy(res->spectra[0], resultDummy, sizeof(resultDummy)-1);
        memcpy(res->finalspectra, resultDummy, sizeof(resultDummy)-1);

        struct timeval tv;
        gettimeofday(&tv, NULL);
        res->timestamp.tv_sec = tv.tv_sec;
        res->timestamp.tv_nsec = 1e3*tv.tv_usec;

        r->takeResult(res);
        delete res;

        usleep(100e3);
    }

    r->close();
    obs.outputWriter = NULL;
    delete r;
}
