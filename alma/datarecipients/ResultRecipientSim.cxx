/**
 *
 * \class ResultRecipientSim
 *
 * Simulates the ResultRecipient interface invoked by a spectral processor after the completion of
 * a new set of spectra from a single baseband (1-pol, or 2-pol auto, or 2-pol auto and cross).
 *
 * Ignores the input data and reads a MIME/BDF file instead, and sends out its contents.
 *
 */

#include "datarecipients/ResultRecipientSim.hxx"
#include "datarecipients/SpectralResultSet.hxx"
#include "mcwrapping/mc_configuration.hxx" // class MCConfiguration
#include "mcwrapping/mc_observation.hxx"   // class MCObservation (has observation metadata and another copy of MCConfiguration)
#include "helper/logger.h"
#include "global_defs.hxx"

#include <boost/noncopyable.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread.hpp>

#include <stdlib.h>
#include <sys/time.h>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <sstream>

#define lockscope  // just a blank define use for nicer syntax of "{ boost::mutex::scoped_lock x(mtx); ... }" scopes

//////////////////////////////////////////////////////////////////////////////////////////////////////

class ResultRecipientSim_impl;
class ResultRecipientSim_impl : private boost::noncopyable
{
    friend class ResultRecipientSim;

    boost::mutex mutex; /// a mutex to serialize access where needed

    std::string root; /// root for 'Content-Location:' e.g. "xyz987654321/1/BB_1/"
    int nQueued;      /// number of results not yet fully written out
    boost::condition_variable alldoneCond; /// signaled when no results are pending and relatively save to quit

    std::string simulation_mime_input;
    std::string outfilepath;
    std::string outfilename;
    std::ofstream outfile;
    struct timespec first_timestamp;

    /** Detached thread launched by every call of takeResult(). Simulation. Actually loads spectra from a file. */
    void results_storer();

    /** C'stor */
    ResultRecipientSim_impl() {
        nQueued = 0;
        first_timestamp.tv_sec = 0;
        first_timestamp.tv_nsec = 0;
    }

};

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** C'stor */
ResultRecipientSim::ResultRecipientSim(std::string outputPath) : ResultRecipient(outputPath)
{
    ResultRecipientSim_impl* impl = new ResultRecipientSim_impl;
    impl->nQueued = 0;
    impl->outfilepath = outputPath;
    impl->outfilename = "/tmp/ResultRecipientSim.mime";
    impl->root = "defaultObs/1/BB_1/";
    impl->simulation_mime_input = "/tmp/nonexistent_input.mime";
    pimpl = (void*)impl;
}

/** D'stor, blocks until reasonably sure all data were written */
ResultRecipientSim::~ResultRecipientSim()
{
    this->close();
    delete (ResultRecipientSim_impl*)pimpl;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Copy certain needed MIME/BDF infos from MCObservation; if used then call this before open() */
void ResultRecipientSim::configureFrom(const MCObservation* obs)
{
    ResultRecipientSim_impl* impl = (ResultRecipientSim_impl*)pimpl;
    lockscope {
        boost::mutex::scoped_lock mmlock(impl->mutex);
        std::string of = obs->observationId + "_" + obs->antennaId + "_" + obs->baseBandName + ".mime";
        std::replace(of.begin(), of.end(), ':', '_');
        std::replace(of.begin(), of.end(), '*', '_');
        std::replace(of.begin(), of.end(), '?', '_');
        std::replace(of.begin(), of.end(), '/', '_');
        impl->outfilename = impl->outfilepath + of;
        impl->root = obs->observationId + "/" + obs->antennaId + "/" + obs->baseBandName + "/";
        impl->first_timestamp.tv_sec = 0;
        impl->first_timestamp.tv_nsec = 0;
        // TODO: copy more infos?
        // TODO: figure out what is the actual proper name of the file
    }
}

/** Open the BDF file */
void ResultRecipientSim::open()
{
    ResultRecipientSim_impl* impl = (ResultRecipientSim_impl*)pimpl;

    impl->outfile.open(impl->outfilename.c_str());
    L_(linfo) << "ResultRecipientSim initialized MIME output to file " << impl->outfilename;
}

/** Block until all results were written */
void ResultRecipientSim::sync()
{
    if (pimpl == NULL) {
        return;
    }

   // Wait for all dispatched writers to finish
    ResultRecipientSim_impl* impl = (ResultRecipientSim_impl*)pimpl;
    lockscope {
        boost::mutex::scoped_lock mmlock(impl->mutex);
        while (impl->nQueued > 0) {
            impl->alldoneCond.wait(mmlock);
        }
    }
}

/** Safely close the MIME file*/
void ResultRecipientSim::close()
{
    if (pimpl == NULL) {
        return;
    }

    // Wait for all dispatched writers to finish
    sync();

    // Close output
    ResultRecipientSim_impl* impl = (ResultRecipientSim_impl*)pimpl;
    if (impl->outfile.is_open()) {
        impl->outfile.close();
        L_(linfo) << "ResultRecipientSim closed MIME output file " << impl->outfilename;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

void ResultRecipientSim::setSourceMIME(std::string inputfile)
{
    ResultRecipientSim_impl* impl = (ResultRecipientSim_impl*)pimpl;
    impl->simulation_mime_input = inputfile;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Pretend to take a spectral dataset and store it in MIME format. Actually store the contents of a fixed test file instead */
void ResultRecipientSim::takeResult(const void* resultset)
{
    const SpectralResultSet* dummy = (const SpectralResultSet*)resultset;

    // Increment the queue length
    lockscope {
        ResultRecipientSim_impl* impl = (ResultRecipientSim_impl*)pimpl;
        boost::mutex::scoped_lock mmlock(impl->mutex);
        impl->nQueued++;
    }

    // Launch a worker that sends some predeterimed MIME data */
    lockscope {
        ResultRecipientSim_impl* impl = (ResultRecipientSim_impl*)pimpl;
        boost::mutex::scoped_lock mmlock(impl->mutex);
        boost::thread store(&ResultRecipientSim_impl::results_storer, impl);
        store.detach();

        if (impl->first_timestamp.tv_sec == 0) {
            impl->first_timestamp = dummy->timestamp;
        }
        float dT = (dummy->timestamp.tv_sec - impl->first_timestamp.tv_sec) + 1e-9*(dummy->timestamp.tv_nsec - impl->first_timestamp.tv_nsec);
        L_(linfo) << "ResultRecipientSim dispatched " << (dummy->nspectra * dummy->bytesperspectrum) << "-byte raw and "
                  << dummy->bytesinfinalspectra << "-byte postprocessed data of time dT=" << std::fixed << std::setprecision(4) << dT << "s to output writer";
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Detached thread launched by every call of takeResult(). Simulation. Actually loads spectra from a file. */
void ResultRecipientSim_impl::results_storer()
{
    std::string fakefile = simulation_mime_input;
    try {
        std::ifstream src(fakefile.c_str(), std::ios::binary);
        outfile << src.rdbuf();
        if (src.tellg() <= 0) {
            L_(lwarning) << "ResultRecipientSim copying from file '" << fakefile << "' returned " << src.tellg();
        }
        //std::cout << "ResultRecipientSim_impl copied " << src.tellg() << " bytes" << std::endl;
    } catch (...) {
        L_(linfo) << "ResultRecipientSim could not copy " << fakefile << " contents to " << outfilename;
    }

    // Finished so can decrement the workers count, notify any listener if this was the last worker
    lockscope {
        boost::mutex::scoped_lock mmlock(mutex);
        if (this->nQueued >= 1) {
            this->nQueued--;
            if (this->nQueued == 0) {
                this->alldoneCond.notify_all();
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
