% Generate a wideband spectrum
%
% Can be used as input to test 'SpectralPostprocessor.cxx' and the
% extraction and calibration of individual spectral windows
%
% Writes XX YY ReIm{XY} into output file.
%
function spectrumGen()

    % Number of channels:
    N = 524288;
    
    % Spectra
    xx = m_makeSpec(N);
    yy = 3*m_makeSpec(N);
    xy = m_makeSpec(N) .* exp(-1i*linspace(-10*pi,10*pi,N)'); % slope in phase
    
    % Save to file
    fd = fopen('widebandSpectrum_XX_YY_XY.bin', 'w');
    fwrite(fd, xx, 'float32');
    fwrite(fd, yy, 'float32');
    xy_Re = real(xy);
    xy_Im = imag(xy);
    xy_ReIm = [xy_Re; xy_Im];
    fwrite(fd, xy_ReIm(:), 'float32');    
    fclose(fd);
    
    % Show
    figure(1),
    clf,
    subplot(4,1,1), plot(xx), title('XX'), xlim([0 N-1]);
    subplot(4,1,2), plot(yy), title('XY'), xlim([0 N-1]);
    subplot(4,1,3), plot(abs(xy)), title('Abs{XY}'), xlim([0 N-1]);
    subplot(4,1,4), plot(angle(xy),'x'), title('Phase{XY}'), xlim([0 N-1]);
    
end

function s = m_makeSpec(N)
    % Noise
    s = 1e-1*(randn(N,1));
    % Baseline
    s = s + 1.5;
    % Spectral components
    s = s + circshift(0.5*gausswin(N,10), +round(0.1*N));
    s = s + circshift(2.8*gausswin(N,50), -round(0.15*N));
    s = s + circshift(3*gausswin(N,500), -round(0.25*N));
    s = s + circshift(15*gausswin(N,500), -round(0.255*N));
    % Discard negative power spectral components
    s(s < 0) = std(s);
end
