#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

const float gray_lut[8] = { -7.0f, -5.0f, -1.0f, -3.0f, +7.0f, +5.0f, +1.0f, +3.0f };

int main(int argc, char** argv)
{
    unsigned char in[48];
    float X[128], Y[128];
    int s;

    FILE *fin, *foutX, *foutY;

    if (argc != 2) {
        printf("Usage: unpack_dtx2float <dtxformatfile.raw>\n");
        return 0;
    }

    fin = fopen(argv[1], "r");
    if (fin == NULL) {
        printf("Error: could not open '%s' for reading\n", argv[1]);
        return -1;
    }

    foutX = fopen("xpol.f32", "w");
    foutY = fopen("ypol.f32", "w");

    while (1) {
        size_t n = fread(in, sizeof(in), 1, fin);
        if (n <= 0) break;
        for (s=0; s<128; s++) {
            unsigned char vx, vy;
            int off = /*0 or 1*/((s%16)/8) + /*0,4,8,12*/4*(int)(s/16);
            int bit = s % 8;
            vx = ((in[0 + off] >> bit)&1) | (((in[16 + off] >> bit)&1) << 1) | (((in[32 + off] >> bit)&1) << 2);
            vy = ((in[2 + off] >> bit)&1) | (((in[18 + off] >> bit)&1) << 1) | (((in[34 + off] >> bit)&1) << 2);
            X[s] = gray_lut[vx];
            Y[s] = gray_lut[vy];
        }
        fwrite(X, sizeof(X), 1, foutX);
        fwrite(Y, sizeof(Y), 1, foutY);
    }

    return 0;
}
