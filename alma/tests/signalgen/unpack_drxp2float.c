#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

const float gray_lut[8] = { -7.0f, -5.0f, -1.0f, -3.0f, +7.0f, +5.0f, +1.0f, +3.0f };

int main(int argc, char** argv)
{
    unsigned char in[12];
    uint32_t w24[4];
    float X[16], Y[16];
    int s;

    FILE *fin, *foutX, *foutY;

    if (argc != 2) {
        printf("Usage: unpack_drxp2float <drxpformatfile.raw>\n");
        return 0;
    }

    fin = fopen(argv[1], "r");
    if (fin == NULL) {
        printf("Error: could not open '%s' for reading\n", argv[1]);
        return -1;
    }

    foutX = fopen("xpol.f32", "w");
    foutY = fopen("ypol.f32", "w");

    while (1) {
        size_t n = fread(in, sizeof(in), 1, fin);
        if (n <= 0) break;
        w24[0] = ((uint32_t)in[0]) << 16 | ((uint32_t)in[1]) << 8 | ((uint32_t)in[2]);
        w24[1] = ((uint32_t)in[3]) << 16 | ((uint32_t)in[4]) << 8 | ((uint32_t)in[5]);
        w24[2] = ((uint32_t)in[6]) << 16 | ((uint32_t)in[7]) << 8 | ((uint32_t)in[8]);
        w24[3] = ((uint32_t)in[9]) << 16 | ((uint32_t)in[10]) << 8 | ((uint32_t)in[11]);
        for (s=0; s<8; s++) {
            X[s+0] = gray_lut[ (w24[0] >> (21 - 3*s)) & 0x07 ];
            X[s+8] = gray_lut[ (w24[1] >> (21 - 3*s)) & 0x07 ];
            Y[s+0] = gray_lut[ (w24[2] >> (21 - 3*s)) & 0x07 ];
            Y[s+8] = gray_lut[ (w24[3] >> (21 - 3*s)) & 0x07 ];
        }
        fwrite(X, sizeof(X), 1, foutX);
        fwrite(Y, sizeof(Y), 1, foutY);
    }

    return 0;
}
