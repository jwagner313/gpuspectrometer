rm -f asm_drxp0.log /tmp/drxp0/asm_drxp0.log

NVARGS="--devices all --concurrent-kernels on --continuous-sampling-interval 1 \
	--cpu-thread-tracing on --cpu-profiling-thread-mode aggregated \
	--force-overwrite --export-profile asm-%p.nvvp"

# Profiling without NUMA settings:
# nvprof $NVARGS ../ASM_MC_service -c asm_drxp0.json $@ 

# Profiling with tie to one processor (multiple cores):
numactl --localalloc --cpunodebind 0 nvprof $NVARGS ../ASM_MC_service -c asm_drxp0.json $@ 

# Profiling with tie to one core:
# numactl --localalloc --physcpubind 0 nvprof $NVARGS ../ASM_MC_service -c asm_drxp0.json $@ 



