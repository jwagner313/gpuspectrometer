/**
 * \class MCMsgCommandResponse_stopObservation
 *
 * \brief Translates an XML observation task received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_observationqueue.hxx"
#include "mcwrapping/mc_configuration.hxx"

#include <string>
#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(stopObservation);

int MCMsgCommandResponse_stopObservation::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    out.put("response.commandId", this->getId());

    // Find the observation that we want to update/stop
    std::string observationId = in.get("observationId", "");
    if (asm_.getObservationQueue().isObservationIdUnique(observationId)) {
        out.put("response.stopObservation.completionCode", "non-existing-observationId");
        return -1;
    }

    // Stop the observation; stop immediate if no time given, or pending if future time given
    boost::optional<std::string> stopTimeUTC_opt = in.get_optional<std::string>("time");
    if (stopTimeUTC_opt) {
        std::string stopTimeUTC = stopTimeUTC_opt.get();
        asm_.getObservationQueue().setObservationStopTime(observationId, stopTimeUTC);
    }

    asm_.getObservationQueue().stopObservation(observationId);
    out.put("response.stopObservation.completionCode", "success");

    return 0;
}
