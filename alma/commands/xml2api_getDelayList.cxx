/**
 * \class MCMsgCommandResponse_getDelayList
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(getDelayList);

int MCMsgCommandResponse_getDelayList::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    using boost::property_tree::ptree;

    out.put("response.commandId", this->getId());
    // out.put("response.getDelayList.completionCode", "success");

    ptree delaylistlist;
    delaylistlist.put("completionCode", "success");
    asm_.getCorrelationSettings().delayLists.put(delaylistlist);
    out.add_child("response.getDelayList", delaylistlist);

    return 0;
}
