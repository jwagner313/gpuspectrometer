
#include "xml2api/xml2api_factory.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(setTestMode);

int MCMsgCommandResponse_setTestMode::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    bool testMode = false;
    if (in.get("", "--") == "true") {
        testMode = true;
    }

    // TODO: invoke spectrometer API

    out.put("response.commandId", this->getId());
    out.put("response.setTestMode.completionCode", "success");
    out.put("response.setTestMode.testMode", testMode ? "true" : "false");

    return 0;
}
