
#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_drxp_status.hxx"
#include "xsd_datatypes/xsd_datatypes.hxx"

#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(clearDrxpAlarms);

int MCMsgCommandResponse_clearDrxpAlarms::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    MCDRXPStatus ds;
    boost::property_tree::ptree dsp;

    XSD::drxpInterfaceId_t iface;
    iface.deviceId = in.get("deviceId", "00:00");
    iface.InterfaceNumber = in.get("InterfaceNumber", 1);
    if (!asm_.getDRXPLookup().isPresent(iface)) {
        L_(lerror) << "getDrxpSerialNumber for non-existend device " << iface.deviceId << " but M&C XSD defines no error code for this case.";
    }
    std::string charDev = asm_.getDRXPLookup().getCharDevice(iface);

    // TODO: actually use charDev!

    asm_.getDRXP()->resetAlarms();
    ds.deviceReadout(asm_.getDRXP());
    ds.getAlarmStatus(dsp);

    out.put("response.commandId", this->getId());
    out.put("response.clearDrxpAlarms.completionCode", "success");
    out.add_child("response.clearDrxpAlarms.status", dsp.get_child("alarmStatus")); // ICD says "response.clearDrxpAlarms.alarmStatus", schema says "response.clearDrxpAlarms.status"...

    return 0;
}
