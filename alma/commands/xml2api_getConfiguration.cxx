/**
 * \class MCMsgCommandResponse_getConfiguration
 *
 * \brief Translates an XML configuration received by M&C command.
 *
 * \author $Author: janw $
 *
 */

#include "xml2api/xml2api_factory.hxx"
#include "mcwrapping/mc_configuration.hxx"

#include <map>
#include <boost/property_tree/ptree.hpp>

XML2API_CREATE_AND_REGISTER_CMD(getConfiguration);

int MCMsgCommandResponse_getConfiguration::handle(const boost::property_tree::ptree& in, boost::property_tree::ptree& out, ASMInterface& asm_)
{
    boost::property_tree::ptree pt;

    out.put("response.commandId", this->getId());

    int req_configId = in.get("configId", -1);
    std::map<unsigned int,MCConfiguration>::iterator pcfg = asm_.getConfigsMap().find(req_configId);
    if (pcfg == asm_.getConfigsMap().end()) {
        out.put("response.getConfiguration.completionCode", "non-existing-configId");
        //out.put("response.getConfiguration.configId", req_configId); // in ICD but not in Schema
        //out.put("response.getConfiguration.singleConfiguration", ""); // Schema bug: not an optional field, but, what to return of no data?
        return -1;
    } else {
        (*pcfg).second.storeInto(pt);
    }

    out.put("response.getConfiguration.completionCode", "success");
    // out.put("response.getConfiguration.configId", req_configId); // in ICD but not in Schema
    out.add_child("response.getConfiguration.singleConfiguration", pt);

    return 0;
}
