#include "drxp/DRXP.hxx"
#include "datarecipients/DataRecipient_iface.hxx"

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

#include <sys/time.h>
#include <string>
#include <sstream>
#include <iostream>

// ToDo: proper build, no include:
#include "kernels/memory_bitcmp_kernels.cu"
#include "cuda_utils.cu"

#define lockscope // just a blank define use for nicer syntax of "{ boost::mutex::scoped_lock x(mtx); ... }" scopes
#define DO_DUMP   // define to write some data into a file

//////////////////////////////////////////////////////////////////////////////////////////////////////

void usage(void)
{
    std::cout << "\nUsage: drxpDataChecker <refFile> <drxpDevs> <gpuIds>\n\n"
              << "  refFile  : file with pattern expected to be sent by DTX and received over DRXP (144MB)\n"
              << "  drxpDevs : comma-separated list of DRXP devices to use, e.g., '/dev/drxpd0,/dev/drxpd1'\n"
              << "  gpuIds   : comma-separated list of GPU cards to use, e.g., '0,1,2,3'\n\n";
    exit(EXIT_FAILURE);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

const int nrawbytes = 144000000;
const int nresultbytes = 2*sizeof(unsigned int);
static boost::mutex m_gpuMutex;

/** Auxiliary class invokeable by DRXP upon reception of new data. Talks to a GPU and
 *  executes the 'cu_bitcmp' kernel to compare new received data against a pattern. */
class GPUChecker : public DataRecipient {

public:
    GPUChecker(const std::string& descriptor, const int gpuId);
    ~GPUChecker();

public:
    /** Load reference pattern from a file */
    void setPattern(const char* patternfile);

    /** Show summary on ostream */
    void summary(std::ostream& os);

public:
    /** Interface inherited by SpectralProcessor from DataRecipient */
    bool takeData(RawDRXPDataGroup data);

private:
    int m_gpuId;
    size_t m_numBlocks;
    size_t m_threadsPerBlock;

    cudaStream_t m_stream;
    cudaEvent_t m_processingComplete;

    size_t refpatternlen;
    unsigned int *d_data;
    unsigned int *h_ref, *d_ref;
    unsigned int *h_results, *d_results;

    std::string m_descriptor;

    size_t m_maxphysthreads;
    size_t m_bit_error_accu;
    size_t m_byte_error_accu;
    size_t m_bit_error_curr;
    size_t m_byte_error_curr;
    size_t m_errorfree_chunks;
    size_t m_faulty_chunks;
};

GPUChecker::GPUChecker(const std::string& desc, const int gpuId)
{
    cudaDeviceProp cudaDevProp;
    m_gpuId = gpuId;
    m_descriptor = desc;

    // Select GPU
    CUDA_CALL( cudaSetDevice(m_gpuId) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, m_gpuId) );
    m_maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;

    // Allocate 144MB arrays, statistics array
    CUDA_CALL( cudaMalloc( (void **)&d_data, nrawbytes ) );
    CUDA_CALL( cudaMalloc( (void **)&d_ref, nrawbytes ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_ref, nrawbytes, cudaHostAllocDefault ) );
    CUDA_CALL( cudaMalloc( (void **)&d_results, nresultbytes ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_results, nresultbytes, cudaHostAllocDefault ) );

    // Stream and event used for async DMA+calc
    CUDA_CALL( cudaStreamCreate( &m_stream ) );
    CUDA_CALL( cudaEventCreateWithFlags(&m_processingComplete, cudaEventDisableTiming|cudaEventBlockingSync) );

    // Reset all data
    m_errorfree_chunks = 0;
    m_faulty_chunks = 0;
    m_bit_error_accu = 0;
    m_byte_error_accu = 0;
    m_bit_error_curr = 0;
    m_byte_error_curr = 0;
    memset(h_ref, 0, nrawbytes);
    memset(h_results, 0, nresultbytes);
    CUDA_CALL( cudaMemcpy( d_ref, h_ref, nrawbytes, cudaMemcpyHostToDevice) );
    CUDA_CALL( cudaMemcpy( d_results, h_results, nresultbytes, cudaMemcpyHostToDevice) );

    // Determine exec layout
    size_t M = cudaDevProp.warpSize;
    size_t N = nrawbytes / sizeof(unsigned int);
    m_numBlocks = div2ceil(N,M);
    m_threadsPerBlock = M;
    printf("GPU exec as <<%zu,%zu>> grid\n", m_numBlocks, m_threadsPerBlock);
    printf("GPU max threads per SM: %d\n", cudaDevProp.maxThreadsPerMultiProcessor);
}

GPUChecker::~GPUChecker()
{
    CUDA_CALL( cudaSetDevice(m_gpuId) );
    CUDA_CALL( cudaEventSynchronize(m_processingComplete) );

    CUDA_CALL( cudaFree(d_data) );
    CUDA_CALL( cudaFree(d_ref) );
    CUDA_CALL( cudaFree(d_results) );
    CUDA_CALL( cudaFreeHost(h_ref) );
    CUDA_CALL( cudaFreeHost(h_results) );

    CUDA_CALL( cudaEventDestroy( m_processingComplete ) );
    CUDA_CALL( cudaStreamDestroy( m_stream ) );
}

/** Show summary on ostream */
void GPUChecker::summary(std::ostream& os)
{
    os << "Data checker status : Nokay=" << m_errorfree_chunks << " Nfaulty=" << m_faulty_chunks << " : "
       << "total errors Nbits=" << m_bit_error_accu << " Nbytes=" << m_byte_error_accu << " : "
       << "current chunk Nbits=" << m_bit_error_curr << " Nbytes=" << m_byte_error_curr << std::endl;
}

/** Load reference pattern from a file */
void GPUChecker::setPattern(const char* patternfile)
{
    // Read the file
    std::ifstream f(patternfile);
    f.read((char*)h_ref, nrawbytes);
    size_t nread = f.gcount();
    f.close();
    if (nread < 1024*1024) {
        std::cerr << "Error: The reference file '" << patternfile << "' contained too little data (" << nread << " bytes)" << std::endl;
        return;
    }
    std::cout << "Info: setPattern() file had " << nread << " bytes" << std::endl;

    // Copy data onto GPU; fill up potentially missing data via repeated appends
    int nrep = 0;
    lockscope {
        boost::mutex::scoped_lock sharedGPUlock(m_gpuMutex);
        CUDA_CALL( cudaSetDevice(m_gpuId) );

        char* h_src = (char*)h_ref;
        char* d_src = (char*)d_ref;
        size_t ntotal = 0;
        while (ntotal < nrawbytes) {
            if ((ntotal + nread) > nrawbytes) {
                nread = nrawbytes - ntotal;
                std::cout << "Warning: setPattern() fill at replication nr " << nrep << " exceeds buffer " << nrawbytes << " by " << nread << " bytes." << std::endl;
            }
            CUDA_CALL( cudaMemcpy( d_src, h_src, nread, cudaMemcpyHostToDevice) );
            h_src += nread;
            d_src += nread;
            ntotal += nread;
            nrep++;
        }
    }
    std::cout << "Info: setPattern() file contents replicated " << nrep << " times into " << nrawbytes << " byte buffer." << std::endl;
}

/** Implentation of the DataRecipient interface, called by DRXP on reception of more data.
 * Asynchronously copies data from the DRXP user buffer onto GPU, then checks it for errors
 * relative to a stored reference pattern.
 */
bool GPUChecker::takeData(RawDRXPDataGroup data)
{
    // Wait for any earlier processing to complete
    CUDA_CALL( cudaEventSynchronize(m_processingComplete) );

    // Initiate data transfer to GPU
    size_t ncopy = data.bufSize;
    if (data.bufSize > nrawbytes) {
        std::cerr << "Warning: GPUChecker::takeData() got " << (data.bufSize-nrawbytes) << " excess bytes over " << nrawbytes << std::endl;
        ncopy = nrawbytes;
    }
    CUDA_CALL( cudaMemcpyAsync(d_data, data.buf, ncopy, cudaMemcpyHostToDevice, m_stream) );

    // Check previous results
    m_bit_error_curr = h_results[0];
    m_byte_error_curr = h_results[1];
    m_bit_error_accu += m_bit_error_curr;
    m_byte_error_accu += m_byte_error_curr;
    if ((m_bit_error_curr > 0) || (m_byte_error_curr > 0)) {
        m_faulty_chunks++;
    } else {
        m_errorfree_chunks++;
    }
    summary(std::cout);

#ifdef DO_DUMP
    if (h_results[0] > 0) {
        std::ofstream ofr("dataCheckerDump.ref");
        ofr.write((char*)h_ref, sizeof(unsigned int)*2048);
        ofr.close();
        std::ofstream ofd("dataCheckerDump.rx");
        ofd.write((const char*)data.buf, sizeof(unsigned int)*2048);
        ofd.close();
    }
#endif

    // Initiate data error checking
    boost::mutex::scoped_lock sharedGPUlock(m_gpuMutex);
    CUDA_CALL( cudaSetDevice(m_gpuId) );
    CUDA_CALL( cudaMemsetAsync(d_results, 0x00, nresultbytes, m_stream) );
    cu_bitcmp <<< m_numBlocks, m_threadsPerBlock, 0, m_stream >>> (d_ref, d_data, d_results);
    CUDA_CHECK_ERRORS("cu_bitcmp");
    CUDA_CALL( cudaMemcpyAsync(h_results, d_results, nresultbytes, cudaMemcpyDeviceToHost) );

    // Enqueue a marker for completed processing
    CUDA_CALL( cudaEventRecord(m_processingComplete, m_stream) );

    return false; // 'False' to indicate to caller that DRXP group to be freed now, not needed later
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct worker_context_tt {
    boost::thread* thrd;
    const char* refFile;
    const char* drxpDevname;
    int gpuId;
} worker_context_t;

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Worker thread: receive data on one DRXP and pass it for checking onto a GPU */
void* drxpGpuWorker(worker_context_t* volatile ctx)
{
    if (ctx == NULL) { return NULL; }

    boost::mutex::scoped_lock sharedGPUlock(m_gpuMutex);
    GPUChecker gpu(std::string(ctx->drxpDevname), ctx->gpuId);
    sharedGPUlock.unlock();

    DRXP drxp;
    drxp.selectDevice(ctx->drxpDevname);
    drxp.resetDevice();
    drxp.startRx();

    gpu.setPattern(ctx->refFile);
    drxp.attachRxRecipient(&gpu);

    int run = 1;
    while (run) {
        boost::this_thread::interruption_point();
        boost::this_thread::yield();
    }

    drxp.detachRxRecipient();
    drxp.stopRx();

    return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    int nworkers;
    worker_context_t* workers;

    if (argc != 4) {
        usage();
    }

    nworkers = 1; // TODO: parse argv[2] for actual workers...
    workers = new worker_context_t[nworkers];

    for (int n=0; n<nworkers; n++) {
        std::cout << "Launching worker " << (n+1) << "/" << nworkers << std::endl;
        workers[n].refFile = argv[1];
        workers[n].drxpDevname = argv[2]; // TODO: split by ','
        workers[n].gpuId = 0; // TODO: split by ','
        workers[n].thrd = new boost::thread(&drxpGpuWorker, &workers[n]);
    }

    std::cout << "All workers launched" << std::endl;
    int run = 1;
    while(run); // TODO: break somehow

    for (int n=0; n<nworkers; n++) {
        workers[n].thrd->interrupt();
        workers[n].thrd->join();
        delete workers[n].thrd;
    }
    delete[] workers;

    return 0;
}
