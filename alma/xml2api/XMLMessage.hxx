/**
 * \class XMLMessage
 *
 * \brief Class to store an XML message
 *
 * The class uses Xerces-C C++ library (if available) to validate XML against
 * a schema file. It then uses the (more robust) Boost PropertyTree to
 * parse the contents of the XML and determine the type of M&C command.
 *
 * Note that if Xerces is used then the Xerces library has to be initialized
 * before the first call to any Xerces functions.
 *
 * \author $Author: janw $
 *
 */

#ifndef XMLMESSAGE_H
#define XMLMESSAGE_H

#include "global_defs.hxx"

#include <string>
#include <boost/property_tree/ptree.hpp>

#if HAVE_XERCES
#include <xercesc/parsers/XercesDOMParser.hpp>
class ParserErrorHandler;
#endif

class XMLMessage {

    private:
        XMLMessage();

    public:
        XMLMessage(const std::string &schemafilename);
        ~XMLMessage();

    public:
        bool isCommand() const { return m_isCommand; }
        const std::string& getCommandName() const { return m_commandName; }
        int getCommandId() const { return m_commandId; }
        boost::property_tree::ptree& getPTree() { return m_pt; }

    public:
        bool load(const char* xmlfilename);
        bool load(std::istream& xmlistream);
        bool load(std::string& xml);
        bool validateXML(const std::string &xml) { return validate(xml); }

    private:
        bool validate(const std::string &xml) const;
        void parse(std::string& xml);

    public:
        std::string generateBasicResponse(boost::property_tree::ptree& pt_out, unsigned int Id, const char* cmd, const char* code, const char* status);

#if HAVE_XERCES
    private:
        xercesc::XercesDOMParser* domParser;
        ParserErrorHandler* parserErrorHandler;
#endif

    private:
        bool m_valid;
        bool m_isCommand;
        unsigned int m_commandId;
        std::string m_commandName;
        std::string m_xmlStr;
        boost::property_tree::ptree m_pt;

};

#endif
