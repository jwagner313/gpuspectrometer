/**
 * \class DRXPPCIeLookup
 *
 * \brief Translates an XSD drxpInterfaceId_t (PCI root and interface <1|2>) into char device
 *
 * \author $Author: janw $
 *
 */

#include "DRXPPCIeLookup.hxx"

#include <glob.h>
#include <stdlib.h>
#include <errno.h>

#include <algorithm>
#include <cstring>
#include <string>
#include <iostream>

DRXPPCIeLookup::DRXPPCIeLookup()
{
    gatherInstalledDevices();
}

void DRXPPCIeLookup::gatherInstalledDevices()
{
    devices.clear();
    interfaces.clear();
    interfaces_ext.clear();

    glob_t glob_result;
    memset(&glob_result, 0, sizeof(glob_result));

    // Look up device symlink of all DRXP char devs, e.g.,
    // /sys/class/drxpd0/drxpd0/device -> ../../../0000:3d:00.0
    // /sys/class/drxpd1/drxpd1/device -> ../../../0000:3d:00.0
    // /sys/class/drxpd2/drxpd2/device -> ../../../0000:b1:00.0
    // /sys/class/drxpd3/drxpd3/device -> ../../../0000:b1:00.0

    const std::string pattern("/sys/class/drxpd*/drxpd*/device");
    if (glob(pattern.c_str(), GLOB_TILDE, NULL, &glob_result) != 0) {
        globfree(&glob_result);
        std::cout << "Glob error " << strerror(errno) << "\n";
        return;
    }

    std::vector<std::string> pciIds;

    for(size_t i = 0; i < glob_result.gl_pathc; ++i) {

        std::string syspath = std::string(glob_result.gl_pathv[i]);
        std::string devname = syspath.substr(11, 6);

        char resolved_name[512];
        memset(resolved_name, '\0', sizeof(resolved_name));
        realpath(syspath.c_str(), resolved_name);
        std::string resolved(resolved_name);
    
        size_t p = resolved.rfind(':');
        std::string pci_loc = resolved.substr(p-2,5);

        drxpInterfaceExt_t drxp;
        drxp.chardev = std::string("/dev/") + devname;
        drxp.interface.deviceId = pci_loc;
        drxp.interface.InterfaceNumber =  1 + std::count(pciIds.begin(), pciIds.end(), pci_loc);

        //std::cout << "found " << syspath << " device " << devname << " links to " << resolved_name << " PCI loc " << pci_loc << " iface " << drxp.interface.InterfaceNumber << "\n";

        pciIds.push_back(pci_loc);
        devices.insert(pci_loc);
        interfaces.push_back(drxp.interface);
        interfaces_ext.push_back(drxp);
    }

}

bool DRXPPCIeLookup::isPresent(const XSD::drxpInterfaceId_t& dev) const
{
    std::vector<XSD::drxpInterfaceId_t>::const_iterator it;
    for (it = interfaces.begin(); it != interfaces.end(); it++) {
        if (*it == dev) {
            return true;
        }
    }
    return false;
}

std::string DRXPPCIeLookup::getCharDevice(const XSD::drxpInterfaceId_t& dev) const
{
    std::vector<drxpInterfaceExt_t>::const_iterator it;
    for (it = interfaces_ext.begin(); it != interfaces_ext.end(); it++) {
        if (it->interface == dev) {
            return it->chardev;
        }
    }
    return std::string("");
}

const std::vector<XSD::drxpInterfaceId_t>& DRXPPCIeLookup::getInstalledInterfaces() const
{
    return interfaces;
}

const std::set<std::string>& DRXPPCIeLookup::getInstalledDevices() const
{
    return devices;
}

int DRXPPCIeLookup::getInstalledInterfacesCount() const
{
    return interfaces.size();
}

int DRXPPCIeLookup::getInstalledDevicesCount() const
{
    return devices.size();
}

int DRXPPCIeLookup::getDeviceInterfaceCount(std::string device) const
{
    int nifaces = 0;
    std::vector<drxpInterfaceExt_t>::const_iterator it;
    for (it = interfaces_ext.begin(); it != interfaces_ext.end(); it++) {
        if (it->interface.deviceId == device) {
            ++nifaces;
        }
    }
    return nifaces;
}
