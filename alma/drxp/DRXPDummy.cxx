/**
 * \class DRXPDummy
 *
 * \brief Dummy implementation, compile with this if no DRXP board is installed in the system.
 *
 * \author $Author: janw $
 *
 */

#include "drxp/DRXPDummy.hxx"
#include "drxp/drxp_const.hxx"     // common DRXP constants
#include "drxp/drxp_bitfields.hxx" // helpful bitfields, structs, and GCC_BITFLD_ORDER defining the bit order
#include "drxp/SFF8431_I2C.h"  // structures for decoding SFF-8431 I2C data fields
#include "drxp/DRXPInfoFrame.hxx"
#include "drxp/SFF8431_infos.hxx"

#include "helper/logger.h"
#include "mcwrapping/mc_drxp_status.hxx"
#include "datarecipients/DataRecipient_iface.hxx"

#define DUMMY_RX_SLOWDOWN_FACTOR 1 // receive 48ms frames at factor*48ms intervals
#define lockscope

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

#include <exception>
#include <cstring>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>

#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <stdint.h>
#include <malloc.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Convert integer to string */
static std::string int_to_stdstr(int64_t x)
{
    return dynamic_cast<std::ostringstream &>((std::ostringstream() << std::dec << x)).str();
}

static int timespec2str(char *buf, int len, struct timespec *ts)
{
    // https://stackoverflow.com/questions/8304259/formatting-struct-timespec
    int ret;
    struct tm t;
    if (gmtime_r(&(ts->tv_sec), &t) == NULL) { return 1; }
    ret = strftime(buf, len, "%F %T", &t);
    if (ret == 0) { return 2; }
    len -= ret - 1;
    ret = snprintf(&buf[strlen(buf)], len, ".%09ld", ts->tv_nsec);
    if (ret >= len) { return 3; }
    return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Private func, open the device */
void DRXPDummy::deviceOpen(bool rx)
{
    m_usermode = DRXPIface::PUSH;

    boost::mutex::scoped_lock iolock(m_iomutex);
    m_fd = 1234;
    m_rx_mode = rx;
    m_rxtx_started = false;
    m_shutdown_rx_thread = false;
    m_shutdown_mon_thread = false;
    m_maddr_size = DRXP_RX_GROUP_SIZE;
    m_maddr = memalign(4096, m_maddr_size);
    for (size_t n=0; n<m_maddr_size; n++) {
        ((unsigned char*)m_maddr)[n] = rand() % 256;
    }
    m_naccepts = 0;
    m_ndiscards = 0;
    m_noverruns = 0;
    m_curr_group = 0;
    m_reference_tickcount = 0;
    m_reference_set = false;
    memset(&m_group_busy, 0, sizeof(m_group_busy));
    clock_gettime(CLOCK_REALTIME, &infoframe.swRxTime_UTC);
    infoframe.hwTickCount_48ms = 0;
    infoframe.roundTime(m_timestamp_granularity_ns);
    L_(linfo) << "Opened " << m_dev << " in mode rx=" << rx;
}

/* Private func, close the device */
void DRXPDummy::deviceClose()
{
    boost::mutex::scoped_lock iolock(m_iomutex);

    stopPeriodicMonitoring();
    stopRx();
    stopTx();

    if (m_evtfd >= 0) {
        m_evtfd = -1;
    }
    if (m_fd >= 0) {
        m_fd = -1;
        L_(linfo) << "Closed " << m_dev;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Prepare DRXP object, without open any device yet (use selectDevice() for that) */
DRXPDummy::DRXPDummy()
{
    // Make sure that compiler produces the expected bitfield order
    dfr_mon_reg_t tmp;
    tmp.mon.RdValue[0] = 0x01;
    if (tmp.volSts.byte0.GCC_BITFLD_ORDER.p1_35VDdrForDdr != true) {
        std::string msg("GCC_BITFLD_ORDER in DRXP C++ module does not match actual bitfield order, please check drxp_bitfields.hxx and recompile!");
        L_(lerror) << msg;
        std::cerr << __func__ << " : " << msg << std::endl;
        throw std::logic_error(msg.c_str());
    }

    // Defaults
    m_fd = -1;
    m_maddr = NULL;
    m_maddr_size = 0;
    m_monitoringThread = NULL;
    m_shutdown_rx_thread = false;
    m_shutdown_mon_thread = false;
    m_rx_recipient = NULL;
    m_rxtx_started = false;
    m_simpattern = memalign(4096, DRXP_RX_DATA_SIZE+DRXP_RX_IFRAME_SIZE);
    m_timestamp_granularity_ns = 48000000;
}

/** Close the DRXP device, without resetting any previously set alarms or flags on the card */
DRXPDummy::~DRXPDummy()
{
    deviceClose();
    free(m_simpattern);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Get pointers and length of all 'groups' in raw DRXP data area. Intended to help (un)pinning in CUDA. */
bool DRXPDummy::getBufferPtrs(size_t* Ngroups, void** groupbufs, size_t* len)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_maddr != NULL) {
        *Ngroups = 1;
        groupbufs[0] = m_maddr;
        *len = m_maddr_size;
        return true;
    } else {
        *Ngroups = 0;
        *len = 0;
        groupbufs[0] = 0;
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Change to another DRXP board (dummy) */
bool DRXPDummy::selectDevice(int iface_nr, bool open_rdonly)
{
    // Derive a char dev name
    m_dev = std::string("<dummy /dev/drxpd") + int_to_stdstr(iface_nr) + std::string(">");
    return selectDevice(m_dev, open_rdonly);
}

bool DRXPDummy::selectDevice(std::string dev_name, bool open_rdonly)
{
    // Config
    m_dev = dev_name;
    m_monitoringThread = NULL;
    m_rxDatapumpThread = NULL;
    m_rx_recipient = NULL;
    m_rx_mode = open_rdonly;
    m_rxtx_started = false;

    // Open the device
    deviceOpen(m_rx_mode);

    // Read out SFP+ details once (manufacturer, part number, etc); monitoring them is not necessary
    deviceGetSFPInfos(0, m_sfpInfos[0]);
    deviceGetSFPInfos(1, m_sfpInfos[1]);
    deviceGetSFPInfos(2, m_sfpInfos[2]);

    return true;
}

/** Reset all flags and alarms */
bool DRXPDummy::resetDevice()
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    L_(linfo) << "Requested a full reset of " << m_dev;

    // Elecs recommends a ~1 second wait after master reset. Sleep 1.5 seconds.
    if (1) {
        struct timespec req={1,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    // Reset individual: checksums, sync statuses, status and voltage warnings/alarms
    // dummy

    // Do an additional but shorter wait after clearing status registers
    if (1) {
        struct timespec req={0,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    // Reset internal states
    m_naccepts = 0;
    m_ndiscards = 0;
    m_noverruns = 0;
    m_reference_set = false;

    return true;
}

/** Reset all alarms */
bool DRXPDummy::resetAlarms()
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    // Short wait
    if (1) {
        struct timespec req={0,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    return true;
}

/** Reset internal synchronization engine on DRXP, forcing a hardware re-sync. */
bool DRXPDummy::resetSync()
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    // Short wait
    if (1) {
        struct timespec req={0,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    return true;
}

/** Reset all checksums */
bool DRXPDummy::resetChecksums()
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    // Short wait
    if (1) {
        struct timespec req={0,(long)0.5e9}, rem={0,0};
        do {
            if (nanosleep(&req, &rem) != 0) { break; }
            req = rem;
        } while ((rem.tv_sec + 1e-9*rem.tv_nsec) > 10e-3);
    }

    return true;
}

/** Read out all device information, flags, alarms, and statuses */
bool DRXPDummy::deviceReadout(MCDRXPStatus& mc)
{
    bool ok = true;
    ok &= versionReadout(mc);
    ok &= dcmstatusReadout(mc);
    ok &= voltageReadout(mc);
    ok &= opticalReadout(mc);
    ok &= counterReadout(mc);
    ok &= syncReadout(mc);
    ok &= receiveStatusReadout(mc);
    ok &= sendStatusReadout(mc);
    ok &= temperaturesReadout(mc);
    ok &= fpgaSEUReadout(mc);

    mc.naccepts = m_naccepts;
    mc.ndiscards = m_ndiscards;
    mc.noverruns = m_noverruns;

    return ok;
}

/** Read out board version information */
bool DRXPDummy::versionReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    mc.deviceName = m_dev;
    mc.serialNumber = std::string("1234");
    mc.product = 0;

    mc.driver_version = 0x01020300; // maj, min, micro, <reserved 0x00>
    mc.fpgaMainLogic_version = 0x1234;
    mc.fpgaPcieLogic_version = mc.fpgaMainLogic_version; // TODO, not found in driver specs!

    mc.invariant();
    return true;
}

/** Read out all counters */
bool DRXPDummy::counterReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    mc.checksumCounter.ch1BitD = 0;
    mc.checksumCounter.ch2BitC = 0;
    mc.checksumCounter.ch3BitB = 0;
    mc.syncErrorCounter.ch1BitD = 0;
    mc.syncErrorCounter.ch2BitC = 0;
    mc.syncErrorCounter.ch3BitB = 0;

    mc.invariant();
    return true;
}

/** Read out DFR Status */
bool DRXPDummy::dcmstatusReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    // Note: Elecs driver spec DFR_STATUS in 6.1.4 says "PLL Locked" but actually the fields mean "PLL Unlocked Alarm"
    mc.dfrStatus.referenceClock125MhzPllUnlock  = 0;
    mc.dfrStatus.ddrClock200MhzPll2Unlock       = 0;
    mc.dfrStatus.ddrClock200MhzPll1Unlock       = 0;
    mc.dfrStatus.dataInputClockPllUnlockCh1BitD = 0;
    mc.dfrStatus.dataInputClockPllUnlockCh2BitC = 0;
    mc.dfrStatus.dataInputClockPllUnlockCh3BitB = 0;
    mc.dfrStatus.syncPatternNotDetectedCh1BitD  = 0;
    mc.dfrStatus.syncPatternNotDetectedCh2BitC  = 0;
    mc.dfrStatus.syncPatternNotDetectedCh3BitB  = 0;

    mc.invariant();
    return true;
}

/** Read out FPGA related voltage alarms (FPGA, on-FPGA MGT, Vddr) and SFP+ 3.3V voltage warnings/alarms */
bool DRXPDummy::voltageReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    mc.dfrVoltageStatus.p0_95VForFpgaCore     = 0;
    mc.dfrVoltageStatus.p1_8VForFpga          = 0;
    mc.dfrVoltageStatus.p1_0VmgtavccForFpga   = 0;
    mc.dfrVoltageStatus.p1_2VmgtavttForFpga   = 0;
    mc.dfrVoltageStatus.p1_8VmgtvccauxForFpga = 0;
    mc.dfrVoltageStatus.p3_3VForFpgaIo        = 0;
    mc.dfrVoltageStatus.p1_35VDdrForDdr       = 0;

    mc.powerAlarm.ch1BitD.p3_3VAlarm   = 0;
    mc.powerAlarm.ch1BitD.p3_3VWarning = 1;
    mc.powerAlarm.ch2BitC.p3_3VAlarm   = 0;
    mc.powerAlarm.ch2BitC.p3_3VWarning = 1;
    mc.powerAlarm.ch3BitB.p3_3VAlarm   = 0;
    mc.powerAlarm.ch3BitB.p3_3VWarning = 1;

    mc.invariant();
    return true;
}

/** Read out optical power measuremets and warnings/alarms */
bool DRXPDummy::opticalReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    // Read SIG_AVG: signal average optical power in nW units, 24-bit
    // TODO: XML Schema does not yet have a command to check the optical power readings (?)
    mc.drxpOpticalPower.ch1BitD_power_nW = 100000;
    mc.drxpOpticalPower.ch2BitC_power_nW = 100000;
    mc.drxpOpticalPower.ch3BitB_power_nW = 100000;
    mc.drxpOpticalPower.ch1BitD_power_dBm = 10.0*log10(mc.drxpOpticalPower.ch1BitD_power_nW*1e-9 / 0.001);
    mc.drxpOpticalPower.ch2BitC_power_dBm = 10.0*log10(mc.drxpOpticalPower.ch2BitC_power_nW*1e-9 / 0.001);
    mc.drxpOpticalPower.ch3BitB_power_dBm = 10.0*log10(mc.drxpOpticalPower.ch3BitB_power_nW*1e-9 / 0.001);

    // Read alarms/warnings of all optical link triplet registers
    mc.alarmStatus.ch1BitD.temperatureWarning       = 0;
    mc.alarmStatus.ch1BitD.outputBiasCurrentWarning = 0;
    mc.alarmStatus.ch1BitD.outputPowerWarning       = 0;
    mc.alarmStatus.ch1BitD.rxInputPowerWarning      = 0;
    mc.alarmStatus.ch1BitD.temperatureAlarm         = 0;
    mc.alarmStatus.ch1BitD.outputBiasCurrentAlarm   = 0;
    mc.alarmStatus.ch1BitD.outputPowerAlarm         = 0;
    mc.alarmStatus.ch1BitD.rxInputPowerAlarm        = 0;
    mc.alarmStatus.ch2BitC.temperatureWarning       = 0;
    mc.alarmStatus.ch2BitC.outputBiasCurrentWarning = 0;
    mc.alarmStatus.ch2BitC.outputPowerWarning       = 0;
    mc.alarmStatus.ch2BitC.rxInputPowerWarning      = 0;
    mc.alarmStatus.ch2BitC.temperatureAlarm         = 0;
    mc.alarmStatus.ch2BitC.outputBiasCurrentAlarm   = 0;
    mc.alarmStatus.ch2BitC.outputPowerAlarm         = 0;
    mc.alarmStatus.ch2BitC.rxInputPowerAlarm        = 0;
    mc.alarmStatus.ch3BitB.temperatureWarning       = 0;
    mc.alarmStatus.ch3BitB.outputBiasCurrentWarning = 0;
    mc.alarmStatus.ch3BitB.outputPowerWarning       = 0;
    mc.alarmStatus.ch3BitB.rxInputPowerWarning      = 0;
    mc.alarmStatus.ch3BitB.temperatureAlarm         = 0;
    mc.alarmStatus.ch3BitB.outputBiasCurrentAlarm   = 0;
    mc.alarmStatus.ch3BitB.outputPowerAlarm         = 0;
    mc.alarmStatus.ch3BitB.rxInputPowerAlarm        = 0;

    mc.invariant();
    return true;
}

/** Read out ALMA frame decoder sync status and metaframe delay */
bool DRXPDummy::syncReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    mc.syncLockStatus.ch1BitD.syncUnlock     = 0;
    mc.syncLockStatus.ch1BitD.syncLockOffset = 123;
    mc.syncLockStatus.ch2BitC.syncUnlock     = 0;
    mc.syncLockStatus.ch2BitC.syncLockOffset = 345;
    mc.syncLockStatus.ch3BitB.syncUnlock     = 0;
    mc.syncLockStatus.ch3BitB.syncLockOffset = 511;

    mc.metaframeDelay.ch1BitD = 100;
    mc.metaframeDelay.ch2BitC = 200;
    mc.metaframeDelay.ch3BitB = 300;

    mc.invariant();
    return true;
}

/** Read out DRXP Receive Status */
bool DRXPDummy::receiveStatusReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    mc.drxpStarted = (m_rxtx_started && m_rx_mode) ? 1 : 0;

    mc.invariant();
    return true;
}

/** Read out DRXP Send Status */
bool DRXPDummy::sendStatusReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    mc.drxpStarted = (m_rxtx_started && !m_rx_mode) ? 1 : 0;

    mc.invariant();
    return true;
}

/** Read out DRXP Temperatures */
bool DRXPDummy::temperaturesReadout(MCDRXPStatus& mc)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    if (m_fd < 0) {
        return false;
    }

    mc.drxpTemperatures.fpga = 30.0f;
    mc.drxpTemperatures.xfp1 = 31.0f;
    mc.drxpTemperatures.xfp2 = 32.0f;
    mc.drxpTemperatures.xfp3 = 33.0f;

    mc.invariant();
    return true;
}

/** Read out DRXP FPGA Single Event Upset error counts and status */
bool DRXPDummy::fpgaSEUReadout(MCDRXPStatus& mc)
{
    mc.fpgaSEUInfo.status = 2; // SEU_STATUS_OBSERVATION
    mc.fpgaSEUInfo.error = 0;
    mc.fpgaSEUInfo.corErrCnt = 0;
    mc.fpgaSEUInfo.uncorErrCnt = 0;
    mc.fpgaSEUInfo.elapsedObs++; // or update via some other way
    mc.fpgaSEUInfo.elapsedTot = mc.fpgaSEUInfo.elapsedObs;
    mc.invariant();
    return true;
}

/** Read out internal SFP+ transceiver information via I2C */
bool DRXPDummy::deviceGetSFPInfos(const int slot, SFF8431_Xceiver_infos_tt& xi)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    xi.infoValid = false;
    if (m_fd < 0) {
        return false;
    }

    xi.manufacturer = std::string("DUMMY-SFP+");
    xi.partNumber = std::string("PARTNR");
    xi.serialNumber = std::string("SERIALR");
    xi.revision = std::string("1");
    xi.wavelength_nm = 1550;
    xi.xceiverFormfactor = std::string("N/A");
    xi.reach_m = 1000;
    xi.reach_km = xi.reach_m / 1000;

    std::stringstream ss;
    ss << m_dev << " SFP+ transceiver " << 1+(slot%3) << " : "
       << xi.manufacturer
       << ", part# " << xi.partNumber << " rev " << xi.revision
       << ", serial# " << xi.serialNumber
       << ", " << xi.wavelength_nm << " nm " << xi.xceiverFormfactor
       << " for " << xi.reach_km << " km reach";

    xi.summary = ss.str();
    xi.infoValid = true;

    L_(linfo) << xi.summary;

    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Attach a data processing recipient that gets the received data once/while RX is running.
 * The recipient object must provide a function 'takeData(data_t*)' into which DRXP RX data will be pushed.
 */
bool DRXPDummy::attachRxRecipient(DataRecipient* dr)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    m_rx_recipient = dr;
    return true;
}

/** Start DRXP RX data pump (thread).
 * The data pump loopRxThread() waits for a new group of 48ms of raw data.
 * The new group is held "reserved" and is passed to the recipient via takeData().
 * The recipient should transfer the data (DMA onto GPU) and block until the transfer completes.
 * Once takeData() returns, the data pump "releases" the group, allowing DRXP DMA to fill it with new data later.
 */
bool DRXPDummy::startRx()
{
    // Already running?
    if (m_rxDatapumpThread != NULL) { // equiv. to 'm_rxtx_started=true'
        L_(lwarning) << "Attempted to start RX, but RX data pump background thread already active!";
        return false;
    }

    // Make sure the device is open in the correct mode
    if (!m_rx_mode) {
       deviceClose();
       deviceOpen(/*rx=*/true);
    }

    // Lock m_iomutex and prepare RX
    if (1) {
        boost::mutex::scoped_lock iolock(m_iomutex);

        // Prepare EventFD
        m_evtfd = 1234;

        // Set up reception
        // dummy

        // Launch datapump thread
        if (m_usermode == DRXPIface::PUSH) {
            m_shutdown_rx_thread = false;
            m_rxDatapumpThread = new boost::thread(&DRXPDummy::rxDatapumpThread, this, &m_rx_recipient);
        } else {
            m_shutdown_rx_thread = false;
            m_rxDatapumpThread = NULL;
        }
    }

    return true;
}

/** Signal the RX data pump to stop and wait until it finishes */
bool DRXPDummy::stopRx()
{
    if ((m_fd < 0) || !m_rxtx_started || !m_rx_mode) {
        return false;
    }

    if (m_rxtx_started) {
        int ioctl_result = 0;
        L_(ldebug) << "Stopped " << m_dev << " DRXP board I/O, ioctl() result = " << ioctl_result;
    }

    if (m_rxDatapumpThread != NULL) {
        m_shutdown_rx_thread = true;
        m_rxDatapumpThread->interrupt();
        m_rxDatapumpThread->join();
        delete m_rxDatapumpThread;
        m_rxDatapumpThread = NULL;
        m_shutdown_rx_thread = false;
    }
    m_rxtx_started = false;
    m_curr_group = 0;
    m_reference_tickcount = 0;
    m_reference_set = false;
    memset(&m_group_busy, 0, sizeof(m_group_busy));

    return true;
}

/** Change the granularity of software timestamp re-alignment */
void DRXPDummy::setTimestampGranularity(int granularity_msec)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    m_timestamp_granularity_ns = 1000000 * (unsigned long)granularity_msec;
}

/** In "polling" mode allow user to check for an available data group */
bool DRXPDummy::getGroup(int* group, void** data, DRXPInfoFrame* meta)
{
    static struct timeval prev_wait;

    while (1) {

        // Dummy: Blocking wait until new data available (or other event)
        {
            boost::this_thread::disable_interruption diTmp;
            struct timeval curr;
            do {
                usleep(DUMMY_RX_SLOWDOWN_FACTOR*1e3);
                gettimeofday(&curr, NULL);
            } while ((curr.tv_sec - prev_wait.tv_sec) + 1e-6*(curr.tv_usec - prev_wait.tv_usec) < DUMMY_RX_SLOWDOWN_FACTOR*48e-3);
            prev_wait = curr;
        }

        lockscope {
            boost::mutex::scoped_lock iolock(m_iomutex);

            // Get a free group
            int grp = (m_curr_group + 1) % 16;
            for (int n = 0; n < 16; n++) {
                if (m_group_busy[grp] == 0) {
                    break;
                }
                grp = (grp + 1) %  16;
            }
            if (!(m_group_busy[grp] == 0)) {
                // Overflow, all groups busy
                infoframe.hwTickCount_48ms += DUMMY_RX_SLOWDOWN_FACTOR;
                infoframe.swRxTime_UTC.tv_nsec += DUMMY_RX_SLOWDOWN_FACTOR*48000000UL;
                infoframe.swRxTime_UTC.tv_sec += infoframe.swRxTime_UTC.tv_nsec / 1000000000UL;
                infoframe.swRxTime_UTC.tv_nsec = infoframe.swRxTime_UTC.tv_nsec % 1000000000UL;
                continue;
            }

            // Grab the group
            *group = grp;
            m_curr_group = grp;
            m_group_busy[grp] = 1;
            break;
        }
    }

    // Data area
    *data = m_maddr;

    // Metaframe info
    if (!m_reference_set) {
        m_reference_set = true;
        clock_gettime(CLOCK_REALTIME, &infoframe.swRxTime_UTC);
        infoframe.roundTime(m_timestamp_granularity_ns);
        *meta = infoframe;
        m_reference_time = meta->swRxTime_UTC;
        m_reference_tickcount = meta->hwTickCount_48ms;
        L_(linfo) << "Tied DRXP timestamps to software UTC "
                  << std::fixed << std::setprecision(6) << (m_reference_time.tv_sec + 1e-9*m_reference_time.tv_nsec)
                  << " + 48ms * (hardware tick N - " << m_reference_tickcount << ")";
    } else {
        *meta = infoframe;
        meta->adjustTime(m_reference_time, m_reference_tickcount);
    }

    // Display some status information
    if (0) {
        L_(ldebug1) << "Found dummy group " << std::setw(2) << *group << " with data; "
                    << "48ms tick " << meta->hwTickCount_48ms << ", " << std::hex << meta->frameStatus << std::dec
                    << ", tstamp " << std::fixed << std::setprecision(6) << (meta->swRxTime_UTC.tv_sec + meta->swRxTime_UTC.tv_nsec*1e-9)
                    << " : naccepts=" << m_naccepts << " ndiscards=" << m_ndiscards << " noverruns=" << m_noverruns;
    } else if (1) {
        const uint TIME_FMT = sizeof("2012-12-31 12:59:59.123456789") + 1;
        char timestr[TIME_FMT];
        timespec2str(timestr, sizeof(timestr), &meta->swRxTime_UTC);
        L_(ldebug1) << "Found dummy group " << std::setw(2) << *group << " with data; "
                    << "48ms tick " << meta->hwTickCount_48ms << ", " << std::hex << meta->frameStatus << std::dec
                    << ", tstamp " << std::fixed << std::setprecision(6) << (meta->swRxTime_UTC.tv_sec + meta->swRxTime_UTC.tv_nsec*1e-9)
                    << " : " << timestr
                    << " : naccepts=" << m_naccepts << " ndiscards=" << m_ndiscards << " noverruns=" << m_noverruns;
    }

    // Next timestamp
    infoframe.hwTickCount_48ms += DUMMY_RX_SLOWDOWN_FACTOR;
    infoframe.swRxTime_UTC.tv_nsec += DUMMY_RX_SLOWDOWN_FACTOR*48000000UL;
    infoframe.swRxTime_UTC.tv_sec += infoframe.swRxTime_UTC.tv_nsec / 1000000000UL;
    infoframe.swRxTime_UTC.tv_nsec = infoframe.swRxTime_UTC.tv_nsec % 1000000000UL;

    return true;
}

/** In "polling" mode allow user to free a data group back to the DRXP */
bool DRXPDummy::releaseGroup(int grp)
{
    boost::mutex::scoped_lock iolock(m_iomutex);
    m_group_busy[grp] = 0;
    return true;
}

/** Return the cumulative nr of overruns since start */
int DRXPDummy::getOverruns()
{
    return m_noverruns;
}

/** The RX data pump thread, waits for new data from DRXP, shuffles it into a DataRecipient object. */
void DRXPDummy::rxDatapumpThread(DataRecipient* volatile* ppdr)
{
    if (m_rxtx_started) {
        L_(lwarning) << "Internal warning: rxDatapumpThread() called while DRXP fd=" << m_fd << ", rx=" << m_rx_mode << ", started=" << m_rxtx_started;
        return;
    }
    if ((m_fd < 0) || !m_rx_mode) {
        L_(lerror) << "Internal error: rxDatapumpThread() called while DRXP fd=" << m_fd << ", rx=" << m_rx_mode << ", started=" << m_rxtx_started;
        m_rxtx_started = false;
        return;
    }

    m_rxtx_started = true;

    void* data;
    DRXPInfoFrame info;
    info.frameStatus = 2; // "Bit[1]: Dummy data is filled"
    info.hwTickCount_48ms = 0;

    try { while (!m_shutdown_rx_thread) {

        int grp;
        bool success = getGroup(&grp, &data, &info);
        if (!success) {
            m_rxtx_started = false;
            m_shutdown_rx_thread = true;
            break;
        }

        // Assemble a data info packet for ::takeData()
        RawDRXPDataGroup data;
        data.frameinfo = &info; // TODO: maybe copy is better?
        data.buf = m_maddr;
        data.bufSize = DRXP_RX_DATA_SIZE;
        data.weight = 1.0;
        data.originator = this;
        data.group = grp;

        // Callback to external handler via ::takeData()
        bool releaseLater = false;
        if ((ppdr != NULL) && (*ppdr != NULL)) {
            boost::this_thread::disable_interruption diTmp;
            releaseLater = (*ppdr)->takeData(data);
            m_naccepts++;
        } else {
            L_(ldebug1) << "Dropping new dummy rx'd data since no valid data recipient registered yet";
            m_ndiscards++;
        }

        // Release the data unless the recipient will handle the data and release it later on its own
        if (!releaseLater) {
            releaseGroup(grp);
        }

    } /*while*/ } catch (boost::thread_interrupted& e) {
        L_(ldebug) << __func__ << " was interrupted and is exiting";
        m_shutdown_rx_thread = true;
    }

    m_rxtx_started = false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

/** Loads contents of file (48ms data + Info Frame) and serves that as fake rx/tx data.
 *  Call after startRx() or startTx().
 */
bool DRXPDummy::loadPattern(const char* filename)
{
    std::ifstream f(filename, std::ios::binary);
    if (!f) {
        L_(lwarning) << __func__ << " could not open pattern file " << filename;
        return false;
    }
    f.read((char*)m_simpattern, DRXP_RX_DATA_SIZE+DRXP_RX_IFRAME_SIZE);
    f.close();

    if (m_maddr != NULL) {
        memcpy(m_maddr, m_simpattern, DRXP_RX_DATA_SIZE+DRXP_RX_IFRAME_SIZE);
    }
    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

/** Start debug output into a file */
bool DRXPDummy::startDebugLogging(const char* filename)
{
    m_debugFile.close();
    m_debugFile.open(filename, std::ofstream::out | std::ofstream::app);
    m_debugFileEnabled = m_debugFile.is_open();
    return m_debugFileEnabled;
}

/** Stop debug output */
bool DRXPDummy::stopDebugLogging()
{
    m_debugFile.close();
    m_debugFileEnabled = m_debugFile.is_open();
    return m_debugFileEnabled;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

bool DRXPDummy::startTx()
{
    // Make sure the device is open in the correct mode
    if (m_rx_mode) {
       deviceClose();
       deviceOpen(/*rx=*/false);
    }

    // Lock m_iomutex and prepare TX
    if (1) {
        boost::mutex::scoped_lock iolock(m_iomutex);

        // Set up memory map

        // TODO: another member function to specify the data to transmit? Or pass a buffer to startTx()?

        // Set up transmit
        m_rxtx_started = true;
    }

    return true;
}

bool DRXPDummy::stopTx()
{
    if ((m_fd < 0) || !m_rxtx_started || m_rx_mode) {
        return false;
    }

    if (m_rxtx_started) {
        int ioctl_result = 0;
        L_(ldebug) << "Stopped DRXP board I/O, ioctl() result = " << ioctl_result;
    }
    m_rxtx_started = false;

    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

void DRXPDummy::monitoringThreadFunc(const char* mgrp, int mport, MCDRXPStatus* mc, float interval_secs)
{
    using namespace boost::asio;
    ip::address destaddr = boost::asio::ip::address::from_string(mgrp);
    ip::udp::endpoint source(ip::address_v4::any(), mport);
    ip::udp::endpoint destination(destaddr, mport);

    while (!m_shutdown_mon_thread) {
        if (1) {
            // Disable thread interruption in this scope
            boost::this_thread::disable_interruption diTmp;

            // Perform readout and update our target MCDRXPStatus
            deviceReadout(*mc);

            // Also send a summary out by multicast if possible
            std::string summary = mc->toString();
            try {
                io_service service;
                ip::udp::socket socket(service);
                socket.open(ip::udp::v4());
                socket.set_option(ip::udp::socket::reuse_address(true));
                socket.set_option(ip::multicast::enable_loopback(true));
                socket.send_to(buffer(summary), destination);
            } catch (...) {
                std::cerr << "Multicast transmission in " << __func__ << "() failed\n";
            }
        }
        // Pause and potential thread exit point
        try {
            boost::this_thread::sleep(boost::posix_time::seconds(interval_secs));
        } catch (boost::thread_interrupted& e) {
            return;
        }
    }
}

/** Start periodic monitoring. Periodically updates the referenced MCDRXPStatus and multicasts an ASCII summary string */
bool DRXPDummy::startPeriodicMonitoring(const char* mgrp, int mport, MCDRXPStatus& mc, float interval_secs)
{
    if (m_monitoringThread != NULL) {
        return false;
    }
    if ((interval_secs < 0.192f) || (interval_secs > 30.0f)) {
        interval_secs = 5.0f;
    }
    m_shutdown_mon_thread = false;
    m_monitoringThread = new boost::thread(&DRXPDummy::monitoringThreadFunc, this, mgrp, mport, &mc, interval_secs);
    return true;
}

/** Stop periodic monitoring */
void DRXPDummy::stopPeriodicMonitoring()
{
    if (m_monitoringThread != NULL) {
        m_shutdown_mon_thread = true;
        m_monitoringThread->interrupt();
        m_monitoringThread->join();
        delete m_monitoringThread;
        m_monitoringThread = NULL;
        m_shutdown_mon_thread = false;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
