#ifndef DRXP_IOCTL_H
#define DRXP_IOCTL_H

#ifndef __LOG_H__
    #define DIO_LOG_ERROR std::cerr
    #define DIO_LOG_DEBUG std::cerr
    #define DIO_endl "\n"
#else
    #define DIO_LOG_ERROR L_(lerror)
    #define DIO_LOG_DEBUG L_(ldebug2)
    #define DIO_endl ""
#endif

#include <iostream>
#include <iomanip>
#include <sys/ioctl.h>

#define DRXP_IOCTL(fd, cmd, arg) do { \
        int _rc = ioctl(fd, cmd, arg); \
        if (_rc == -1) { \
            DIO_LOG_ERROR << __func__ << " ioctl(0x" << std::hex << cmd << ", " << std::dec << arg << ") error, errno=" << errno << " : " << strerror(errno) << DIO_endl; \
        } else { \
            DIO_LOG_DEBUG << __func__ << " ioctl(0x" << std::hex << cmd << ", " << std::dec << arg << ") ok, returned " << _rc << DIO_endl; \
        } \
    } while (0)

#define DRXP_IOCTL_IPTR(fd, cmd, parg) do { \
        int _rc = ioctl(fd, cmd, parg); \
        if (_rc == -1) { \
            DIO_LOG_ERROR << __func__ << " ioctl(0x" << std::hex << cmd << ", " << std::dec << *(parg) << ") error, errno=" << errno << DIO_endl; \
        } else { \
            DIO_LOG_DEBUG << __func__ << " ioctl(0x" << std::hex << cmd << ", " << std::dec << *(parg) << ") ok, returned " << _rc << ", value " << *(parg) << DIO_endl; \
        } \
    } while (0)

#define DRXP_IOCTL_PTR(fd, cmd, parg) do { \
        int _rc = ioctl(fd, cmd, parg); \
        if (_rc == -1) { \
            DIO_LOG_ERROR << __func__ << " ioctl(0x" << std::hex << cmd << ", <ptr>) error, errno=" << errno << DIO_endl; \
        } else { \
            DIO_LOG_DEBUG << __func__ << " ioctl(0x" << std::hex << cmd << ", <ptr>) ok, returned " << _rc << DIO_endl; \
        } \
    } while (0)

#define DRXP_I2C(fd, parg) do { \
        int _rc = ioctl(fd, DRXPD_GET_I2C_DATA, parg); /* arg is type of "ST_DRXPD_I2CRD*" */ \
        if (_rc == -1) { \
            DIO_LOG_ERROR << __func__ << " ioctl(DRXPD_GET_I2C_DATA) error, errno=" << errno << DIO_endl; \
        } else { \
            DIO_LOG_DEBUG << __func__ << " ioctl(DRXPD_GET_I2C_DATA) ok" << DIO_endl; \
        } \
    } while (0)

#define WRITE_CTL_REG(basereg,ctl) do { \
        int _rc;\
        ctl.ItemID = basereg; \
        WRITE_CTL_REG_DbgPrint(ctl); \
        _rc = ioctl(m_fd, DRXPD_SET_CONTROL, &ctl); \
        if (_rc == -1) { \
            DIO_LOG_ERROR << __func__ << " ioctl(0x" << std::hex << DRXPD_SET_CONTROL \
                          << ",[ctl.ItemID=0x" << std::hex << basereg << std::dec << "]) error, errno=" << errno << DIO_endl; \
           return false; \
        } else { \
            DIO_LOG_DEBUG << __func__ << " ioctl(0x" << std::hex << DRXPD_SET_CONTROL \
                          << ",[ctl.ItemID=0x" << std::hex << basereg << std::dec << "]) ok, returned " << _rc << DIO_endl; \
        } \
    } while(0)

#endif // DRXP_IOCTL_H
