////////////////////////////////////////////////////////////////////////////////////////////////
// Polyphase Filterbank Job Preparations
////////////////////////////////////////////////////////////////////////////////////////////////

// External flags:
//   DO_COMPLEX_PFB : 0|1  determines whether PFB output is real (0), or complex (1) with a zero imag part

#ifndef PFB_SETUP_CU
#define PFB_SETUP_CU

#include "cuda_utils.cu"
#include "pfb_fir.h" // defaults for PFB_HC_NCHAN and PFB_HC_TAPS if not defined
#include "pfb_job.h"
#include <cuda.h>
#include <cufft.h>

//extern __constant__ float c_pfb_coeffs[PFB_HC_NCHAN*PFB_HC_TAPS]; // does not work in CUDA 7.5+ compilers

/**
 * Allocate arrays on GPU and host, and transfer a copy of the PFB coefficients.
 * Uses arguments provided in 'pfb_job_t' (coefficients, data size in .nsamples and .nrawbytes).
 * The PFB coefficients get time reversed during copying to make later convolution easier.
 */
void allocatePFBJob(pfb_job_t* j)
{
    // Some values derived from loaded PFB coefficients
    j->Lcoeffs = j->Nch * j->Ntaps * sizeof(float);
    j->nPFB    = j->maxphysthreads / j->Nch;

    // Allocate memory for the input and output data
    CUDA_CALL( cudaMalloc( (void **)&j->d_samples,    j->nrawbytes ) );
    CUDA_CALL( cudaMalloc( (void **)&j->d_idata,      sizeof(float)*j->nsamples ) );
    CUDA_CALL( cudaMalloc( (void **)&j->d_pfbout,     2*sizeof(float)*j->nsamples ) );
    CUDA_CALL( cudaMalloc( (void **)&j->d_pfbout_td,  2*sizeof(float)*j->nsamples ) );
    CUDA_CALL( cudaMalloc( (void **)&j->d_pfb_8bit,   2*sizeof(char)*j->nsamples ) );

    CUDA_CALL( cudaHostAlloc( (void **)&j->h_samples, j->nrawbytes, cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&j->h_idata,   sizeof(float)*j->nsamples, cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&j->h_pfbout,  2*sizeof(float)*j->nsamples, cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&j->h_pfb_8bit,2*sizeof(char)*j->nsamples, cudaHostAllocDefault ) );

    // Initialize memory
    CUDA_CALL( cudaMemset( j->d_samples,   0x00, j->nrawbytes ) );
    CUDA_CALL( cudaMemset( j->d_idata,     0x00, sizeof(float)*j->nsamples ) );
    CUDA_CALL( cudaMemset( j->d_pfbout,    0x00, 2*sizeof(float)*j->nsamples ) );
    CUDA_CALL( cudaMemset( j->d_pfbout_td, 0x00, 2*sizeof(float)*j->nsamples ) );
    CUDA_CALL( cudaMemset( j->d_pfb_8bit,  0x00, 2*sizeof(char)*j->nsamples ) );

    // Copy PFB coeffs from 2D into a flat 1D array. Flip them to make convolution easier.
    CUDA_CALL( cudaMallocHost( (void **)&j->h_pfb_coeff_1D, j->Lcoeffs ) );
    for (int ch = 0; ch < j->Nch; ch++) {
        for (int tap = 0; tap < j->Ntaps; tap++) {
            j->h_pfb_coeff_1D[ch*j->Ntaps + tap] = j->h_pfb_coeff[ch][(j->Ntaps-1) - tap];
        }
    }
    CUDA_CALL( cudaMalloc( (void **)&j->d_pfb_coeff_1D, j->Lcoeffs ) );
    CUDA_CALL( cudaMemcpy(j->d_pfb_coeff_1D, j->h_pfb_coeff_1D, j->Lcoeffs, cudaMemcpyHostToDevice) );

#if PFB_COEFFS_FIT_CONSTMEM
    CUDA_CALL( cudaMemcpyToSymbol(c_pfb_coeffs, j->h_pfb_coeff_1D, j->Lcoeffs, 0, cudaMemcpyHostToDevice) );
#endif

    // Allocate memory for sample statistics and (TODO:)re-quantization of channels to 2-bit complex
    CUDA_CALL( cudaHostAlloc( (void **)&j->h_stats, sizeof(float) * j->Nch * 3, cudaHostAllocDefault) );
    CUDA_CALL( cudaMalloc( (void **)&j->d_stats, sizeof(float) * j->Nch * 3) );
    j->d_stats_chMeans = &j->d_stats[0];
    j->d_stats_chVars  = &j->d_stats[2*j->Nch];
    j->h_stats_chMeans = &j->h_stats[0];
    j->h_stats_chVars  = &j->h_stats[2*j->Nch];
}

/**
 * Release allocated arrays.
 */
void freePFBJob(pfb_job_t* j)
{
    CUDA_CALL( cudaFree(j->d_pfb_coeff_1D) );
    CUDA_CALL( cudaFree(j->d_samples) );
    CUDA_CALL( cudaFree(j->d_idata) );
    CUDA_CALL( cudaFree(j->d_pfbout) );
    CUDA_CALL( cudaFree(j->d_pfbout_td) );
    CUDA_CALL( cudaFree(j->d_stats) );
    CUDA_CALL( cudaFree(j->d_pfb_8bit) );
    CUDA_CALL( cudaFreeHost(j->h_pfb_coeff_1D) );
    CUDA_CALL( cudaFreeHost(j->h_samples) );
    CUDA_CALL( cudaFreeHost(j->h_idata) );
    CUDA_CALL( cudaFreeHost(j->h_pfbout) );
    CUDA_CALL( cudaFreeHost(j->h_stats) );
    CUDA_CALL( cudaFreeHost(j->h_pfb_8bit) );
    // could also free: float** j->h_h_pfb_coeff, one free() per channel
}

/**
 * Additional preparation steps before running PFB.
 * Sets up the post-FIR step for FFT.
 */
void preparePFBJob_FFT(pfb_job_t* j)
{
    // Initialize post-PFB IFFT
    size_t batch = j->nsamples / j->Nch;
    j->ifft_output_samples = batch * j->Nch;

    int dimn[1] = {j->Nch};         // DFT size
    int inembed[1] = {0};           // ignored for 1D xform
    int onembed[1] = {0};           // ignored for 1D xform
    int istride = 1, ostride = 1;   // step between successive in(out) elements
    int idist = j->Nch;             // step between batches (R2C input = real, C2C input = complex)
    if (j->do_complex_pfb) {
        int odist = j->Nch;         // step between batches (R2C output = 1st Nyquist only; let overwrite N/2+1 point!)
        printf("Using PFB with complex output (pairs of {re,0.0f}) followed by %zu-batched %d-point C2C IFFT\n", batch,j->Nch);
        CUFFT_CALL( cufftPlanMany(&j->ifftplan, 1, dimn,
            inembed, istride, idist,
            onembed, ostride, odist,
            CUFFT_C2C, batch)
        );
    } else {
        int odist = j->Nch/2+0;     // step between batches (R2C output = 1st Nyquist only; let overwrite N/2+1 point!)
        printf("Using PFB with real output followed by %zu-batched %d-point R2C FFT, note that cuFFT lacks R2C IFFT.\n", batch,j->Nch);
        CUFFT_CALL( cufftPlanMany(&j->ifftplan, 1, dimn,
            inembed, istride, idist,
            onembed, ostride, odist,
            CUFFT_R2C, batch)
        );
    }
    #if defined(CUDA_VERSION) && (CUDA_VERSION < 8000)
    CUFFT_CALL( cufftSetCompatibilityMode(j->ifftplan, CUFFT_COMPATIBILITY_NATIVE) );
    #endif
}

#endif // PFB_SETUP_CU
