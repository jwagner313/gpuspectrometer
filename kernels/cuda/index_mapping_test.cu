/////////////////////////////////////////////////////////////////////////////////////
//
// GPU device functions to re-map post-FFT indexing
//
// reindex_skip_Nyquist : take bin index that assumes no Nyquist was produced by FFT,
//                        return memory index that hops over Nyquist bin actually
//                        produced by FFT
//
// (C) 2018 Jan Wagner
//
/////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include "index_mapping.cu"

int main()
{
    size_t Lfft = 8;
    size_t nch_with_Nyquist = Lfft/2 + 1;
    size_t nch_without_Nyquist = Lfft/2;
    bool passed = true;
    size_t ind;

    // try to map linear 0...N to underlying nch data hopping over the Nyquist bin each time
    // example, Lfft=8, nch=5 with Nyquist
    // fftbin  0 1 2 3 4(N) 5 6 7 8 9(N) 10 11
    // indx    0 1 2 3  5 6 7 8 10 11
    printf("Test of remapping %zu-point r2c FFT output of %zu bins (with Nyquist) to linear space that hops over each Nyquist\n", Lfft, nch_with_Nyquist);
    for (ind = 0; ind < 16*(Lfft/2+0); ind++) {
        size_t map_ind = reindex_skip_Nyquist(nch_with_Nyquist, nch_without_Nyquist, ind);
        printf(" nyq_free[%zu] = memory[%zu]\n", ind, map_ind);
        if (map_ind>0 && ((map_ind+1) % nch_with_Nyquist)==0) {
            printf("fail! map_ind=%zu+1 (+1 since 0-based index) is multiple of Nch+Nyq %zu\n", map_ind, nch_with_Nyquist);
            passed = false;
        }
    }

    printf("Result: %s\n", passed ? "PASS" : "FAIL");
    return 0;
}
