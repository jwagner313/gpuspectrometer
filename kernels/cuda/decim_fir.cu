////////////////////////////////////////////////////////////////////////////////////////////////
//
// Decimating FIR filter
//
// Compared to a PFB FIR this produces only one output channel.
//
// (C) 2016 Jan Wagner
//
// Refs:
// http://www.ws.binghamton.edu/fowler/fowler%20personal%20page/EE521_files/IV-05%20Polyphase%20Filters_2003.pdf
// http://developer.download.nvidia.com/compute/cuda/1.1-Beta/x86_website/projects/reduction/doc/reduction.pdf
//
////////////////////////////////////////////////////////////////////////////////////////////////

#define N_DECFIR_UNROLL  8 // unrolling factor
#define N_DECFIR_MAXTAP  1024
#define N_DECFIR_BLOCKSZ 64

#include <cuda.h>
#include <assert.h>

__constant__ float c_decim_FIR_w[N_DECFIR_MAXTAP];

/** FIR filter with weights w[0..N-1] and decimation by factor R>1. Weights are stored in global memory. */
__global__ void cu_decim_FIR(const float* __restrict__ src, const float* __restrict__ w, float* __restrict__ dst, const size_t Nsamp, const size_t N, const size_t R)
{
    const size_t threadID   = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t numThreads = blockDim.x * gridDim.x;

    size_t n_in  = threadID*R;
    size_t n_out = threadID;
    while (n_in < Nsamp) {
        float y = 0.0f;
        for (size_t tap=0; tap<N; tap+=N_DECFIR_UNROLL) {
           #pragma unroll
           for (int t=0; t<N_DECFIR_UNROLL; t++) {
               y += src[n_in + tap + t] * w[tap + t]; // 670 Ms/s @ N=1024 R=16; no gain by different indexing like "xptr[t]*wptr[t], xptr+=UNROLL, wptr+=UNROLL"
           }
        }
        dst[n_out] = y;
        n_in  += numThreads*R;
        n_out += numThreads;
    }
}

/** FIR filter with weights w[0..N-1] and decimation by factor R>1. Weights are stored in constant memory 'c_decim_FIR_w' */
__global__ void cu_decim_FIR_const(const float* __restrict__ src, float* __restrict__ dst, const size_t Nsamp, const size_t N, const size_t R)
{
    const size_t threadID   = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t numThreads = blockDim.x * gridDim.x;

    size_t n_in  = threadID*R;
    size_t n_out = threadID;
    while (n_in < Nsamp) {
        float y = 0.0f;
        for (size_t tap=0; tap<N; tap+=N_DECFIR_UNROLL) {
           #pragma unroll
           for (int t=0; t<N_DECFIR_UNROLL; t++) {
               y += src[n_in + tap + t] * c_decim_FIR_w[tap + t];
           }
        }
        dst[n_out] = y;
        n_in  += numThreads*R;
        n_out += numThreads;
    }
}

__global__ void cu_decim_FIR_csm(const float* __restrict__ src, const float* __restrict__ w, float* __restrict__ dst, const size_t Nsamp, const size_t N, const size_t R)
{
    const size_t threadID   = blockIdx.x * blockDim.x + threadIdx.x;
    const size_t numThreads = blockDim.x * gridDim.x;

    __shared__ float s_decim_FIR_w[N_DECFIR_MAXTAP];
    for (int i=threadIdx.x; i<N_DECFIR_MAXTAP; i+=blockDim.x) {
         s_decim_FIR_w[i] = c_decim_FIR_w[i]; // ~710 Ms/s @ N=1024 R=16
         //s_decim_FIR_w[i] = w[i]; // ~710 Ms/s @ N=1024 R=16
    }
     __syncthreads();

    size_t n_in  = threadID*R;
    size_t n_out = threadID;
    while (n_in < Nsamp) {
        float y = 0.0f;
        for (size_t tap=0; tap<N; tap+=N_DECFIR_UNROLL) {
           #pragma unroll
           for (int t=0; t<N_DECFIR_UNROLL; t++) {
               y += src[n_in + tap + t] * s_decim_FIR_w[tap + t];
           }
        }
        dst[n_out] = y;
        n_in  += numThreads*R;
        n_out += numThreads;
    }
}

/**
 * FIR filter with weights w[0..N-1] and decimation by factor R>1.
 * Each CUDA block produces a single FIR output sample y_decim[n]=y[nR]=FIR[x[nR],w].
 *  Each block evaluates the FIR in polyphase decomposition, each one thread is one phase.
 */
__global__ void cu_decim_FIR_csm2(const float* __restrict__ src, const float* __restrict__ w, float* __restrict__ dst, const size_t Nsamp, const size_t N, const size_t R)
{
    // assert(N_DECFIR_BLOCKSZ == blockDim.x);
    // assert((N % N_DECFIR_BLOCKSZ) == 0);

    // threadIdx : 32-tap subset of N-tap filter
    // blockIdx  : index of a single sample
    // gridDim   : how many samples in parallel

    __shared__ float y_sub[N_DECFIR_BLOCKSZ];
    __shared__ float s_decim_FIR_w[N_DECFIR_BLOCKSZ][N_DECFIR_MAXTAP/N_DECFIR_BLOCKSZ];
    for (int i=0; i<N_DECFIR_MAXTAP/N_DECFIR_BLOCKSZ; i++) {
         //s_decim_FIR_w[i][threadIdx.x] = c_decim_FIR_w[threadIdx.x + i*N_DECFIR_BLOCKSZ];
         s_decim_FIR_w[i][threadIdx.x] = w[threadIdx.x + i*N_DECFIR_BLOCKSZ];
    }
     __syncthreads();

    size_t n_out = blockIdx.x;
    size_t n_in  = n_out * R;
    while (n_in < Nsamp) {

        y_sub[threadIdx.x] = 0.0f;

        // Sum all N taps in subsets of warpsize
        const float* xptr = src + n_in + threadIdx.x;
        #pragma unroll
        for (int subtap = 0; subtap < N_DECFIR_MAXTAP/N_DECFIR_BLOCKSZ; subtap ++) {
            float wn = s_decim_FIR_w[subtap][threadIdx.x];
            //float xn = src[n_in + subtap*N_DECFIR_BLOCKSZ + threadIdx.x]; // slow indexing
            float xn = *xptr; xptr += N_DECFIR_BLOCKSZ; // faster indexing
            y_sub[threadIdx.x] += xn * wn;
        }
        __syncthreads();

        // Indexing src[n_in + subtap*N_DECFIR_BLOCKSZ + threadIdx.x]
        //     1.0 Gs/s for N=1024 R=16 blksz=32
        //     2.0 Gs/s for N=1024 R=16 blksz=64
        //     2.1 Gs/s for N=1024 R=16 blksz=128
        // Indexing *xtpr, xptr+=N_DECFIR_BLOCKSZ
        //     1.9 Gs/s for N=1024 R=16 blksz=32
        //     2.0 Gs/s for N=1024 R=16 blksz=64
        //     2.1 Gs/s for N=1024 R=16 blksz=128

#if 1
        // Combine subsets into final N-tap sum; do reduction in shared mem
        for (int s = 1; s < N_DECFIR_BLOCKSZ; s *= 2) {
            if (threadIdx.x % (2*s) == 0) {
                y_sub[threadIdx.x] += y_sub[threadIdx.x + s];
            }
            __syncthreads();
        }

        // Store in global mem
        if (threadIdx.x == 0) {
            atomicAdd(&dst[n_out], y_sub[0]);
        }
#elif 0
        // Combine subsets into final N-tap sum; do reduction in shared mem as in "Optimizing Parallel Reduction in CUDA" (turns out slower than the above method!)
        for (int s = N_DECFIR_BLOCKSZ/2; s > 0; s >>= 1) {
            if (threadIdx.x < s) {
                y_sub[threadIdx.x] += y_sub[threadIdx.x + s];
            }
            __syncthreads();
        }

        // Store in global mem
        if (threadIdx.x == 0) {
            atomicAdd(&dst[n_out], y_sub[0]);
        }
#else
        // No reduction in shared memory, just do it in global memory
        atomicAdd(&dst[n_out], y_sub[threadIdx.x]);
#endif

        n_out += gridDim.x;
        n_in  += gridDim.x*R;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef BENCH

#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif
#define CHECK_TIMING 1
#include "cuda_utils.cu"

static double sinc(double x)
{
    return (x ==0) ? 1.0 : sin(x)/x;
}

/** Coeff generator. Like KVN PFB mkh.c it creates a Hamming-windowed sinc(). Use N that is a power of 2. */
static void make_pfb_coeffs(float *w, const double low_Hz, const double high_Hz, const size_t N)
{
    const double L = N/2 - 1;
    double fc_lo = low_Hz/(N-1), fc_hi = high_Hz/(N-1);
    double wc_lo = 2*M_PI*fc_lo, wc_hi = 2*M_PI*fc_hi;
    double nm = 1.0; // / (fc_hi - fc_lo);
    double sum = 0.0;
    for (size_t n=0; n<N-1; n++) {
        double wn = nm;
        wn *= fc_hi*sinc((n-L)*wc_hi) - fc_lo*sinc((n-L)*wc_lo); // sinc weights
        wn *= 0.54 - 0.46*cos(2*M_PI*((double)n)/(N-1)); // Hamming window
        w[n] = (float)wn;
        sum += w[n];
    }
    w[N-1] = 0.0f; // or w[N-1] = w[0] ?
    if (1) {
        // Unity gain
        sum += w[N-1];
        for (size_t n=0; n<N; n++) { w[n]/=sum; }
    }
    if (1) {
        FILE* f;
        f = fopen("decim_fir.coeff", "w");
        for (size_t n=0; n<N; n++) {
            fprintf(f, "%e\n", w[n]);
        }
    }
}

size_t maxphysthreads;
size_t nsamples, i;
float *d_data, *h_data, *d_filtered;
float *h_coeff, *d_coeff;

size_t N=N_DECFIR_MAXTAP, R=16;
const double f0 = 0.0, f1 = 8.0e6, fs = 512e6;
bool verbose = false;

void checkOutput()
{
    CUDA_CALL( cudaMemcpy(h_data, d_data, (nsamples/R)*sizeof(float), cudaMemcpyDeviceToHost) );
    for (i = 0; i < 8; i++) {
        printf("kernel in: [%2zd..%2zd] %5.3f %5.3f %5.3f %5.3f  %5.3f %5.3f %5.3f %5.3f\n",
                8*i, 8*i+7,
                h_data[8*i+0], h_data[8*i+1], h_data[8*i+2], h_data[8*i+3],
                h_data[8*i+4], h_data[8*i+5], h_data[8*i+6], h_data[8*i+7]
        );
    }

    CUDA_CALL( cudaMemcpy(h_data, d_filtered, (nsamples/R)*sizeof(float), cudaMemcpyDeviceToHost) );
    for (i = 0; i < 8; i++) {
        printf("kernel out: [%2zd..%2zd] %5.3f %5.3f %5.3f %5.3f  %5.3f %5.3f %5.3f %5.3f\n",
                8*i, 8*i+7,
                h_data[8*i+0], h_data[8*i+1], h_data[8*i+2], h_data[8*i+3],
                h_data[8*i+4], h_data[8*i+5], h_data[8*i+6], h_data[8*i+7]
        );
    }
    for (i = (nsamples-64)/8; i < (nsamples-8)/8; i++) {
        printf("kernel out: [%2zd..%2zd] %5.3f %5.3f %5.3f %5.3f  %5.3f %5.3f %5.3f %5.3f\n",
                8*i, 8*i+7,
                h_data[8*i+0], h_data[8*i+1], h_data[8*i+2], h_data[8*i+3],
                h_data[8*i+4], h_data[8*i+5], h_data[8*i+6], h_data[8*i+7]
        );
    }
}

int main(int argc, char** argv)
{
    cudaDeviceProp cudaDevProp;
    cudaEvent_t tstart, tstop;

    assert(N <= N_DECFIR_MAXTAP);
    if (argc == 2) {
        R = atoi(argv[1]);
        if ((R < 2) || (R > 256)) {
            printf("Usage: %s [<decimation ratio R>]   where ratios R=2,4,8,..,256 are supported\n", argv[0]);
            return -1;
        }
    }


    CUDA_CALL( cudaSetDevice(CUDA_DEVICE_NR) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, CUDA_DEVICE_NR) );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;

    CUDA_CALL( cudaEventCreate( &tstart ) );
    CUDA_CALL( cudaEventCreate( &tstop ) );

    nsamples = 8000000;
    CUDA_CALL( cudaMalloc( (void **)&d_data, nsamples*sizeof(float) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_filtered, nsamples*sizeof(float) ) );
    CUDA_CALL( cudaMalloc( (void **)&d_coeff, N_DECFIR_MAXTAP*sizeof(float) ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_data, nsamples*sizeof(float), cudaHostAllocDefault ) );
    CUDA_CALL( cudaHostAlloc( (void **)&h_coeff, N_DECFIR_MAXTAP*sizeof(float), cudaHostAllocDefault ) );
    memset(h_coeff, 0, N_DECFIR_MAXTAP*sizeof(float));

    make_pfb_coeffs(h_coeff, f0, f1, N);
    CUDA_CALL( cudaMemcpy(d_coeff, h_coeff, N*sizeof(float), cudaMemcpyHostToDevice) );
    CUDA_CALL( cudaMemcpyToSymbol(c_decim_FIR_w, h_coeff, N*sizeof(float), 0, cudaMemcpyHostToDevice) );

    for (size_t n=0; n<nsamples; n++) {
        h_data[n] = 2.0*sin(2*M_PI*(f1-f0)*n*0.45/fs);
        h_data[n] += 16.0*sin(2*M_PI*(f1-f0)*n*2.315/fs);
    }
    CUDA_CALL( cudaMemcpy(d_data, h_data, nsamples*sizeof(float), cudaMemcpyHostToDevice) );

    /* For Octave:
     w = load('decim_fir.coeff');
     f0 = 0.0, f1 = 8.0e6, fs = 512e6;  N=1024, R=16;
     n = 0:10000;
     d = 2*sin(2*pi*(f1-f0)*n*0.45/fs) + 16*sin(2*pi*(f1-f0)*n*2.315/fs);
     x = filter(w(:), 1, d(:)); % filter(B,A,x)
     x = decimate(d, R);
     save('-ascii', 'decim_fir.out', 'x');
     */

    // Some extra info
    printf("FIR : %zu coeffs, decimation R=%zu\n", N, R);
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d, %zu threads max.\n",
        CUDA_DEVICE_NR, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize, maxphysthreads
    );

    /////////////////////////////////////////////////////////////////////////////////

    size_t threadsPerBlock = cudaDevProp.warpSize;
    //size_t numBlocks = div2ceil(nsamples,threadsPerBlock); // single FIR output sample per thread
    size_t numBlocks = div2ceil(maxphysthreads,threadsPerBlock); // multiple FIR output samples per thread

    printf("Layout: %zu blocks x %zu threads\n", numBlocks, threadsPerBlock);
    CUDA_CALL( cudaMemset(d_filtered, 0, nsamples*sizeof(float)) );
    CUDA_TIMING_START(tstart, 0);
    cu_decim_FIR <<<numBlocks, threadsPerBlock>>> ( d_data, d_coeff, d_filtered, nsamples, N, R);
    CUDA_CHECK_ERRORS("decimFIR");
    CUDA_TIMING_STOP(tstop, tstart, 0, "decimFIR", nsamples);
    if (verbose) { checkOutput(); }

#if 1
    printf("Layout: %zu blocks x %zu threads\n", numBlocks, threadsPerBlock);
    CUDA_CALL( cudaMemset(d_filtered, 0, nsamples*sizeof(float)) );
    CUDA_TIMING_START(tstart, 0);
    cu_decim_FIR_const <<<numBlocks, threadsPerBlock>>> ( d_data, d_filtered, nsamples, N, R);
    CUDA_CHECK_ERRORS("decimFIR_const");
    CUDA_TIMING_STOP(tstop, tstart, 0, "decimFIR_const", nsamples);
    if (verbose) { checkOutput(); }
#endif

#if 1
    printf("Layout: %zu blocks x %zu threads\n", numBlocks, threadsPerBlock);
    CUDA_CALL( cudaMemset(d_filtered, 0, nsamples*sizeof(float)) );
    CUDA_TIMING_START(tstart, 0);
    cu_decim_FIR_csm <<<numBlocks, threadsPerBlock>>> ( d_data, d_coeff, d_filtered, nsamples, N, R);
    CUDA_CHECK_ERRORS("decimFIR_csm");
    CUDA_TIMING_STOP(tstop, tstart, 0, "decimFIR_csm", nsamples);
    if (verbose) { checkOutput(); }
#endif

#if 1
    threadsPerBlock = N_DECFIR_BLOCKSZ;
    numBlocks = div2ceil(maxphysthreads,threadsPerBlock);
    //size_t shm = N_DECFIR_BLOCKSZ*sizeof(float) + N_DECFIR_MAXTAP*sizeof(float); // kernel<<grid,block,dynamic shm,stream>>
    printf("Layout: %zu blocks x %zu threads\n", numBlocks, threadsPerBlock);
    CUDA_CALL( cudaMemset(d_filtered, 0, nsamples*sizeof(float)) );
    CUDA_TIMING_START(tstart, 0);
    cu_decim_FIR_csm2 <<<numBlocks, threadsPerBlock>>> ( d_data, d_coeff, d_filtered, nsamples, N, R);
    CUDA_CHECK_ERRORS("decimFIR_csm2");
    CUDA_TIMING_STOP(tstop, tstart, 0, "decimFIR_csm2", nsamples);
    if (verbose) { checkOutput(); }
#endif

    /////////////////////////////////////////////////////////////////////////////////

    return 0;
}

#endif

