// Tests the simultaneous bi-directional transfer speed for GPU
// cards that support simultaneous DMA between RAM to/from GPU.
//
// Result on GTX TITAN X running at PCIe gen3 x16:
//  2 x 390.6 MiB : 0.0369 s GPU : 0.0369s CPU : 20.673 GB/s
// Result on GTX 1080 running at PCIe gen3 x8 (due to CPU limitations):
//   2 x 390.6 MiB : 0.0727 s GPU : 0.0727s CPU : 10.496 GB/s
//
// Profile with
// $ nvprof  --profile-api-trace none --print-gpu-trace ./membw
//
// usage: membw [<deviceNr>] [<1=serial mode enable>]
//

#include <cuda.h>
#include <helper_cuda.h>

#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>
#include <sys/time.h>

int main(int argc, char** argv)
{
    int device = 0, run_serially = 0, max_iter = 100;
    cudaDeviceProp cuDev;
    size_t nbyte = 409600 * 1000;

    cudaEvent_t tstart, tstop;
    cudaEvent_t tstart_sub, tstop_sub;
    struct timeval tv_start, tv_stop;
    float dt_msec, dt_msec_sub = 1e7, dt_cpu;

    float *d_idata, *h_idata;
    float *d_odata, *h_odata;
    cudaStream_t s[2];

    if (argc > 1) {
        device = atoi(argv[1]);
    }
    if (argc > 2) {
        run_serially = atoi(argv[2]);
    }

    checkCudaErrors( cudaSetDevice(device) );
    checkCudaErrors( cudaEventCreate( &tstart ) );
    checkCudaErrors( cudaEventCreate( &tstop ) );
    checkCudaErrors( cudaEventCreate( &tstart_sub ) );
    checkCudaErrors( cudaEventCreate( &tstop_sub ) );
    checkCudaErrors( cudaGetDeviceProperties(&cuDev, device) );
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %s, %d DMA engines\n",
        device, cuDev.name, cuDev.major, cuDev.minor,
        cuDev.deviceOverlap ? "data transfer concurrency" : "no data transfer concurrency",
        cuDev.deviceOverlap ? cuDev.asyncEngineCount : 1
    );

    checkCudaErrors( cudaStreamCreate(&s[0]) );
    checkCudaErrors( cudaStreamCreate(&s[1]) );
    checkCudaErrors( cudaMalloc( (void **)&d_idata, nbyte ) );
    checkCudaErrors( cudaMalloc( (void **)&d_odata, nbyte ) );
    checkCudaErrors( cudaHostAlloc( (void **)&h_idata, nbyte, cudaHostAllocDefault ) ); // host->dev
    checkCudaErrors( cudaHostAlloc( (void **)&h_odata, nbyte, cudaHostAllocDefault ) ); // dev->host

    double_t L_mib = nbyte/1048576.0;

    if (run_serially == 1) {
        s[0] = 0; s[1] = 0;
        printf("Changed Stream IDs to 0 to force running H2D, D2H xfers serially.\n");
    }

    gettimeofday(&tv_start, NULL);
    checkCudaErrors( cudaDeviceSynchronize() );
    checkCudaErrors( cudaEventRecord(tstart) );

    for (int i = 0; i < max_iter; i+=2) {
        float dt_msec_tmp;
        // We make four calls for transfers here so that the code can be easily edited
        // and the order/selection of CUDA streams can be changed!
        // H2D+D2H : first time
        checkCudaErrors( cudaEventRecord(tstart_sub) );
        checkCudaErrors( cudaMemcpyAsync(d_idata, h_idata, nbyte, cudaMemcpyHostToDevice, s[0]) );
        checkCudaErrors( cudaMemcpyAsync(h_odata, d_odata, nbyte, cudaMemcpyDeviceToHost, s[1]) );
        // H2D+D2H : second time
        checkCudaErrors( cudaMemcpyAsync(d_idata, h_idata, nbyte, cudaMemcpyHostToDevice, s[0]) );
        checkCudaErrors( cudaMemcpyAsync(h_odata, d_odata, nbyte, cudaMemcpyDeviceToHost, s[1]) );
        checkCudaErrors( cudaEventRecord(tstop_sub) );
        checkCudaErrors( cudaEventSynchronize(tstop_sub) );
        checkCudaErrors( cudaEventElapsedTime(&dt_msec_tmp, tstart_sub, tstop_sub) );
        dt_msec_tmp /= 2.0f;
        if (dt_msec_tmp < dt_msec_sub) {
            dt_msec_sub = dt_msec_tmp;
        }
        printf(
            "%d/%d : %.4f ms : best so far %.4f ms per transfer pair = %.3f GB/s\n",
            i, max_iter, dt_msec_tmp, dt_msec_sub, 2*nbyte/(dt_msec_sub*1e-3*1024.0*1048576.0)
        );
    }

    checkCudaErrors( cudaEventRecord(tstop) );
    checkCudaErrors( cudaEventSynchronize(tstop) );
    checkCudaErrors( cudaEventElapsedTime(&dt_msec, tstart, tstop) );
    dt_msec /= max_iter;

    gettimeofday(&tv_stop, NULL);
    dt_cpu = (tv_stop.tv_sec - tv_start.tv_sec) + 1e-6*(tv_stop.tv_usec - tv_start.tv_usec);
    dt_cpu /= max_iter;

    double R_total = 2*nbyte/(dt_msec*1e-3*1024.0*1048576.0); // 2 x nbyte were copied per H2D+D2H pair
    printf(" 2 x %.1f MiB : %.4f s GPU : %.4fs CPU : %.3f GB/s\n",
         L_mib, dt_msec*1e-3, dt_cpu, R_total
    );

    // Interesting to check this in CUDA nvprof:
    // checkCudaErrors( cudaMemcpyAsync(d_odata, h_odata, nbyte, cudaMemcpyDeviceToHost, s[1]) );
    // checkCudaErrors( cudaMemcpyAsync(h_odata, d_odata, nbyte, cudaMemcpyDeviceToHost, s[1]) );
    // --> shows two transfers happening concurrently, [CUDA memcpy HtoD] then [CUDA memcpy DtoH]!
    //     this suggests cudaMemcpyAsync() ignores the 'cudaMemcpyDeviceToHost' argument...

    return 0;
}

