#ifndef DUMPFILE_IO_H
#define DUMPFILE_IO_H

#include <stdio.h>
#include <stdlib.h>

void write_to_binary_file(const char* fname, const void* d, size_t len);
void write_float32_to_text_file(const char* fname, const float* d, size_t len);
void write_float32_to_binary_file(const char* fname, const float* d, size_t len);

#endif // DUMPFILE_IO_H

