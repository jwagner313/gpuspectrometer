% Filter a random noise plus tones synthetic signal with a PFB.
% The output of the PFB is always complex-valued since critically sampled.
function pfb_test2()
    close all;

    real_data_file = 'C:\MatlabData\h2o-maser-100ms.float32';
    %real_data_file = '';

    %% Design a PFB
    Nch  = 32;    % Number of channels in the FIR filter bank
    Ntap = 2*Nch; % Number of taps in each FIR filter (total: Nch*Ntap coeffs)
    Rdecim = Nch; % Decimation ratio (input width div. by target output channel width)
    
    [H,Hq,h] = pfb_coeffs(Nch, Ntap, 'pfb.coeff', Rdecim, 'fir1');

    %% Test filtering a specific signal with the designed PFB

    % Settings
    Np = 512*4;
    Nsamp = Nch*Ntap*Np;
    t = 1:Nsamp;

    % Data from file, or synthetic
    if numel(real_data_file)>0,
        fd = fopen(real_data_file, 'rb');
        x = fread(fd, [1 Nsamp], 'float32');
        fclose(fd);
        use_complex = 0;
    else
        % Data mode: 1 to use complex input, 0 to use real-valued input
        use_complex = 0;
        
        % Noise floor
        x = zeros(size(t));
        x = x + 0.1*(randn(size(t)) + 1i*randn(size(t)));

        % Frequency comb with one tone per channel
        Ntones = Nch;
        w_tones = zeros(1,Ntones);
        a_tones = zeros(1,Ntones);
        for tn=1:Ntones,
            o1 = 0.023; % 0.123 looks ok, 0.023 reveals folding
            w_tones(tn) = 2*pi * mod((o1 + (tn^0.8)*(1/(1.45*Nch))), 0.5);
            a_tones(tn) = 1 + (tn-1)/Nch;
        end
        a_tones(3) = 0;
        x = x + a_tones * exp(1i*w_tones' * t);        
    end    
    if ~use_complex,
        x = real(x);
    end
    
    % Process in PFB to get outputs Y (complex, time domain) 
    % and y (real, time domain, before IFFT)
    [Y,y] = pfb_filtering(Hq, x); 
    
    % Y = real(Y); % doesn't seem to produce good results... aliasing?
    
    % Crop away the start of the filter response
    Y = Y(:,(Ntap+1):end);
    y = y(:,(Ntap+1):end);

    
    % Prepare spectra of output channels
    % Note: individual channels should be covering f0_ch + [-fs/2..+fs/2] 
    %       and be spaced in multiples of fs, i.e., f0_ch = N*fs
    fs_in  = 1.0;
    fs_out = fs_in/Nch; % not fs_in/Rdecim==2*bandwidth since band can be oversampled
    F_in   = abs(fft(x .* hamming(numel(x))'));
    F_out  = abs(fft(Y,[],2));
    f_in   = linspace(0,fs_in,numel(x));
    f_out  = linspace(-fs_out/2,+fs_out/2,size(F_out,2));
    f0_ch_out = (0:(Nch-1)) * fs_out;

    % Show
    figure(), clf, set(gca,'Color','w');
    subplot(2,1,1),
        semilogy(f_in, F_in, 'rx:');
        title('Input spectrum');
        xlabel('Normalized Frequency (f_{in}/f_{s})');
        axis tight;
    subplot(2,1,2),
        cm = jet(ceil(Nch/2)+1);
        cm = [cm; flipud(cm)];
        for cc=1:Nch,
            yy = F_out(cc,:);
            yy = fftshift(yy); % necessary -- double-sideband complex output :-/
            xx = f_out + f0_ch_out(cc);
            xx = mod(xx, 1.0);
            semilogy(xx, yy, 'x', 'Color',cm(cc,:));
            hold on;
        end
        title('Output spectra from PFB channels');
        xlabel('Normalized Frequency (f_{out}/(f_{s}/N_{ch}) in each channel)');
        axis tight;
        xlim([0, 1.0]);
    

end

