/////////////////////////////////////////////////////////////////////////////////////
//
// Usage: benchmark [<deviceNr>] [<fftLength>]
//
// Basic benchmark of spectral processing functions.
// Carries out 2-bit/3-bit unpack -> r2c FFT -> cross-power accumulation.
//
/////////////////////////////////////////////////////////////////////////////////////

#include "decoder_2b32f_kernels.cu"
#include "decoder_3b32f_kernels.cu"
#include "arithmetic_xmac_kernels.cu"
#include "arithmetic_autospec_kernels.cu"
#include "arithmetic_winfunc_kernels.cu"

#include "dumpfile_io.c"

#include <assert.h>
#include <stdio.h>
#include <math.h>
#include <cuda.h>
#include <cufft.h>

/////////////////////////////////////////////////////////////////////////////////////

#define CHECK_TIMING 0 // 1 to activate CUDA_TIMING_START()/_STOP() timing, 0 otherwise
#include "cuda_utils.cu"

#define DUMP_RAW 0     // 1 to write <16kB of raw input (2-bit/8-bit) into files "benchmark_raw_%d_%d.txt"
#define DUMP_DECODED 0 // 1 to write <64ksamples of unpacked data into files "benchmark_float32_%d_%d.txt"
#define DUMP_DFT 0     // 1 to write first 4 DFT results (Re,Im on alternate lines) into files "benchmark_fft_%d_%d.txt"
#define DUMP_AP 0      // 1 to write the final time-averaged power spectrum into files "benchmark_apspec_%d_%d.txt"
#define DUMP_2POL 0    // 1 to write the final time-averaged auto&cross spectra into files "benchmark_2polspec_%d_%d.txt"
#define DUMP_2POL_CONV 0 // 1 to convert the xpol part from {Real,Imag} into {Mag,Phase(deg)} for easier checking
#define DO_3BIT        1 // 1 to use 3-bit unpack (ALMA), 0 to use 2-bit (VLBI/VDIF)
#define DO_WINDOWING   1 // 1 to apply Hann or Hamming window function to data
#define DO_CROSS       1 // 1 to calculate cross-products, 0 to just do single autocorr

#define MAX_FFT_LENGTH (8*1024ULL*1024ULL)  // maximum FFT length to use
//#define BATCH_NUMFFT   1000               // uncomment to use a fixed batch size, regardless of FFT lengths

//#define INFILE "/scratch/jwagner/s14db02c_KVNYS_No0006.vdif"
//#define INFILE "/opt/alma/share/synthetic_2bit.vdif"
#define INFILE "randomfile"

/////////////////////////////////////////////////////////////////////////////////////

#define scope

#ifndef CUDA_DEVICE_NR
    #define CUDA_DEVICE_NR 0
#endif

#if DO_3BIT
    #define SAMP_PER_WORD24 8
#else
    #define SAMP_PER_WORD24 12
#endif

#ifndef BATCH_NUMFFT
    // GPU memory upper limit:
    //   200*MAX_FFT cufftComplex fits on GTX 1080
    //   400*MAX_FFT cufftComplex fits on GTX TITAN X
    #define C_INPUT_BATCH_PRODUCT (100*MAX_FFT_LENGTH)
#else
    #define C_INPUT_BATCH_PRODUCT (BATCH_NUMFFT*MAX_FFT_LENGTH)
#endif

#define INPUT_BUF_SIZE (16 + C_INPUT_BATCH_PRODUCT)

/////////////////////////////////////////////////////////////////////////////////////

static const size_t table_FFT_lens[] = {
    // 2^N : 64-point to 8M-point
    64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608,
    // decimal : 500-point to ~8M-point
    100, 200, 500, 1000, 2000, 4000, 8000, 10000, 16000, 20000, 32000, 50000, 64000, 100000, 128000, 200000, 250000, 256000,
    500000, 512000, 1000000, 1024000, 2000000, 2048000, 4000000, 4096000, 5000000, 8000000, 8192000
};

/////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    cudaDeviceProp cudaDevProp;
    size_t maxphysthreads;

    cudaEvent_t globalstart, globalstart_noDMA, globalstop, globalstop_noDMA;
    cudaEvent_t substart, substop;
    float etime, globaltime, globaltime_noDMA;
    size_t nfree, ntotal, memreq;

    size_t numBlocks, threadsPerBlock;
    size_t FFT_OUTPUT_SIZE, OUTPUT_SIZE;
    size_t INPUT_BYTES;
    size_t INPUT_WORD24;
    size_t INPUT_SAMPS;
    size_t FFT_LENGTH = 0; // Number of FFT points

    // size of memory for digitized samples
    unsigned char *h_rawdata;
    unsigned char *d_rawdata;
    float *d_idata;
    float *h_idata;
    cufftComplex *d_odata;

    cufftHandle fftplan;

    float *h_autoPS;
    float *d_autoPS;
    float4 *h_xc_ac;
    float4 *d_xc_ac;

    size_t SIZE_START = 1024, SIZE_STOP = MAX_FFT_LENGTH;
    int devNr = CUDA_DEVICE_NR;

    // Args
    while (argc > 1) {
        int v = atoi(argv[1]);
        if (v < 16) {
            devNr = v;
        } else {
            SIZE_START = v;
            SIZE_STOP = SIZE_START;
        }
        argc--; argv++;
    }

    // Select device and do some preparations
    CUDA_CALL( cudaSetDevice(devNr) );
    CUDA_CALL( cudaDeviceReset() );
    CUDA_CALL( cudaMemGetInfo(&nfree, &ntotal) );
    CUDA_CALL( cudaGetDeviceProperties(&cudaDevProp, devNr) );
    printf("CUDA Device #%d : %s, Compute Capability %d.%d, %d threads/block, warpsize %d\n",
        devNr, cudaDevProp.name, cudaDevProp.major, cudaDevProp.minor,
        cudaDevProp.maxThreadsPerBlock, cudaDevProp.warpSize
    );
    printf("Running mode:\n"
           "   windowing=%s\n"
           "   cross-power=%s\n"
           "   unpack=%s\n",
           DO_WINDOWING ? "YES" : "no",
           DO_CROSS ? "YES" : "no",
           DO_3BIT ? "3-bit" : "2-bit"
    );
    maxphysthreads = cudaDevProp.multiProcessorCount * cudaDevProp.maxThreadsPerMultiProcessor;

    CUDA_CALL( cudaEventCreate( &globalstart ) );
    CUDA_CALL( cudaEventCreate( &globalstop ) );
    CUDA_CALL( cudaEventCreate( &globalstart_noDMA ) );
    CUDA_CALL( cudaEventCreate( &globalstop_noDMA ) );
    CUDA_CALL( cudaEventCreate( &substart ) );
    CUDA_CALL( cudaEventCreate( &substop ) );

    // Read headerless raw data from file
    unsigned char *signals;
    printf("Reading %lu bytes from %s...\n", (size_t)INPUT_BUF_SIZE, INFILE);
    CUDA_CALL( cudaHostAlloc( (void **)&signals,sizeof(char)*INPUT_BUF_SIZE,cudaHostAllocDefault ) );
    FILE* fin = fopen(INFILE, "rb");
    if (fin == NULL) {
            perror(INFILE);
            exit(-1);
    }
    fseek(fin, 0, SEEK_SET);
    (void)fread(signals, INPUT_BUF_SIZE, 1, fin);
    fclose(fin);

    // Heading for reports
    printf("%10s %10s %10s %10s %10s\n", "Lfft","Nfft","T [msec]","R [Gs/s/pol]", "Rcomp[Gs/s/pol]");

    // Individual benchmarks
    for (size_t fi = 0; fi < sizeof(table_FFT_lens)/sizeof(table_FFT_lens[0]); fi++) {

        // Number of FFT points
        if (SIZE_START != SIZE_STOP) {
            FFT_LENGTH = table_FFT_lens[fi]; // auto, from table
        } else {
            FFT_LENGTH = SIZE_START; // single, from cmd line
        }
        if (FFT_LENGTH > SIZE_STOP) {
            break;
        }

        // Aim to keep the amount of input samples constant by adjusting batch size
#ifndef BATCH_NUMFFT
        size_t BATCH_NUMFFT = C_INPUT_BATCH_PRODUCT / FFT_LENGTH;
#endif
        memreq = 4*FFT_LENGTH*BATCH_NUMFFT*sizeof(float) + 4*(FFT_LENGTH/2)*sizeof(float4);
        if ( memreq > nfree ) {
            while ( memreq > nfree ) {
                BATCH_NUMFFT -= 16;
                memreq = 4*FFT_LENGTH*BATCH_NUMFFT*sizeof(float) + 4*(FFT_LENGTH/2)*sizeof(float4);
            }
            // printf("Reduced number of %d-point FFTs to %zu fit GPU, now %.2f GB needed, fits into %.2f GB free.\n", FFT_LENGTH, BATCH_NUMFFT, 1e-9*memreq, 1e-9*nfree);
        }

        // Determine array sizes
        INPUT_SAMPS = FFT_LENGTH*BATCH_NUMFFT;
        INPUT_BYTES = (FFT_LENGTH*BATCH_NUMFFT*3)/SAMP_PER_WORD24;
        INPUT_WORD24 = INPUT_BYTES/3;
        FFT_OUTPUT_SIZE = FFT_LENGTH/2+1; // DC bin, other bins, Nyquist bin
        OUTPUT_SIZE = FFT_LENGTH/2;       // DC bin, other bins, no Nyquist

        // Prepare r2c FFT
        scope {
            int dimn[1] = {(int)FFT_LENGTH};// DFT size
            int inembed[1] = {0};           // ignored for 1D xform
            int onembed[1] = {0};           // ignored for 1D xform
            int istride = 1, ostride = 1;   // step between successive in(out) elements
            int idist = FFT_LENGTH;         // step between batches (R2C input = real)
            int odist = FFT_OUTPUT_SIZE;    // step between batches (R2C output = 1st Nyquist only)
            CUFFT_CALL( cufftPlanMany(&fftplan, 1, dimn,
                inembed, istride, idist,
                onembed, ostride, odist,
                CUFFT_R2C,
                BATCH_NUMFFT)
            );
            #if defined(CUDA_VERSION) && (CUDA_VERSION < 8000)
            CUFFT_CALL( cufftSetCompatibilityMode(fftplan, CUFFT_COMPATIBILITY_NATIVE) );
            #endif
        }

        // Allocate space for the input data
        CUDA_CALL( cudaMalloc( (void **)&d_rawdata,    INPUT_BYTES) );
        CUDA_CALL( cudaMalloc( (void **)&d_idata,      sizeof(float)*INPUT_SAMPS ) );
        CUDA_CALL( cudaHostAlloc( (void **)&h_rawdata, INPUT_BYTES, cudaHostAllocWriteCombined|cudaHostAllocPortable ) );
        CUDA_CALL( cudaHostAlloc( (void **)&h_idata,   sizeof(float)*INPUT_SAMPS, cudaHostAllocPortable ) );

        // Allocate space for the output data and auto power spectrum
        CUDA_CALL( cudaMalloc( (void **)&d_odata,     sizeof(cufftComplex)*(FFT_OUTPUT_SIZE*BATCH_NUMFFT+4) ) );
        CUDA_CALL( cudaMalloc( (void **)&d_autoPS,    sizeof(cufftReal)*OUTPUT_SIZE ) );
        CUDA_CALL( cudaMalloc( (void **)&d_xc_ac,     (2*sizeof(cufftReal)+1*sizeof(cufftComplex))*OUTPUT_SIZE ) );
        CUDA_CALL( cudaHostAlloc( (void **)&h_autoPS, sizeof(cufftReal)*OUTPUT_SIZE, cudaHostAllocPortable ) );
        CUDA_CALL( cudaHostAlloc( (void **)&h_xc_ac,  (2*sizeof(cufftReal)+1*sizeof(cufftComplex))*OUTPUT_SIZE, cudaHostAllocPortable ) );

        // Initialize memory
        CUDA_CALL( cudaMemset( d_rawdata, 0x00, INPUT_BYTES ) );
        CUDA_CALL( cudaMemset( d_idata,   0x00, sizeof(float)*INPUT_SAMPS ) );
        CUDA_CALL( cudaMemset( d_odata,   0x00, sizeof(cufftComplex)*(FFT_OUTPUT_SIZE*BATCH_NUMFFT+4)) );
        CUDA_CALL( cudaMemset( d_autoPS,  0x00, sizeof(cufftReal)*OUTPUT_SIZE ) );
        CUDA_CALL( cudaMemset( d_xc_ac,   0x00, (2*sizeof(cufftReal)+1*sizeof(cufftComplex))*OUTPUT_SIZE ) );
        memcpy(h_rawdata, signals, INPUT_BYTES);

        ////////////////////////////////////////////////////////////////////////////
        // SPECTRAL PROCESSING /////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////

        // Begin benchmarking of DMA+kernels
        CUDA_CALL( cudaEventRecord(globalstart, 0) );

        // Copy raw data sample from host to device
        CUDA_TIMING_START(substart, 0);
        CUDA_CALL( cudaMemcpy(d_rawdata, h_rawdata, INPUT_BYTES, cudaMemcpyHostToDevice) );
        CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "CPU-->GPU", INPUT_SAMPS);
        if (DUMP_RAW) {
            char filename[1024];
            sprintf(filename, "benchmark_raw_%zu_%zu.txt", FFT_LENGTH, BATCH_NUMFFT);
            write_float32_to_text_file(filename, (float*)h_rawdata, m_min(INPUT_BYTES,65536/4)); // actually 2-bit data, not float32
        }

        // Begin kernels-only benchmarking
        CUDA_CALL( cudaEventRecord(globalstart_noDMA, 0) );

        // Unpack 2-bit/3-bit samples into 32-bit float
        if (DO_3BIT) {
            threadsPerBlock = cudaDevProp.warpSize;
            numBlocks = div2ceil(INPUT_WORD24, threadsPerBlock);
            if (!DO_WINDOWING) {
                if (!DO_CROSS) {
                    CUDA_TIMING_START(substart, 0);
                    cu_decode_3bit1ch <<< numBlocks, threadsPerBlock >>> (d_rawdata, (float4 *)d_idata, INPUT_BYTES);
                    CUDA_CHECK_ERRORS("cu_decode_3bit1ch");
                    CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "3bit1ch", INPUT_SAMPS);
                } else {
                    float* d_x = d_idata;
                    float* d_y = d_idata + BATCH_NUMFFT*FFT_LENGTH/2;
                    if ((BATCH_NUMFFT*FFT_LENGTH/2) % sizeof(float4) > 0) {
                        d_y += sizeof(float4) - (BATCH_NUMFFT*FFT_LENGTH/2) % sizeof(float4);
                    }
                    CUDA_TIMING_START(substart, 0);
                    cu_decode_3bit2ch_split <<< numBlocks, threadsPerBlock >>> (d_rawdata, (float4 *)d_x, (float4 *)d_y, INPUT_BYTES);
                    CUDA_CHECK_ERRORS("cu_decode_3bit2ch");
                    CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "3bit2ch", INPUT_SAMPS);
                }
            } else {
                if (!DO_CROSS) {
                    CUDA_TIMING_START(substart, 0);
                    cu_decode_3bit1ch_Hamming <<< numBlocks, threadsPerBlock >>> (d_rawdata, (float4 *)d_idata, INPUT_BYTES, FFT_LENGTH);
                    CUDA_CHECK_ERRORS("cu_decode_3bit1ch_Hamming");
                    CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "3bit1ch_Hamm", INPUT_SAMPS);
                } else {
                    float* d_x = d_idata;
                    float* d_y = d_idata + BATCH_NUMFFT*FFT_LENGTH/2;
                    if ((BATCH_NUMFFT*FFT_LENGTH/2) % sizeof(float4) > 0) {
                        d_y += sizeof(float4) - (BATCH_NUMFFT*FFT_LENGTH/2) % sizeof(float4);
                    }
                    CUDA_TIMING_START(substart, 0);
                    cu_decode_3bit2ch_Hamming_split <<< numBlocks, threadsPerBlock >>> (d_rawdata, (float4 *)d_x, (float4 *)d_y, INPUT_BYTES, FFT_LENGTH);
                    CUDA_CHECK_ERRORS("cu_decode_3bit2ch_Hamming");
                    CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "3bit2ch_Hamm", INPUT_SAMPS);
                }
            }
        } else {
            threadsPerBlock = cudaDevProp.warpSize;
            numBlocks = div2ceil(INPUT_BYTES, threadsPerBlock);
            if (!DO_WINDOWING) {
                CUDA_TIMING_START(substart, 0);
                cu_decode_2bit1ch <<< numBlocks, threadsPerBlock >>> (d_rawdata, (float4 *)d_idata, INPUT_BYTES);
                CUDA_CHECK_ERRORS("cu_decode_2bit1ch");
                CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "2bit1ch", INPUT_SAMPS);
            } else {
                CUDA_TIMING_START(substart, 0);
                cu_decode_2bit1ch_Hamming <<< numBlocks, threadsPerBlock >>> (d_rawdata, (float4 *)d_idata, INPUT_BYTES, FFT_LENGTH);
                CUDA_CHECK_ERRORS("cu_decode_2bit1ch_Hamming");
                CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "2bit1ch_Hamm", INPUT_SAMPS);
            }
        }

        // Fourier transform
        CUDA_TIMING_START(substart, 0);
        CUFFT_CALL( cufftExecR2C(fftplan, (cufftReal*)d_idata, d_odata) );
        CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "r2c fft", INPUT_SAMPS);

        // Calculate power spectrum (XX; 1-pol)
        if (!DO_CROSS) {
            threadsPerBlock = 2 * cudaDevProp.warpSize;
            numBlocks = div2ceil(max(OUTPUT_SIZE,maxphysthreads), threadsPerBlock);
            CUDA_TIMING_START(substart, 0);
            //autoPowerSpectrum_v3 <<< numBlocks, threadsPerBlock >>> (d_odata,d_autoPS,OUTPUT_SIZE,BATCH_NUMFFT);
            autoPowerSpectrum_v3_skipNyquist <<< numBlocks, threadsPerBlock >>> (d_odata,d_autoPS,OUTPUT_SIZE,BATCH_NUMFFT);
            CUDA_CHECK_ERRORS("ac_v3");
            CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "ac_v3", OUTPUT_SIZE*BATCH_NUMFFT);

            // Get the results
            CUDA_CALL( cudaEventRecord(globalstop_noDMA, 0) );
            CUDA_TIMING_START(substart, 0);
            CUDA_CALL( cudaMemcpyAsync(h_autoPS, d_autoPS, sizeof(float)*OUTPUT_SIZE, cudaMemcpyDeviceToHost) );
            CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "GPU-->Host (1pol)", OUTPUT_SIZE);

        // Calculate power and cross-power spectra : interpret data as dual-pol
        } else {

            // 1st half of post-FFT d_odata : complex spectra LCP
            // 2nd half of post-FFT d_odata : complex spectra RCP
            float2 *pol1 = (float2*)d_odata;
            float2 *pol2 = pol1 + FFT_OUTPUT_SIZE*BATCH_NUMFFT/2;

            threadsPerBlock = 2 * cudaDevProp.warpSize;
            numBlocks = div2ceil(64*OUTPUT_SIZE, threadsPerBlock);
            CUDA_TIMING_START(substart, 0);
            //cu_accumulate_2pol <<< numBlocks, threadsPerBlock >>> (pol1, pol2, d_xc_ac, OUTPUT_SIZE, BATCH_NUMFFT/2);
            cu_accumulate_2pol_skipNyquist <<< numBlocks, threadsPerBlock >>> (pol1, pol2, d_xc_ac, OUTPUT_SIZE, BATCH_NUMFFT/2);
            CUDA_CHECK_ERRORS("xmac_2pol");

            size_t numSamples = OUTPUT_SIZE*(BATCH_NUMFFT/2); // "rate" as complex samples per second per polarization
            CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "xmac AC,XC", numSamples);

            // Get the results
            CUDA_CALL( cudaEventRecord(globalstop_noDMA, 0) );
            CUDA_TIMING_START(substart, 0);
            CUDA_CALL( cudaMemcpyAsync(h_xc_ac, d_xc_ac, sizeof(float4)*OUTPUT_SIZE, cudaMemcpyDeviceToHost) );
            CUDA_TIMING_STOP_ACC(substop, substart, etime, 0, "GPU-->Host (2pol)", 4*OUTPUT_SIZE);
        }

        CUDA_CALL( cudaEventRecord(globalstop, 0) );
        CUDA_CALL( cudaEventSynchronize(globalstop) );
        CUDA_CALL( cudaEventElapsedTime( &globaltime, globalstart, globalstop ) );
        CUDA_CALL( cudaEventElapsedTime( &globaltime_noDMA, globalstart_noDMA, globalstop_noDMA ) );

        // Throughput report
        float rate_total = 1e-9*(INPUT_SAMPS)/(globaltime*1e-3);
        float rate_noDMA = 1e-9*(INPUT_SAMPS)/(globaltime_noDMA*1e-3);
        if (DO_CROSS) {
            // half of data were from other polarization; report throughput per polarization
            rate_total /= 2.0f;
            rate_noDMA /= 2.0f;
        }
        printf("%10zu %10zu %10.5f %10.3f %10.3f\n",FFT_LENGTH,BATCH_NUMFFT,globaltime*1e-3,rate_total,rate_noDMA); // samples | seconds | Gs/s


        ////////////////////////////////////////////////////////////////////////////
        // DEBUG DUMPS /////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////

        // Dump: decoded samples into benchmark_float32_<nchan>_<lbatch>.txt / .bin
        if (DUMP_DECODED) {
            char filename[1024];
            CUDA_CALL( cudaMemcpy(h_idata, (float *)d_idata, sizeof(float)*INPUT_SAMPS, cudaMemcpyDeviceToHost) );
            sprintf(filename, "benchmark_float32_%zu_%zu.txt", FFT_LENGTH, BATCH_NUMFFT);
            write_float32_to_text_file(filename, h_idata, m_min(INPUT_SAMPS,65536));
            sprintf(filename, "benchmark_float32_%zu_%zu.bin", FFT_LENGTH, BATCH_NUMFFT);
            write_float32_to_binary_file(filename, h_idata, INPUT_SAMPS);
        }

        // Dump: complex FFT output data into benchmark_fft_%zu_%zu.txt / .bin
        if (DUMP_DFT) {
            char filename[1024];
            int n = m_min(4, BATCH_NUMFFT);
            CUDA_CALL( cudaMemcpy(h_idata, (float *)d_odata, FFT_OUTPUT_SIZE*n, cudaMemcpyDeviceToHost) );
            sprintf(filename, "benchmark_fft_%zu_%zu.txt", FFT_LENGTH, BATCH_NUMFFT);
            write_float32_to_text_file(filename, h_idata, FFT_OUTPUT_SIZE*n);
            sprintf(filename, "benchmark_fft_%zu_%zu.bin", FFT_LENGTH, BATCH_NUMFFT);
            write_float32_to_binary_file(filename, h_idata, FFT_OUTPUT_SIZE*n);
        }

        // Dump: averaged power spectrum / spectra
        if (DUMP_AP) {
            char filename[1024];
            if (!DO_CROSS) {
                sprintf(filename, "benchmark_apspec_%zu_%zu.txt", FFT_LENGTH, BATCH_NUMFFT);
                write_float32_to_text_file(filename, h_autoPS, OUTPUT_SIZE);
            }
            if (DO_CROSS) {
                // Data are .x=XX, .y=Re{XY}, .z=Im{XY}. w=YY and we are interested only in XX and YY
                // Grab XX first:
                char filename[1024];
                sprintf(filename, "benchmark_apspec_XX_%zu_%zu.txt", FFT_LENGTH, BATCH_NUMFFT);
                for (size_t k=0; k<OUTPUT_SIZE; k++) {
                        float4 tmp = h_xc_ac[k];
                        h_autoPS[k] = tmp.x; // XX
                }
                write_float32_to_text_file(filename, h_autoPS, OUTPUT_SIZE);
                // Grab YY next:
                sprintf(filename, "benchmark_apspec_YY_%zu_%zu.txt", FFT_LENGTH, BATCH_NUMFFT);
                for (size_t k=0; k<OUTPUT_SIZE; k++) {
                        float4 tmp = h_xc_ac[k];
                        h_autoPS[k] = tmp.w; // YY
                }
                write_float32_to_text_file(filename, h_autoPS, OUTPUT_SIZE);
            }
        }

        // Dump: 2-pol power spectrum in mag, phase form
        if (DUMP_2POL && DO_CROSS) {
            // Store |XY|, angle(XY)
            char filename[1024];
            sprintf(filename, "benchmark_2polspec_%zu_%zu.txt", FFT_LENGTH, BATCH_NUMFFT);
            if (DUMP_2POL_CONV) {
                // Data are .x=XX, .y=Re{XY}, .z=Im{XY}. w=YY, convert XY from {Real,Imag} into {Mag,Phase}
                for (size_t k=0; k<OUTPUT_SIZE; k++) {
                    float4 tmp = h_xc_ac[k];
                    h_xc_ac[k].y = sqrt(tmp.y*tmp.y + tmp.z*tmp.z);
                    h_xc_ac[k].z = atan2(tmp.z, tmp.y) * 180.0/M_PI;
                }
            }
            write_float32_to_text_file(filename, (float*)h_xc_ac, 4*OUTPUT_SIZE);
        }

        CUDA_CALL( cudaFree(d_rawdata) );
        CUDA_CALL( cudaFree(d_idata) );
        CUDA_CALL( cudaFree(d_odata) );
        CUDA_CALL( cudaFree(d_autoPS) );
        CUDA_CALL( cudaFree(d_xc_ac) );

        CUDA_CALL( cudaFreeHost(h_rawdata) );
        CUDA_CALL( cudaFreeHost(h_idata) );
        CUDA_CALL( cudaFreeHost(h_autoPS) );
        CUDA_CALL( cudaFreeHost(h_xc_ac) );

        CUFFT_CALL( cufftDestroy(fftplan) );

        if (SIZE_START == SIZE_STOP) {
            break;
        }

    } // N-loop

    return 0;
}
