// Some common helper functions for checking calls to CUDA  (JanW)
//
// This file can be included in a crude way by
//   #include "helpers.cu"

#include <stdio.h>
#include <cufft.h>

// Integer divide with ceil (example with 3/4 and 4/4: div2ceil(3,4) == div2ceil(4,4) == 1)
#define div2ceil(x,y) ((x+y-1)/y)

#define CUDA_CALL(x) m_CUDA_CALL(x,__FILE__,__LINE__)  // checkCudaErrors() from CUDA Examples helper_cuda.h
void m_CUDA_CALL(cudaError_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != cudaSuccess) {
                (void) fprintf(stderr, "ERROR: File <%s>, Line %d: %s\n",
                               pcFile, iLine, cudaGetErrorString(iRet));
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}

#define CUFFT_CALL(x) m_CUFFT_CALL(x,__FILE__,__LINE__)
void m_CUFFT_CALL(cufftResult_t iRet, const char* pcFile, const int iLine)
{
        if (iRet != CUFFT_SUCCESS) {
                (void) fprintf(stderr, "CuFFT ERROR: File <%s>, Line %d: %d\n",
                               pcFile, iLine, iRet);
                cudaDeviceReset();
                exit(EXIT_FAILURE);
        }
        return;
}

#define CUDA_TIMING_START(event,stream) \
   do { if (CHECK_TIMING) CUDA_CALL( cudaEventRecord(event, stream) ); } while(0)

#define CUDA_TIMING_STOP(stop,start,total_time,stream,msg,Nsamples) \
   do { if (CHECK_TIMING) { \
            float dt_msec; \
            CUDA_CALL( cudaEventRecord(stop, stream) ); \
            CUDA_CALL( cudaEventSynchronize(stop) ); \
            CUDA_CALL( cudaEventElapsedTime(&dt_msec, start, stop) ); \
            fprintf(stderr, "  %-18s : %7.5f s : %7.3f Ms/s\n", msg, 1e-3*dt_msec, \
                1e-6*Nsamples/(1e-3*dt_msec)); \
            total_time += dt_msec; \
        } \
   } while(0)

void CUDA_CHECK_ERRORS(const char* kernel_name)
{
       cudaError_t e = cudaGetLastError();
       if (e == cudaSuccess) {
              // fprintf(stderr, "  %s<<<x,y>>>: OK\n", kernel_name);
       } else {
              fprintf(stderr, "  %s<<<x,y>>>: Error: %s\n", kernel_name, cudaGetErrorString(e));
       }
}


// Vector element wise operators

__device__ __host__ float4  operator+( const float4 lhs, const float4 rhs)
{
    float4 res = { lhs.x + rhs.x , lhs.y + rhs.y , lhs.z + rhs.z , lhs.w + rhs.w };
    return res;
}

__device__ __host__ float4  operator-( const float4 lhs, const float4 rhs)
{
    float4 res = { lhs.x - rhs.x , lhs.y - rhs.y , lhs.z - rhs.z , lhs.w - rhs.w };
    return res;
}

__device__ __host__ float4  operator*( const float4 lhs, const float4 rhs)
{
    float4 res = { lhs.x * rhs.x , lhs.y * rhs.y , lhs.z * rhs.z , lhs.w * rhs.w };
    return res;
}

