/*
 * FFT with input window (Hamming)
 *
 * Method 1 -- window the data first, then do FFT
 * Method 2 -- window the data during FFT via cuFFT 'LoadR' Callback
 *
 * Compiling:
    nvcc -dc -m64 -O0 -g callback.cu -I/usr/local/cuda-7.5/samples/common/inc/ -o callback.o
    nvcc -m64 -g -lcufft_static -lculibos callback.o -gencode=arch=compute_52,code=sm_52 -o callback
 *
 * Example run:
   ./callback
      Setup : 8192 cufftReal input samples in 16000 batches with 4097 cufftComplex outputs
      Plain r2c 1D FFT           : total time 8.045 ms : rate 16.3 Gs/s : data sum 6.485254e+07
      Naive (float1 window, FFT) : total time 17.415 ms : rate 7.5 Gs/s : data sum 5.188235e+06
      Naive (float4 window, FFT) : total time 12.001 ms : rate 10.9 Gs/s : data sum 5.188235e+06
      Callback (window in cuFFT) : total time 18.423 ms : rate 7.1 Gs/s : data sum 5.188235e+06
*/

#include <cuda.h>
#include <cufft.h>
#include <cufftXt.h>
#include <stdio.h>

#include "helper_functions.h" // /usr/local/cuda-7.5/samples/common/inc/
#include "helper_cuda.h"

#define M_2PI_f32 6.283185307179586f
#define div2ceil(x,y) ((x+y-1)/y)

// Device-side (CUDA 7.5) //////////////////////////////////////////////////////////////////
// Hamming: w[n] = 0.54 - 0.46*cos(2*pi*n/(N-1))

typedef struct cu_window_cb_params_tt {
    size_t fftlen;
} cu_window_cb_params_t;

/** Apply Hamming window in-place to real sample data. */
__global__ void cu_window_hamming(float* inout, size_t fftlen, size_t datalen)
{
    // Performance result on TITAN X: 7.4 Gs/s combined with FFT
    size_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    float omega = M_2PI_f32/(fftlen-1);
    if (idx < datalen) {
        inout[idx] *= 0.54f - 0.46f * __cosf(omega*(idx % fftlen));
    }
}

/** Apply Hamming window in-place to real sample data (4 samples at once). */
__global__ void cu_window_hamming_float4(float4* inout, size_t fftlen_f4, size_t datalen_f4)
{
    // Performance result on TITAN X: 11 Gs/s combined with FFT
    size_t idx_f4 = blockIdx.x * blockDim.x + threadIdx.x;
    size_t N = 4 * fftlen_f4;
    size_t n = 4 * (idx_f4 % fftlen_f4);

    float omega = M_2PI_f32/(N-1);
    if (idx_f4 < datalen_f4) {
        float4 v = inout[idx_f4];
        float4 wv = {
            v.x * (0.54f - 0.46f*__cosf(omega*(n+0))),
            v.y * (0.54f - 0.46f*__cosf(omega*(n+1))),
            v.z * (0.54f - 0.46f*__cosf(omega*(n+2))),
            v.w * (0.54f - 0.46f*__cosf(omega*(n+3)))
        };
        inout[idx_f4] = wv;
    }
}

/** cuFFT Callback to apply Hamming window at FFT's data-fetching stage.
 * Works only on 64-bit Linux, with nvcc -m64 -dc, and linking -lcufft_static -lculibos.
 */
__device__ cufftReal cu_window_hamming_cufftCallbackLoadR(void *dataIn, size_t offset, void *callerInfo, void *sharedPointer)
{
    // Performance result on TITAN X: 7.1 Gs/s combined with FFT
    cu_window_cb_params_t* my_params = (cu_window_cb_params_t*)callerInfo;
    cufftReal n     = (cufftReal)(offset % my_params->fftlen);
    cufftReal omega = M_2PI_f32/(my_params->fftlen-1);
    cufftReal w     = 0.54f - 0.46f*__cosf(omega*n);
    return w * ((cufftReal*)dataIn)[offset];
}

__device__ cufftCallbackLoadR cu_window_hamming_cufftCallbackLoadR_ptr = cu_window_hamming_cufftCallbackLoadR;

/** Data generator */
__global__ void cu_testdata(float* dst, const size_t N)
{
    size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < N) {
        dst[i] = __cosf(M_2PI_f32*((float)i)/512.0f);
    }
}

/** Simple helper for a comparison of FFT output results */
double sum(float* d, float* h, size_t n)
{
    double s = 0.0;
    checkCudaErrors( cudaMemcpy(h, d, n*sizeof(float), cudaMemcpyDeviceToHost) );
    for (size_t i = 0; i < n; i++) {
        s += h[i];
    }
    return s;
}

/* Host-side code */
int main(void)
{
    size_t nchan = 4096;      // FFT length is 2*nchan
    size_t nbatches = 16000;  // Number of batched FFTs
    float* d_fft_in;
    float* d_fft_out;
    float* h_data;
    size_t nin = 2*nchan, nout = nchan+1;
    double s;

    size_t numBlocksWinfunc, numBlocksTestdata, threadsPerBlock = 32;

    cudaEvent_t tstart, tstop;
    float dT_msec;

    printf("Setup : %zu cufftReal input samples in %zu batches with %zu cufftComplex outputs\n", nin, nbatches, nout);

    // Prepare GPU
    checkCudaErrors( cudaSetDevice(0) );
    checkCudaErrors( cudaDeviceReset() );
    checkCudaErrors( cudaEventCreate(&tstart) );
    checkCudaErrors( cudaEventCreate(&tstop) );
    checkCudaErrors( cudaMalloc((void**)&d_fft_in,   sizeof(cufftReal)*nin*nbatches) );
    checkCudaErrors( cudaMalloc((void**)&d_fft_out,  sizeof(cufftComplex)*nout*nbatches) );
    checkCudaErrors( cudaMallocHost((void**)&h_data, sizeof(cufftComplex)*nout*nbatches, cudaHostAllocDefault) );
    checkCudaErrors( cudaMemset(d_fft_in,  0x00,     sizeof(cufftReal)*nin*nbatches) );
    checkCudaErrors( cudaMemset(d_fft_out, 0x00,     sizeof(cufftComplex)*nout*nbatches) );

    // Plan a batched 1D Real-to-Complex FFT: two plans
    cufftHandle planr2c;    // No callback
    cufftHandle planr2c_cb; // With LoadR callback
    int dimn[1] = {nin};
    int inembed[1] = {0};
    int onembed[1] = {0};
    int istride = 1;
    int ostride = 1;
    int idist = nin;
    int odist = nout;
    checkCudaErrors( cufftPlanMany(&planr2c,    1, dimn, inembed, istride, idist, onembed, ostride, odist, CUFFT_R2C, nbatches) );
    checkCudaErrors( cufftPlanMany(&planr2c_cb, 1, dimn, inembed, istride, idist, onembed, ostride, odist, CUFFT_R2C, nbatches) );

    // Assign "Load Callback" with user settings to the FFT plan
    cu_window_cb_params_t* h_userparams;
    cu_window_cb_params_t* d_userparams;
    checkCudaErrors( cudaMallocHost((void **)&h_userparams, sizeof(cu_window_cb_params_t), cudaHostAllocDefault) );
    checkCudaErrors( cudaMalloc((void **)&d_userparams, sizeof(cu_window_cb_params_t)) );
    h_userparams->fftlen = nin;
    checkCudaErrors( cudaMemcpy(d_userparams, h_userparams, sizeof(cu_window_cb_params_t), cudaMemcpyHostToDevice) );
    cufftCallbackLoadR h_windowfuncCallback;
    checkCudaErrors( cudaMemcpyFromSymbol(&h_windowfuncCallback, cu_window_hamming_cufftCallbackLoadR_ptr, sizeof(h_windowfuncCallback)) );
    checkCudaErrors( cufftXtSetCallback(planr2c_cb, (void **)&h_windowfuncCallback, CUFFT_CB_LD_REAL, (void**)&d_userparams) );

    // 0) Test just the FFT speed
    numBlocksTestdata = div2ceil(nin*nbatches,threadsPerBlock);
    cu_testdata <<< numBlocksTestdata, threadsPerBlock >>> (d_fft_in, nin*nbatches);
    checkCudaErrors( cudaEventRecord(tstart, 0) );
    checkCudaErrors( cufftExecR2C(planr2c, (cufftReal*)d_fft_in, (cufftComplex*)d_fft_out) );
    checkCudaErrors( cudaEventRecord(tstop, 0) );
    checkCudaErrors( cudaEventSynchronize(tstop) );
    checkCudaErrors( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
    s = sum(d_fft_out, h_data, 2*nout*nbatches);
    printf("Plain r2c 1D FFT           : total time %.3f ms : rate %.1f Gs/s : data sum %e\n",
        dT_msec, 1e-9*(nin*nbatches)/(1e-3*dT_msec), s
    );

    // 1a) Naive approach : window first (in float-sized accesses), then FFT
    cu_testdata <<< numBlocksTestdata, threadsPerBlock >>> (d_fft_in, nin*nbatches);
    checkCudaErrors( cudaEventRecord(tstart, 0) );
    numBlocksWinfunc = div2ceil(nin*nbatches,threadsPerBlock);
    cu_window_hamming <<< numBlocksWinfunc, threadsPerBlock >>> ((float*)d_fft_in, nin, nin*nbatches);
    checkCudaErrors( cudaGetLastError() );
    checkCudaErrors( cufftExecR2C(planr2c, (cufftReal*)d_fft_in, (cufftComplex*)d_fft_out) );
    checkCudaErrors( cudaEventRecord(tstop, 0) );
    checkCudaErrors( cudaEventSynchronize(tstop) );
    checkCudaErrors( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
    s = sum(d_fft_out, h_data, 2*nout*nbatches);
    printf("Naive (float1 window, FFT) : total time %.3f ms : rate %.1f Gs/s : data sum %e\n",
        dT_msec, 1e-9*(nin*nbatches)/(1e-3*dT_msec), s
    );

    // 1b) Naive approach : window first (in float4-sized accesses), then FFT
    cu_testdata <<< numBlocksTestdata, threadsPerBlock >>> (d_fft_in, nin*nbatches);
    checkCudaErrors( cudaEventRecord(tstart, 0) );
    numBlocksWinfunc = div2ceil((nin/4)*nbatches,threadsPerBlock);
    cu_window_hamming_float4 <<< numBlocksWinfunc, threadsPerBlock >>> ((float4*)d_fft_in, nin/4, (nin/4)*nbatches);
    checkCudaErrors( cudaGetLastError() );
    checkCudaErrors( cufftExecR2C(planr2c, (cufftReal*)d_fft_in, (cufftComplex*)d_fft_out) );
    checkCudaErrors( cudaEventRecord(tstop, 0) );
    checkCudaErrors( cudaEventSynchronize(tstop) );
    checkCudaErrors( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
    s = sum(d_fft_out, h_data, 2*nout*nbatches);
    printf("Naive (float4 window, FFT) : total time %.3f ms : rate %.1f Gs/s : data sum %e\n",
        dT_msec, 1e-9*(nin*nbatches)/(1e-3*dT_msec), s
    );

    // 2) Callback approach: window during FFT, at data loading stage
    cu_testdata <<< numBlocksTestdata, threadsPerBlock >>> (d_fft_in, nin*nbatches);
    checkCudaErrors( cudaEventRecord(tstart, 0) );
    checkCudaErrors( cufftExecR2C(planr2c_cb, (cufftReal*)d_fft_in, (cufftComplex*)d_fft_out) );
    checkCudaErrors( cudaEventRecord(tstop, 0) );
    checkCudaErrors( cudaEventSynchronize(tstop) );
    checkCudaErrors( cudaEventElapsedTime(&dT_msec, tstart, tstop) );
    s = sum(d_fft_out, h_data, 2*nout*nbatches);
    printf("Callback (window in cuFFT) : total time %.3f ms : rate %.1f Gs/s : data sum %e\n",
        dT_msec, 1e-9*(nin*nbatches)/(1e-3*dT_msec), s
    );

    return 0;
}
