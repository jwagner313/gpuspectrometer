#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <random>
#include <cmath>

#include <cstring>
#include <quadmath.h> // libquadmath-devel

#define MAX_ITER  1000000000
#define STEP_ITER 100000

/** Returns sum = sum + y, and stores the running value of compensation. */
template<typename T> T kahanSum(T sum, T x, T& c)
{
   volatile T e = x - c;   // new input minus current compensation
   volatile T t = sum + e; // add "large" sum and new "small" number, looses low-order digits
   c = (t - sum) - e;      // new compensation to recover lost low-order digits
   return t;
}

/** Print 128-bit floating point value */
std::ostream& operator<< (std:: ostream& os, __float128 x)
{
   static char temp[250];
   memset(temp, 0, sizeof(temp));
   quadmath_snprintf(temp, sizeof(temp)-1, "%.20Qe", x);
   os << temp;
   return os;
}

//////////////////////////////////////////////////////////////////////////

int main()
{
  std::random_device rd;
  std::mt19937 generator(rd());

  long long n = 0;

  __float128 sumq = 0, sumq_Kahan = 0, compq = 0;
  double sum = 0,  sum_Kahan = 0,  comp = 0;
  float  sumf = 0, sumf_Kahan = 0, compf = 0;

  __float128 meanq = 0;
  double mean = 0;
  float  meanf = 0;

#if 1
  // Half-normal distribution is abs(normal)
  std::normal_distribution<double> distribution(/*mu*/0.0, /*sigma*/1.0);
  __float128 theoretical_mean = std::sqrt(M_2_PI); // sigma*sqrt(2/pi)
#else
  // Uniform distrib.
  std::uniform_real_distribution<double> distribution(0, 1);
  __float128 theoretical_mean = 0.5;
#endif

  std::ofstream outfile;
  outfile.open("accuracy_Kahan.txt");
  while (n < MAX_ITER) {

    // Add another chunk of STEP_ITER new samples to our running sums
    for (long long nsub = 0; nsub < STEP_ITER; nsub++, n++) {
        double v = std::abs(distribution(generator));
        float vf = v;
        sumf += vf; // 32-bit naive summation
        sum  += v;  // 64-bit naive summation
        sumq += v;
        sumf_Kahan = kahanSum<float>(sumf_Kahan, vf, compf); // 32-bit Kahan summation
        sum_Kahan = kahanSum<double>(sum_Kahan, v, comp);    // 64-bit Kahan summation
        sumq_Kahan = kahanSum<__float128>(sumq_Kahan, (__float128)v, compq); // 128-bit Kahan summation
        meanf += (vf - meanf)/float(n+1); // 32-bit running mean (D.Knuth)
        mean += (v - mean)/double(n+1);   // 64-bit running mean (D.Knuth)
        meanq += (v - meanq)/__float128(n+1); // 128-bit running mean
    }

    __float128 davgq = sumq/(__float128)n;
    __float128 davg = sum/(__float128)n;
    __float128 davgf = sumf/(__float128)n;
    __float128 davgq_Kahan = sumq_Kahan/(__float128)n;
    __float128 davg_Kahan = sum_Kahan/(__float128)n;
    __float128 davgf_Kahan = sumf_Kahan/(__float128)n;

    std::ostringstream osummary;
    osummary
      << n << " "                                          //  1: num N values
      << (theoretical_mean - davgq) << " "        //  2: 128b direct vs expectation
      << (theoretical_mean - davgq_Kahan) << " "  //  3: 128b  Kahan vs expectation
      << (theoretical_mean - davg) << " "         //  4: 64b  direct vs expectation
      << (theoretical_mean - davg_Kahan) << " "   //  5: 64b   Kahan vs expectation
      << (theoretical_mean - davgf) << " "        //  6: 32b  direct vs expectation
      << (theoretical_mean - davgf_Kahan) << " "  //  7: 32b   Kahan vs expectation

      << (fabsq(davgq - davg)) << " "             //  8: 64b direct vs 128b
      << (fabsq(davgq - davg_Kahan)) << " "       //  9: 64b  Kahan vs 128b
      << (fabsq(davgq - davgf)) << " "            // 10: 32b direct vs 128b
      << (fabsq(davgq - davgf_Kahan)) << " "      // 11: 32b  Kahan vs 128b

      << (fabsq(davg - davg_Kahan)) << " "        // 12: 64b  Kahan vs 64b direct
      << (fabsq(davgf - davgf_Kahan)) << " "      // 13: 32b  Kahan vs 32b direct

      << (theoretical_mean - meanq) << " "        // D. Knuth running mean 128b
      << (theoretical_mean - mean) << " "         // D. Knuth running mean 64b
      << (theoretical_mean - meanf) << " "        // D. Knuth running mean 32b
      << std::endl;
    outfile << osummary.str();
    std::cout << "(" << (100.0*n/MAX_ITER) << "%) " << osummary.str();

  }

  outfile.close();

  return 0;
}

