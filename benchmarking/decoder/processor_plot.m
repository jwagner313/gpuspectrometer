%
% Matlab / GNU Octave script
% processor_check.m
%
% Plots time accumulated auto and "cross" power spectra produced
% by processor_gpu from a (single-channel...) VDIF file
%

Lfft = 32768;   % must be same as in decoder_pu.cu
fn_xx = '/scratch/jwagner/gpu/xmac_xx_f32.bin';
fn_yy = '/scratch/jwagner/gpu/xmac_yy_f32.bin';
fn_xy = '/scratch/jwagner/gpu/xmac_xy_cplx32.bin';
fn_Fx = '/scratch/jwagner/gpu/xmac_FFTx_f32.bin';

Lxx = Lfft/2;
Lxy = 2*Lxx;

fid_xx = fopen(fn_xx, 'r');
fid_yy = fopen(fn_yy, 'r');
fid_xy = fopen(fn_xy, 'r');
fid_Fx = fopen(fn_Fx, 'r');

while 1,

   xx = fread(fid_xx, [1 Lxx], 'float32');
   yy = fread(fid_yy, [1 Lxx], 'float32');
   xy = fread(fid_xy, [1 Lxy],  'float32');
   Fx = fread(fid_Fx, [1 Lfft], 'float32');

   if numel(xx)~=Lxx || numel(yy)~=Lxx || numel(xy)~=Lxy || numel(Fx)~=Lfft,
      fprintf(1, 'EOF');
      break;
   end

   xy = xy(1:2:(Lxy))  + 1i * xy(2:2:(Lxy));
   Fx = Fx(1:2:(Lfft)) + 1i * Fx(2:2:(Lfft));

   figure(1), clf;
   subplot(4,1,1), plot(abs(Fx)), title('FFT(X)'), axis tight;
   subplot(4,1,2), semilogy(xx), title('XX'), axis tight;
   subplot(4,1,3), semilogy(abs(xy(2:end))), axis tight, title('abs(XY)');
   subplot(4,1,4), plot(angle(xy)*180/pi,'kx'), title('arg(XY)'), axis tight;

   x = input('Any key to continue >');
end

fclose('all');

x = input('Any key to exit >');
