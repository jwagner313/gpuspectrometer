
Note that if "#define NO_IO" is commented out, the decoder
will write a 32-bit float file /scratch/jwagner/gpu/decoded.bin

A time-averaged spectrum of those data can be made easily using
the Octave script 'cdecoder_check.m' ($ ocatve cdecoder_check.m)
to subjectively verify that e.g. H2O masers and bandpass shape 
look correct compared to the decoder 2-bit input VDIF file.


** CPU  mds0, polaris (16-core x hyperthreaded)

decode_2bit_1channel_cpu_blocking_v1a
  Ldft    5120*200  Threads 16
    Non-threaded: ~1800 Ms/s
    Threaded    : ~6700 Ms/s
  Ldft    5120*1600  Threads 16
    Threaded    : ~9800 Ms/s

decode_2bit_1channel_cpu_blocking_v1b
  Ldft    5120*200  Threads 16
    Non-threaded: ~1800 Ms/s
    Threaded    : ~6600 Ms/s

decode_2bit_1channel_cpu_blocking_v2
  Ldft    5120*200  Threads 16
    Non-threaded: ~1800 Ms/s
    Threaded    : ~6700 Ms/s
  Ldft    5120*1600  Threads 16
    Threaded    : ~9900 Ms/s


** CPU  mars (4-core x hyperthreaded)

without realtime_init(1), and input is 10032-byte/frame VDIF

decode_2bit_1channel_cpu_blocking_v2
   Ldft 10000*200    Threads 8
    Threaded    : ~5900 Ms/s
   Ldft 10000*800    Threads 8
    Threaded    : ~6120 Ms/s
    Non-threaded: ~2450 Ms/s
   Ldft 10000*800    Threads 4
    Threaded    : ~6200 Ms/s

decode_2bit_1channel_cpu_blocking_v1a
   Ldft 10000*800    Threads 4
    Threaded    : ~6200 Ms/s, ~6300 peak

decode_2bit_1channel_cpu_blocking_v1b
   Ldft 10000*800    Threads 4
    Threaded    : ~6100 Ms/s, ~6200 peak


